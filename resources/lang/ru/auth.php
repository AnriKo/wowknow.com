<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Эти учетные данные не обнаружены в нашей базе',
    'throttle' => 'Слишком много попыток войти. Пожалуйста попробуйте снова через :seconds секунд.',
    'logout' => 'Выйти',
    'register' => 'Регистрация',
    'register_go' => 'Зарегистрироваться',
    'register_already' => 'Уже зарегистрированы?',
    'enter' => 'Войти',
    'your_mail' => 'Ваш email',
    'your_pass' => 'Пароль',
    'your_name' => 'Ваше имя',
    'captcha' => 'Капча (поставьте галочку если вы не робот :)',
    'forget_pass' => 'Забыли свой пароль?',
    'type_register' => 'Выберите тип регистрации на сайте',
    'tutor_register' => 'Регистрация для репетиторов',
    'conpany_register' => 'Регистрация для учебных заведений',
    'enter_page' => 'Страница входа на сайт',
    'register_page' => 'Страница регистрации на сайте',
    're_pass_page' => 'Страница восстановления пароля',
    're_pass' => 'Восстановление пароля',
    'send_re_pass_link' => 'Отправить ссылку на восстановление пароля',
    'new_pass' => 'Новый пароль',
    're_new_pass' => 'Повторите новый пароль',
    'set_new_pass' => 'Установить новый пароль',
    'message' => 'Сообщение',
    'send' => 'Отправить',
    'enter_message' => 'Введите собщение',

];
