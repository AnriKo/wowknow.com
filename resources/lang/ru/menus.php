<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'tutors' => 'Репетиторы',
    'my_profile' => 'Мой профиль',
    'cur_lang' => 'Русский',
    'tutor_prof_rule' => 'Правила заполнения профиля учителя',
    'edit_prof' => 'Редактирование профиля',
    'basic_info' => 'Базовая информация',
    'main_info' => 'Основная информация',
    'subjects_info' => 'Предметы преподавания',
    'edu_info' => 'Мое образоване, интересы',
    'work_with_pupil' => 'Работа с учениками',
    'review_pupil' => 'Отзывы учеников',
    'subject' => 'Предмет',
    'select_subject' => 'Выберите предмет',
    'teach_place' => 'Место занятий',
    'place_local' => 'Локальные',
    'place_online' => 'Онлайн',
    'your_city' => 'Ваш город',
    'select_city' => 'Выберите ваш город',
    'sub_subject' => 'Направления',
    'tutor_gen' => 'Пол',
    'not_matter' => 'Любой',
    'male' => 'Мужской',
    'female' => 'Женский',
    'pupil_age' => 'Возраст ученика',
    'age_before_3' => 'Малыши до 3 лет',
    'age_4_6' => 'Дети (4-6)',
    'age_6_12' => 'Младшие школьники (6-12)',
    'age_12_17' => 'Школьники (12-17)',
    'age_17_23' => 'Студенты (17-23)',
    'age_23_40' => 'Взрослые (23-40)',
    'age_40_' => 'Взрослые (40+)',
    'for_tutors' => 'Репетиторам',
    'for_pupils' => 'Ученикам',
    'education' => 'Образование',
    'tutor_reg' => 'Регистрация репетитора',
    'pupils_anons' => 'Обьявления учеников',
    'tutor_search' => 'Поиск репетитора',
    'set_anons' => 'Подать обьявление на обучение',
    'blog' => 'Статьи',
    'about_proj' => 'О проекте',
    'slag' => 'Найди учителя вместе с нами',
    'our_mail' => 'Наша почта',
    'reklama_on_site' => 'Реклама на сайте',
    'search_tut' => 'Найти репетитора',
    'univers' => 'Вузы',
    'courses' => 'Курсы',
    'search_courses' => 'Курсы',
    'add_courses' => 'Добавить компанию',
    'development' => 'Разработка сайта, хостинг',
    'share_page' => 'Поделитесь страницей',


];
