<?php

return [

    'all_cities' => 'Всі міста',
    'all_subjects' => 'Всі напрямки',
     'page_rewievs' => 'Відгуки',
    'page_rewiev' => 'Відгук',
    'page_add_rewievs' => 'Додати відгук',
    'page_direction_study' => 'Напрямки навчання',
    'univer_descr' => 'Опис вищого навчального закладу',
    'page_rewievs_about' => 'Відгуки про навчальний заклад',
    'page_rate_univer' => 'Оцініть ВНЗ по рейтингу от 1 до 5 зірок',

];
