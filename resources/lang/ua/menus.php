<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'tutors' => 'Репетитори',
    'my_profile' => 'Мій профіль',
    'cur_lang' => 'Українська',
    'tutor_prof_rule' => 'Правила заповнення профілю вчителя',
    'edit_prof' => 'Редагування профілю',
    'basic_info' => 'Базова інформація',
    'main_info' => 'Основна інформація',
    'subjects_info' => 'Предмети викладання',
    'edu_info' => 'Моя освіта, інтереси',
    'work_with_pupil' => 'Робота з учнями',
    'review_pupil' => 'Відгуки учнів',
    'subject' => 'Предмет',
    'select_subject' => 'Виберіть предмет',
    'teach_place' => 'Місце занять',
    'place_local' => 'Локальні',
    'place_online' => 'Онлайн',
    'your_city' => 'Ваше місто',
    'select_city' => 'Виберіть ваше місто',
    'sub_subject' => 'Напрямки',
    'tutor_gen' => 'Стать',
    'not_matter' => 'Не має значення',
    'male' => 'Чоловічий',
    'female' => 'Жіночий',
    'pupil_age' => 'Вік учня',
    'age_before_3' => 'Малюки до 3 років',
    'age_4_6' => 'Діти (4-6)',
    'age_6_12' => 'Молодші школярі (6-12)',
    'age_12_17' => 'Школярі (12-17)',
    'age_17_23' => 'Студенти (17-23)',
    'age_23_40' => 'Дорослі (23-40)',
    'age_40_' => 'Дорослі (40+)',
    'for_tutors' => 'Репетиторам',
    'for_pupils' => 'Учням',
    'education' => 'Освіта',
    'tutor_reg' => 'Зареєструватися репетитором',
    'pupils_anons' => 'Оголошення учнів',
    'tutor_search' => 'Пошук репетитора',
    'set_anons' => 'Дати оголошення на пошук репетитора',
    'blog' => 'Статті',
    'about_proj' => 'Про проект',
    'slag' => 'Допомагаємо знайти вчителя',
    'our_mail' => 'Наша пошта',
    'reklama_on_site' => 'Реклама на сайті',
    'search_tut' => 'Знайти репетитора',
    'univers' => 'ВНЗ',
    'courses' => 'Освітні курси',
    'search_courses' => 'Курси',
    'add_courses' => 'Додати компанію ',
    'development' => 'Розробка сайту, хостинг',
    'share_page' => 'Поділіться сторінкою',


];
