<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Ці дані користувача не виявлені в нашій базі',
    'throttle' => 'Занадто багато спроб увійти. Будь ласка спробуйте знову через: seconds секунд.',
    'logout' => 'Вийти',
    'register' => 'Реєстрація',
    'register_go' => 'Зареєструватися',
    'register_already' => 'Вже зареєстровані?',
    'enter' => 'Увійти',
    'your_mail' => 'Ваш email',
    'your_pass' => 'Пароль',
    'your_name' => "Ваше ім'я",
    'captcha' => 'Захист від роботів',
    'forget_pass' => 'Забули свій пароль?',
    'type_register' => 'Виберіть тип реєстрації на сайті',
    'tutor_register' => 'Реєстрація для репетиторів',
    'conpany_register' => 'Реєстрація для компаній (додавання курсів)',
    'enter_page' => 'Сторінка входу на сайт',
    'register_page' => 'Сторінка реєстрації на сайті',
    're_pass_page' => 'Сторінка відновлення пароля',
    're_pass' => 'Відновлення пароля',
    'send_re_pass_link' => 'Відправити ссилку на відновлення пароля',
    'new_pass' => 'Новий пароль',
    're_new_pass' => 'Повторіть новий пароль',
    'set_new_pass' => 'Встановити новий пароль',
    'message' => 'Повідомлення',
    'send' => 'Надіслати',
    'enter_message' => 'Введіть повідомлення',

];
