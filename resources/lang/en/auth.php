<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'logout' => 'Выйти',
    'register' => 'Регистрация',
    'register_go' => 'Зарегистрироваться',
    'register_already' => 'Уже зарегистрированы?',
    'enter' => 'Войти',
    'your_mail' => 'Ваш E-Mail адрес',
    'your_pass' => 'Пароль',
    'your_name' => 'Ваше имя',
    'captcha' => 'Капча (защита от роботов)',
    'forget_pass' => 'Забыли свой пароль?',
    'type_register' => 'Выберите тип регистрации на сайте',
    'tutor_register' => 'Регистрация для репетиторов',
    'conpany_register' => 'Регистрация для компаний (добавление курсов)',
    'enter_page' => 'Страница входа на сайт',
    'register_page' => 'Страница регистрации на сайте',
    're_pass_page' => 'Страница восстановления пароля',
    're_pass' => 'Восстановление пароля',
    'send_re_pass_link' => 'Отправить ссылку на восстановление пароля',
    'new_pass' => 'Новый пароль',
    're_new_pass' => 'Повторите новый пароль',
    'set_new_pass' => 'Установить новый пароль',

];
