@extends('layouts.app_sidebar')

@section('title_page')
  @lang('auth.register_page')
@endsection

@section('content')

        <div class="col-md-9">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <b>@lang('auth.register')</b>
                </div>

                <div class="panel-body">
                    <form class="form-horizontal check_captcha" method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">@lang('auth.your_name')</label>

                            <div class="col-md-6">
                                <input type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">@lang('auth.your_mail')</label>

                            <div class="col-md-6">
                                <input type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">@lang('auth.your_pass')</label>

                            <div class="col-md-6">
                                <input type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label">@lang('auth.captcha')<span class="require"></span></label>
                            <div class="col-md-6">
                                {!! app('captcha')->display() !!}
                                <span style="display: none;" id="captcha_verify"></span>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    @lang('auth.register_go')
                                </button>
                            </div>
                        </div>

                        <input value="{{ $_GET['next'] }}" type="hidden" name="next_page">

{{--                        <input type="hidden" name="redirectTo" value="home">--}}
                    </form>
                </div>
            </div>
        </div>

@endsection
