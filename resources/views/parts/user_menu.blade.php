@if(Auth::user()->type == 'user_teach')

<?php

$route_name = Request::route()->getName();

?>

  <div class="panel panel-default col-lg-12 col-md-12 col-sm-12 col-xs-6 no_padding">
    <label class="control-label side-bar-label">@lang('menus.edit_prof')</label>
    <div class="panel-body">
      <ul class="nav-vertical">
        <li>
          <a {{ $route_name == 'tutor_account_basic' ? 'class=active' : '' }} href="{{ route('tutor_account_basic') }}">
            @lang('menus.basic_info')
          </a>
        </li>
        <li>
          <a {{ $route_name == 'tutor_account_main_info' ? 'class=active' : '' }} href="{{ route('tutor_account_main_info') }}">
            @lang('menus.main_info')
          </a>
        </li>
        <li>
          <a {{ $route_name == 'tutor_account_subjects_info' ? 'class=active' : '' }} href="{{ route('tutor_account_subjects_info') }}">
            @lang('menus.subjects_info')
          </a>
        </li>
        <li>
          <a {{ $route_name == 'tutor_account_about_me_info_save' ? 'class=active' : '' }} href="{{ route('tutor_account_about_me_info_save') }}">
            @lang('menus.edu_info')
          </a>
        </li>
      </ul>
    </div>
  </div>  

  <div class="panel panel-default col-lg-12 col-md-12 col-sm-12 col-xs-6 no_padding">
    <label class="control-label side-bar-label">@lang('menus.work_with_pupil')</label>
    <div class="panel-body">
      <ul class="nav-vertical">
        <li><a {{ $route_name == 'requests_index' || $route_name == 'requests_one' ? 'class=active' : '' }} href="{{ route('requests_index') }}">Заявки клиентов</a></li>
        <li><a {{ $route_name == 'my.review.show' ? 'class=active' : '' }} href="{{ route('my.review.show') }}">@lang('menus.review_pupil') <span style="color: red">{{ App\MyHelpers\Helper_two::count_teacher_review() }}</span></a></li>
      </ul>
    </div>
  </div>

{{--   <div class="panel panel-default col-lg-12 col-md-12 col-sm-12 col-xs-6 no_padding">
    <label class="control-label side-bar-label">Профиль на других языках</label>
    <div class="panel-body">
      <ul class="nav-vertical">
        <li><a {{ $route_name == 'tutor_langs' ? 'class=active' : '' }} href="{{ route('tutor_langs') }}">Добавить переводы профиля</a></li>
      </ul>
    </div>
  </div> --}}

  @if( Auth::user()->user_teacher->active != '1')
    <div style="margin-bottom: 10px;">
      <a target="_blank" id="show_how_put_teacher_profile" href="/articles/pravila-sozdaniya-profilya-repetitora"><i class="fa fa-info-circle" aria-hidden="true"></i> @lang('menus.tutor_prof_rule')</a>
    </div>  
  @endif

@else

  <div class="panel panel-default">
    <label class="control-label side-bar-label">Меню пользователя</label>
    <div class="panel-body">
      <ul class="nav-vertical">
        <li><a {{ Request::route()->getName() == 'company_all_my_notes' ? 'class=active' : '' }} href="{{ route('company_all_my_notes') }}">Мои учебные заведения</a></li>
        <li><a {{ Request::route()->getName() == 'edit_profile' ? 'class=active' : '' }} href="{{ route('edit_profile') }}">Редактирование профиля</a></li>
        <li><a href="{{ route('one_company_vip', ["id" => 'all', "type" => 'all' ]) }}">Продвижение компании на сайте</a></li>
      </ul>
    </div>
  </div>  

@endif

@section('script_partials')


<script type="text/javascript">

$(function() {
  var unread_message = $('#unread_message');
  if ( unread_message.length > 0) {
    $( ".messages" ).append( "<span class='label label-danger'>"+unread_message.text()+"</span>" );

  }

  var sent_request = $('#sent_request');
  if ( sent_request.length > 0) {
    $( ".requests" ).append( "<span class='label label-danger'>"+sent_request.text()+"</span>" );

  }

});

</script>


@endsection


