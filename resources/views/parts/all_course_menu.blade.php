<div id="first_part_bar_menu">

    <div class="panel panel-default">
        <label class="control-label side-bar-label">
            Все курсы компании
        </label>
        <div class="panel-body">

            <div id="about_register_comp" class="one-block-menu">
                <strong>Сейчас вы просматриваете все курсы которые создали</strong> <br>
                <p>Для изменения данных курса, нажмите на кнопку "Изменить" возле нужного вам курса.</p>
                <img src="/front/images/3_langs.png" alt=""><br>
                Информацию о курсах вы<strong> можете описать на 3 языках</strong> это увеличит посещаемость вашей страницы курсов и компании 
            </div>
     
        </div>
    </div>

</div>