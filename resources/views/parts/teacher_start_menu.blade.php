

<div class="panel panel-default">
  <label class="control-label side-bar-label">@lang('menus.edit_prof')</label>
  <div class="panel-body">

    <ul class="nav-vertical">
      <li>
        <a href="{{ route('tutor_account_basic') }}">
          @lang('menus.basic_info')
        </a>
      </li>
      <li>
        <a class="prevent_default_link" href="#">
          @lang('menus.main_info')
        </a>
      </li>
      <li>
        <a class="prevent_default_link" href="#">
          @lang('menus.subjects_info')
        </a>
      </li>
      <li>
        <a class="prevent_default_link" href="#">
          @lang('menus.edu_info')
        </a>
      </li>
    </ul>
  </div>
</div>  
{{-- 
<div class="panel panel-default col-lg-12 col-md-12 col-sm-12 col-xs-6 no_padding">
  <label class="control-label side-bar-label">Профиль на других языках</label>
  <div class="panel-body">
    <ul class="nav-vertical">
      <li><a class="prevent_default_link" href="#">Добавить переводы профиля</a></li>
    </ul>
  </div>
</div>
 --}}
{{-- <div class="panel panel-default col-lg-12 col-md-12 col-sm-12 col-xs-6 no_padding">
  <label class="control-label side-bar-label">@lang('menus.work_with_pupil')</label>
  <div class="panel-body">
    <ul class="nav-vertical">
      <li><a class="prevent_default_link" href="#">@lang('menus.review_pupil') </a></li>
    </ul>
  </div>
</div> --}}

<div style="margin-top: 10px;">
  <a target="_blank" id="show_how_put_teacher_profile" href="/articles/pravila-sozdaniya-profilya-repetitora"><i class="fa fa-info-circle" aria-hidden="true"></i> @lang('menus.tutor_prof_rule')</a>
</div>  