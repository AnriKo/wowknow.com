  <div class="panel panel-default">
    <label class="control-label side-bar-label">Действия на сайте</label>
    <div class="panel-body">
      <ul class="nav-vertical">
        @if (Auth::guest() )
            <li><a data-next-page="{{ route('tutor_account_basic') }}" title="Регистрация учителя" href="#" data-toggle="modal" data-target="#basic_register">Стать учителем</a></li>
        @else
            <li><a title="Регистрация учителя" href="{{ route('tutor_account_basic') }}">Стать учителем</a></li>
        @endif
        @if (Auth::guest() )
            <li><a data-next-page="{{ route('create_new_task') }}" title="Создать обьявление о поиске учителя" href="#" data-toggle="modal" data-target="#basic_register">Подать обьявление на поиск учителя</a></li>
        @else
            <li><a title="Создать обьявление о поиске учителя" href="{{ route('create_new_task') }}">Подать обьявление на поиск учителя</a></li>
        @endif
        <li><a href="{{ route('tutor_basic_search') }}">Все репетиторы</a></li>
        <li><a href="{{ route('task_search_subject_city', ['subject'=> 'subject','city'=> 'all-city']) }}">Обьявления учеников</a></li>
      </ul>
    </div>
  </div>  