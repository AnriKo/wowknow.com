@if (Auth::guest())

<!-- Modal login -->
<div class="modal fade" id="basic_login" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="panel-heading">
        <b>
            @lang('auth.enter')
        </b>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

        <form class="form-horizontal" method="POST" action="{{ route('login') }}">
            {{ csrf_field() }}

            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <label for="email" class="col-md-4 control-label">@lang('auth.your_mail')</label>
                <div class="col-md-6">
                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>
                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <label for="password" class="col-md-4 control-label">@lang('auth.your_pass')</label>
                <div class="col-md-6">
                    <input id="password" type="password" class="form-control" name="password" required>
                    @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{!! $errors->first('password') !!}</strong>
                        </span>
                    @endif
                </div>
            </div>       

            <div class="form-group">
                <div class="col-md-8 col-md-offset-4">
                    <button type="submit" class="btn btn-primary">
                        @lang('auth.enter')
                    </button>

{{--                    <a class="btn btn-default" id="reg_from_mod_login" href="#" data-toggle="modal" data-target="#basic_register" data-next-page="{{ route('tutor_account_basic') }}">Зарегистрироваться</a>--}}
                    <a class="btn btn-default" href="/register?next={{ route('tutor_account_basic') }}">
                        Зареєструватися
                    </a>

                </div>
                <a style="float: right; color: #889cad; padding: 16px 16px 0px 0px;" class="" href="{{ route('password.request') }}">
                        @lang('auth.forget_pass')
                    </a>
            </div>

            <input type="hidden" name="next_page">

        </form>
      </div>
    </div>
  </div>
</div>

<!-- Modal select type register -->
<div class="modal fade" id="modal_question_what_register" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="panel-heading"><b>@lang('auth.type_register')</b>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <div id="select_type_of_register" class="modal-body">

            <a data-next-page="{{ route('tutor_account_basic') }}" data-toggle="modal" data-target="#basic_register">@lang('auth.tutor_register')</a>
            <a data-next-page="{{ route('tutor_account_basic') }}" data-toggle="modal" data-target="#basic_register">@lang('auth.conpany_register')</a>

      </div>
    </div>
  </div>
</div>

<!-- Modal register -->
<div class="modal fade" id="basic_register" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="panel-heading"><b>@lang('auth.register')</b>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

        <form id="form_register" class="form-horizontal" method="POST" action="{{ route('register') }}">
            {{ csrf_field() }}

            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                <label for="name" class="col-md-4 control-label">@lang('auth.your_name')<span class="require"></span></label>
                <div class="col-md-6">
                    <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>
                    @if ($errors->has('name'))
                        <span class="help-block">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <label for="email_register" class="col-md-4 control-label">@lang('auth.your_mail')<span class="require"></span></label>
                <div class="col-md-6">
                    <input id="email_register" type="email" class="form-control" name="email" value="{{ old('email') }}" required>
                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <label for="password_register" class="col-md-4 control-label">@lang('auth.your_pass')<span class="require"></span></label>
                <div class="col-md-6">
                    <input id="password_register" type="password" class="form-control" name="password" required minlength="6">
                    @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group">
{{--                <label for="password-confirm" class="col-md-4 control-label">@lang('auth.captcha')<span class="require"></span></label>--}}
{{--                <div class="col-md-6">--}}
{{--                    <div class="g-recaptcha" data-sitekey="{{ env('CAPTCHA_SITEKEY', 'default_sitekey') }}"></div>--}}
{{--                    --}}{{-- {!! app('captcha')->display() !!} --}}
{{--                    <span style="display: none;" id="captcha_verify"/></span>--}}
{{--                </div>--}}
                <label for="jCaptcha" class="col-md-4 control-label">@lang('auth.captcha')<span class="require"></span></label>
                <div class="col-md-6">
                    <input id="jCaptcha" class="jCaptcha form-control" type="text" placeholder="Введіть тут результат">
                </div>
            </div>            

            <div class="form-group">
                <div class="col-md-8 col-md-offset-4">
                    <button style="margin-bottom: 5px;" type="submit" class="btn btn-primary">
                        @lang('auth.register_go')
                    </button>
                    <div style="float: right;" >
                        <span style="font-size: 70%;">@lang('auth.register_already')</span>
                        <a style="border-color: #337ab7; color: #084d88; background-color: #f1f1f1;" id="log_from_mod_reg" class="btn btn-default" href="#" data-toggle="modal" data-target="#basic_login">@lang('auth.enter')</a>
                    </div>
                </div>
            </div>

            <input type="hidden" name="next_page">
        </form>
      </div>
    </div>
  </div>
</div>

@endif