<div id="footer" class="container-fluid">
    <div class="row">
    	<div class="container">
	        <div class="col-md-12">
	            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 footer_part_menu">
	            	<h4>
		            	Вакансии
		            </h4>
					<ul>
						@if (Auth::guest() )
	                        <li>
	                        	<a data-next-page="{{ route('tutor_account_basic') }}" title="Регистрация учителя" href="#" data-toggle="modal" data-target="#basic_register" >
		                        	Работа для репетиторов
		                        </a>
		                    </li>
	                    @else
	                        <li>
	                        	<a title="Регистрация учителя" href="{{ route('tutor_account_basic') }}">
		                        	@lang('menus.tutor_reg')
		                        </a>
		                    </li>
	                    @endif
						<li>
							<a href="{{ route('task_search_subject_city', ['subject'=> 'subject','city'=> 'all-city']) }}">
								@lang('menus.pupils_anons')
							</a>
						</li>
						<li>
							<a href="{{ route('blog.one_page', 'pravila-sozdaniya-profilya-repetitora') }}">
								@lang('menus.tutor_prof_rule')
							</a>
						</li>
					</ul>	
	            </div>
	            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 footer_part_menu">
	            	<h4>
		            	Учебные заведения
		            </h4>
					<ul>
						<li>
                              <a href="{{ route('univer_look_cities') }}">@lang('menus.univers')
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('courses_basic_search', ['subject'=> 'subject','city'=> 'city']) }}">@lang('menus.search_courses')
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('schools_search', ['city'=> 'city']) }}">Альтернативные школы
                                </a>
                            </li>
                            						<li>
							<a href="{{ route('posts.index') }}">
								@lang('menus.blog')
							</a>
						</li>
						<li>
							<a href="{{ route('home') }}">
								@lang('menus.about_proj')
							</a>
						</li>
{{--                             @if (Auth::guest())
                                <li>
                                    <a class="add_info_button" data-next-page="{{ route('company_dasboard') }}" data-toggle="modal" data-target="#basic_register">
                                        @lang('menus.add_courses') <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                    </a>
                                </li>
                            @else
                                <li>
                                    <a class="add_info_button" href="{{ route('company_dasboard') }}">
                                        @lang('menus.add_courses') <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                    </a>
                                </li>
                            @endif --}}
					</ul>	
	            </div>
	            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 footer_part_menu">
	            	<h4>
		            	Цитаты, поэзия
		            </h4>
					<ul>
<li> <a style="color: #dede71;" href="https://sofispace.com/" target="_blank">Цитати відомих людей, українська поезія, вірші, сайт "Простір думок"</a></li>
					</ul>	
	            </div>
	            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 footer_part_menu">
	            	<h4><a title="Главная страница" style="color: #fff;" href="{{ route('home') }}"><span style="color: yellow;">WOW</span>KNOW</a></h4>
	            	<p>
                        <a target="_blank" id="wowknow_fb_group_footer" href="https://www.facebook.com/wowknow/">Страница в Facebook</a>
                    </p>
	            	<p> <a href="/articles/razmeschenie-reklamy-na-sajte">@lang('menus.reklama_on_site')</a></p>
	            	<p><a href="mailto:info.wowknow@gmail.com">info.wowknow@gmail.com</a></p>
					
	            </div>
	        </div>
	    </div>    
    </div>
</div>