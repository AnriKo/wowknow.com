<div class="container-fluid" style="padding: 0px">
    <nav class="navbar navbar-default navbar-static-top">
        <div class="container">
            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button id="button_header_menu" type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

            </div>

            <div class="" id="non_collapse_navbar">
                <ul class="nav navbar-nav main_link_nav">
                    <li>
                        <a title="Главная страница" href="{{ route('home') }}" class="btn btn-info logo_link"><span style="color: yellow;">WoW</span>know</a>
                    </li>
                </ul>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav main_link_nav">
                    <li>
                        <a {{ (Request::route()->getName() == 'tutor_basic_search' ) ? 'class=active' : '' }} href="{{ route('tutor_basic_search') }}">@lang('menus.search_tut')
                            </a>
                    </li>
                    @if (Auth::guest() )
                        <li>
                            <a data-next-page="{{ route('tutor_account_basic') }}" title="@lang('menus.tutor_reg')" href="/register?next={{ route('tutor_account_basic') }}">@lang('menus.tutor_reg') <i class="fa fa-plus-circle" aria-hidden="true"></i></a>
                        </li>
                    @else
                        @if(Auth::user()->type != "user_teach")
                            <li>
                                <a {{ Request::route()->getName() == 'tutor_account_basic' ? 'class=active' : '' }} title="@lang('menus.tutor_reg')" href="{{ route('tutor_account_basic') }}">@lang('menus.tutor_reg') <i class="fa fa-plus-circle" aria-hidden="true"></i></a>
                            </li>
                        @endif    
                    @endif
                    <li>
                        <a {{ (Request::route()->getName() == 'posts.index' || Request::route()->getName() == "posts.one_category" || Request::route()->getName() == "blog.one_page") ? 'class=active' : '' }} href="{{ route('posts.index') }}" class="btn-info">
                            @lang('menus.blog')
                        </a>
                    </li>
                    <li>
                        <a {{ Request::route()->getName() == 'contacts' ? 'class=active' : '' }} href="{{ route('contacts') }}" class="btn-info">
                           Контакти
                        </a>
                    </li>

                    <li id="right_top_menu" >

                        <button id="button_to_right_top_menu" type="button" class="navbar-toggle collapsed" aria-expanded="false">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>

                        <ul class="nav navbar-nav navbar-right">
{{--                         <li>
                            <a target="_blank" id="wowknow_fb_group" href="https://www.facebook.com/wowknow/"></a>
                        </li> --}}

                        <!-- Authentication Links -->
                        @if (Auth::guest())
                            <li><a href="#" data-toggle="modal" data-target="#basic_login">@lang('auth.enter')</a></li>
                        @else
                            <li id="user_profile_menu" class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"  aria-haspopup="true" aria-expanded="true">
                                    {{ Auth::user()->name }} <i class="fa fa-angle-down" aria-hidden="true"></i>
                                </a>
                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        @if(Auth::user()->type == 'user_teach')
                                            <a href="{{ route('tutor_account_basic') }}">
                                                <i class="fa fa-btn fa-user"></i>
                                                @lang('menus.my_profile')
                                            </a>
                                        @elseif(Auth::user()->type == 'user_company')
                                             <a href="{{ route('company_all_my_notes') }}">
                                                <i class="fa fa-btn fa-user"></i>
                                                @lang('menus.my_profile')
                                            </a>
                                        @else
                                             <a href="{{ route('company_all_my_notes') }}">
                                                <i class="fa fa-btn fa-user"></i>
                                                @lang('menus.my_profile')
                                            </a>
                                        @endif
                                    </li>
                                    <li>
                                        <a href="{{ url('/logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            <i class="fa fa-sign-out" aria-hidden="true"></i>
                                            @lang('auth.logout')
                                        </a>
                                        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endif
{{--                             <li>
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"  aria-haspopup="true" aria-expanded="true">@lang('menus.cur_lang')<i class="fa fa-angle-down" aria-hidden="true"></i>
                                </a>
                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="{{ route('setlocale', ['lang' => 'ru']) }}">Русский</a>
                                    </li>
                                    <li>
                                        <a href="{{ route('setlocale', ['lang' => 'ua']) }}">Українська</a>
                                    </li>
                                    <li>
                                        <a href="{{ route('setlocale', ['lang' => 'en']) }}" >English</a>
                                    </li>
                                </ul>
                            </li> --}}
                    </ul>

                    </li>

                </ul>

            </div>
        </div>
    </nav>
</div>    

    @if (Session::has('success'))

        <div id="flash_message">
            

            <div id="flash_message_wrapp" class="alert alert-success" role="alert">
                <div id="flash_message_text">
                    {!! Session::get('success') !!}
                </div>    
            </div>

        </div>

    @endif

    @if (Session::has('success_long_time'))

        <div id="flash_message_long_time">
            

            <div id="flash_message_wrapp" class="alert alert-success" role="alert">
                <div id="flash_message_text">
                    {{ Session::get('success_long_time') }}
                </div>   
                <span id="close_flash_message">Скрыть <i class="fa fa-times-circle" aria-hidden="true"></i></span> 
            </div>

        </div>

    @endif

    @if (count($errors) > 0)
        
        <div class="alert alert-danger" role="alert">
            <strong>Ошибка:</strong>
            @foreach ($errors->all() as $error)
                <li>{!! $error !!}</li>
            @endforeach
        </div>

    @endif