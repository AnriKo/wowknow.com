<!DOCTYPE html>
<html lang="en">
<head>

    <!-- Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-74359018-2"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());
      gtag('config', 'UA-74359018-2');
    </script>

    <title>@yield('title_meta')</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="@yield('description_meta', 'Образовательный портал, поиск репетиторов. Сообщество репетиторов, регистрация для репетиторов, найти курсы обучения')">
    <meta name="keywords" content="@yield('keywords_meta')">

    <meta property="og:title" content="@yield('title_meta')" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="{{url()->current()}}" />
    <meta property="og:description" content="@yield('description_meta', 'Образовательный портал, поиск репетиторов. Сообщество репетиторов, регистрация для репетиторов, найти курсы обучения')" />
    <meta property="og:image" content="@yield('og_image_meta', URL::asset('front/images/wowknow_img.png') )" />
    <meta property="auth" content="{{ Auth::user() ? 'true' : 'false' }}" />

    <!-- Fonts -->
    {{-- <script src="https://use.fontawesome.com/9211174925.js"></script> --}}
{{--     <link href="https://fonts.googleapis.com/css?family=PT+Sans:400,700&amp;subset=cyrillic" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Fira+Sans+Condensed:200,300,400&amp;subset=cyrillic" rel="stylesheet"> --}}

    <script
      src="https://code.jquery.com/jquery-2.2.3.min.js"
      integrity="sha256-a23g1Nt4dtEYOj7bR+vTu7+T8VP13humZFBJNIYoEJo="
      crossorigin="anonymous"></script>

{{--    <script src="https://www.google.com/recaptcha/api.js" async defer></script>--}}
    {{ Html::style('front/css/bootstrap.min.css') }}
    {{ Html::style('front/css/main.css') }}
    @yield('style')

{{--    <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>--}}
{{--    <script>--}}
{{--         (adsbygoogle = window.adsbygoogle || []).push({--}}
{{--              google_ad_client: "ca-pub-3870576311603929",--}}
{{--              enable_page_level_ads: true--}}
{{--         });--}}
{{--    </script>--}}
    

</head>