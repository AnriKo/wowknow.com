<div id="first_part_bar_menu">

    <div class="panel panel-default">
        <label class="control-label side-bar-label">
            Редактирование курса
        </label>
        <div class="panel-body">

            <div id="about_register_comp" class="one-block-menu">
                <strong>Сейчас вы редактируете курс вашей компании</strong> <br>
                <ul style="padding-left: 19px">
                  <li>Описывайте свой курс как можно подробнее минимум 5-7 предложений</li>
                  <li>Курсов вы можете создать столько сколько предлагает ваша компания</li>
                  <li>Один курс может быть привязан не более чем к 3-м категориям</li>
                </ul>
                <img src="/front/images/3_langs.png" alt=""><br>
                Информацию о курсах вы<strong> также можете описать на 3 языках</strong> это увеличит посещаемость вашей страницы курсов и компании 
            </div>
     
        </div>
    </div>

</div>