<a id="promo_for_tutors_link" data-next-page="{{ route('tutor_account_basic') }}" data-toggle="modal" data-target="#basic_register">
	<img src="/front/images/sidebar_for_tutors.jpg" alt="Регистрация для репетиторов">
	<p id="promo_for_tutors">
		Собираем команду лучших преподавателей. <br>Вы еще не с нами? <span style="color: #ff7729">Регистрируйтесь сейчас!</span></p>
</a>

<link href="https://fonts.googleapis.com/css?family=Roboto+Slab:700&amp;subset=cyrillic-ext" rel="stylesheet">

<style>
	#promo_for_tutors{
	font-family: 'Roboto Slab', serif;
    color: #000;
    font-size: 23px;
    position: absolute;
    bottom: 0;
    padding: 5px 15px;
    padding-bottom: 11px;
    margin-bottom: 0;
    width: 100%;
    line-height: 26px;
    border-top: 4px solid #fff;
    border-bottom: 2px solid #fff;
    background-color: rgba(193, 242, 255, 0.82);
    width: 285px;
	}
	#promo_for_tutors_link{
		position: relative;
    	display: block;
    	cursor: pointer;
	}
	##promo_for_tutors img{
		width: 100%;
	}
</style>


{{--
@php 
	$top_tuts = App\MyHelpers\Helper_two::top_tutors(); 
	$top_course = App\MyHelpers\Helper_two::top_company(); 
	$local = App::getLocale(); 
@endphp

@if(count($top_course) > 0)
	<div class="top_courses">
		<h4><span>ТОП</span> Учебные заведения</h4>
		@foreach($top_course as $item)
			<div class="one_top_tut">
				@if($item->getTable() == 'courses')
					<a href="{{ route('cours_one_show', ['alias' => $item->alias ]) }}">
						<div class="name">{{ $item->name }}</div>
						<div class="profile_title"><i style="color: #3fc9d0" class="fa fa-map-marker" aria-hidden="true"></i> {{ $item->title_ru }} - {{ $item->street }}</div>
						    <div class="subjects">
		                       	@foreach($item->cats as $cat)
		                           <span class="subject"> {{ $cat->name_ru }} </span>
		                        @endforeach
		                    </div>
		                    <span class="company_type">курсы</span>
						<div class="clear_both"></div>
					</a>
				@elseif($item->getTable() == 'schools')	
					<a href="{{ route('schools_one_show', ['alias' => $item->alias ]) }}">
						<div class="name">{{ $item->name }}</div>
						<div class="profile_title"><i style="color: #3fc9d0" class="fa fa-map-marker" aria-hidden="true"></i> {{ $item->title_ru }} - {{ $item->street }}</div>
						    @if($item->class)
                                <div class="subjects">
                                    <i style="color: orange" class="fa fa-child" aria-hidden="true"></i> Классы обучения - ({{$item->class}})
                                </div>
                            @endif
							<span class="company_type">школы</span>
						<div class="clear_both"></div>
					</a>
				@endif
			</div>
		@endforeach
	</div>
@endif

@if(count($top_tuts) > 0)
	<div class="top_tutors">
		<h4><span>ТОП</span> Репетиторы</h4>
		@foreach($top_tuts as $item)
			<div class="one_top_tut">
				<a href="{{ route('tutor_page', ['slug' => $item->slug ]) }}">
					<div class="avatar"><img src="{{ URL::asset('storage/users/') }}/{{ $item->avatar }}" alt=""></div>
					<div class="name">{{ $item->name }} {{ $item->last_name }}</div>
					<div class="profile_title">{{ $item->profile_title }}</div>
				    <div class="subjects">
                        @foreach($item->subjects as $subject)
                           <span class="subject">{{ $subject->{'name_'.$local} }}</span>
                        @endforeach
                    </div>
					<div class="clear_both"></div>
				</a>
			</div>
		@endforeach
	</div>
@endif

--}}

{{-- <div class="reklama_right_side_bar" id="default">
	<!-- Вертикальний блок -->
	<ins class="adsbygoogle"
	     style="display:block"
	     data-ad-client="ca-pub-3870576311603929"
	     data-ad-slot="5602492573"
	     data-ad-format="auto"
	     data-full-width-responsive="true"></ins>
	<script>
	     (adsbygoogle = window.adsbygoogle || []).push({});
	</script>
</div>
 --}}