@extends('emails/layouts/basic_template')

@section('email_title')
	Заявка от учителя на обучение
@endsection

@section('email_content')
	<h2>Здравствуйте, {{ $user_pupil }}</h2>
	<p style="text-align: left;">Учитель <b>{{ $user_teacher }}</b> отправил вам заявку на обучение. Он готов начать с вами заниматься</p>
	<p style="text-align: left;">
		Пожалуйста свяжитесь с учителем и уточните все детали, <br>
		вы можете это сделать в своем профиле в разделе "Заявки на обучение" на сайте  <a href="{{ route('requests') }}">{!! Config::get('app.domain') !!}</a>
	</p>
@endsection

@section('email_button_text')
	Посмотреть свои заявки
@endsection

@section('email_button_url')
{{ route('requests' ) }}
@endsection