@extends('emails/layouts/basic_template')

@section('email_title')
	Здравствуйте, {{ $who_create_request->name }}
@endsection

@section('email_title_second')
	
@endsection

@section('email_content')
	<h2>Вашу заявку принял учитель {{ $who_accept_request->name }}</h2>
	<p>С Вами готовы начать заниматься</p>
	<p>
		Пожалуйста свяжитесь с учителем и уточните все детали, <br>
		вы можете это сделать в своем профиле в разделе "Заявки на обучение" на сайте  <a href="{{ route('requests') }}">{!! Config::get('app.domain') !!}</a>
	</p>
	<p>Желаем хороших занятий!</p>
@endsection

@section('email_button_text')
	Перейти к моим заявкам
@endsection

@section('email_button_url')
{{ route('requests' ) }}
@endsection