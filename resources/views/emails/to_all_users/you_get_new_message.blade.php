@extends('emails/layouts/basic_template')

@section('email_title')
	Здравствуйте, {{ $recipient }}
@endsection

@section('email_content')
	<h2>У вас новое сообщение, от пользователя {{ $sender }}</h2>
	<p>
		Для просмотра и ответа на сообщение перейдите на сайт {!! Config::get('app.domain') !!} в свой профиль. <br>
	</p>
@endsection

@section('email_button_text')
	Прочитать сообщение
@endsection

@section('email_button_url')
{{ route('conversations' ) }}
@endsection