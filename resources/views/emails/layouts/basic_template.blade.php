<!DOCTYPE html>
<html lang="it">
    <head>
        <meta http-equiv="Content-Type" content="text/html charset=UTF-8" />
            <title>WowKnow - портал поиска репетиторов</title>

            <meta charset="utf-8"><meta name="viewport" content="width=device-width"><style type="text/css">#ko_onecolumnBlock_6 .textintenseStyle a, #ko_onecolumnBlock_6 .textintenseStyle a:link, #ko_onecolumnBlock_6 .textintenseStyle a:visited, #ko_onecolumnBlock_6 .textintenseStyle a:hover {color: #fff;color: ;text-decoration: none;text-decoration: none;font-weight: bold;}
#ko_onecolumnBlock_6 .textlightStyle a:visited, #ko_onecolumnBlock_6 .textlightStyle a:hover {color: #3f3d33;color: ;text-decoration: none;text-decoration: ;font-weight: bold;}</style><style type="text/css">

#outlook a{padding:0;} 
.ReadMsgBody{width:100%;} .ExternalClass{width:100%;} 
.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div{line-height: 100%;} 
body, table, td, a{-webkit-text-size-adjust:100%; -ms-text-size-adjust:100%;} 
table, td{mso-table-lspace:0pt; mso-table-rspace:0pt;} 
img{-ms-interpolation-mode:bicubic;} 

body{margin:0; padding:0;}
img{border:0; height:auto; line-height:100%; outline:none; text-decoration:none;}
table{border-collapse:collapse !important;}
body{height:100% !important; margin:0; padding:0; width:100% !important;}
th{font-weight: normal;}
h2{font-size: 1.2em;}
.border_table tr{
  border-bottom: 1px solid #e1e1e1;
}
.border_table th{
  padding: 4px 12px;
}
table{
  text-align: left;
}

.appleBody a{color:#68440a; text-decoration: none;}
.appleFooter a{color:#999999; text-decoration: none;}

@media screen and (max-width: 525px) {

    table[class="wrapper"]{
      width:100% !important;
      min-width:0px !important;
  }

  td[class="mobile-hide"]{
      display:none;}

      img[class="mobile-hide"]{
          display: none !important;
      }

      img[class="img-max"]{
          width:100% !important;
          max-width: 100% !important;
          height:auto !important;
      }

      table[class="responsive-table"]{
          width:100%!important;
      }

      td[class="padding"]{
          padding: 10px 5% 15px 5% !important;
      }

      td[class="padding-copy"]{
          padding: 10px 5% 10px 5% !important;
          text-align: center;
      }

      td[class="padding-meta"]{
          padding: 30px 5% 0px 5% !important;
          text-align: center;
      }

      td[class="no-pad"]{
          padding: 0 0 0px 0 !important;
      }

      td[class="no-padding"]{
          padding: 0 !important;
      }

      td[class="section-padding"]{
          padding: 10px 15px 10px 15px !important;
      }

      td[class="section-padding-bottom-image"]{
          padding: 10px 15px 0 15px !important;
      }

      td[class="mobile-wrapper"]{
        padding: 10px 5% 15px 5% !important;
    }

    table[class="mobile-button-container"]{
        margin:0 auto;
        width:100% !important;
    }

    a[class="mobile-button"]{
        width:80% !important;
        padding: 15px !important;
        border: 0 !important;
        font-size: 16px !important;
    }

}
</style>
</head>
<body style="margin: 0; padding: 0;" bgcolor="#ffffff" align="center">

<table border="0" cellpadding="0" cellspacing="0" width="100%" id="ko_titleBlock_5">
    <tbody>
        <tr class="row-a">
            <td bgcolor="#7ca9e1" align="center" class="section-padding" style="padding: 10px;">
                <table border="0" cellpadding="0" cellspacing="0" width="500" style="padding: 0 0 20px 0;" class="responsive-table">
                    <tbody>
                        <tr>
                            <td align="center" class="padding-copy" colspan="2" style="padding: 0 0 0px 0px; font-size: 21px; font-family: Helvetica; font-weight: normal; color: #ffffff;">
                                @yield('email_title')
                                <a href="wowknow.com" style="display: block; margin-top: 5px; text-decoration: none; color: #fff;">
                                  <span style="color: yellow;">WoW</span>know.com
                                </a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table>

<table border="0" cellpadding="0" cellspacing="0" width="100%" id="ko_onecolumnBlock_6">
    <tbody>
        <tr class="row-a">
            <td bgcolor="#eceff3" align="center" class="section-padding" style="padding-top: 30px; padding-left: 15px; padding-bottom: 30px; padding-right: 15px;">
                <table style="background: #fff;" border="0" cellpadding="0" cellspacing="0" width="500" class="responsive-table">
                    <tbody>
                        <tr>
                            <td>
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tbody>
                                        <tr>
                                            <td>
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tbody>
                                                        <tr>
                                                            <td align="center" class="padding-copy" style="font-size: 20px; font-family: Helvetica, Arial, sans-serif; color: #3F3D33; padding-top: 10px;">
                                                                @yield('email_title_second')
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" class="padding-copy textlightStyle" style=" font-size: 14px; line-height: 21px; font-family: Helvetica, Arial, sans-serif; color: #3F3D33;padding: 0px 10px;">
                                                                <p style="margin:0px;">
                                                                    @yield('email_content')
                                                                </p>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="mobile-button-container">
                                                    <tbody>
                                                        <tr>
                                                            <td align="center" style="padding: 25px 0 0 0;" class="padding-copy">
                                                                <table border="0" cellspacing="0" cellpadding="0" class="responsive-table">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td align="center">
                                                                                <a target="_new" class="mobile-button" style="margin: 17px 0 20px 0; display: inline-block; font-size: 18px; font-family: Helvetica, Arial, sans-serif; font-weight: normal; color: #ffffff; text-decoration: none; background-color: #ff560d; padding-top: 9px; padding-bottom: 9px; padding-left: 25px; padding-right: 25px; border-radius: 3px; -webkit-border-radius: 3px; -moz-border-radius: 3px; border-bottom: 3px solid #c20000;" href="@yield('email_button_url')">
                                                                                        @yield('email_button_text')
                                                                                </a>
                                                                            </td>
                                                                        </tr>

                                                                        @if(View::hasSection('email_button_text_2'))

                                                                        <tr>
                                                                            <td align="center">
                                                                                <a target="_new" class="mobile-button" style="margin-bottom: 20px; display: inline-block; font-size: 18px; font-family: Helvetica, Arial, sans-serif; font-weight: normal; color: #ffffff; text-decoration: none; background-color: #c4c4c4; padding-top: 9px; padding-bottom: 9px; padding-left: 25px; padding-right: 25px; border-radius: 3px; -webkit-border-radius: 3px; -moz-border-radius: 3px; border-bottom: 3px solid #7c7c7c;" href="@yield('email_button_url_2')">
                                                                                        @yield('email_button_text_2')
                                                                                </a>
                                                                            </td>
                                                                        </tr>

                                                                        @endif

                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table>

<table border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width: 500px;" id="ko_footerBlock_2">
    <tbody>
        <tr>
            <td bgcolor="#ffffff" align="center">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                    <tbody>
                        <tr>
                            <td style="padding: 20px 0px 20px 0px;">

                            <table width="500" border="0" cellspacing="0" cellpadding="0" align="center" class="responsive-table">
                                <tbody>
                                    <tr style="text-align: center;">
                                        <td>
                                            <a target="_new" style="margin-top: 5px; padding: 10px 20px; color: #bcb1b1; display: inline-block; text-decoration: none; font-weight: bold; font-family: Helvetica, Arial, sans-serif" href="{{ route('home') }}">
                                                <span >Wow</span>Know.com
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center" valign="middle" style="font-size: 12px; line-height: 18px; font-family: Helvetica, Arial, sans-serif; color: #3F3D33;">
                                            <span class="appleFooter" style="color: #3F3D33;">
                                               WoW портал пошуку репетиторів та вчителів.
                                            </span><br>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>
        </td>
    </tr>
</tbody>
</table>
</body>
</html>
