@extends('emails/layouts/basic_template')

@section('email_title')
	{{ $for_tutor_mail['status'] }}
@endsection

@section('email_content')

		<table class="border_table" style="width:100%">
 
		  <tr>
		  	<th>Анкета репетитора на сайте</th>
		    <th>
		    	<a href="{{ route('tutor_page', ['slug' => $for_tutor_mail['tutor_link'] ]) }}" class="btn btn-info">{{ $for_tutor_mail['tutor_name'] }}</a>
			</th>
		  </tr> 

		  <tr>
		    <th>ID репетитора</th>
		    <th>
		    	{{$for_tutor_mail['tutor_id']}}
			</th>
		  </tr> 

		  <tr>
		    <th>Контакты клиента</th>
		    <th>
			   <strong>Телефон:</strong> {{ $for_tutor_mail['client_phone'] }} <br>
			   <strong>Почта:</strong> {{ $for_tutor_mail['client_mail'] }} <br>
			   <strong>Имя клиента:</strong> {{ $for_tutor_mail['client_name'] }} <br>
			</th>
		  </tr>	   

		</table>

@endsection

