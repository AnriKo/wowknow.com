<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Користувач додав нову статтю на сайт</title>
</head>
<body>
    <p>Користувач додав нову статтю на сайт</p>
	<p>
		Для перевірки статті та її оприлюднення - перейдіть в адмін панель за посиланням <br>
        <a target="_blank" href="{{ route('home') }}/admin/post_edit/{{ $id }}">
			{{ route('home') }}/admin/post_edit/{{ $id }}
		</a>
	</p>
	<p>
		Для всіх користувачів стаття доступна за посиланням <br>
		<a target="_blank" href="{{ route('home') }}/articles/{{ $slug }}">
			{{ route('home') }}/articles/{{ $slug }}
		</a>
	</p>

</body>
</html>