<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Лист з контактної форми сайту WowKnow</title>
</head>
<body>
	<p>
		<strong>Користувач:</strong> {{ $user_name }} <br>
		<strong>Пошта:</strong> {{ $user_email }} <br>
		<strong>Повідомлення:</strong> {{ $user_message }} <br>
	</p>
	<p>Лист з контактної форми сайту WowKnow</p>
	<p>
		Для відповіді користувачу не відповідайте безпосередньо на цей лист.<br>
		Напишіть нового, окремого листа в вашій поштовій системі, та надішліть на пошту користувача  {{ $user_email }}
	</p>
</body>
</html>