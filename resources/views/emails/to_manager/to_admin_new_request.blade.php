@extends('emails/layouts/basic_template')

@section('email_title')
	Пришла новая заявка от клинта к репетитору
@endsection

@section('email_content')
	<p>Привет Андрей</p>
		<h3>Репетитор</h3> 

		<table class="border_table" style="width:100%">
		  <tr>
		    <th>Контакты репетитора</th>
		    <th>
		    	<strong>Телефон:</strong> {{ $for_tutor_mail['tutor_phone'] }} <br>
			   	<strong>Почта:</strong> {{ $for_tutor_mail['tutor_mail'] }}
			</th>
		  </tr>  
		  <tr>
		    <th>Анкета репетитора на сайте</th>
		    <th>
		    	<a href="{{ route('tutor_page', ['slug' => $for_tutor_mail['tutor_link'] ]) }}" class="btn btn-info">{{ $for_tutor_mail['tutor_name'] }}</a>
			</th>
		  </tr> 
		  <tr>
		    <th>ID репетитора</th>
		    <th>
		    	{{$for_tutor_mail['tutor_id']}}
			</th>
		  </tr>   
		</table>

		<h3>Клиент</h3>
		<table class="border_table" style="width:100%">
		  <tr>
		    <th>Контакты клиента</th>
		    <th>
			   <strong>Телефон:</strong> {{ $for_tutor_mail['client_phone'] }} <br>
			   <strong>Почта:</strong> {{ $for_tutor_mail['client_mail'] }}
			</th>
		  </tr>
		  <tr>
		    <th>Имя клиента</th>
		    <th>{{ $for_tutor_mail['client_name'] }}</th>
		  </tr>  
		  <tr>
		    <th>Предмет</th>
		    <th>{{ implode(", ", $for_tutor_mail['subjects']) }}</th>
		  </tr>
		  <tr>
		    <th>Место преподавания</th>
		    <th>{{ $for_tutor_mail['teach_place'] }}</th>
		  </tr>
		  @if($for_tutor_mail['pupil_adress'] != '')
			<tr>
				<th>Адрес ученика</th>
				<th>{{ $for_tutor_mail['pupil_adress'] }}</th>
			</tr>
		  @endif
		  <tr>
		    <th>Возраст ученика</th>
		    <th>{{ $for_tutor_mail['age'] }}</th>
		  </tr>
		  <tr>
		    <th>Текущий уровень знаний</th>
		    <th>{{ $for_tutor_mail['level'] }}</th> 
		  </tr>
		  <tr>
		    <th>Продолжительность курса занятий</th>
		    <th>{{ $for_tutor_mail['course_length'] }}</th> 
		  </tr>
		  <tr>
		    <th>Количество занятий в неделю</th>
		    <th>{{ $for_tutor_mail['times_a_week'] }}</th> 
		  </tr> 
		  <tr>
		    <th>Цель занятий, уточнения</th>
		    <th>{{ $for_tutor_mail['message'] }}</th>
		  </tr>   
		</table>

	</p>
@endsection

