@extends('emails/layouts/basic_template')

@section('email_title')
	Вы успешно зарегистрировались репетитором
@endsection

@section('email_title_second')
	Здравствуйте, {{ $recipient }}
@endsection

@section('email_content')

	<p>Ваша страница репетитора в интернете <br> 
		<a style="color: #9597ff" href="{{ route('tutor_page', ['slug' => $teacher_slug ]) }}">{{ route('tutor_page', ['slug' => $teacher_slug ]) }}</a> <br>
		посмотрите ее и отредактируйте если необходимо.
	</p>
	<p style="text-align: left;">
		<strong>Чтобы больше людей узнало о вас как о репетиторе, вы можете сделать такие действия:</strong>  <br>
		
	    <ul class="nav-vertical">
	      <li>переведите свою страницу на другие языки, это во много раз увеличит просмотры вашего профиля.<br> 
	      	Страница для добавления переводов <a href="{{ route('tutor_langs') }}">{{ route('tutor_langs') }}</a> находится в вашем профиле. Сейчас доступны: английский, украинский и русский языки.
	      </li>
	      <li>поделитесь своей страницей в соц. сетях (это можно сделать нажав на иконку в правом верхнем углу вашей <br> 
	      	<a href="{{ route('tutor_page', ['slug' => $teacher_slug ]) }}"> созданной странцы репетитора</a>).
	      </li>
	      <li>разместите информацию о себе в нашей странице фейсбук 
	      	<a href="https://www.facebook.com/wowknow/">facebook.com/wowknow</a>
	      	Будем признательны за Like нашей fb странички ).
	      </li>
	    </ul>
	</p>
	<p>Благодарим вас, за то что присоединились к нашему образовательному сообществу. <br> Хорошего дня и отличных учеников!</p>

@endsection

@section('email_button_text')
	Посмотреть свою страницу
@endsection

@section('email_button_url')
{{ route('tutor_page', ['slug' => $teacher_slug ]) }}
@endsection