@extends('emails/layouts/basic_template')

@section('email_title')
	Клиент выбрал вас репетитором
@endsection

@section('email_content')
	<h2>Здравствуйте, {{ $for_tutor_mail['tutor_name'] }}</h2>
	<p>На сайте wowknow.com клиент выбрал вас репетитором, и отправил вам заявку. Если условия вам подходят, нажмите кнопку <strong>Взять заказ</strong> после этого мы отправим вам контактные данные клиента и вы начнете обучение.</p>
		<b>Детали заказа</b> 
		<table class="border_table" style="width:100% ; font-weight: normal; text-align: left;">
		  <tr>
		    <th>Предмет</th>
		    <th>{{ implode(", ", $for_tutor_mail['subjects']) }}</th>
		  </tr>
		  <tr>
		    <th>Место преподавания</th>
		    <th>{{ $for_tutor_mail['teach_place'] }}</th>
		  </tr>
		  @if($for_tutor_mail['pupil_adress'] != '')
			<tr>
				<th>Адрес ученика</th>
				<th>{{ $for_tutor_mail['pupil_adress'] }}</th>
			</tr>
		  @endif
		  <tr>
		    <th>Возраст ученика</th>
		    <th>{{ $for_tutor_mail['age'] }}</th>
		  </tr>
		  <tr>
		    <th>Текущий уровень знаний</th>
		    <th>{{ $for_tutor_mail['level'] }}</th> 
		  </tr>
		  <tr>
		    <th>Продолжительность курса занятий</th>
		    <th>{{ $for_tutor_mail['course_length'] }}</th> 
		  </tr>
		  <tr>
		    <th>Количество занятий в неделю</th>
		    <th>{{ $for_tutor_mail['times_a_week'] }}</th> 
		  </tr> 
		  <tr>
		    <th>Цель занятий, уточнения</th>
		    <th>{{ $for_tutor_mail['message'] }}</th>
		  </tr> 
		  <tr>
		    <th>Стоимость занятия</th>
		    <th>Указанная вами в анкете (обговариваете напрямую с клиентом после обмена контактами)</th>
		  </tr>
		  <tr>
		    <th>Имя клиента</th>
		    <th>{{ $for_tutor_mail['client_name'] }}</th>
		  </tr>  
		  <tr>
		    <th>Цена заказа</th>
		    <th>150грн (оплачиваете после первого проведенного занятия)</th>
		  </tr>   
		</table>
	</p>
	<p>Если детали заказа вам подходят, нажмите на кнопку взять заказ и мы обменяем вас контактами с клиентом после чего вы сможете договориться о дате и месте проведения первого занятия.</p>
	<p>На любые ваши вопросы мы можем ответить по номеру 050 695 64 78 (Phone/Viber/Telegram)</p>
@endsection

@section('email_button_text')
	Взять этот заказ
@endsection

@section('email_button_url')
	{{ route('tutor_auth', [ 'requestid' =>  $for_tutor_mail['requestid'] ]) }}?id={{ $for_tutor_mail['tutor_id'] }}&auth=need&report=yes
@endsection

@section('email_button_text_2')
	Отказаться от заказа
@endsection

@section('email_button_url_2')
	{{ route('tutor_auth', [ 'requestid' =>  $for_tutor_mail['requestid'] ]) }}?id={{ $for_tutor_mail['tutor_id'] }}&auth=need&report=no
@endsection