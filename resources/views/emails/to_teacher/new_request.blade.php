@extends('emails/layouts/basic_template')

@section('email_title')
	Ученик хочет с вами заниматься
@endsection

@section('email_title_second')
	Здравствуйте, {{ $user_teacher }}
@endsection

@section('email_content')
	<p>Вы получили новую заявку на обучение от пользователя, {{ $user_pupil }}</p>
	<p>
		Пожалуйста свяжитесь с автором заявки и уточните все детали. <br>
		Если условия вам подходят приймите заявку и начинайте обучение.
		Вы можете это сделать в своем профиле в разделе "Заявки на обучение" на сайте <br> 
		<a href="{{ route('requests') }}">{!! Config::get('app.domain') !!}</a>
	</p>
@endsection

@section('email_button_text')
	Перейти к моим заявкам
@endsection

@section('email_button_url')
{{ route('requests' ) }}
@endsection