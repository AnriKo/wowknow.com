@extends('emails/layouts/basic_template')

@section('email_title')
	Новый заказ по вашему предмету
@endsection

@section('email_content')
	<h2>Здравствуйте, {{ $recipient }}</h2>
	<p>На сайте появились новые заказы от учеников по предмету &#8222;<b style="font-size: 110%">{{ $task_subject->name_ru }}</b>&#8220;</p>
	<p>
		<b>Детали заказа:</b> 
		<table class="border_table" style="width:100% ; font-weight: normal; text-align: left;">
		  <tr>
		    <th>Имя автора заявки:</th>
		    <th>{{ $new_task->name }}</th>
		  </tr>
		  <tr>
		    <th>Имя ученика:</th>
		    <th>{{ $new_task->pupil_name }}</th>
		  </tr>
		  <tr>
		    <th>Предмет:</th>
		    <th>{{ $task_subject->name_ru }}</th> 
		  </tr> 
		  <tr>
		    <th>Описание:</th>
			<th>{{ $new_task->text }}</th>
		  </tr>  
		  <tr>
		    <th>Место проведения занятий:</th>
		    <th>
		    	@if($new_task->teach_online == '1') 
		    		- Занятия онлайн <br>
		     	@endif
		    	@if($new_task->teach_local == '1') 
		    		- Занятия в городе {{ $new_task->teach_city_en }}, <br>
		    		@if($new_task->teach_local_pupil == '1')
		    			на територии ученика, <br>
		    		@endif
		    		@if($new_task->teach_local_teacher == '1')
		    			на територии учителя. <br>
		    		@endif
		     	@endif
		 	</th>
		  </tr>  
		  <tr>
		    <th>Цена:</th>
		    <th>{{ $new_task->price_def }} {{ $new_task->cur_def }} </th>
		  </tr>  
		</table>
	</p>
	<p>Если вам подходит описание, свяжитесь с автором заявки, уточните детали и начинайте занятия. Место проведения уроков, цену занятия, вы сможете обговорить при личном общении, более детально.</p>
	<p>Хорошего дня и отличных занятий!</p>
@endsection

@section('email_button_text')
	Посмотреть новые заказы
@endsection

@section('email_button_url')
{{ route('task_search_subject_city', [$task_subject->id, 'all-city']) }}
@endsection