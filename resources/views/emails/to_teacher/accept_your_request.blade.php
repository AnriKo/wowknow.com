@extends('emails/layouts/basic_template')

@section('email_title')
	Ваша заявка принята!
@endsection

@section('email_content')
	<h2>Здравствуйте, {{ $who_create_request->name }}</h2>
	<p>Ученик {{ $who_accept_request->name }} принял вашу заявку</p>
	<p>
		Пожалуйста свяжитесь с учеником и уточните все детали, <br>
		вы можете это сделать в своем профиле в разделе "Заявки на обучение".</a>
	</p>
@endsection

@section('email_button_text')
	Перейти в свой профиль
@endsection

@section('email_button_url')
{{ route('tutor_account_requests_to_me' ) }}
@endsection