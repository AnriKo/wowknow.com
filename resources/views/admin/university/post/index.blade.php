
@extends('templates.admin.layout')

@section('content')
<div class="">

    <div class="row">

        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Вузы <a href="{{route('university.create')}}" class="btn btn-primary btn-xs"><i class="fa fa-plus"></i> Создать новый </a></h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                {{-- dd($categories) --}}
                    <table id="datatable-buttons" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Заголовок</th>
                                <th>Алиас</th>
                                <th>Предметы</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>ID</th>
                                <th>Заголовок</th>
                                <th>Алиас</th>
                                <th>Предметы</th>
                                <th>Action</th>
                            </tr>
                        </tfoot>
                        <tbody>
                            @if (count($posts))
                            @foreach($posts as $row)
                            <tr>
                                <td>{{$row->id}}</td>
                                <td><a href="{{ route('university.edit', ['id' => $row->id ]) }}">{{$row->title}}</a></td>
                                <td>{{ $row->slug }}</td>
                                <td>
                                    @foreach($row->subjects_name as $row_in)
                                        {{$row_in->name_ru}}, 
                                    @endforeach
                                </td>
                                <td>
                                    <a href="{{ route('university.edit', ['id' => $row->id ]) }}" class="btn btn-info btn-xs"><i class="fa fa-pencil" title="Edit"></i> </a>
                                </td>
                            </tr>
                            @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@stop