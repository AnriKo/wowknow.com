@extends('templates.admin.layout')

@section('content')

    <h1 class="col-md-offset-2">Создание универа</h1>

    <form method="POST" action="{{ route('university.store') }}" class="form-horizontal" enctype="multipart/form-data"> 

        {{ csrf_field() }}

        <div class="form-group">
          <label class="col-sm-2 control-label" for="title">Логотип универа</label>
          <div class="col-sm-10">

              <input name="logo_univer" id="upload" type="file" class="cropit-image-input">

          </div> 
        </div>


        <div class="form-group">
            <label class="col-sm-2 control-label" for="title">Заголовок</label>
            <div class="col-sm-10">
                <input required type="text" class="form-control" id="title" name="title">
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-2 control-label" for="slug">Алиас</label>
            <div class="col-sm-10">
                <input required type="text" class="form-control" id="slug" name="slug">
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-2 control-label" for="tags">Категория</label>
            <div class="col-sm-10">

                <select id="js-example-basic-hide-search-multi" multiple="multiple" class="form-control" name="subjects[]">

                    @foreach($tags as $row)

                      <option value="{{ $row->id }}">{{ $row->name_ru }}</option>  
                        
                    @endforeach

                </select>
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-2 control-label" for="content">Краткая инфрмация, адрес</label>
            <div class="col-sm-10">
                <textarea placeholder="В виде таблицы" id="adress_info" name="adress_info" class="ckeditor form-control"></textarea>
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-2 control-label" for="content">Описание</label>
            <div class="col-sm-10">
                <textarea id="content" name="description" class="ckeditor form-control"></textarea>
            </div>
        </div>

{{--         <div class="form-group">
            <label class="col-sm-2 control-label">Статус</label>
            <div class="col-sm-10">
                <label class="gender"><input @if($post->post_status == 'published') checked @endif value="published" type="radio" name="post_status"> Опубликовать</label> <br>
                <label class="gender"><input @if($post->post_status == 'no_published') checked @endif value="no_published" type="radio" name="post_status">Пока не публиковать</label>
            </div>
        </div> --}}

        <input class="col-md-offset-2 btn btn-primary" type="submit" value="Создать">

      </form>

@endsection

@section('style')

{{ Html::style('front/select2/select2.css') }}
<style>
.select2-selection__rendered li{
  color:#000;
}
#content_ifr{
  height: 500px !important;
}
</style>
@endsection

@section('scripts')

{{ Html::script('front/select2/select2.full.js') }}
<script src="{{ URL::to('admin/js/new_package/tinymce/js/tinymce/tinymce.min.js') }}"></script>
<script src="/vendor/laravel-filemanager/js/lfm.js"></script>

<script>
    var editor_config = {
        path_absolute: "/",
        selector: '#content, #adress_info',
        plugins: [
            "advlist autolink lists link image charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars code fullscreen",
            "insertdatetime media nonbreaking save table contextmenu directionality",
            "emoticons template paste textcolor colorpicker textpattern"
        ],
        toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
        relative_urls: false,
        file_browser_callback: function(field_name, url, type, win) {
          var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
          var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

          var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
          if (type == 'image') {
            cmsURL = cmsURL + "&type=Images";
          } else {
            cmsURL = cmsURL + "&type=Files";
          }

          tinyMCE.activeEditor.windowManager.open({
            file : cmsURL,
            title : 'Filemanager',
            width : x * 0.8,
            height : y * 0.8,
            resizable : "yes",
            close_previous : "no"
          });
        },

    };

    tinymce.init(editor_config);

    $('#lfm').filemanager('image');

    //select2
$('#js-example-basic-hide-search-multi').select2({
    multiple: true,
});


</script>

@endsection