
@extends('templates.admin.layout')

@section('content')
<div class="">

    <div class="row">

        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Предметы обучения в Университетах <a href="{{route('university_subjects.create')}}" class="btn btn-primary btn-xs"><i class="fa fa-plus"></i> Создать новый </a></h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                {{-- dd($tags) --}}
                    <table id="datatable-buttons" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Название</th>
                                <th>Алиас</th>
                                <th># университетов</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>ID</th>
                                <th>Название</th>
                                <th>Алиас</th>
                                <th># университетов</th>
                                <th>Action</th>
                            </tr>
                        </tfoot>
                        <tbody>
                            @if (count($tags))
                            @foreach($tags as $row)
                            <tr>
                                <td>{{$row->id}}</td>
                                <td>{{$row->name}}</td>
                                <td>{{ $row->slug }}</td>
                                <td>{{ $row->posts_num }}</td>
                                <td>
                                    <a href="#" class="btn btn-info btn-xs"><i class="fa fa-pencil" title="Edit"></i> </a>
                                </td>
                            </tr>
                            @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@stop


