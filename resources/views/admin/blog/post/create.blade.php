@extends('templates.admin.layout')

@section('content')

    <h1 class="col-md-offset-2">Создание поста</h1>

    <form method="POST" action="{{ route('blog_post.store') }}" class="form-horizontal" enctype="multipart/form-data"> 

        {{ csrf_field() }}

        <div class="form-group">
          <label class="col-sm-2 control-label" for="title">Главная картинка поста</label>
          <div class="col-sm-10">

             <div class="input-group">
               <span class="input-group-btn">
                 <a id="lfm" data-input="thumbnail" data-preview="holder" class="btn btn-primary">
                   <i class="fa fa-picture-o"></i> Выбрать картинку
                 </a>
               </span>
               <input required id="thumbnail" class="form-control" type="text" name="filepath">
             </div>
             <img id="holder" style="margin-top:15px;max-height:100px;">

          </div> 
        </div>

        <div class="form-group">
          <label class="col-sm-2 control-label" for="title">Картинка поста маленькая (210x170px)</label>
          <div class="col-sm-10">

             <div class="input-group">
               <span class="input-group-btn">
                 <a id="post_image_sm" data-input="sm_thumbnail" data-preview="sm_holder" class="btn btn-primary">
                   <i class="fa fa-picture-o"></i> Выбрать картинку
                 </a>
               </span>
               <input required id="sm_thumbnail" class="form-control" type="text" name="sm_image">
             </div>
             <img id="sm_holder" style="margin-top:15px;max-height:100px;">

          </div> 
        </div>

        <div class="form-group">
            <label class="col-sm-2 control-label" for="title">Заголовок</label>
            <div class="col-sm-10">
                <input required type="text" class="form-control" id="title" name="title">
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-2 control-label" for="slug">Алиас</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="slug" name="slug">
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-2 control-label" for="tags">Категория</label>
            <div class="col-sm-10">
                <select multiple class="form-control" name="tags[]" id="tags">
                  @foreach($tags as $row_id => $row_name)
                    <option value="{{$row_id}}">{{ $row_name }}</option>
                  @endforeach
                </select>
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-2 control-label" for="excerpt">Отрывок</label>
            <div class="col-sm-10">
                <textarea id="excerpt" name="excerpt" class="ckeditor form-control"></textarea>
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-2 control-label" for="content">Контент</label>
            <div class="col-sm-10">
                <textarea id="content" name="post_content" class="ckeditor form-control"></textarea>
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-2 control-label">Статус</label>
            <div class="col-sm-10">
                <label class="gender"><input checked value="published" type="radio" name="post_status">   
                  Опубликовать
                </label> <br>
                <label class="gender"><input value="no_published" type="radio" name="post_status"> 
                  Пока не публиковать
                </label>
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-2 control-label">Тип</label>
            <div class="col-sm-10">
                <label class="gender"><input checked value="1" type="radio" name="post_or_page"> 
                  Статья в блоге
                </label> <br>
                <label class="gender"><input value="0" type="radio" name="post_or_page"> 
                  Страница отдельная
                </label>
            </div>
        </div>

        <input class="col-md-offset-2 btn btn-primary" type="submit" value="Сохранить">

      </form>

@endsection

@section('scripts')

<script src="{{ URL::to('admin/js/new_package/tinymce/js/tinymce/tinymce.min.js') }}"></script>
<script src="/vendor/laravel-filemanager/js/lfm.js"></script>

<script>
    var editor_config = {
        path_absolute: "/",
        selector: '#content',
        plugins: [
            "advlist autolink lists link image charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars code fullscreen",
            "insertdatetime media nonbreaking save table contextmenu directionality",
            "emoticons template paste textcolor colorpicker textpattern"
        ],
        toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
        relative_urls: false,
        file_browser_callback: function(field_name, url, type, win) {
          var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
          var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

          var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
          if (type == 'image') {
            cmsURL = cmsURL + "&type=Images";
          } else {
            cmsURL = cmsURL + "&type=Files";
          }

          tinyMCE.activeEditor.windowManager.open({
            file : cmsURL,
            title : 'Filemanager',
            width : x * 0.8,
            height : y * 0.8,
            resizable : "yes",
            close_previous : "no"
          });
        },

    };

    tinymce.init(editor_config);

    $('#lfm').filemanager('image');
    $('#post_image_sm').filemanager('image');
</script>

@endsection