@extends('layouts/app_sidebar')

@section('title_page')
  Заявка номер ({{ $request->id }}). Клиент {{ $request->client_name }}
@endsection

@section('side_bar')

@include('parts/user_menu')

@endsection

@section('content')

<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{ route('requests_index') }}">Все заявки</a></li>
    <li class="breadcrumb-item active" aria-current="page">Заявка номер ({{ $request->id }}). Клиент {{ $request->client_name }}</li>
  </ol>
</nav>

<div id="tutor_block" class="panel panel-default">
    <div class="panel-body">

@if($request_detail['status'] == 'accept' || $request_detail['status'] == 'see')

        <div style="margin-bottom:15px;" class="col-md-12 no_padding">

          @if($request_detail['status'] == 'accept')
            <div class="lets_go">
              <p >Свяжитесь как можно скорее с клиентом, уточните место и время первого занятия и начинайте обучение!</p>
            </div>
          @endif  

          <table class="table table-striped table-bordered">

              <tbody>
                  <tr>
                      <td>Номер заявки</td>
                      <td>{{ $request->id }}</td>
                  </tr>
                  <tr>
                    <td>Цена заказа</td>
                    <td>150грн (оплачиваете после первого проведенного занятия)</td>
                  </tr>  

                  @if($request_detail['status'] == 'accept')

                    <tr>
                        <td><strong>Контактная информация клиента</strong></td>
                        <td>
                          <strong>Телефон</strong>: {{ $request->phone }} <br>
                          <strong>Почта</strong>: {{ $request->email }} <br>
                          <strong>Имя</strong>: {{ $request->client_name }} <br>

                        </td>
                    </tr>

                  @endif

                  <tr>
                      <td>Детали заявки</td>
                      <td>
                        
                        <table class="table_in_table">
                          <tr>
                            <td>Предмет</td>
                            <td>{{ implode(", ", $request_detail['subjects']) }}</td>
                          </tr>
                          <tr>
                            <td>Место преподавания</td>
                            <td>{{ $request_detail['teach_place'] }}</td>
                          </tr>
                          @if($request_detail['pupil_adress'] != '')
                          <tr>
                            <td>Адрес ученика</td>
                            <td>{{ preg_replace('/[0-9]+/', '', $request_detail['pupil_adress']) }}</td>
                          </tr>
                          @endif
                          <tr>
                            <td>Возраст ученика</td>
                            <td>{{ $request_detail['age'] }}</td>
                          </tr>
                          <tr>
                            <td>Текущий уровень знаний</td>
                            <td>{{ $request_detail['level'] }}</td> 
                          </tr>
                          <tr>
                            <td>Продолжительность курса занятий</td>
                            <td>{{ $request_detail['course_length'] }}</td> 
                          </tr>
                          <tr>
                            <td>Количество занятий в неделю</td>
                            <td>{{ $request_detail['times_a_week'] }}</td> 
                          </tr> 
                          <tr>
                            <td>Цель занятий, уточнения</td>
                            <td>{{ $request_detail['message'] }}</td>
                          </tr> 
                          <tr>
                            <td>Стоимость занятия</td>
                            <td>Указанная вами в анкете (Уточняете напрямую с клиентом)</td>
                          </tr>
                        </table>
                      </td>
                  </tr>
              </tbody>
          </table>

        </div>

@elseif($request_detail['status'] == 'no_accept')

        <div style="margin-bottom:15px;" class="col-md-12 no_padding">

          <div class="lets_go">
            <p >Вы отклонили заявку</p>
          </div>

          <p>Мы будем вам благодарны если вы укажите причину, почему вам не подошла эта заявка. Выберите вариант или напишите свой. Спасибо</p>

          {!! Form::open(['route' => 'tutor_noassept_report']) !!}

          <div class="checkbox">
            <label><input name="option" type="checkbox" value="У меня достаточно учеников сейчас">У меня достаточно учеников сейчас</label>
          </div>
          <div class="checkbox">
            <label><input name="option" type="checkbox" value="Я больше не занимаюсь преподаванием">Я больше не занимаюсь преподаванием</label>
          </div>
          <div class="checkbox">
            <label><input name="option" type="checkbox" value="Цена заявки очень высокая">Цена заявки очень высокая</label>
          </div>

          <input type="hidden" name="requestid" value="{{ $request_detail['requestid'] }}">

          <textarea  id="message" placeholder="Напишите свою причину" name="review_text" class="form-control"></textarea>

          <button id="send_mail_button" type="submit" class="float-left btn btn-success">
            Отправить <i class="fa fa-paper-plane" aria-hidden="true"></i>
          </button>

          </form>

        </div>  


@endif        

    </div>
</div>

@endsection

@section('style')
<style>
  #tutor_block {
      margin-top: 0px;
  }
  .table_in_table tr{
    border-bottom: 1px solid #e7e7e7;
  }
  .table_in_table td{
    padding: 3px 0;
  }
  textarea.form-control{
    border-radius: 3px;
    max-height: 150px;
    margin-bottom: 15px;
  }
</style>
@endsection

@section('script')


  <script>

  </script>
  
@endsection

      

