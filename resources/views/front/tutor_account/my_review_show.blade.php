@extends('layouts/app_sidebar')

@section('title_page')
  Работа с учениками
@endsection

@section('side_bar')

@include('parts/user_menu')

@endsection

@section('content')

<div id="tutor_block" class="panel panel-default">
    <div class="panel-body">
{{-- dd($reviews) --}}

      <div id="second_title">
        <h2>Мои отзывы</h2>
      </div>

        <div class="col-md-12 no_padding">

          <table id="datatable" class="table table-striped table-bordered">
              <thead>
                  <tr>
                      <th>Отзыв</th>
                      <th>Дата</th>
                      <th>Автор</th>
                  </tr>
              </thead>
              <tbody>
                  @if (count($reviews))
                  @foreach($reviews as $review)
                  <tr class="one_review " >
                      <td>
                        @if($review->rating == 2)
                            <i style="color: #5BC963;" title="Отлично! Очень понравилось заниматься с учителем" class="fa fa-smile-o" aria-hidden="true"></i>
                        @elseif($review->rating == 1)
                            <i style="color: #d9534f;" title="Не понравилось! Отрицательная оценка" class="fa fa-frown-o" aria-hidden="true"></i>
                        @endif
                        {{$review->review }}
                      </td>
                      <td>
                        <div title="Дата отправки последнего сообщения" class="small_text">
                          {{ date('d.m.Y', $review->created_at->timestamp) }}
                        </div>
                      </td>
                      <td>
                          {{ $review->sender_name }} <br> {{ $review->sender_email }}
                      </td>
                  </tr>
                  @endforeach
                  @endif
              </tbody>
          </table>

        </div>

    </div>
</div>

@endsection

@section('style')
  <link href="{{asset('admin/css/dataTables.bootstrap.min.css')}}" rel="stylesheet">
@endsection

@section('script')

  <script src="{{asset('admin/js/jquery.dataTables.min.js')}}"></script>
  <script src="{{asset('admin/js/dataTables.bootstrap.min.js')}}"></script>

  <script>
    $(document).ready(function() {

      $('#datatable').dataTable({
        "order": [[2, 'desc']],
        "language": {
          "emptyTable": "<span style='line-height:22px'>Сдесь будут отображаться ваши отзывы от учеников. <br> <b>Вы также можете перенести на наш сайт ваши отзывы из других сайтов если они у вас есть.</b> <br> Сделать это можно на вашей @if( Auth::user()->user_teacher->active == '1') <a target='_blank' href='{{ route('tutor_page', ['slug' => Auth::user()->user_teacher->slug ]) }}'>странице репетитора</a> @else странице репетитора после заполения всех разделов из меню 'Редактирование профиля' @endif . Просто добавьте все ваши отзывы поочереди на сайт</span>",
        },
        "bLengthChange": false
      });

    });  
  </script>
  
@endsection

      

