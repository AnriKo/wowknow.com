@extends('layouts/app_sidebar')

@section('title_page')
  Редактирование профиля преподавателя 
  @if(Auth::user()->type == "user_teach" && Auth::user()->user_teacher->active == "1")
    <div id='my_profile_link'><a target="_blank" href='{{ route('tutor_page', ['slug' => Auth::user()->user_teacher->slug ]) }}'>Моя страница в поиске, посмотреть<i class='fa fa-external-link' aria-hidden='true'></i></a></div>
  @else
    <div class="danger_bd" id='my_profile_link'><a title="Вы еще не заполнили все необходимые разделы своего профиля" href='#'><i class="fa fa-exclamation-circle" aria-hidden="true"></i>Вашей анкеты еще нету в поиске</a></div>
  @endif
@endsection

@section('side_bar')

@include('parts/user_menu')

@endsection

@section('content')

<div id="tutor_block" class="panel panel-default">
    <div class="panel-body">

      <div id="second_title">
        <h2>Базовая информация преподавателя</h2>

        @if(Auth::user()->type == "user_teach" && Auth::user()->user_teacher->active == "1")
          <div title="Рассказать о своем профиле учителя в соц сети" id="share_my_profile" >
            <a target="_blank" href='{{ route('tutor_page', ['slug' => Auth::user()->user_teacher->slug ]) }}#share_profile'>
              Рассказать о своем профиле<i style="font-size: 90%" class="fa fa-share" aria-hidden="true"></i>
            </a>  
          </div>
        @endif

      </div>

        {!! Form::open(['route' => 'tutor_update_basic']) !!}

            <div class="col-md-12 no_padding">

            <div class="col-lg-12 col-md-12 col-ms-12 col-xs-12 one_form_block">
              <label for="tutor_name" class="control-label col-lg-3 col-md-3 col-sm-3">Адрес вашей страницы</label>
              <div class="col-md-9 col-lg-9 col-sm-9">

                @if($teacher->active == 0)
                  Ваша страница еще не активирована. Заполните все разделы из меню "Редактирование профиля"
                @else
                  <a target="_blank" href="http://wowknow.com/tutors/{{ $teacher->slug }}">wowknow.com/tutors/{{ $teacher->slug }}</a>
                @endif  

              </div>  
            </div>

            <div id="foto_content" class="col-lg-12 col-md-12 col-ms-12 col-xs-12 one_form_block">
              <label class="control-label col-lg-3 col-md-3 col-sm-3">Ваше фото<span class="require"></label></label>
              <div class="col-md-6 col-lg-5 col-sm-9">
                @if (!empty(Auth::user()->avatar))
                <div id="isset_image">
                  <img src="{{ URL::asset('storage/users/') }}/{{ Auth::user()->avatar }}" alt="exist-avatar">
                </div>
                @else
                <div id="isset_image">
                  <img src="{{ URL::asset('front/images/tutor_avatar_crop/default.jpg') }}" alt="default-avatar">
                </div>
                @endif
                <div class="image-editor">
                  <div class="cropit-preview"></div>
                  <div class="image-size-label">
                    <input type="range" class="cropit-image-zoom-input">
                  </div>
                  <input id="upload" type="file" class="cropit-image-input">
                  <input type="hidden" id="image_crop" name="image_crop">
                </div>
                <div>
                  <label id="label_for_upload_file" for="upload">
                    <span>Загрузить свое фото</span><i class="fa fa-upload" aria-hidden="true"></i>
                  </label>
                </div> 
              </div> 
            </div>

            <div class="col-lg-12 col-md-12 col-ms-12 col-xs-12 one_form_block">
              <label for="tutor_name" class="control-label col-lg-3 col-md-3 col-sm-3">Ваше имя<span class="require"></span></label>
              <div class="col-md-5 col-lg-5 col-sm-5">
                <input required id="tutor_name" value="{{ Auth::user()->name }}" name="name" type="text" class="form-control"/>
              </div>  
            </div>

            <div class="col-lg-12 col-md-12 col-ms-12 col-xs-12 one_form_block">
              <label class="control-label col-lg-3 col-md-3 col-sm-3">Ваша фамилия<span class="require"></span></label>
              <div class="col-md-5 col-lg-5 col-sm-5">
                <input placeholder="Введите вашу фамилию" required value="{{ $teacher->last_name }}" name="last_name" type="text" class="form-control"/>
              </div>  
            </div>

            <div class="col-lg-12 col-md-12 col-ms-12 col-xs-12 one_form_block">
              <label class="control-label col-lg-3 col-md-3 col-sm-3">Ваш E-mail<span class="require"></span></label>
              <div class="col-md-5 col-lg-5 col-sm-5">
                <input placeholder="Введите вашу електронную почту" required value="{{ Auth::user()->email }}" name="email" type="email" class="form-control"/>
              </div>  
            </div>

            <div class="col-lg-12 col-md-12 col-ms-12 col-xs-12 one_form_block">
              <label class="control-label col-lg-3 col-md-3 col-sm-3">Пол<span class="require"></span></label>
              <div class="col-md-5 col-lg-5 col-sm-5">
                <label class="gender"><input @if ($teacher->gender == 'man') checked @endif value="man" type="radio" name="gender">Мужской</label>
                <label class="gender"><input @if ($teacher->gender == 'woman') checked @endif value="woman" type="radio" name="gender">Женский</label>
              </div>
            </div>

            <div class="col-lg-12 col-md-12 col-ms-12 col-xs-12 one_form_block">
              <label class="control-label col-lg-3 col-md-3 col-sm-3">Страна проживания<span class="require"></span></label>
              <div class="col-md-5 col-lg-5 col-sm-5 col-xs-12">
                <select class="form-control js-example-basic-single" name="country_teach" id="country_live_id">
                  <option value="">Выбрать страну проживания</option>
                  @foreach($contries as $country)

                    <option @if($country->code == $teacher->country_teach) selected="selected" @endif value="{{ $country->code }}">{{ $country->name_ru }}</option>

                  @endforeach
                </select>
              </div>  
              <div class="tips col-lg-4 col-md-4 col-sm-4 col-xs-12">Выберите страну в которой вы проживаете</div>
            </div>

             <div class="col-lg-12 col-md-12 col-ms-12 col-xs-12 one_form_block">
              <label class="control-label col-lg-3 col-md-3 col-sm-3">Год рождения</label>
              <div class="col-md-5 col-lg-5 col-sm-5 col-xs-12">
                <select class="form-control" name="year_born" id="">
                  <option value="">Вибрать</option>
                  @foreach ( $data_user['years_born'] as $one_year)
                      <option @if ($teacher->year_born == $one_year) selected="selected" @endif value="{{ $one_year }}">{{ $one_year }}</option>
                  @endforeach
                </select>
              </div>
            </div>

            <div class="col-lg-12 col-md-12 col-ms-12 col-xs-12 one_form_block">
              <label class="control-label col-lg-3 col-md-3 col-sm-3">Номер телефона</label>
              <div class="col-md-5 col-lg-5 col-sm-5 col-xs-12">
               <input placeholder="Ваш номер телефона" value="{{ $teacher->phone }}" name="phone" type="text" class="form-control"/>
              </div> 
              <div class="tips col-lg-4 col-md-4 col-sm-4 col-xs-12">Мы не отправляем информационных sms сообщений. Ваш номер будет использоваться только для более быстрой связи учеников с вами.</div>
            </div>

        </div>

        <div id="save-tutor-info-wrap">
        
          <input id="save-tutor-info"  class="btn btn-success" value="Сохранить" type="submit">

        </div>

        {!! Form::close() !!}

    </div>
</div>



@endsection

@section('style')

{{ Html::style('front/select2/select2.css') }}

@endsection

@section('script')

{{ Html::script('front/select2/select2.full.js') }}
{{ Html::script('front/cropie_avatar/jquery.cropit.js') }}

<script type="text/javascript">

$(function() {

$(".js-example-basic-single").select2();

var ua = $('#country_live_id').find('option[value="ua"]');
var ru = $('#country_live_id').find('option[value="ru"]');
var by = $('#country_live_id').find('option[value="by"]');
var kz = $('#country_live_id').find('option[value="kz"]');
var first = $('#country_live_id').find('option[value=""]');
$('#country_live_id').prepend(kz).prepend(by).prepend(ru).prepend(ua).prepend(first);

$('.image-editor').cropit({
  imageState: {
    
  },
  imageBackground: true,
  imageBackgroundBorderWidth: 15, // Width of background border
  smallImage: 'stretch',
});

$('#save-tutor-info').click(function() {

  if ($('#upload').val() != '') {

    var image_crop = document.getElementById('image_crop');
    var imageData = $('.image-editor').cropit('export');
    image_crop.value = imageData;

  }
  
});


$('#upload').on('change', function(){
  if ($(this).val() != '') {
    $('#isset_image').hide();
    $('.image-editor').show();
    console.log('no empty');
  }
});

if ($('#isset_image img').attr("alt") == 'exist-avatar') {
  $('#label_for_upload_file span').text('Загрузить другое фото');
}


});

</script>

@endsection

      

