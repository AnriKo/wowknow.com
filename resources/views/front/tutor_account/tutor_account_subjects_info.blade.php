@extends('layouts/app_sidebar')

@section('title_page')
  Редактирование профиля преподавателя 
  @if(Auth::user()->type == "user_teach" && Auth::user()->user_teacher->active == "1")
    <div id='my_profile_link'><a target="_blank" href='{{ route('tutor_page', ['slug' => Auth::user()->user_teacher->slug ]) }}'>Моя страница в поиске, посмотреть <i class='fa fa-external-link' aria-hidden='true'></i></a></div>
  @else
    <div class="danger_bd" id='my_profile_link'><a title="Вы еще не заполнили все необходимые разделы своего профиля" href='#'><i class="fa fa-exclamation-circle" aria-hidden="true"></i>Вашей анкеты еще нету в поиске</a></div>
  @endif
@endsection

@section('side_bar')

@include('parts/user_menu')

@endsection

@section('content')

<div id="tutor_block" class="panel panel-default">
    <div class="panel-body">

      <div id="second_title">
        <h2>Редактирование предметов преподавателя</h2>
        
        @if(Auth::user()->type == "user_teach" && Auth::user()->user_teacher->active == "1")
          <div title="Рассказать о своем профиле учителя в соц сети" id="share_my_profile" >
            <a target="_blank" href='{{ route('tutor_page', ['slug' => Auth::user()->user_teacher->slug ]) }}#share_profile'>
              Рассказать о своем профиле<i style="font-size: 90%" class="fa fa-share" aria-hidden="true"></i>
            </a>  
          </div>
        @endif

        {!! Form::open(['route' => 'tutor_account_subjects_info_save']) !!}

        <div style="margin-bottom:15px;" class="col-md-12 no_padding">

           <p class="info_tips"><i class="fa fa-info-circle" aria-hidden="true"></i>Выберите предметы по которым вы проводите занятия, и поднаправления этих предметов </p>
            <div id="subjects_block" class="col-md-12 col-lg-12 col-sm-12 col-xs-12 no_padding">
            @if ($data_user['subject']->count() > 0)

              @foreach ( $data_user['subject'] as $one_subject)
                  
                <div class="subject_one_block col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 one_form_block">
                    <label class="control-label col-lg-2 col-md-12 col-sm-12 col-xs-12">Предмет<span class="require"></span></label>
                    <div class="col-md-12 col-lg-7 col-sm-12 col-xs-12">
                      <select name="subject_name[]" class="subject_name js-example-basic-multiple form-control" >
                        <option value="{{ $one_subject->subject_id }}">{{ $one_subject->name_ru }}</option>
                      </select>
                      <span class="delete_subject btn btn-default">
                        <i class="fa fa-trash-o" aria-hidden="true"></i>
                        Удалить предмет
                      </span>
                    </div>

                  </div>

                  @if($one_subject->sub_dir_all->count() > 0)

                    <div id="{{ $one_subject->subject_id }}_subject_id" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 one_form_block form-group-sub-dir">
                      <label id="{{ $one_subject->subject_direction }}" class="control-label col-lg-2 col-md-12 col-sm-12 col-xs-12">Выберите направления обучения по этому предмету<span class="require"></span></label>
                      <div class="checkbox_group col-md-12 col-lg-7 col-sm-12 col-xs-12">

                        @foreach($one_subject->sub_dir_all as $one_sub_dir)

                            <div class='checkbox subject_direction_block'>
                              <input
                                @foreach($one_subject->sub_dir_saved as $one_sub_dir_saved)
                                  {{ $one_sub_dir->id == $one_sub_dir_saved->subject_direction_id ? 'checked' : '' }}
                                @endforeach
                               class='css-checkbox subject_direction_name' name='subject_direction[{{ $one_sub_dir->subject_id }}][{{ $one_sub_dir->id }}]' id='chk_{{ $one_sub_dir->id }}' type='checkbox' value='{{ $one_sub_dir->id }}' />
                              <label class='css-label' for='chk_{{ $one_sub_dir->id }}'>{{ $one_sub_dir->name_ru }}</label>
                              <div class='form-group'>
                                <textarea placeholder='Опишите подробнее, детали преподавания, ваш опыт' oninput='SuperDuperFunction();' class='subject_direction_detail form-control' name='subject_direction_exp[{{ $one_sub_dir->id }}]' cols='30' rows='2'><?php 
                                  foreach($one_subject->sub_dir_saved as $one_sub_dir_saved) { 
                                    if($one_sub_dir->id == $one_sub_dir_saved->subject_direction_id) { 
                                      echo $one_sub_dir_saved->subject_direction_exp;
                                    } 
                                  } ?></textarea>
                              </div>
                            </div>
                          
                        @endforeach

                      </div>
                      <div class="tips">В этом предмете есть более расширенные направления преподавания, выберите те направления которые вы преподаете, и опишите ваш опыт преподавания этих направлений</div>
                    </div>
                  @endif

                </div>         

                @endforeach

            @else

            <div class="subject_one_block col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 one_form_block">
                <label class="control-label col-lg-2 col-md-12 col-sm-12 col-xs-12">
                  Предмет<span class="require"></span>
                </label>
                <div class="col-md-12 col-lg-7 col-sm-12 col-xs-12">
                  <select name="subject_name[]" class="subject_name js-example-basic-multiple form-control" ></select><span class="delete_subject btn btn-default"><i class="fa fa-trash-o" aria-hidden="true"></i>Удалить предмет</span>
                </div>
              </div>
            </div>

            @endif

            </div>
  
            <div id="add_subject_block" class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
              <span id="add_subject" class="btn btn-primary">
                <i class="fa fa-plus-circle" aria-hidden="true"></i>
                  Добавить новый предмет в свой профиль
              </span>
            </div>

            <div id="create_new_subject_wrap_block" class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
              <p id="do_not_find_subject"><i class="fa fa-info-circle" aria-hidden="true"></i>Не нашли нужного вам предмета в базе сайта? <span>нажмите сюда</span></p>
              <div  id="create_new_subject_block">

                <div id="create_new_subject_title">Если не удалось найти предметов которые вы преподаете в поиске, тогда можете добавить нужный вам предмет в этом поле и через некоторое время (после проверки) он сразу отобразиться в поиске</div>
                <div class="form-group">
                  <label class="control-label">Добавить новый предмет в&nbsp;базу сайта</label>
                  <input class="form-control" type="text" name="create_new_subject" id="create_new_subject">
                  <span id="create_new_subject_button" class="btn btn-default">Создать новый предмет</span>
                  <div id="sucess_create_new_subject"></div>
                </div>

              </div>
            </div>
          
        </div>

        <div id="save-tutor-info-wrap">
          <input id="save-tutor-info"  class="btn btn-success" value="Сохранить" type="submit">
        </div>

        {!! Form::close() !!}

    </div>
</div>

@endsection

@section('style')

{{ Html::style('front/select2/select2.css') }}

@endsection

@section('script')

{{ Html::script('front/select2/select2.full.js') }}

<script type="text/javascript">

$(function() {

//вставка предметов
$('.subject_name').select2({
    tags: false,
    placeholder: "Выберите предмет из списка",
    minimumInputLength: 1,
    //selecting: foo(),
    ajax: {
        url: '{{ url('/') }}/subject-search-name-for-id',
        delay: 100,
        dataType: 'json',
        data: function (params) {
            var query = {
                subject: $.trim(params.term)
            };
            return query;
        },
        processResults: function (data) {
            return {
                results: data
            };
        },
        cache: true
    }
});

$('.subject_name').on('select2:select', function (evt) {
    var subject_name = $('.subject_name');
});

//добавить новый предмет
var add_subject = $('#add_subject');
add_subject.on('click', function() {
  $("#subjects_block").append('<div class="subject_one_block col-lg-12 col-md-12 col-sm-12 col-xs-12"><div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 one_form_block"><label class="control-label col-lg-2 col-md-12 col-sm-12 col-xs-12">Предмет<span class="require"></span></label><div class="col-md-12 col-lg-7 col-sm-12 col-xs-12"><select name="subject_name[]" class="subject_name js-example-basic-multiple form-control" ></select><span class="delete_subject btn btn-default"><i class="fa fa-trash-o" aria-hidden="true"></i>Удалить предмет</span></div></div></div>');

  //вставка предметов
  $('.subject_name').select2({
      tags: false,
      placeholder: "Выберите предмет из списка",
      minimumInputLength: 1,
      ajax: {
          url: '{{ url('/') }}/subject-search-name-for-id',
          delay: 100,
          dataType: 'json',
          data: function (params) {
              var query = {
                  subject: $.trim(params.term)
              };
              return query;
          },
          processResults: function (data) {
              return {
                  results: data
              };
          },
          cache: true
      }
  });

  //удалить новый предмет
  var delete_subject = $('.delete_subject');
  delete_subject.on('click', function() {

    var subject_one_block = $('.subject_one_block').length;
    if(subject_one_block > 1){
      var x = $(this).closest('.subject_one_block').remove();
    }else{
      $(this).attr( "title", "У вас должен быть хотябы один предмет, последний предмет удалить нельзя" )
    }
    
  });

  //вывод поднаправлений предмета преподавания для добавленных предметов
  $('.subject_name').on('select2:select', function (evt) {
      show_sub(this);
      change_sub_to_main_subject(this);
  });

});

//вывод поднаправлений предмета преподавания для предмета вначале формы по умолчанию
$('.subject_name').on('select2:select', function (evt) {
  show_sub(this);
  change_sub_to_main_subject(this);
});

function change_sub_to_main_subject(var_this){

  var subject_id = $(var_this).val();
  $.ajax({
    type: "GET",
    url: "{{ route('change_sub_to_main_subject') }}",
    data: {subject_id:subject_id},
    success: function(data){
      console.log('Hello change_sub_to_main_subject || ' + data['name']);
        if (data != '0') {
          var option = new Option(data['name'], data['id'], true, true);
          $(var_this).append(option).trigger('change');
        }
      }
    });
}

function show_sub(var_this){

  var subject_id = $(var_this).val();
  var $this = $(var_this).closest('.subject_one_block');
  $.ajax({
    type: "GET",
    url: "{{ route('sub_dir') }}",
    data: {subject_id:subject_id},
    success: function(data){
      //console.log('Hello');
        if (data != '0') {
          $this.find('.form-group-sub-dir').remove();
          $this.append('<div id="'+subject_id+'_subject_id" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 one_form_block form-group-sub-dir"><label class="control-label col-lg-2 col-md-12 col-sm-12 col-xs-12">Выберите направления обучения по этому предмету<span class="require"></span></label><div class="checkbox_group col-md-12 col-lg-7 col-sm-12 col-xs-12"></div></div>');
          var sub_dir_checkbox = $this.find('.checkbox_group');
          $(data).each( function(i, elem) {
              sub_dir_checkbox.append( "<div class='checkbox subject_direction_block'><input class='css-checkbox subject_direction_name' name='subject_direction["+subject_id+"][" + elem.id + "]' id='chk_" + elem.id + "' type='checkbox' value='_' /><label class='css-label' for='chk_" + elem.id + "'>" + elem.name_ru + "</label><div class='form-group'><textarea placeholder='Опишите подробнее, детали преподавания ваш опыт' oninput='SuperDuperFunction();' class='subject_direction_detail form-control' name='subject_direction_exp[" + elem.id + "]' id='' cols='30' rows='2'></textarea></div></div>" );
            }
          );
          $this.find('#'+subject_id+'_subject_id').append('<div class="tips">В этом предмете есть более расширенные направления преподавания, выберите те направления которые вы преподаете, и опишите ваш опыт преподавания этих направлений</div>');
        }
      }
    });
}

//удалить новый предмет
var delete_subject = $('.delete_subject');
delete_subject.on('click', function() {

  var subject_one_block = $('.subject_one_block').length;
  if(subject_one_block > 1){
    var x = $(this).closest('.subject_one_block').remove();
  }else{
    $(this).attr( "title", "У вас должен быть хотябы один предмет, последний предмет удалить нельзя" )
  }
    
});

//открытие блока по добавлению нового предмета
var do_not_find_subject = $('#do_not_find_subject');
var create_new_subject_block = $('#create_new_subject_block');
do_not_find_subject.on('click', function() {
  create_new_subject_block.toggle("slow", function(){
    if (create_new_subject_block.is(':visible')) {
      do_not_find_subject.children('span').text('Скрыть');
    }else{
      do_not_find_subject.children('span').text('Нажмите сюда');
    }
  });

});

//создаем новый предмет
  $('#create_new_subject_button').click(function(){
      if ($('#create_new_subject').val() != '') {
        $.ajax({
            type: "GET",
            url: "{{ route('create_new_subject') }}",
            data: {subject:$('#create_new_subject').val()},
            success: function(data) {
                $('#sucess_create_new_subject').append(data);
            }
        });
      }
  });



});

</script>

@endsection

      

