@extends('layouts/app_sidebar')

@section('title_page')
  Заявки от клиентов
@endsection

@section('side_bar')

@include('parts/user_menu')

@endsection

@section('content')

<div id="tutor_block" class="panel panel-default">
    <div class="panel-body">

        <div style="margin-bottom:15px;" class="col-md-12 no_padding">

          <table id="datatable" class="table table-striped table-bordered">
              <thead>
                  <tr>
                      <th>Номер</th>
                      <th>Статус</th>
                      <th>Клиент</th>
                      <th>Детали</th>
                      <th>Дата</th>
                      <th>Действия</th>
                  </tr>
              </thead>
              <tfoot>
                  <tr>
                      <th>Номер</th>
                      <th>Статус</th>
                      <th>Клиент</th>
                      <th>Детали</th>
                      <th>Дата</th>
                      <th>Действия</th>
                  </tr>
              </tfoot>
              <tbody>
                  @if (count($requests))
                  @foreach($requests as $request)
                  <tr style="{{ $request->status == 'sent'? 'background-color: #ffe3dd' : '' }}{{ $request->status == 'accept'? 'background-color: #ddffde' : '' }}">
                      <td>
                        {{ $request->id }}
                      </td>
                      <td>
                        @if($request->status == 'sent')
                          Ожидает ответа!
                        @elseif($request->status == 'accept')
                          Заявка принята
                        @elseif($request->status == 'no_accept')
                          Заявка отклонена
                        @endif
                      </td>
                      <td>{{$request->client_name }}</td>
                      <td>Возраст ученика:{{ $request->age }} <br> {{ $request->message }}</td>
                      <td>{{ date('d.m.Y - H:i', $request->created_at->timestamp) }}</td>
                      <td>
                          <a style="width: 100%;" href="{{ route('requests_one', [ 'requestid' =>  $request->id ]) }}?status=see" class="btn btn-info btn-xs">Посмотреть заявку</a>
                          <a style="width: 100%;" href="{{ route('requests_one', [ 'requestid' =>  $request->id ]) }}?id={{ Auth()->user()->id }}&status=accept" class="btn btn-success btn-xs">Принять заявку</a>
                          <a style="width: 100%;" href="{{ route('requests_one', [ 'requestid' =>  $request->id ]) }}?id={{ Auth()->user()->id }}&status=no_accept" class="btn btn-danger btn-xs">Отклонить заявку</a>
                      </td>
                  </tr>
                  @endforeach
                  @endif
              </tbody>
          </table>

        </div>

    </div>
</div>

@endsection

@section('style')
  <link href="{{asset('admin/css/dataTables.bootstrap.min.css')}}" rel="stylesheet">
  <style>
      .btn {
      padding: 2px 13px;
      margin-bottom: 2px;
        }
      #tutor_block {
          margin-top: 0px;
      }
  </style>
@endsection

@section('script')

  <script src="{{asset('admin/js/jquery.dataTables.min.js')}}"></script>
  <script src="{{asset('admin/js/dataTables.bootstrap.min.js')}}"></script>

  <script>
    $(document).ready(function() {

      $('#datatable').dataTable();

    });  
  </script>
  
@endsection

      

