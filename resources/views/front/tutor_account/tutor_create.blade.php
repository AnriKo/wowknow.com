@extends('layouts/app_sidebar')

@section('title_page')
  Создание профиля преподавателя
@endsection

@section('side_bar')

@include('parts/teacher_start_menu')

@endsection

@section('content')

<div id="tutor_block" class="panel panel-default">
    <div class="panel-body">

      <div id="second_title">
        <h2>Базовая информация преподавателя</h2>
      </div>

        {!! Form::open(['route' => 'tutor_save_basic']) !!}

        <div id="foto_content" class="col-lg-12 col-md-12 col-ms-12 col-xs-12 one_form_block">
          <label class="control-label col-lg-3 col-md-3 col-sm-3">Ваше фото<span class="require"></label>
          <div class="col-md-6 col-lg-5 col-sm-9">
            <div id="isset_image">
              <img src="{{ URL::asset('front/images/tutor_avatar_crop/default.jpg') }}" alt="default-avatar">
            </div>
            <div class="image-editor">
              <div class="cropit-preview"></div>
              <div class="image-size-label">
                <input  type="range" class="cropit-image-zoom-input">
              </div>
              <input id="upload" type="file" class="cropit-image-input">
              <input type="hidden" id="image_crop" name="image_crop">
            </div>
            <div {{-- class="col-lg-4 col-md-4 col-sm-5 col-xs-12"  --}}>
              <label data-tooltip="Выберите свое фото"  id="label_for_upload_file" for="upload">
                <span>Загрузить свое фото</span><i class="fa fa-upload" aria-hidden="true"></i>
              </label>
            </div> 
          </div> 
        </div>

        <div class="col-md-12 no_padding">

            <div class="col-lg-12 col-md-12 col-ms-12 col-xs-12 one_form_block">
              <label for="tutor_name" class="control-label col-lg-3 col-md-3 col-sm-3">Ваше имя<span class="require"></span></label>
              <div class="col-md-5 col-lg-5 col-sm-5">
                <input required id="tutor_name" name="name" type="text" class="form-control" value="{{ Auth::user()->name }}"/>
              </div>  
            </div>

            <div class="col-lg-12 col-md-12 col-ms-12 col-xs-12 one_form_block">
              <label class="control-label col-lg-3 col-md-3 col-sm-3">Ваша фамилия<span class="require"></span></label>
              <div class="col-md-5 col-lg-5 col-sm-5">
                <input placeholder="Введите вашу фамилию" required name="last_name" type="text" class="form-control"/>
              </div>  
            </div>

            <div class="col-lg-12 col-md-12 col-ms-12 col-xs-12 one_form_block">
              <label class="control-label col-lg-3 col-md-3 col-sm-3">Ваш E-mail<span class="require"></span></label>
              <div class="col-md-5 col-lg-5 col-sm-5">
                <input placeholder="Введите вашу електронную почту" required value="{{ Auth::user()->email }}" name="email" type="email" class="form-control"/>
              </div>  
            </div>

            <div class="col-lg-12 col-md-12 col-ms-12 col-xs-12 one_form_block">
              <label class="control-label col-lg-3 col-md-3 col-sm-3">Пол<span class="require"></span></label>
              <div class="col-md-5 col-lg-5 col-sm-5">
                <label class="gender"><input value="man" type="radio" name="gender" required>Мужской</label>
                <label class="gender"><input value="woman" type="radio" name="gender">Женский</label>
              </div>
            </div>

            <div class="col-lg-12 col-md-12 col-ms-12 col-xs-12 one_form_block">
              <label class="control-label col-lg-3 col-md-3 col-sm-3">Страна проживания<span class="require"></span></label>
              <div class="col-md-5 col-lg-5 col-sm-5 col-xs-12">
                <select required class="form-control js-example-basic-single" name="country_teach" id="country_live_id">
                  <option value="">Выбрать страну проживания</option>
                  @foreach($contries as $key => $country)
                    <option value="{{ $country->code }}">{{ $country->name_ru }}</option>
                  @endforeach
                </select>
              </div>  
              <div class="tips col-lg-4 col-md-4 col-sm-4 col-xs-12">Выберите страну в которой вы проживаете</div>
            </div>

            <div class="col-lg-12 col-md-12 col-ms-12 col-xs-12 one_form_block">
              <label class="control-label col-lg-3 col-md-3 col-sm-3">Год рождения</label>
              <div class="col-md-5 col-lg-5 col-sm-5 col-xs-12">
                <select class="form-control" name="year_born" id="year_born">
                  <option value="">Вибрать</option>
                  @foreach ( $data_user['years_born'] as $one_year)
                      <option value="{{ $one_year }}">{{ $one_year }}</option>
                  @endforeach
                </select>
              </div>
            </div>

            <div class="col-lg-12 col-md-12 col-ms-12 col-xs-12 one_form_block">
              <label class="control-label col-lg-3 col-md-3 col-sm-3">Номер телефона</label>
              <div class="col-md-5 col-lg-5 col-sm-5 col-xs-12">
               <input placeholder="Ваш номер телефона" name="phone" type="text" class="form-control"/>
              </div> 
              <div class="tips col-lg-4 col-md-4 col-sm-4 col-xs-12">Мы не отправляем информационных sms сообщений. Ваш номер будет использоваться только для более быстрой связи учеников с вами.</div>
            </div>

        </div>

        <div id="save-tutor-info-wrap">
        
          <input id="save-tutor-info"  class="btn btn-success" value="Сохранить" type="submit">

        </div>

        {!! Form::close() !!}
        
    </div>
</div>

<!-- Modal explain why dont use any links-->
<div class="modal fade" id="why_dont_use_link" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <div class="modal-title" id="exampleModalLabel"><i class="fa fa-exclamation-circle" aria-hidden="true"></i><b>Сначала заполните эту страницу потом будут доступны другие разделы!</b></div>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Всего для создания профиля репетитора вам нужно заполнить и сохранить 4 части вашего профиля.
        Все они находяться в блоке меню "Редактирование профиля" и называються: <br> 
        <b>
        Базовая информация <br>
        Основная информация <br>
        Предметы преподавания <br>
        Образование, Интересы <br>
        </b>
        Начать нужно с этой страницы <b>"Базовая информация преподавателя".</b> После этого будут доступны другие разделы
      </div>
    </div>
  </div>
</div>

@endsection

@section('style')

{{ Html::style('front/select2/select2.css') }}

@endsection

@section('script')

{{ Html::script('front/select2/select2.full.js') }}
{{ Html::script('front/cropie_avatar/jquery.cropit.js') }}

<script type="text/javascript">

$(function() {

  //функция подсказки создания
function showTip(current) {
    console.log(current);
    var tipText = current.attr('data-tooltip');
    var element = document.createElement('div');
    element.className = 'my_tooltip';
    element.innerHTML = tipText;
    $(current).append(element);
    $(element).delay( 5000 ).fadeOut('slow');
}

  $(".js-example-basic-single").select2();
  $("#year_born").select2();

  var ua = $('#country_live_id').find('option[value="ua"]');
  var ru = $('#country_live_id').find('option[value="ru"]');
  var by = $('#country_live_id').find('option[value="by"]');
  var kz = $('#country_live_id').find('option[value="kz"]');
  var first = $('#country_live_id').find('option[value=""]');
  $('#country_live_id').prepend(kz).prepend(by).prepend(ru).prepend(ua).prepend(first);

  $('.image-editor').cropit({
    imageState: {
      
    },
    imageBackground: true,
    imageBackgroundBorderWidth: 15, // Width of background border
    smallImage: 'stretch',
  });

  $('#save-tutor-info').click(function(event) {

    if ($('#upload').val() != '') {

      var image_crop = document.getElementById('image_crop');
      var imageData = $('.image-editor').cropit('export');
      image_crop.value = imageData;

    }else{

      var label_for_upload_file = $('#label_for_upload_file').css('border', '2px solid #e74c3c');
      showTip(label_for_upload_file);
      setTimeout( function(){
        label_for_upload_file.css('border','none');
      },5000);
      $('html, body').animate({
          scrollTop: label_for_upload_file.offset().top-200
      }, 1000);
      event.preventDefault();
      console.log( 'Please, check image' );

    }
    
  });

  $('#upload').on('change', function(){
    if ($(this).val() != '') {
      $('#isset_image').hide();
      $('.image-editor').show();
      console.log('no empty');
    }
  });

  if ($('#isset_image img').attr("alt") == 'exist-avatar') {
    $('#label_for_upload_file span').text('Загрузить другое фото');
  }

  $('.prevent_default_link').on('click', function(e){
    e.preventDefault();
    $('#why_dont_use_link').modal('show');
  });

});

</script>

@endsection

      

