@extends('layouts/app_sidebar')

@section('title_page')
  Редактирование профиля преподавателя 
  @if(Auth::user()->type == "user_teach" && Auth::user()->user_teacher->active == "1")
    <div id='my_profile_link'><a target="_blank" href='{{ route('tutor_page', ['slug' => Auth::user()->user_teacher->slug ]) }}'>Моя страница в поиске, посмотреть <i class='fa fa-external-link' aria-hidden='true'></i></a></div>
  @else
    <div class="danger_bd" id='my_profile_link'><a title="Вы еще не заполнили все необходимые разделы своего профиля" href='#'><i class="fa fa-exclamation-circle" aria-hidden="true"></i>Вашей анкеты еще нету в поиске</a></div>
  @endif
@endsection

@section('side_bar')

@include('parts/user_menu')

@endsection

@section('content')

<div id="tutor_block" class="panel panel-default">
    <div class="panel-body">


      <div id="second_title">
        <h2>Мое образование, хобби</h2>

        @if(Auth::user()->type == "user_teach" && Auth::user()->user_teacher->active == "1")
          <div title="Рассказать о своем профиле учителя в соц сети" id="share_my_profile" >
            <a target="_blank" href='{{ route('tutor_page', ['slug' => Auth::user()->user_teacher->slug ]) }}#share_profile'>
              Рассказать о своем профиле<i style="font-size: 90%" class="fa fa-share" aria-hidden="true"></i>
            </a>  
          </div>
        @endif

        {!! Form::open(['route' => 'tutor_account_about_me_info_save']) !!}

        <div style="margin-bottom:15px;" class="col-lg-12 col-md-12 col-ms-12 col-xs-12 no_padding">

           <p class="info_tips"><i class="fa fa-info-circle" aria-hidden="true"></i>Заполните ваше образование. <br> Если вы еще учитесь укажите год поступления и приблизительный год окончания вашего вуза.</p>

            <div id="subjects_block" class="col-lg-12 col-md-12 col-ms-12 col-xs-12 no_padding">
            @if ($data_user['education']->count() > 0)

                @foreach ( $data_user['education'] as $one_education)
                  
                <div class="subject_one_block col-lg-12 col-md-12 col-ms-12 col-xs-12">

                  <div class="col-lg-12 col-md-12 col-ms-12 col-xs-12 one_form_block">
                    <label class="control-label col-lg-2 col-md-2 col-sm-12 col-xs-12">Заведение<span class="require"></span></label>
                    <div class="col-md-10 col-lg-10 col-sm-12 col-xs-12">
                      <input value="{{ $one_education->name_institut }}" name="name_institut[]" type="text" class="form-control"/>
                    </div>
                  </div>

                  <div class="col-lg-12 col-md-12 col-ms-12 col-xs-12 one_form_block">
                    <label class="control-label col-lg-2 col-md-2 col-sm-12 col-xs-12">Степень, специальность<span class="require"></span></label>
                    <div class="col-md-10 col-lg-10 col-sm-12 col-xs-12">
                      <input value="{{ $one_education->degree }}" name="degree[]" type="text" class="form-control"/>
                    </div>  
                  </div>

                  <div class="time_edu_block col-lg-12 col-md-12 col-ms-12 col-xs-12 one_form_block">
                    <label class="control-label col-lg-2 col-md-2 col-sm-12 col-xs-12">Время учебы<span class="require"></span></label>
                    <div class="col-md-10 col-lg-10 col-sm-12 col-xs-12">
                      <select class="form-control" name="year_enter_edu[]" id="">
                        <option value="">Год начала</option>
                        @foreach ( $data_user['years_study_begin'] as $one_year)
                            <option @if ($one_education->year_enter_edu == $one_year) selected="selected" @endif value="{{ $one_year }}">{{ $one_year }}</option>
                        @endforeach
                      </select>
                      <select class="form-control" name="year_finish_edu[]" id="">
                        <option value="">Год окончания</option>
                        @foreach ( $data_user['years_study_end'] as $one_year)
                            <option @if ($one_education->year_finish_edu == $one_year) selected="selected" @endif value="{{ $one_year }}">{{ $one_year }}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>

                  <div class="col-lg-12 col-md-12 col-ms-12 col-xs-12 one_form_block">
                    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                       <span class="delete_subject btn btn-default">
                          <i class="fa fa-trash-o" aria-hidden="true"></i>
                          Удалить место учебы
                        </span>
                     </div>
                  </div> 

                </div>         

                @endforeach

            @else

                <div class="subject_one_block col-lg-12 col-md-12 col-ms-12 col-xs-12">

                  <div class="col-lg-12 col-md-12 col-ms-12 col-xs-12 one_form_block">
                    <label class="control-label col-lg-2 col-md-2 col-sm-12 col-xs-12">Заведение<span class="require"></span></label>
                    <div class="col-md-10 col-lg-10 col-sm-12 col-xs-12">
                      <input required value="" name="name_institut[]" type="text" class="form-control"/>
                    </div>
                  </div>

                  <div class="col-lg-12 col-md-12 col-ms-12 col-xs-12 one_form_block">
                    <label class="control-label col-lg-2 col-md-2 col-sm-12 col-xs-12">Степень, специальность<span class="require"></span></label>
                    <div class="col-md-10 col-lg-10 col-sm-12 col-xs-12">
                      <input required value="" name="degree[]" type="text" class="form-control"/>
                    </div>  
                  </div>

                  <div class="time_edu_block col-lg-12 col-md-12 col-ms-12 col-xs-12 one_form_block">
                    <label class="control-label col-lg-2 col-md-2 col-sm-12 col-xs-12">Время учебы<span class="require"></span></label>
                    <div class="col-md-10 col-lg-10 col-sm-12 col-xs-12">
                      <select required class="form-control" name="year_enter_edu[]" id="">
                        <option value="">Год начала</option>
                        @foreach ( $data_user['years_study_begin'] as $one_year)
                            <option value="{{ $one_year }}">{{ $one_year }}</option>
                        @endforeach
                      </select>
                      <select required class="form-control" name="year_finish_edu[]" id="">
                        <option value="">Год окончания</option>
                        @foreach ( $data_user['years_study_end'] as $one_year)
                            <option value="{{ $one_year }}">{{ $one_year }}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>

                  <div class="col-lg-12 col-md-12 col-ms-12 col-xs-12 one_form_block">
                    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                       <span class="delete_subject btn btn-default">
                          <i class="fa fa-trash-o" aria-hidden="true"></i>
                          Удалить место учебы
                        </span>
                     </div>
                  </div>   

                </div>   

            @endif

            </div>
  
            <div id="add_subject_block" class="col-lg-12 col-md-12 col-ms-12 col-xs-12">
              <span id="add_subject" class="btn btn-default">
                <i class="fa fa-plus-circle" aria-hidden="true"></i>
                  Добавить место учебы
              </span>
            </div>

            <div id="tutor_hobie_text" class="col-lg-12 col-md-12 col-ms-12 col-xs-12 one_form_block">
              <label class="control-label col-lg-2 col-md-12 col-sm-12 col-xs-12">Ваши хобби, интересы <span style="display: inline-block; font-size: 85%;" class="info_tips">( +20 пунктов к рейтингу )</span></label>
              <div class="col-md-12 col-lg-8 col-sm-12 col-xs-12">
                <textarea class="form-control" name="tutor_hobie_text" id="" cols="40" rows="20">{{ $teacher->tutor_hobie_text }}</textarea>
              </div>  
              <div class="tips col-lg-2 col-md-12 col-sm-12 col-xs-12">Клиенты могут использовать этот раздел, чтобы лучше узнать вас, почувствовать вашу личность и посмотреть, хорошо ли вы будете соответствовать их сыну или дочери.</div>
            </div>
        
        </div>

        <div id="save-tutor-info-wrap">
          <input id="save-tutor-info"  class="btn btn-success" value="Сохранить" type="submit">
        </div>

        {!! Form::close() !!}

    </div>
</div>

@endsection

@section('style')

{{ Html::style('front/select2/select2.css') }}

@endsection

@section('script')

{{ Html::script('front/select2/select2.full.js') }}

<script type="text/javascript">

$(function() {

//добавить новое место учебы
var add_subject = $('#add_subject');
add_subject.on('click', function() {
  $("#subjects_block").append('<div class="subject_one_block col-lg-12 col-md-12 col-ms-12 col-xs-12"><div class="col-lg-12 col-md-12 col-ms-12 col-xs-12 one_form_block"><label class="control-label col-lg-2 col-md-2 col-sm-12 col-xs-12">Заведение<span class="require"></span></label><div class="col-md-10 col-lg-10 col-sm-12 col-xs-12"><input value="" name="name_institut[]" type="text" class="form-control"/></div></div><div class="col-lg-12 col-md-12 col-ms-12 col-xs-12 one_form_block"><label class="control-label col-lg-2 col-md-2 col-sm-12 col-xs-12">Степень, специальность<span class="require"></span></label><div class="col-md-10 col-lg-10 col-sm-12 col-xs-12"><input value="" name="degree[]" type="text" class="form-control"/></div></div><div class="time_edu_block col-lg-12 col-md-12 col-ms-12 col-xs-12 one_form_block"><label class="control-label col-lg-2 col-md-2 col-sm-12 col-xs-12">Время учебы<span class="require"></span></label><div class="col-md-10 col-lg-10 col-sm-12 col-xs-12"><select class="form-control" name="year_enter_edu[]" id=""><option value="">Год начала</option>@foreach ( $data_user['years_study_begin'] as $one_year)<option value="{{ $one_year }}">{{ $one_year }}</option>@endforeach</select><select class="form-control" name="year_finish_edu[]" id=""><option value="">Год окончания</option>@foreach ( $data_user['years_study_end'] as $one_year)<option value="{{ $one_year }}">{{ $one_year }}</option>@endforeach</select></div></div><div class="col-lg-12 col-md-12 col-ms-12 col-xs-12 one_form_block"><div class="col-md-12 col-lg-12 col-sm-12 col-xs-12"><span class="delete_subject btn btn-default"><i class="fa fa-trash-o" aria-hidden="true"></i>Удалить место учебы</span></div></div></div>');

  //удалить новый предмет только что добавленный только на странцице
  var delete_subject = $('.delete_subject');
  delete_subject.on('click', function() {
    var x = $(this).closest('.subject_one_block').remove();
    console.log(x);
  });

});

//удалить новый предмет уже сохраненный в базе
var delete_subject = $('.delete_subject');
delete_subject.on('click', function() {
  var subjects = $('.subject_one_block');
  console.log('b-'+subjects.length);
  if(subjects.length > 1){
    var x = $(this).closest('.subject_one_block').remove();
  }

});

});


</script>


@endsection

      

