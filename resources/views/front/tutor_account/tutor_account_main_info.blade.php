@extends('layouts/app_sidebar')

@section('title_page')
  Редактирование профиля преподавателя 
  @if(Auth::user()->type == "user_teach" && Auth::user()->user_teacher->active == "1")
    <div id='my_profile_link'><a target="_blank" href='{{ route('tutor_page', ['slug' => Auth::user()->user_teacher->slug ]) }}'>Моя страница в поиске, посмотреть <i class='fa fa-external-link' aria-hidden='true'></i></a></div>
  @else
    <div class="danger_bd" id='my_profile_link'><a title="Вы еще не заполнили все необходимые разделы своего профиля" href='#'><i class="fa fa-exclamation-circle" aria-hidden="true"></i>Вашей анкеты еще нету в поиске</a></div>
  @endif
@endsection

@section('side_bar')

@include('parts/user_menu')

@endsection

@section('content')

<div id="tutor_block" class="panel panel-default">
    <div class="panel-body">

      <div id="second_title">
        <h2>Основная информация преподавателя</h2>

        @if(Auth::user()->type == "user_teach" && Auth::user()->user_teacher->active == "1")
          <div title="Рассказать о своем профиле учителя в соц сети" id="share_my_profile" >
            <a target="_blank" href='{{ route('tutor_page', ['slug' => Auth::user()->user_teacher->slug ]) }}#share_profile'>
              Рассказать о своем профиле<i style="font-size: 90%" class="fa fa-share" aria-hidden="true"></i>
            </a>  
          </div>
        @endif
        
      </div>

        {!! Form::open(['route' => 'tutor_account_main_info_save']) !!}

        <div class="col-md-12 no_padding">

          <div style="padding-right: 14px;" id="title-block" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 one_form_block">
            <label class="control-label col-lg-2 col-md-12 col-sm-12 col-xs-12">Заголовок профиля<span class="require"></span></label>
            <div class="col-md-12 col-lg-7 col-sm-12 col-xs-12">
              <input placeholder="Введите заголовок ващего профиля" required value="{{ $teacher->profile_title }}" name="profile_title" type="text" class="form-control"/>
            </div>  
            <div class="tips col-lg-3 col-md-12 col-sm-12 col-xs-12">Заговолок для вашего профиля, например: "Опытный репетитор английского языка для детей".</div>
          </div>

          <div style="padding-right: 14px;" id="tutor_education_text" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 one_form_block">
            <label class="control-label col-lg-2 col-md-12 col-sm-12 col-xs-12">О вас, вашем образовании, опыте, методике преподавания<span class="require"></span><span class="little_grey">минимум 150 знаков</span></label>
            <div class="col-md-12 col-lg-7 col-sm-12 col-xs-12">
              <textarea minlength="150" required class="form-control" name="tutor_edu_text" id="" cols="40" rows="20">{{ $teacher->tutor_edu_text }}</textarea>
            </div>  
            <div class="tips col-lg-3 col-md-12 col-sm-12 col-xs-12">Опишите сдесь любое дополнительное образование, курсы, мастер класы которые вы проходили. Ваш опыт. Расскажите о вашей методике преподавания, достижениях в области репетиторства и т. д.</div>
          </div>

          <div id="tutor_video" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 one_form_block">
            <label class="control-label col-lg-2 col-md-12 col-sm-12 col-xs-12">
              Добавить видео презентацию
              <span style="display: inline-block; font-size: 85%;" class="info_tips">( +30 пунктов к рейтингу )</span>
            </label>
            <div class="col-md-12 col-lg-7 col-sm-12 col-xs-12">
              <input value="{{ $teacher->video_link['link'] }}" name="video_link" type="text" class="form-control"/>       
              @if($teacher->video_link['video_type'] == 'youtube')
                <button id="{{ $teacher->user_id }}" type="button" class="btn btn-default" data-toggle="modal" data-target="#video_user">Посмотреть свое видео <i class="fa fa-youtube-play" aria-hidden="true"></i></button>
              @endif
            </div>
            <div class="tips col-lg-3 col-md-12 col-sm-12 col-xs-12">Если у вас есть ваша видео презинтация себя, сдесь вы можете прикрепить ссылку на видео в YouTub оно будет доступно для просмотра в вашем профиле.</div>
          </div>

          <div class="bd-top col-lg-12 col-md-12 col-sm-12 col-xs-12 one_form_block">
            <label class="control-label col-lg-3 col-md-3 col-sm-3 col-xs-12">Место проведения <br />занятий<span class="require"></span></label>
            <div data-tooltip="Выбрите одно или несколько значений" class="col-md-5 col-lg-5 col-sm-5 checkbox_group teach_place_group">
              <div class="checkbox">
                  <label><input @if ($teacher->teach_online == '1') checked="checked" @endif class="teach_place_inputs" name="teach_place[online]" id="is_skype_lessons" type="checkbox" value="1">Онлайн занятия (по Skype)</label>
              </div>
              <div class="checkbox" id="is_local_lessons_wrapp">
                  <label><input @if ($teacher->teach_local == '1') checked="checked" @endif  @if( App\MyHelpers\Helper::local_teach_country($teacher->country_teach)) disabled @endif class="teach_place_inputs" name="teach_place[local]" id="is_local_lessons" type="checkbox" value="1">Провожу локальные занятия в моем городе</label>
              </div>
            </div>
            @if( App\MyHelpers\Helper::local_teach_country($teacher->country_teach))
              <div style="color: red;max-width: 220px;float: right;line-height: 1.4em; " class="tips col-lg-4 col-md-4 col-sm-4 col-xs-12 tips_dissabled">Вы можете выбрать только занятия с учеником онлайн. Локальные занятия с выбором города проживания, сейчас доступны пока только для 4 стран: Укранина, Россия, Белорусь, Казахстан (Изменить свою страну проживания можно в разделе профиля "Базовая информация")</div>
            @else
              <div class="tips col-lg-4 col-md-4 col-sm-4 col-xs-12">Выберите тип уроков, вы можете преподавать и онлайн уроки и уроки по месту жительства выбрав ваш город</div>
            @endif
          </div>

          <div id="local_teach_detail" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 one_form_block">
              <label class="control-label col-lg-3 col-md-3 col-sm-3 col-xs-12">Место проведения локальных занятий<span class="require"></span></label>
              <div data-tooltip="Выбрите одно или несколько значений" class="checkbox_group localteach_place col-md-5 col-lg-5 col-sm-5 col-xs-12" id="localteach_place">
                <div class="checkbox">
                    <label><input class="teach_place_local" @if ($teacher->teach_local_pupil == '1')) checked="checked" @endif name="teach_local_pupil" type="checkbox" value="1">На выезде (на територии ученика)</label>
                </div>
                <div class="checkbox">
                    <label><input class="teach_place_local" @if ($teacher->teach_local_me == '1')  checked="checked" @endif name="teach_local_me" type="checkbox" value="1">На моей територии (у себя дома)</label>
                </div>
              </div>
            </div>

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 one_form_block">
              <label class="control-label col-lg-3 col-md-3 col-sm-3 col-xs-12">Страна проживания<span class="require"></span></label>
              <div class="col-md-5 col-lg-5 col-sm-5 col-xs-12">
                <select class="form-control" name="country_teach" id="country_teach_id">
                  @if($teacher->country_teach == 'ua') <option selected="selected" value="ua">Украина</option> @endif
                  @if($teacher->country_teach == 'by') <option selected="selected" value="by">Белорусь</option> @endif
                  @if($teacher->country_teach == 'ru') <option selected="selected" value="ru">Россия</option> @endif 
                  @if($teacher->country_teach == 'kz') <option selected="selected" value="kz">Казахстан</option> @endif 
                </select>
              </div>  
              <div class="tips col-lg-4 col-md-4 col-sm-4 col-xs-12">Страну проживания вы можете изменить в разделе "Базовая информация"</div>
            </div>

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 one_form_block">
              <label for="city_id" class="control-label col-lg-3 col-md-3 col-sm-3 col-xs-12">Ваш город<span class="require"></span></label>
              <div class="col-md-5 col-lg-5 col-sm-5 col-xs-12">
                <select name="teach_city_en" data-ajax--cache="true" id="city_id" class="form-control js-data-example-ajax">
                  @if ($teacher->teach_city_en)
                    <option value="{{ $teacher->teach_city_en }}">{{ $teacher->teach_city_en }}</option>
                  @endif
                </select>
              </div>  
            </div>

          </div>

          <div id="skype_login_block" class="col-lg-12 col-md-12 col-ms-12 col-xs-12">
            <div class="col-lg-12 col-md-12 col-ms-12 col-xs-12 one_form_block">
              <label class="control-label col-lg-3 col-md-3 col-sm-3 col-xs-12">Ваш логин Skype</label>
              <div class="col-md-5 col-lg-5 col-sm-5 col-xs-12">
                <input value="{{ $teacher->skype_login }}" name="skype_login" type="text" class="form-control"/>
              </div>  
            </div>
          </div>

          <div class="col-lg-12 col-md-12 col-ms-12 col-xs-12 one_form_block bd-top">
            <label class="control-label col-lg-3 col-md-3 col-sm-3 col-xs-12">Возраст учеников<span class="require"></span></label>
            <div data-tooltip="Выбрите одно или несколько значений" class="checkbox_group pupil_age_checbox_group col-md-5 col-lg-5 col-sm-5 col-xs-12">

              <div class="checkbox">
                  <label><input class="pupil_age_input" @if (strripos($teacher->pupil_age, 'a2'))  checked="checked" @endif name="pupil_age[]" type="checkbox" value=".a2">Дошкольники (до 6)</label>
              </div>
              <div class="checkbox">
                  <label><input class="pupil_age_input" @if (strripos($teacher->pupil_age, 'a3'))  checked="checked" @endif name="pupil_age[]" type="checkbox" value=".a3">Младшие школьники (6-12)</label>
              </div>
              <div class="checkbox">
                  <label><input class="pupil_age_input" @if (strripos($teacher->pupil_age, 'a4'))  checked="checked" @endif name="pupil_age[]" type="checkbox" value=".a4">Школьники (12-17)</label>
              </div>
              <div class="checkbox">
                  <label><input class="pupil_age_input" @if (strripos($teacher->pupil_age, 'a5'))  checked="checked" @endif name="pupil_age[]" type="checkbox" value=".a5">Студенты (17-23)</label>
              </div>                            
              <div class="checkbox">
                  <label><input class="pupil_age_input" @if (strripos($teacher->pupil_age, 'a6'))  checked="checked" @endif name="pupil_age[]" type="checkbox" value=".a6">Взрослые (23-40)</label>
              </div>
              <div class="checkbox">
                  <label><input class="pupil_age_input" @if (strripos($teacher->pupil_age, 'a7'))  checked="checked" @endif name="pupil_age[]" type="checkbox" value=".a7">Взрослые (40+)</label>
              </div>
            </div>
          </div>

{{--           <div class="col-lg-12 col-md-12 col-ms-12 col-xs-12 one_form_block bd-top">
            <label class="control-label col-lg-3 col-md-3 col-sm-3 col-xs-12">Дни занятий</label>
            <div class="checkbox_group col-md-5 col-lg-5 col-sm-5 col-xs-12">
              <div class="checkbox">
                  <label><input @if (strripos($teacher->teach_days, '1'))  checked="checked" @endif name="teach_days[]" type="checkbox" value=".1">Понедельник</label>
              </div>
              <div class="checkbox">
                  <label><input @if (strripos($teacher->teach_days, '2'))  checked="checked" @endif name="teach_days[]" type="checkbox" value=".2">Вторник</label>
              </div>
              <div class="checkbox">
                  <label><input @if (strripos($teacher->teach_days, '3'))  checked="checked" @endif name="teach_days[]" type="checkbox" value=".3">Среда</label>
              </div>
              <div class="checkbox">
                  <label><input @if (strripos($teacher->teach_days, '4'))  checked="checked" @endif name="teach_days[]" type="checkbox" value=".4">Четверг</label>
              </div>
              <div class="checkbox">
                  <label><input @if (strripos($teacher->teach_days, '5'))  checked="checked" @endif name="teach_days[]" type="checkbox" value=".5">Пятница</label>
              </div>
              <div class="checkbox">
                  <label><input @if (strripos($teacher->teach_days, '6'))  checked="checked" @endif name="teach_days[]" type="checkbox" value=".6">Суббота</label>
              </div>
              <div class="checkbox">
                  <label><input @if (strripos($teacher->teach_days, '7'))  checked="checked" @endif name="teach_days[]" type="checkbox" value=".7">Воскресенье</label>
              </div>
            </div>
            <div class="tips col-lg-4 col-md-4 col-sm-4 col-xs-12">Выберите дни в которые вы можете проводить занятия</div>
            
          </div> --}}

          <div style="margin-bottom: 15px;" id="price_block" class="col-lg-12 col-md-12 col-ms-12 col-xs-12 one_form_block bd-top">
            <label class="control-label col-lg-3 col-md-3 col-sm-3 col-xs-12">Стоимость урока</label>
            <div class="col-md-5 col-lg-5 col-sm-5 col-xs-12">
              <input value="{{ $teacher_price ? $teacher_price->price_def : ''  }}" name="price_value" type="text" class="form-control"/>
              <select class="form-control" name="price_valute">
                <option @if ($teacher_price && $teacher_price->cur_def == 'UAH') selected="selected" @endif value="UAH">грн</option>
                <option @if ($teacher_price && $teacher_price->cur_def == 'RUB') selected="selected" @endif value="RUB">руб</option>
                <option @if ($teacher_price && $teacher_price->cur_def == 'USD') selected="selected" @endif value="USD">$</option>
                <option @if ($teacher_price && $teacher_price->cur_def == 'BYN') selected="selected" @endif value="BYN">белор. руб</option> 
                <option @if ($teacher_price && $teacher_price->cur_def == 'KZT') selected="selected" @endif value="KZT">теңге</option>
              </select>
              60 мин
            </div>  
            <div class="tips col-lg-4 col-md-4 col-sm-4 col-xs-12">Укажите приблизительную цену одного урока и выберите валюту </div>
          </div>

          <div id="exp_block" class="col-lg-12 col-md-12 col-ms-12 col-xs-12 one_form_block">
            <label class="control-label col-lg-3 col-md-3 col-sm-3 col-xs-12">Опыт преподавания</label>
            <div class="checkbox_group col-md-5 col-lg-5 col-sm-5 col-xs-12">
              <input value="{{ $teacher->teach_exp }}" placeholder="лет" name="teach_exp" type="text" class="form-control"/> 
              <span>Лет</span>
            </div>
            <div class="tips col-lg-4 col-md-4 col-sm-4 col-xs-12">Сколько лет вы занимаетесь преподаванием</div>
          </div>
                   
          <div id="group_block" class="col-lg-12 col-md-12 col-ms-12 col-xs-12 one_form_block">
            <label class="control-label col-lg-3 col-md-3 col-sm-3 col-xs-12">Груповые занятия</label>
            <div id="group-inputs-block" class="checkbox_group col-md-5 col-lg-5 col-sm-5 col-xs-12">
              <div id="group-labels">
                <label class="grey_text"><input @if ($teacher->is_group_teach == '1') checked="checked" @endif class="yes_group_teach" name="is_group_teach" type="radio" value="1">Да</label>
                <label class="grey_text"><input @if ($teacher->is_group_teach == '0') checked="checked" @endif class="yes_group_teach" name="is_group_teach" type="radio" value="0">Нет</label>
              </div>
            </div>
            <div class="tips col-lg-4 col-md-4 col-sm-4 col-xs-12">Проводите ли вы груповые занятия?</div>
          </div>

          <div class="col-lg-12 col-md-12 col-ms-12 col-xs-12 one_form_block" id="group_size_block">
            <label class="control-label col-lg-3 col-md-3 col-sm-3 col-xs-12">Размер группы</label>
            <div class="col-md-5 col-lg-5 col-sm-5 col-xs-12">
              <label class="grey_text checkbox_group">Группа до<input value="{{ $teacher->group_size }}" name="group_size" type="text" class="form-control"/>человек</label>
            </div>
            <div class="tips col-lg-4 col-md-4 col-sm-4 col-xs-12">Укажите максимальный размер группы</div>
          </div>

        </div>

        <div id="save-tutor-info-wrap">
          <input id="save-tutor-info"  class="btn btn-success" value="Сохранить" type="submit">
        </div>

        {!! Form::close() !!}

    </div>
</div>

<!-- Modal watch video -->
  <div class="modal fade" id="video_user" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <div class="modal-title" id="exampleModalLabel">Ваша видео презентация</div>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <iframe width="560" height="315" src="http://www.youtube.com/embed/{{ $teacher->video_link['video_id'] }}?autoplay=0" frameborder="0"></iframe>
        </div>
      </div>
    </div>
  </div>

@endsection

@section('style')

{{ Html::style('front/select2/select2.css') }}

@endsection

@section('script')

{{ Html::script('front/select2/select2.full.js') }}

<script type="text/javascript">

$('#city_id').select2({
    placeholder: "Выберите свой город",
    minimumInputLength: 2,
    ajax: {
        url: '{{ url('/') }}/city-search',
        delay: 100,
        dataType: 'json',
        data: function (params, locale) {
            var query = {
                city: $.trim(params.term),
                locale: $('#country_teach_id').val(),
            };
            return query;
        },
        processResults: function (data) {
            return {
                results: data
            };
        },
        cache: true
    }
});

$(function() {

// проверки обязательных чекбоксов на пустоту
$('#save-tutor-info').on('click', function(event){
  valthis(event);
});  

function valthis(submit_button) {
var checkBoxes = document.getElementsByClassName( 'teach_place_inputs' );
var isChecked = false;
    for (var i = 0; i < checkBoxes.length; i++) {
        if ( checkBoxes[i].checked ) {
          isChecked = true;
        };
    };
    if ( isChecked ) {

          console.log( 'At least one checkbox checked!' );
          if(checkBoxes[1].checked ){

              var teach_place_local = document.getElementsByClassName( 'teach_place_local' );
              var isChecked_next = false;
              for (var i = 0; i < teach_place_local.length; i++) {
                  if ( teach_place_local[i].checked ) {
                    isChecked_next = true;
                  };
              };

              if ( isChecked_next ) {
                var city_id = $('#city_id');
                if(city_id.val() == null){
                  city_id.attr("required", "true");
                }
                
                console.log( 'At least one isChecked_next checked!' );
              }else{
                var localteach_place = $('.localteach_place').css('border', '1px solid #ffb23e');

                showTip(localteach_place);

                setTimeout( function(){
                  localteach_place.css('border','none');
                },5000);
                $('html, body').animate({
                    scrollTop: localteach_place.offset().top
                }, 1000);
                submit_button.preventDefault();
                console.log( 'Please, check at least one checkbox!' );
              }

          }else{
            var teach_place_local = $( '.teach_place_local' ).prop('checked', false);;
          }

          //проверка возраста учеников выбранных
          var pupil_age_input = document.getElementsByClassName( 'pupil_age_input' );
          var isChecked_age = false;
              for (var i = 0; i < pupil_age_input.length; i++) {
                  if ( pupil_age_input[i].checked ) {
                    isChecked_age = true;
                  };
              };
          if ( isChecked_age ) {
            console.log( 'At least one isChecked_age checked!' );
          }else{
              var pupil_age_checbox_group = $('.pupil_age_checbox_group').css('border', '1px solid #ffb23e');
              showTip(pupil_age_checbox_group);
              setTimeout( function(){
                pupil_age_checbox_group.css('border','none');
              },5000);
              $('html, body').animate({
                  scrollTop: pupil_age_checbox_group.offset().top
              }, 1000);
              submit_button.preventDefault();
          }

    } else {

          var teach_place_group = $('.teach_place_group').css('border', '1px solid #ffb23e');
          showTip(teach_place_group);
          setTimeout( function(){
            teach_place_group.css('border','none');
          },5000);
          $('html, body').animate({
              scrollTop: teach_place_group.offset().top
          }, 1000);
          submit_button.preventDefault();
    }   
}

//функция подсказки создания
function showTip(current) {
    console.log(current);
    var tipText = current.attr('data-tooltip');
    var element = document.createElement('div');
    element.className = 'my_tooltip';
    element.innerHTML = tipText;
    $(current).append(element);
    $(element).delay( 5000 ).fadeOut('slow');
}


//выбор уроков по skype
var skype_login_block = $('#skype_login_block');
skype_login_block.hide('fast');
var is_skype_lessons = $('#is_skype_lessons');
if(is_skype_lessons.is(':checked')){
    skype_login_block.show('fast');
}
is_skype_lessons.on('change', function() {

    if (is_skype_lessons.prop('checked')) {
        skype_login_block.show();
    }else{
        skype_login_block.hide();
    }

});

//выбор уроков локальных
var local_teach_detail = $('#local_teach_detail');
local_teach_detail.hide('fast');
var is_local_lessons = $('#is_local_lessons');
if(is_local_lessons.is(':checked')){
    local_teach_detail.show('fast');
}
is_local_lessons.on('change', function() {

    if (is_local_lessons.prop('checked')) {
        local_teach_detail.show();
    }else{
        local_teach_detail.hide();
    }

});

//выбор груповых занятий
var yes_group_teach = $('.yes_group_teach');
var group_size_block = $('#group_size_block').hide();
var more_info_teach = $('#more_info_teach');
yes_group_teach.on('change', function() {
    if ($(this).val() == 1) {
        group_size_block.show();
        more_info_teach.height(more_info_teach.height()+62);
    }else{
        group_size_block.hide();
        if (more_info_teach.height() > 865) {
            more_info_teach.height(more_info_teach.height()-62);
        }
    }
});
if(yes_group_teach.first().is(':checked')){
    group_size_block.show();
}else{
    group_size_block.hide();
}

});

</script>

@endsection

      

