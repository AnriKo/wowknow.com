@extends('layouts/app_sidebar')

@section('title_page')
  Создание профиля на дополнительных языках
  @if(Auth::user()->type == "user_teach" && Auth::user()->user_teacher->active == "1")
    <div id='my_profile_link'><a target="_blank" href='{{ route('tutor_page', ['slug' => Auth::user()->user_teacher->slug ]) }}'>Моя страница в поиске, посмотреть <i class='fa fa-external-link' aria-hidden='true'></i></a></div>
  @else
    <div class="danger_bd" id='my_profile_link'><a title="Вы еще не заполнили все необходимые разделы своего профиля" href='#'><i class="fa fa-exclamation-circle" aria-hidden="true"></i>Вашей анкеты еще нету в поиске</a></div>
  @endif
@endsection

@section('side_bar')

@include('parts/user_menu')

@endsection

@section('content')

<div id="tutor_block" class="panel panel-default without_sub_title">
    <div id="create_language" class="panel-body">

        {!! Form::open(['route' => 'tutor_langs_save']) !!}

        <div class="col-md-12 no_padding">

          <p style="margin-bottom: 20px; border-bottom: 1px solid #e7e7e7; padding-bottom: 4px;"> <strong><i style="color: #ff7d45" class="fa fa-info-circle" aria-hidden="true"></i> Важная информация.</strong> <br>
            Язык сайта по умолчанию сейчас русский, поэтому он должен быть заполнен обязательно. <br> Если вы к примеру хотите перевести свой профиль на английский, установите верхний переключатель над каждым полем в положение "in English" и заполните все поля на этой странице на английском. <br> 
            Сверху, над каждим полем отображается зеленая галочка того языка на котором оно уже заполнено.

          </p>

          <div class="col-lg-12 col-md-12 col-ms-12 col-xs-12 one_form_block">
            <label for="tutor_name" class="control-label col-lg-2 col-md-12 col-sm-12 col-xs-12">Ваше имя</label>
            <div class="col-md-12 col-lg-7 col-sm-12 col-xs-12 lands_inputs">

               <ul class="nav nav-pills">
                  @foreach($langs as $lang_code => $element)
                      <li @if($loop->first) class="active" @endif >
                          <a href="#tutor_name_{{$lang_code}}" data-toggle="tab">
                            <i class="fa fa-pencil" aria-hidden="true"></i>
                            {{App\MyHelpers\Helper::lang_name($lang_code)}}
                            <i class="fa fa-check-square"></i>
                          </a>
                      </li>
                  @endforeach
              </ul>

              <div class="tab-content clearfix">
                @foreach($langs as $lang_code => $element)
                    <div data-tooltip="Имя на языке по умолчанию, обязательно" class="tab-pane @if($loop->first) active @endif" id="tutor_name_{{$lang_code}}">
                      <input 
                      @if($loop->first) value="{{ Auth::user()->name }}" @endif 
                      @if($loop->iteration == '2' && $ua != "") value="{{ $ua->name }}" @endif
                      @if($loop->iteration == '3' && $en != "") value="{{ $en->name }}" @endif
                      id="id_tutor_name_{{$lang_code}}" name="lang_{{ $loop->iteration }}[name]" type="text" class="form-control input_lang"/>
                    </div>

                @endforeach
              </div>

            </div>  
          </div>

          <div class="col-lg-12 col-md-12 col-ms-12 col-xs-12 one_form_block">
            <label class="control-label col-lg-2 col-md-12 col-sm-12 col-xs-12">Ваша фамилия</label>
            <div class="col-md-12 col-lg-7 col-sm-12 col-xs-12 lands_inputs">

              <ul class="nav nav-pills">
                  @foreach($langs as $lang_code => $element)
                      <li @if($loop->first) class="active" @endif >
                          <a href="#tutor_last_name_{{$lang_code}}" data-toggle="tab">
                            <i class="fa fa-pencil" aria-hidden="true"></i>
                            {{App\MyHelpers\Helper::lang_name($lang_code)}}
                            <i class="fa fa-check-square"></i>
                          </a>
                      </li>
                  @endforeach
              </ul>

              <div class="tab-content clearfix">
                @foreach($langs as $lang_code => $element)
                    <div data-tooltip="Фамилия на языке по умолчанию" class="tab-pane @if($loop->first) active @endif" id="tutor_last_name_{{$lang_code}}">
                      <input 
                      @if($loop->first) value="{{ $teacher->last_name }}" @endif 
                      @if($loop->iteration == '2' && $ua != "") value="{{ $ua->last_name }}" @endif
                      @if($loop->iteration == '3' && $en != "") value="{{ $en->last_name }}" @endif

                      id="id_tutor_last_name_{{$lang_code}}" name="lang_{{ $loop->iteration }}[last_name]" type="text" class="form-control input_lang"/>
                    </div>

                @endforeach
              </div>

            </div>  
          </div>

          <div style="padding-right: 14px;" id="title-block" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 one_form_block">
            <label class="control-label col-lg-2 col-md-12 col-sm-12 col-xs-12">Заголовок профиля</label>
            <div class="col-md-12 col-lg-10 col-sm-12 col-xs-12 lands_inputs">

              <ul class="nav nav-pills">
                  @foreach($langs as $lang_code => $element)
                      <li @if($loop->first) class="active" @endif >
                          <a href="#profile_title_{{$lang_code}}" data-toggle="tab">
                            <i class="fa fa-pencil" aria-hidden="true"></i>
                            {{App\MyHelpers\Helper::lang_name($lang_code)}}
                            <i class="fa fa-check-square"></i>
                          </a>
                      </li>
                  @endforeach
              </ul>

              <div class="tab-content clearfix">
                @foreach($langs as $lang_code => $element)
                    <div data-tooltip="Заголовок, на языке по умолчанию" class="tab-pane @if($loop->first) active @endif" id="profile_title_{{$lang_code}}">
                      <input 
                      @if($loop->first) value="{{ $teacher->profile_title }}" @endif 
                      @if($loop->iteration == '2' && $ua != "") value="{{ $ua->profile_title }}" @endif
                      @if($loop->iteration == '3' && $en != "") value="{{ $en->profile_title }}" @endif

                      id="id_profile_title_{{$lang_code}}" name="lang_{{ $loop->iteration }}[profile_title]" type="text" class="form-control input_lang"/>
                    </div>

                @endforeach
              </div>

            </div>  
          </div>

          <div style="padding-right: 14px;" id="tutor_education_text" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 one_form_block">
            <label class="control-label col-lg-2 col-md-12 col-sm-12 col-xs-12">О вас, вашем образовании, опыте, методике преподавания, подробнее</label>
            <div class="col-md-12 col-lg-10 col-sm-12 col-xs-12 lands_inputs">

              <ul class="nav nav-pills">
                  @foreach($langs as $lang_code => $element)
                      <li @if($loop->first) class="active" @endif >
                          <a href="#tutor_edu_text_{{$lang_code}}" data-toggle="tab">
                            <i class="fa fa-pencil" aria-hidden="true"></i>
                            {{App\MyHelpers\Helper::lang_name($lang_code)}}
                            <i class="fa fa-check-square"></i>
                          </a>
                      </li>
                  @endforeach
              </ul>

              <div class="tab-content clearfix">
                  @foreach($langs as $lang_code => $element)
                      <div data-tooltip="О вас, на языке по умолчанию, обязательно" class="tab-pane @if($loop->first) active @endif" id="tutor_edu_text_{{$lang_code}}">
                        <textarea class="form-control input_lang" name="lang_{{ $loop->iteration }}[tutor_edu_text]" id="id_tutor_edu_text_{{$lang_code}}" cols="40" rows="20"><?php 
                        if($loop->first){ echo $teacher->tutor_edu_text; } 
                        if($loop->iteration == '2' && $ua != ""){ echo $ua->tutor_edu_text; } 
                        if($loop->iteration == '3' && $en != ""){ echo $en->tutor_edu_text; } 
                        ?></textarea>
                      </div>
                  @endforeach
              </div>

            </div>  

          </div>

          <div id="tutor_hobie_text" class="col-lg-12 col-md-12 col-ms-12 col-xs-12 one_form_block">
            <label class="control-label col-lg-2 col-md-12 col-sm-12 col-xs-12">Ваши хобби, интересы</label>
            <div class="col-md-12 col-lg-10 col-sm-12 col-xs-12 lands_inputs">

              <ul class="nav nav-pills">
                  @foreach($langs as $lang_code => $element)
                      <li @if($loop->first) class="active" @endif >
                          <a href="#tutor_hobie_text_{{$lang_code}}" data-toggle="tab">
                            <i class="fa fa-pencil" aria-hidden="true"></i>
                            {{App\MyHelpers\Helper::lang_name($lang_code)}}
                            <i class="fa fa-check-square"></i>
                          </a>
                      </li>
                  @endforeach
              </ul>

              <div class="tab-content clearfix">
                  @foreach($langs as $lang_code => $element)
                      <div data-tooltip="Заполните описание компании" class="tab-pane @if($loop->first) active @endif" id="tutor_hobie_text_{{$lang_code}}">
                        <textarea class="form-control input_lang hobbi_block" name="lang_{{ $loop->iteration }}[tutor_hobie_text]" id="id_tutor_hobie_text_{{$lang_code}}" cols="40" rows="20"><?php 
                        if($loop->first){ echo $teacher->tutor_hobie_text; } 
                        if($loop->iteration == '2' && $ua != ""){ echo $ua->tutor_hobie_text; } 
                        if($loop->iteration == '3' && $en != ""){ echo $en->tutor_hobie_text; } 
                        ?></textarea>
                      </div>
                  @endforeach
              </div>
            </div>  

          </div>

        </div>

        @foreach($langs as $code => $val)
          <input type="hidden" name="lan_code_{{ $loop->iteration }}" value="{{ $code }}">
        @endforeach
        <input type="hidden" name="course" value="{{ $teacher->id }}">

        <div id="save-tutor-info-wrap">
          <input id="save-tutor-info"  class="btn btn-success" value="Сохранить" type="submit">
        </div>

        {!! Form::close() !!}

    </div>
</div>

@endsection

@section('style')


@endsection

@section('script')


<script type="text/javascript">

$(function() {
 
//заполненность инпутов языковых
$('.input_lang').on('keyup', function(){
  if($(this).val() != ''){
    var attr = $(this).parent().attr('id');
    $("a[href='#"+attr+"']").find('.fa-check-square').show();
  }else{
    var attr = $(this).parent().attr('id');
    $("a[href='#"+attr+"']").find('.fa-check-square').hide();
  }
});

  //заполненность описания, названия на разных языках
  var tab_pane = $('.tab-pane');
  tab_pane.map(function() {
    var input = $(this).find('.form-control');
    if(input.val() != ""){
      var id = $(this).attr('id');
      $("a[href='#"+id+"']").find('.fa-check-square').show();
    }
  });

//функция подсказки создания
function showTip(current) {
    console.log(current);
    var tipText = current.attr('data-tooltip');
    var element = document.createElement('div');
    element.className = 'my_tooltip';
    element.innerHTML = tipText;
    $(current).append(element);
    $(element).delay( 7000 ).fadeOut('slow');
}

$('#save-tutor-info').click(function(event) {

  var inputs_requared = $('.tab-content .tab-pane:first-child input, .tab-content .tab-pane:first-child textarea:not(.hobbi_block)');
  inputs_requared.map( function(index, element) {

      if($(element).val() == ''){
        var first_child = $(element).closest('.tab-content').siblings('.nav-pills').find('li:first-child');
        first_child.find('a').tab('show');
        first_child.css('border', '1px solid orange').css('border-bottom', 'none');
        setTimeout( function(){
          first_child.css('border','none');
        },8000);
        //$('#block_company_name li:first-child a').tab('show');
        var show_tip = $(element).closest('.tab-pane');
        showTip(show_tip);
        event.preventDefault();

          $('html, body').animate({
              scrollTop: show_tip.offset().top-200
          }, 1000);
      }
  });

  $('textarea').map(function(){
    var s = $(this).val();
    var result = s.replace(/style="[^"]*"/g, '');
    $(this).val(result); 
  });
  
});

});

</script>

@endsection

      

