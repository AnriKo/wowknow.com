@extends('layouts/app_sidebar')

@section('title_page')
    @if(isset($data_tasks['subject']) and $data_tasks['subject'] != '')
        Заявки учеников на обучение по предмету "{{$data_tasks['subject']->name_ru}}"
        @if($data_tasks['task_city_en'] != 'all-city')
            в городе "{{$data_tasks['task_city_current']}}"
        @endif
        @if(isset($data_tasks['teach_online']) && $data_tasks['teach_online'] == 'online')
           - онлайн
        @endif
    @else
        Поиск учеников, все заявки на обучение.
    @endif
@endsection

@section('side_bar')

<div id="first_part_bar_menu">
    <div class="panel panel-default">
        <label class="control-label side-bar-label">Предмет</label>
        <div class="panel-body">
            <select name="subject_id" id="subject_name" class="js-example-basic-multiple form-control" >
                @if(isset($data_tasks['subject']) and $data_tasks['subject'] != '')
                    <option value="{{$data_tasks['subject']->id}}">{{$data_tasks['subject']->name_ru}}</option>
                @endif
            </select>
            <div class="tips_hide">
                <p class="tip_text">По какому предмету ищете ученика?</p>
                <span class="tip_corner"></span>
            </div>
     
        </div>
    </div>
    <div class="panel panel-default">
        <label class="control-label side-bar-label">Место занятий</label>
        <div class="panel-body">

            <div class="one-block-menu">
                 <div class="wrap" id="type_lessons">
                    <input type="radio" id="local" value="local" name="online_local" @if(Request::query('type_lessons') === 'local') checked @endif  /><label for="local">Локальные</label>
                    <input type="radio" id="online" value="online" name="online_local" @if(Request::query('type_lessons') === 'online') checked @endif /><label for="online">Онлайн</label> 
                </div>
            </div>
            <div id="city_block" class="one-block-menu">
                <label class="control-label">Ваш город</label>
                <select name="city_id" data-ajax--cache="true" id="city_id" class="form-control js-data-example-ajax">
                    @if(isset($data_tasks['task_city_en']))
                        <option value="{{$data_tasks['task_city_en']}}">{{$data_tasks['task_city_current']}}</option>
                    @endif
                </select>
                <span id="city_id_zaglushka" class="select2 select2-container select2-container--default" dir="ltr" style="width: 310px;">
                    <span class="selection">
                        <span class="select2-selection select2-selection--single" role="combobox" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2-subject_name-container">
                            <span id="select2-subject_name-container" class="select2-selection__rendered">
                                <span class="select2-selection__placeholder">Выберите ваш город</span>
                            </span>
                            <span class="select2-selection__arrow" role="presentation">
                                <b role="presentation"></b>
                            </span>
                        </span>
                    </span>
                    <span class="dropdown-wrapper" aria-hidden="true"></span>
                </span>
            </div>
     
        </div>
    </div>
    <div class="panel panel-default">
        <label class="control-label side-bar-label">Пол репетитора</label>
        <div class="panel-body">
            <div class="one-block-menu">
                <div class="wrap" id="gender">
                    <div>
                        <input class="css-checkbox" type="radio" id="gender_mw" value="mw" name="gender_teacher" @if(Request::query('gender') === 'mw' or Request::query('gender') == '') checked @endif  /><label class="css-label" for="gender_mw">Не имеет значения</label>
                    </div>
                    <div>
                        <input class="css-checkbox" type="radio" id="gender_m" value="m" name="gender_teacher" @if(Request::query('gender') === 'm') checked @endif /><label class="css-label" for="gender_m">Мужской</label> 
                    </div>
                    <div>
                        <input class="css-checkbox" type="radio" id="gender_w" value="w" name="gender_teacher" @if(Request::query('gender') === 'w') checked @endif /><label class="css-label" for="gender_w">Женский</label> 
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>  

<div id="second_part_bar_menu">  
    <div id="slider_price_block" class="panel panel-default">
        <label class="control-label side-bar-label">
            Стоимость занятия в 
            <select id="selec_valute_in_sidebar" class="selec_valute" name="price_valute">
              <option @if (Cookie::get('cod_valute') == 'UAH') selected="selected" @endif value="UAH">UAH</option>
              <option @if (Cookie::get('cod_valute') == 'RUB') selected="selected" @endif value="RUB">RUB</option>
              <option @if (Cookie::get('cod_valute') == 'USD') selected="selected" @endif value="USD">USD</option>
              <option @if (Cookie::get('cod_valute') == 'KZT') selected="selected" @endif value="KZT">KZT</option>
              <option @if (Cookie::get('cod_valute') == 'BYN') selected="selected" @endif value="BYN">BYN</option>
            </select>
        </label>
        <div class="panel-body">

            <div class="one-block-menu">
                <div id="slider_price"></div>
                    <span class="small-font">от</span>
                    <input step="10" type="number" id="input-with-keypress-0">
                    @if (Cookie::get('cod_valute') == 'UAH') <span class="small-font">UAH</span> @endif
                    @if (Cookie::get('cod_valute') == 'RUB') <span class="small-font">RUB</span> @endif
                    @if (Cookie::get('cod_valute') == 'USD') <span class="small-font">USD</span> @endif
                    @if (Cookie::get('cod_valute') == 'KZT') <span class="small-font">KZT</span> @endif
                    @if (Cookie::get('cod_valute') == 'BYN') <span class="small-font">BYN</span> @endif
                    &nbsp;
                    <span class="small-font">до</span> 
                    <input step="10" type="number" id="input-with-keypress-1">
                    @if (Cookie::get('cod_valute') == 'UAH') <span class="small-font">UAH</span> @endif
                    @if (Cookie::get('cod_valute') == 'RUB') <span class="small-font">RUB</span> @endif
                    @if (Cookie::get('cod_valute') == 'USD') <span class="small-font">USD</span> @endif
                    @if (Cookie::get('cod_valute') == 'KZT') <span class="small-font">KZT</span> @endif
                    @if (Cookie::get('cod_valute') == 'BYN') <span class="small-font">BYN</span> @endif
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <label class="control-label side-bar-label">Возраст ученика</label>
        <div class="panel-body">

            <div class="one-block-menu">

                <div id="pupil_age" class="checkbox_group">
                  <div class="checkbox">
                      <input id=".a1" @if (strripos(Request::query('p_age'), '1' ))  checked="checked" @endif name="pupil_age[]" type="checkbox" value=".a1" class="css-checkbox"><label for=".a1" class="css-label">Малыши до 3 лет</label>
                  </div>
                  <div class="checkbox">
                      <input id=".a2" @if (strripos(Request::query('p_age'), '2' ))  checked="checked" @endif name="pupil_age[]" type="checkbox" value=".a2" class="css-checkbox"><label for=".a2" class="css-label">Дети (4-6)</label>
                  </div>
                  <div class="checkbox">
                      <input id=".a3" @if (strripos(Request::query('p_age'), '3' ))  checked="checked" @endif name="pupil_age[]" type="checkbox" value=".a3" class="css-checkbox"><label for=".a3" class="css-label">Младшие школьники (6-12)</label>
                  </div>
                  <div class="checkbox">
                      <input id=".a4" @if (strripos(Request::query('p_age'), '4' ))  checked="checked" @endif name="pupil_age[]" type="checkbox" value=".a4" class="css-checkbox"><label for=".a4" class="css-label">Школьники (12-17)</label>
                  </div>
                  <div class="checkbox">
                      <input id=".a5" @if (strripos(Request::query('p_age'), '5' ))  checked="checked" @endif name="pupil_age[]" type="checkbox" value=".a5" class="css-checkbox"><label for=".a5" class="css-label">Студенты (17-23)</label>
                  </div>                            
                  <div class="checkbox">
                      <input id=".a6" @if (strripos(Request::query('p_age'), '6' ))  checked="checked" @endif name="pupil_age[]" type="checkbox" value=".a6" class="css-checkbox"><label for=".a6" class="css-label">Взрослые (23-40)</label>
                  </div>
                  <div class="checkbox">
                      <input id=".a7" @if (strripos(Request::query('p_age'), '7' ))  checked="checked" @endif name="pupil_age[]" type="checkbox" value=".a7" class="css-checkbox"><label for=".a7" class="css-label">Взрослые (40+)</label>
                  </div>
                </div>

            </div>
        </div>
    </div>
</div>

@endsection

@section('content')

<div id="search-task" class="panel panel-default">
    <div class="panel-body">

        @foreach($tasks as $task)

            <div class="row one_row one_pupil">
                <div class="col-md-9 col-sm-9 col-xs-12 no_padding">
                    <h5 class="task_title">
                        {{ mb_strimwidth($task->text, 0, 55, "...") }} 
                        @if($task->country_id)
                            <span class="flag flag-{{ $task->country_id }}"></span>
                        @endif 
                    </h5>
                    <div class="profile_info">

                        <div class="profile_info_block_1">
                            <div class="profile_info_one_block">
                                <i class="fa fa-book" aria-hidden="true"></i>  
                                <div class="profile_info_text">  
                                    {{ $task->task_subject }}
                                </div>
                            </div>

                            <div class="profile_info_one_block">
                                <i class="fa fa-map-marker" aria-hidden="true"></i>
                                <div class="profile_info_text">  
                                    Место занятий:
                                    @if($task->teach_online == '1')
                                        онлайн (Skype) <br>
                                    @endif 
                                    @if($task->teach_local == '1')
                                        @if($task->teach_local_pupil == "1" || $task->teach_local_teacher == "1")
                                            <span style="cursor: pointer;" data-toggle="tooltip" data-placement="top" 
                                                title="
                                                    @if($task->teach_local_pupil == "1" && $task->teach_local_teacher == "1")
                                                        (на територии ученика либо учителя)
                                                    @elseif($task->teach_local_pupil == "1") 
                                                        (на територии ученика)   
                                                    @elseif($task->teach_local_teacher == "1") 
                                                        (на територии учителя)   
                                                    @endif
                                                ">
                                                в г. 
                                                @if($data_tasks['task_city_current'] == 'Выберите свой город')
                                                    {{ $task->teach_city_en }}
                                                @else
                                                    {{ $data_tasks['task_city_current'] }}
                                                @endif
                                                <i style="float: none; color: #d8d8d8;" class="fa fa-info-circle" aria-hidden="true"></i>
                                            </span>
                                        @else
                                            в г. 
                                            @if($data_tasks['task_city_current'] == 'Выберите свой город')
                                                {{ $task->teach_city_en }}
                                            @else
                                                {{ $data_tasks['task_city_current'] }}
                                            @endif 
                                        @endif
                                    @endif
                                    {{-- $data_tasks['teach_city_current'] --}}
                                </div>
                            </div>

                            <div class="profile_info_one_block">
                                <i class="fa fa-user" aria-hidden="true"></i>
                                <div class="profile_info_text get_user_name">  
                                    Автор:
                                    {{ $task->name }}
                                </div>
                            </div>
                        </div>
                        <div class="profile_info_block_2">
                            @if($task->pupil_age != '')
                            <div class="profile_info_one_block">
                                <i class="fa fa-graduation-cap" aria-hidden="true"></i>
                                <div class="profile_info_text">  
                                    Возраст ученика: {{ App\MyHelpers\Helper::pupil_age($task->pupil_age) }}
                                </div>
                            </div>
                            @endif

                            @if(($task->gender != '') and ($task->gender != 'mw') )
                                @if ($task->gender == 'man')
                                    <div class="profile_info_one_block">
                                        <i class="fa fa-male" aria-hidden="true"></i>
                                        <div class="profile_info_text">  
                                            Пол преподавателя:  мужчина 
                                        </div>
                                    </div>
                                @else
                                    <div class="profile_info_one_block">
                                        <i class="fa fa-female" aria-hidden="true"></i>
                                        <div class="profile_info_text">  
                                            Пол преподавателя:  женщина
                                        </div>
                                    </div>
                                @endif
                            @endif

                            @if($task->task_price != '')
                            <div class="profile_info_one_block">
                                <i class="fa fa-money" aria-hidden="true"></i>
                                <div class="profile_info_text">  
                                    Цена: ~<span data-valute="{{ $task->task_price }}" class="price_value">{{ $task->task_price }}</span>  <span class="price_detale">{{ $valut_conv }}/занятие</span>
                                </div>
                            </div>
                            @endif

                            @if($task->pupil_name != '')
                            <div class="profile_info_one_block">
                                <i class="fa fa-user" aria-hidden="true"></i>
                                <div class="profile_info_text get_user_name">  
                                    Имя ученика:
                                    {{ $task->pupil_name }}
                                </div>
                            </div>
                            @endif

                        </div>
                        <div class="clear_both"></div>

                        <div class="task_about">
                            {{ $task->text }}
                        </div>

                        @if(mb_strlen($task->text) > 164)
                        <div class="show_task">
                            Развернуть заявку <i class="fa fa-angle-down" aria-hidden="true"></i>
                        </div>
                        @endif

                    </div>
                </div> 
                <div class="col-md-3 col-sm-3 col-xs-12 no_padding link_pupil">
                    @if(Auth::check())

                        @if(Auth::user()->type == 'user_teach')
                            <div class="view_profile">
                                <button data-id_task="{{ $task->id }}" data-id_pupil="{{ $task->user_id }}" type="button" class="btn btn-default" data-toggle="modal" data-target="#send_request_pupil">Ответить на заявку</button>
                            </div>
                            <div class="send_meil">
                                <button data-id_pupil="{{ $task->user_id }}" type="button" class="btn btn-info send_mail_open_modal" data-toggle="modal" data-target="#send_user_mail">Написать <i class="fa fa-envelope" aria-hidden="true"></i></button>
                            </div>
                        @else
                            <div class="view_profile">
                                <button data-id_task="{{ $task->id }}" data-id_pupil="{{ $task->user_id }}" type="button" class="btn btn-default" data-toggle="modal" data-target="#not_teacher">Ответить на заявку</button>
                            </div>
                            <div class="send_meil">
                                <button data-id_pupil="{{ $task->user_id }}" type="button" class="btn btn-info send_mail_open_modal" data-toggle="modal" data-target="#not_teacher">Написать <i class="fa fa-envelope" aria-hidden="true"></i></button>
                            </div>
                        @endif    
                    @else
                        <div class="view_profile">
                            <button type="button" class="btn btn-default" data-toggle="modal" data-target="#basic_login">Ответить на заявку</button>
                        </div>               
                        <div class="send_meil">
                            <button type="button" class="btn btn-info send_mail_open_modal" data-toggle="modal" data-target="#basic_login">Написать <i class="fa fa-envelope" aria-hidden="true"></i></button>
                        </div> 
                    @endif       
                    
                </div>
            </div>

        @endforeach

        {{ $tasks->render() }}
 
      
    </div>
</div>

@if(Auth::check())
<!-- Modal send mail -->
<div class="modal fade" id="send_user_mail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <div class="modal-title" id="exampleModalLabel"> Ученик <span id="get_user_name"></span>Отправление сообщения</div>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <textarea placeholder="Введите собщение" id="message_body" name="message" class="form-control"></textarea>
        <input id="user_number" type="hidden" name="recipients[]" value="">
      </div>
      <div class="modal-footer">
        <button id="send_mail_button" type="button" class="btn btn-primary">Отправить</button>
      </div>
    </div>
  </div>
</div>

<!-- Modal send request to pupil if Auth-->
<div class="modal fade" id="send_request_pupil" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <div class="modal-title" id="exampleModalLabel"> 
            <b>Ответить на объявление ученика ...</b>
        </div>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Ученику <br> <b id="get_user_name_request"></b> <br> прийдет сообщение что вы хотите начать с ним заниматься</p>
        <button  data-toggle="collapse" data-target="#textarea_wrap" type="button" class="btn btn-default">Прикрепить сообщение к заявке</button>
        <div style="margin-top:20px;" class="collapse" id="textarea_wrap">
            <textarea placeholder="Введите собщение" id="request_body" name="description" class="form-control"></textarea>
        </div>    
        <input id="user_number_request" type="hidden" name="user_number_request" value="">
        <input id="pupil_task_id" type="hidden" name="pupil_task_id" value="">
      </div>
      <div class="modal-footer">
        <button id="send_request_button" type="button" class="btn btn-primary">Отправить</button>
      </div>
    </div>
  </div>
</div>

<!-- Modal send mail -->
<div class="modal fade" id="not_teacher" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <div class="modal-title" id="exampleModalLabel">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            <b>! Отправлять письма и отвечать на заявки учеников могуть только пользователи с заполненым профилем учителя</b><br>
        </div>
      </div>
      <div class="modal-body">
        Чтобы стать учителем пройдите по следующей ссылке и заполните свой профиль учителя
        <input id="user_number" type="hidden" name="recipients[]" value="">
      </div>
      <div class="modal-footer">
        <a href="{{ route('tutor_account_basic') }}" class="btn btn-primary">Стать учителем</a>
      </div>
    </div>
  </div>
</div>

@endif

@endsection

@section('style')

{{ Html::style('front/select2/select2.css') }}
{{ Html::style('front/nouislider/nouislider.css') }}
{{ Html::style('front/css/flags.css') }}

@endsection

@section('script')

{{ Html::script('front/select2/select2.full.js') }}
{{ Html::script('front/nouislider/nouislider.js') }}

<script type="text/javascript">

//получаем текущий путь
var url_cur = new Url();
//выбираем параметры юрл
var params = '?'+window.location.search.replace( '?', ''); 

    //показываем или скрываем выбор города
    var subject_name = $('#subject_name');
    var city_id_zaglushka = $('#city_id_zaglushka');
    var city_id = $('#city_id');
    city_id_zaglushka.on('click', function(){
        subject_name.next().addClass("border_red");
        subject_name.next().next('.tips_hide').show();
    });

    if (subject_name.val() == null) {
        city_id.hide();
    }else{
        city_id_zaglushka.hide();
        $('#city_id').select2({
            placeholder: "Выберите город",
            minimumInputLength: 1,
            ajax: {
                url: '{{ url('/') }}/city-search-all',
                delay: 100,
                dataType: 'json',
                data: function (params, locale) {
                    var query = {
                        city: $.trim(params.term),
                        locale: $('#country_teach_id').val(),
                    };
                    return query;
                },
                processResults: function (data) {
                    return {
                        results: data
                    };
                },
                cache: true
            }
        });
    }

//скрываем выбор города если уроки онлайн
$('#local').on('click', function(){
    if ($(this).is( ":checked" )) {
        $('#city_block').show();
    }else{
        $('#city_block').hide();
    }
});
if ($('#local').is( ":checked" )) {
    $('#city_block').show();
}else{
    $('#city_block').hide();
}


//отправка сообщения ученику
var send_mail = $('.send_mail');
send_mail.on('click', function(){
    var user_id = $(this).attr('data-id_pupil');
    $('body').append('<div class="send_mail_wrapp_block"><div class="form-group send_mail_inner_block"><label class="control-label">Сообщение</label><textarea name="message" class="form-control"></textarea><input type="checkbox" name="recipients[]" value="'+user_id+'"></div></div>');
});

$('#send_user_mail').on('show.bs.modal', function (e) {

    var $button = $(e.relatedTarget); 
    var name_user = $(e.relatedTarget).closest('.one_pupil').find('.get_user_name').text();
    $('#user_number').val($button.attr('data-id_pupil'));
    $('#get_user_name').text(name_user);
})

$('#send_user_mail').on('hide.bs.modal', function (e) {
    $('#message_body').val('');
    $('.success_message').remove();
})

$('#send_mail_button').on('click', function(e){
    var $user_id = $('#user_number').val();
    var $message = $('#message_body').val();
    var modal_footer = $('#send_user_mail').find('.modal-footer')
    $.ajax({
      type: "POST",
      url: "{{ route('message.store') }}",
      data: { "_token": "{{ csrf_token() }}",
                "recipient_id" : $user_id,
                "message" : $message,
            },
      success: function(data){
            $('#message_body').val('');
            modal_footer.append('<div class="success_message">'+data+'</div>');
        }
    });
});    

//ответ на заявку ученика от репетитора
    $('#send_request_pupil').on('show.bs.modal', function (e) {
        var $button = $(e.relatedTarget); 
        var name_user = $(e.relatedTarget).closest('.one_pupil').find('.get_user_name').text();
        $('#user_number_request').val($button.attr('data-id_pupil'));
        $('#pupil_task_id').val($button.attr('data-id_task'));
        $('#get_user_name_request').text(name_user);
    })

    $('#send_request_pupil').on('hide.bs.modal', function (e) {
        $('#request_body').val('');
        $('.success_message').remove();
    }) 

    $('#send_request_button').on('click', function(e){
        var $message = $('#request_body').val();
        var pupil_id = $('#user_number_request').val();
        var pupil_task_id = $('#pupil_task_id').val();
        var modal_footer = $('#send_request_pupil').find('.modal-footer')
        $.ajax({
          type: "POST",
          url: "{{ route('request.response.task') }}",
          data: { "_token": "{{ csrf_token() }}",
                    "pupil_id" : pupil_id,
                    "pupil_task_id" : pupil_task_id,
                    "message" : $message
                },
          success: function(data){
                    $('#request_body').val('');
                    modal_footer.append('<div class="success_message">'+data+'</div>');
            }
        });
    });


//скрываем выбор города если уроки онлайн

if ($('#online').is( ":checked" )) {
    $('#city_block').hide();
}

$(function() {

    //слайдер цены
    var url_cur = new Url();
    
    var input0 = document.getElementById('input-with-keypress-0');
    var input1 = document.getElementById('input-with-keypress-1');
    var inputs = [input0, input1];
        //выбираем куки валюты
        var cod_valute = '{{ Cookie::get('cod_valute') }}';
        var set_max;
        var step;
        switch(cod_valute) {
          case 'UAH':
            set_max = 900;
            step = 5;
            break
          case 'RUB':  // if (x === 'value2')
            set_max = 7000;
            step = 10;
            break
          case 'USD':  // if (x === 'value2')
            set_max = 100;
            step = 1;
            break
          case 'KZT':  // if (x === 'value2')
            set_max = 30000;
            step = 50;
            break
          case 'BYN':  // if (x === 'value2')
            set_max = 100;
            step = 1;
            break
          default:
            set_max = 900;
            step = 5;
            break
        }
        console.log(set_max);

    var set_min = 0;
    //var set_max = 900;
    if (url_cur.query.price_min != undefined && url_cur.query.price_max != undefined) {      
        set_min = url_cur.query.price_min;
        var set_max_exist = url_cur.query.price_max;
    }else{
        set_max_exist = set_max;
    }

    var keypressSlider = document.getElementById('slider_price');
        noUiSlider.create(keypressSlider, {
        start: [set_min, set_max_exist],
        step: step,
        connect: true,
        direction: 'ltr',
        range: {
            'min': 0,
            'max': set_max
        }
    });

    

function setSliderHandle(i, value) {
    var r = [null,null];
    r[i] = value;
    keypressSlider.noUiSlider.set(r);
}

// Listen to keydown events on the input field.
inputs.forEach(function(input, handle) {

    input.addEventListener('change', function(){
        setSliderHandle(handle, this.value);
    });

    input.addEventListener('keydown', function( e ) {

        var values = keypressSlider.noUiSlider.get();
        var value = Number(values[handle]);

        // [[handle0_down, handle0_up], [handle1_down, handle1_up]]
        var steps = keypressSlider.noUiSlider.steps();

        // [down, up]
        var step = steps[handle];

        var position;

        switch ( e.which ) {

            case 13:
                setSliderHandle(handle, this.value);
                break;

            case 38:

                // Get step to go increase slider value (up)
                position = step[1];

                // false = no step is set
                if ( position === false ) {
                    position = 1;
                }

                // null = edge of slider
                if ( position !== null ) {
                    setSliderHandle(handle, value + position);
                }

                break;

            case 40:

                position = step[0];

                if ( position === false ) {
                    position = 1;
                }

                if ( position !== null ) {
                    setSliderHandle(handle, value - position);
                }

                break;
        }
    });
});



keypressSlider.noUiSlider.on('update', function( values, handle ) {
    inputs[handle].value = values[handle];
});

keypressSlider.noUiSlider.on('change', function(values, handle){

        var url_cur = new Url();
        var price_min = keypressSlider.noUiSlider.get()[0].split('.')[0];
        var price_max = keypressSlider.noUiSlider.get()[1].split('.')[0];
        console.log('Изменили мин'+' '+price_min);
        console.log('Изменили мах'+' '+price_max);
        url_cur.query.price_min = price_min;
        url_cur.query.price_max = price_max;
        var url = url_cur.toString();
        window.location.href = url;
    
});


    //выбор предмета для поиска
    $('#subject_name').select2({
        tags: false,
        placeholder: "Выберите предмет",
        minimumInputLength: 1,
        ajax: {
            url: '{{ url('/') }}/subject-search-name',
            delay: 100,
            dataType: 'json',
            data: function (params) {
                var query = {
                    subject: $.trim(params.term)
                };
                return query;
            },
            processResults: function (data) {
                return {
                    results: data
                };
            },
            cache: true
        }
    });

    //отправляет пользователя после выбора предмета
    $('select#subject_name').on('select2:select', function (evt) {

        var city = $('#city_id').val();
        if (city == null) {
            var city = 'all-city';
        }
        var subject = $('#subject_name').val();
        var url = '{{ route("task_search_subject_city", [":subject", ":id"]) }}';
        url = url.replace(':id', city);
        url = url.replace(':subject', subject);

        if (params.length > 3) {
            url += params;
        }

        window.location.href = url;
    });

    //отправляет пользователя после выбора города
    $('select#city_id').on('select2:select', function (evt) {

        var city = $('#city_id').val();
        @if(isset($data_tasks['subject']) and $data_tasks['subject'] != '')
            var subject = {{ $data_tasks['subject']->id }};
        @else
            var subject = 'all';
        @endif
           
        var url = '{{ route("task_search_subject_city", [":subject", ":id"]) }}';
        url = url.replace(':id', city);
        url = url.replace(':subject', subject);

        if (params.length > 3) {
            url += params;
            url = url.replace("online", 'local');
        }else{
            url += '?type_lessons=local';
        }

        window.location.href = url;
    });
    //отправляет пользователя после выбора типа урока online
    $('#type_lessons #online').click( function (evt) {

        var url_cur = new Url();

        var params = $("input:checked").val();

        if (params == 'online') {

            var x  = url_cur.paths();
            url_cur.paths([x[0], x[1], 'all-city']);

        }

        url_cur.query.type_lessons = params;
        var url = url_cur.toString();
        window.location.href = url;
        
    });  
    //отправляет пользователя после выбора пола репетитора
    $('#gender input').click( function (evt) {

        var url_cur = new Url();
        var params = $("#gender input:checked").val();
        url_cur.query.gender = params;
        if (params == 'mw') {
            delete url_cur.query.gender;
        }
        var url = url_cur.toString();
        window.location.href = url;
        
    });    
    //отправляет пользователя после выбора возраста ученика
    $('#pupil_age input').click( function (evt) {

        var url_cur = new Url();

        var inputs = $('#pupil_age input');

        var params = '';

        $.each(inputs, function(index, value){
            if ($(this).prop("checked")) {
                params += ($(this).val());
            }
            
        });

        if (params != '') {
            url_cur.query.p_age = params;
        }else{
            delete url_cur.query.p_age
        }

        var url = url_cur.toString();
        console.log(url);
        window.location.href = url;
        
    });   
    //раскрытие заявки
    $('.task_title, .show_task').on('click', function(){
        var task_about = $(this).closest('.one_row').find('.task_about');
        var show_task = $(this).closest('.one_row').find('.show_task');
        if (task_about.css('max-height') == 'none') {
            show_task.text('Развернуть заявку');
            show_task.append('<i class="fa fa-angle-down" aria-hidden="true"></i>');
            task_about.css({"maxHeight":"37px"});
        }else{
            task_about.css({"maxHeight":"none"});
            show_task.text('Свернуть заявку');
            show_task.append('<i class="fa fa-angle-up" aria-hidden="true"></i>');
        }
        
    });
});

$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip(); 
});

</script>

@endsection

