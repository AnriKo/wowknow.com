@extends('layouts/app_sidebar')

@section('title_page')
  Создание обьявления на поиск учителя
@endsection

@section('side_bar')

 @include('parts/user_menu')

@endsection

@section('content')

<div id="tutor_block" class="panel panel-default">
    <div class="panel-body">

      <div id="second_title">
        <h2>Заполните поля и сохраните обьявление</h2>
      </div>

        {!! Form::open(['route' => 'create_new_task_post']) !!}

        <div style="margin-bottom:15px;" class="col-md-12 no_padding">
        <p class="info_tips"><i class="fa fa-info-circle" aria-hidden="true"></i>Поля обозначены <span style="font-size:140%;">*</span> обязательны для заполнения.</p>

          <div class="col-lg-12 col-md-12 col-ms-12 col-xs-12 one_form_block">
            <label class="col-lg-2 col-md-3 col-sm-12  col-xs-12 no_padding">Предмет обучения<span class="require"></span></label>
            <div class="col-md-5 col-lg-7 col-sm-12 col-xs-12">
              <select required name="subject_id" class="subject_name js-example-basic-multiple form-control" ></select>
            </div>
            <div class="tips col-lg-3 col-md-4 col-sm-12 col-xs-12">Укажите предмет по которому хотите найти учителя.</div>
          </div>

          <div class="col-lg-12 col-md-12 col-ms-12 col-xs-12 one_form_block">
            <label class="control-label col-lg-2 col-md-12 col-sm-12 col-xs-12 no_padding">Ваше обьявление, подробности<span class="require"></span></label>
            <div class="col-md-12 col-lg-7 col-sm-12 col-xs-12">
              <textarea required class="form-control" name="task_text" id="" cols="40" rows="20"></textarea>
            </div>  
            <div class="tips col-lg-3 col-md-12 col-sm-12 col-xs-12">Опишите подробнее детали, например возраст ученика, уровень подготовки, время удобное для занятий</div>
          </div>

          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 one_form_block">
            <label class="control-label col-lg-2 col-md-3 col-sm-3 col-xs-12">Ваша страна проживания<span class="require"></span></label>
            <div class="col-md-5 col-lg-5 col-sm-8 col-xs-12">
              <select data-tooltip="Сначала выберите страну проживания, потом город" required class="form-control" name="country_teach" id="country_teach_id">
                <option value="">Выбрать страну</option>
                <option value="ua">Украина</option>
                <option value="ru">Россия</option>
                <option value="by">Белорусь</option>
                <option value="kz">Казахстан</option>
              </select>
            </div>
          </div>

          <div style="border-bottom: 1px solid #dedddd; padding-bottom: 8px;" class="col-lg-12 col-md-12 col-ms-12 col-xs-12 one_form_block">
            <label class="control-label col-lg-2 col-md-3 col-sm-3 col-xs-12">Место проведения занятий<span class="require"></span></label>

            <div data-tooltip="Выбрите одно или несколько значений" id="teach_place_checkbox" class="teach_place_group checkbox_group col-md-9 col-lg-7 col-sm-9 col-xs-12">
              <div class="checkbox">
                  <label><input class="teach_place_inputs" name="teach_place[online]" id="is_skype_lessons" type="checkbox" value="1">Уроки по Skype (онлайн)</label>
              </div>
              <div class="checkbox">
                  <label><input class="teach_place_inputs" id="is_local_lessons" name="teach_place[local]" type="checkbox" value="1">Локальные (в моем городе)</label>
              </div>
            </div>

            <div class="tips col-lg-3 col-md-4 col-sm-12 col-xs-12">Выбрите одно или несколько значений</div>

            <div id="local_teach_detail" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
  
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 one_form_block">  
                <label class="control-label col-lg-2 col-md-3 col-sm-3 col-xs-12">Ваш город<span class="require"></span></label>
                <div class="col-md-5 col-lg-5 col-sm-8 col-xs-12">
                  <select required name="teach_city_en" data-ajax--cache="true" id="city_id" class="form-control js-data-example-ajax">
                  </select>
                </div>  
              </div>

              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 one_form_block">
                <label class="control-label col-lg-2 col-md-2 col-sm-2 col-xs-12">Место проведения локальных занятий<span class="require"></span></label>
                <div data-tooltip="Выбрите одно или несколько значений" class="localteach_place checkbox_group col-md-6 col-lg-6 col-sm-6 col-xs-12" id="localteach_place">
                  <div class="checkbox">
                      <label><input class="teach_place_local" name="teach_local_pupil" type="checkbox" value="1">На територии ученика (учитель приезжает к вам)</label>
                  </div>
                  <div class="checkbox">
                      <label><input class="teach_place_local" name="teach_local_teacher" type="checkbox" value="1">На територии учителя</label>
                  </div>
                </div>
              </div>

            </div>

            <div id="skype_login_block" class="form-group col-lg-12 col-md-12 col-ms-12 col-xs-12">
              <label class="control-label col-lg-2 col-md-3 col-sm-3 col-xs-12">Ваш логин Skype<span class="require"></span></label>
              <div class="col-md-5 col-lg-5 col-sm-8 col-xs-12">
                <input value="{{ Auth::user()->skype }}" name="skype_login" type="text" class="form-control"/>
              </div>
            </div>

          </div>

          <div class="col-lg-12 col-md-12 col-ms-12 col-xs-12 one_form_block">
            <label class="control-label col-lg-2 col-md-3 col-sm-3 col-xs-12">Желаемый пол репетитора</label>
            <div class="col-md-9 col-lg-10 col-sm-9 col-xs-12">
              <label class="gender"><input checked value="mw" type="radio" name="gender">Не важно</label>
              <label class="gender"><input value="man" type="radio" name="gender">Мужской</label>
              <label class="gender"><input value="woman" type="radio" name="gender">Женский</label>
            </div>  
          </div>

          <div class="col-lg-12 col-md-12 col-ms-12 col-xs-12 one_form_block">
            <label class="control-label col-lg-2 col-md-3 col-sm-3 col-xs-12">
              Возраст ученика<span class="require"></span>
            </label>
            <div data-tooltip="Выбрите одно или несколько значений"  class="pupil_age_checbox_group col-md-5 col-lg-5 col-sm-8 col-xs-12">
              <select required class="form-control" name="pupil_age">
                <option selected value="">Выберите возраст ученика</option>
                <option value="a1">Малыш до 3 лет</option>
                <option value="a2">Ребенок (4-6)</option>
                <option value="a3">Младший школьник (6-12)</option>
                <option value="a4">Школьник (12-17)</option>
                <option value="a5">Студент (17-23)</option>
                <option value="a6">Взрослый (23-40)</option>
                <option value="a7">Взрослый (40+)</option>
              </select>
            </div>
          </div>

          <div id="price_block" class="col-lg-12 col-md-12 col-ms-12 col-xs-12 one_form_block">
            <label class="control-label col-lg-2 col-md-3 col-sm-3 col-xs-12">Стоимость урока<span class="require"></span></label>
            <div class="col-md-5 col-lg-7 col-sm-6 col-xs-12">
              <input required name="price_value" type="text" class="form-control"/>
              <select class="form-control" name="price_valute">
                <option @if (Cookie::get('cod_valute') == 'UAH') selected="selected" @endif value="UAH">грн</option>
                <option @if (Cookie::get('cod_valute') == 'RUB') selected="selected" @endif value="RUB">руб</option>
                <option @if (Cookie::get('cod_valute') == 'USD') selected="selected" @endif value="USD">$</option>
                <option @if (Cookie::get('cod_valute') == 'KZT') selected="selected" @endif value="KZT">теңге</option>
                <option @if (Cookie::get('cod_valute') == 'BYN') selected="selected" @endif value="BYN">белор. руб</option>
              </select>
            </div>
            <div class="tips col-lg-3 col-md-4 col-sm-3 col-xs-12">Укажите приблизительную цену одного занятия которую вы готовы оплачивать</div>
          </div>

          <div class="col-lg-12 col-md-12 col-ms-12 col-xs-12 one_form_block">
            <label for="tutor_name" class="control-label col-lg-2 col-md-3 col-sm-3 col-xs-12">Ваше имя<span class="require"></span></label>
            <div class="col-md-5 col-lg-5 col-sm-8 col-xs-12">
              <input required @if (Auth::check()) value="{{ Auth::user()->name }}" @endif id="tutor_name" name="name" type="text" class="form-control"/>
            </div>  
          </div>

          <div class="col-lg-12 col-md-12 col-ms-12 col-xs-12 one_form_block">
            <label for="pupil_name" class="control-label col-lg-2 col-md-3 col-sm-3 col-xs-12">Имя ученика<span class="require"></span></label>
            <div class="col-md-5 col-lg-5 col-sm-8 col-xs-12">
              <input required id="pupil_name" name="pupil_name" type="text" class="form-control"/>
            </div>  
          </div>

          <div class="col-lg-12 col-md-12 col-ms-12 col-xs-12 one_form_block">
            <label for="tutor_email" class="control-label col-lg-2 col-md-3 col-sm-3 col-xs-12">Ваш Email<span class="require"></span></label>
            <div class="col-md-5 col-lg-5 col-sm-8 col-xs-12">
              <input required @if (Auth::check()) value="{{ Auth::user()->email }}" @endif id="tutor_email" name="email" type="text" class="form-control"/>
            </div>  
          </div>    
                
          <div class="col-lg-12 col-md-12 col-ms-12 col-xs-12 one_form_block">
            <label for="tutor_phone" class="control-label col-lg-2 col-md-3 col-sm-3 col-xs-12">Телефон</label>
            <div class="col-md-5 col-lg-5 col-sm-8 col-xs-12">
              <input id="tutor_phone" name="phone" type="text" class="form-control"/>
            </div>  
          </div>
 
        </div>

        <div id="save-tutor-info-wrap">
          <input id="save-tutor-info"  class="btn btn-success" value="Сохранить и опубликовать заявку" type="submit">
        </div>

        {!! Form::close() !!}

    </div>
</div>

@endsection

@section('style')

{{ Html::style('front/select2/select2.css') }}

@endsection

@section('script')

{{ Html::script('front/select2/select2.full.js') }}

<script type="text/javascript">

$(function() {

//вставка предметов
$('.subject_name').select2({
    tags: false,
    placeholder: "Выберите предмет",
    minimumInputLength: 1,
    ajax: {
        url: '{{ url('/') }}/subject-search-name',
        delay: 100,
        dataType: 'json',
        data: function (params) {
            var query = {
                subject: $.trim(params.term)
            };
            return query;
        },
        processResults: function (data) {
            return {
                results: data
            };
        },
        cache: true
    }
});

$('.subject_name').on('select2:select', function (evt) {
  change_sub_to_main_subject(this);
});

function change_sub_to_main_subject(var_this){

  var subject_id = $(var_this).val();
  $.ajax({
    type: "GET",
    url: "{{ route('change_sub_to_main_subject') }}",
    data: {subject_id:subject_id},
    success: function(data){
      console.log('Hello change_sub_to_main_subject');
        if (data != '0') {
          var option = new Option(data['name'], data['id'], true, true);
          $(var_this).append(option).trigger('change');
        }
      }
    });
}

$('#city_id').select2({
    placeholder: "Выберите свой город",
    minimumInputLength: 2,
    ajax: {
        url: '{{ url('/') }}/city-search',
        delay: 100,
        dataType: 'json',
        beforeSend: function(){ 
            var country_teach_id = $('#country_teach_id');
            if(country_teach_id.val() == ''){
                country_teach_id.css('border', '3px solid #ffb23e');
                showTip(country_teach_id);
                setTimeout( function(){
                  country_teach_id.css('border','none');
                },5000);
                $('html, body').animate({
                    scrollTop: country_teach_id.offset().top
                }, 1000);
            }
        },
        data: function (params, locale) {
            var query = {
                city: $.trim(params.term),
                locale: $('#country_teach_id').val(),
            };
            return query;
        },
        processResults: function (data) {
            return {
                results: data
            };
        },
        cache: true
    }
});

// проверки обязательных чекбоксов на пустоту
$('#save-tutor-info').on('click', function(event){
  valthis(event);
});  

function valthis(submit_button) {
var checkBoxes = document.getElementsByClassName( 'teach_place_inputs' );
var isChecked = false;
    for (var i = 0; i < checkBoxes.length; i++) {
        if ( checkBoxes[i].checked ) {
          isChecked = true;
        };
    };
    if ( isChecked ) {

          console.log( 'At least one checkbox checked!' );
          if(checkBoxes[1].checked ){

              var teach_place_local = document.getElementsByClassName( 'teach_place_local' );
              var isChecked_next = false;
              for (var i = 0; i < teach_place_local.length; i++) {
                  if ( teach_place_local[i].checked ) {
                    isChecked_next = true;
                  };
              };

              if ( !isChecked_next ) {

                var localteach_place = $('.localteach_place').css('border', '1px solid #ffb23e');
                showTip(localteach_place);
                setTimeout( function(){
                  localteach_place.css('border','none');
                },5000);
                $('html, body').animate({
                    scrollTop: localteach_place.offset().top
                }, 1000);
                submit_button.preventDefault();
                console.log( 'Please, check at least one checkbox!' );
              }

          }else{
            
            var city_id = $('#city_id');
            city_id.removeAttr("required");

            var teach_place_local = $( '.teach_place_local' ).prop('checked', false);;
          }

    } else {

          var teach_place_group = $('.teach_place_group').css('border', '1px solid #ffb23e');
          showTip(teach_place_group);
          setTimeout( function(){
            teach_place_group.css('border','none');
          },5000);
          $('html, body').animate({
              scrollTop: teach_place_group.offset().top
          }, 1000);
          submit_button.preventDefault();
    }   
}

//функция подсказки создания
function showTip(current) {
    console.log(current);
    var tipText = current.attr('data-tooltip');
    var element = document.createElement('div');
    element.className = 'my_tooltip';
    element.innerHTML = tipText;
    $(current).append(element);
    $(element).delay( 5000 ).fadeOut('slow');
}

//выбор уроков по skype
var skype_login_block = $('#skype_login_block');
skype_login_block.hide('fast');
var is_skype_lessons = $('#is_skype_lessons');
if(is_skype_lessons.is(':checked')){
    skype_login_block.show('fast');
}
is_skype_lessons.on('change', function() {

    if (is_skype_lessons.prop('checked')) {
        skype_login_block.show();
    }else{
        skype_login_block.hide();
    }

});

//выбор уроков локальных
var local_teach_detail = $('#local_teach_detail');
local_teach_detail.hide('fast');
var is_local_lessons = $('#is_local_lessons');
if(is_local_lessons.is(':checked')){
    local_teach_detail.show('fast');
}
is_local_lessons.on('change', function() {

    if (is_local_lessons.prop('checked')) {
        local_teach_detail.show();
    }else{
        local_teach_detail.hide();
    }

});




});

</script>

@endsection

      

