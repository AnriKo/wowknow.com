@extends('layouts/app_sidebar_right')

@php

@endphp

@section('title_meta')
    {{$data_courses['title']}}, найти
@endsection

@section('title_page')
    {{$data_courses['title']}}
@endsection

@section('side_bar')
    @include('parts/adsense_sidebar')
@endsection

@section('content')

<?php $name = "name_".$local;  $title_loc = "title_".$local; ?>

<div id="search_page" class="panel-default">

    <div id="top_current_page_menu" class="no_margin row">

        <div style="padding-right: 15px" class="one_block_menu col-lg-6 col-md-6 col-sm-6 col-xs-12 no_padding">
            <select style="width: 100%" name="city_id" id="city_id">
                <option value="city">Выберите город</option>
                @foreach($cities_course as $one_city)
                    <option @if($one_city->city == $data_courses['city']) selected @endif value="{{$one_city->city}}">
                        @if($one_city->country == 'ua')
                            {{ $one_city->city_ua->title_ru }}
                        @elseif($one_city->country == 'ru')
                            {{ $one_city->city_ru->title_ru }}
                        @elseif($one_city->country == 'kz')
                            {{ $one_city->city_kz->title_ru }}
                        @endif
                    </option>
                @endforeach
            </select>
            
        </div>

        <div class="one_block_menu col-lg-6 col-md-6 col-sm-6 col-xs-12 no_padding">
            <select name="subject_id" id="subject_name_course" class="js-example-basic-single form-control" >
                <option value="subject">Выберите предмет</option>
                @foreach($courses_group as $group) 
                    <optgroup label="{{ $group->{$name} }}">
                        @if($data_courses['subject'] != '')
                          @foreach($group->company_cours_cat as $one_cat)
                            <option @if($data_courses['subject'] == $one_cat->alias) selected @endif value="{{ $one_cat->alias }}">{{ $one_cat->{$name} }}</option>
                          @endforeach
                        @else
                          @foreach($group->company_cours_cat as $one_cat)
                            <option value="{{ $one_cat->alias }}">{{ $one_cat->{$name} }}</option>
                          @endforeach
                        @endif  
                    </optgroup>      
                @endforeach
            </select>

        </div>

    </div>

    <div class="panel-body">

        <div class="row" id="top_courses"></div>

        @foreach($courses as $cours)

                <div class="@if($cours->top == '1') top_item @endif row one_row one_courses">
                    <div class="col-lg-7 col-md-7 col-sm-6 col-xs-12 no_padding">
                        <div class="name">
                            <a href="{{ route('cours_one_show', ['alias' => $cours->alias ]) }}">
                                {{ $cours->name }}
                            </a>
                        </div>
                        <div class="cours_location"><i class="fa fa-map-marker" aria-hidden="true"></i> 
                                @if( $cours->country == 'ru')
                                    @if(isset($cours->city_ru->{$title_loc}))
                                        <span class="city">
                                            {{-- <span class="flag flag-ru"></span> --}}
                                            {{ $cours->city_ru->{$title_loc} }}
                                        </span>    
                                    @endif    
                                @elseif( $cours->country == 'ua')
                                    @if(isset($cours->city_ua->{$title_loc}))

                                        <span class="city">
                                            {{ $cours->city_ua->{$title_loc} }}
                                            {{-- <span class="flag flag-ua"></span> --}}
                                        </span>

                                    @endif     
                                @elseif( $cours->country == 'kz')
                                    @if(isset($cours->city_kz->{$title_loc}))
                                        
                                        <span class="city">
                                            {{-- <span class="flag flag-kz"></span> Казахстан<br> --}}
                                            {{ $cours->city_kz->{$title_loc} }}
                                        </span><br>
                                        
                                    @endif    
                                @endif

                                @if($cours->street)
                                    <span class="street">
                                        - ({{ $cours->street }})
                                    </span><br>
                                @endif
                        </div>
                        <div class="profile_info">
                            @if($cours->type == '0')
                                <div class="online_course">
                                    <i style="color: #8957ff" class="fa fa-globe"></i> 
                                    <span class="sub_title">Онлайн курс</span> <br>
                                </div>
                            @endif

                            <i style="color: orange" class="fa fa-tag" aria-hidden="true"></i>
                            <div class="teach_subjects">
                                @foreach($cours->cats as $cat)

                                   <span class="one_subject"> {{ $cat->name_ru }} </span>

                                @endforeach
                            </div>
                        </div> 
                    </div> 
                    <div class="col-lg-5 col-md-5 col-sm-6 col-xs-12 no_padding company_info"> 

                        <p>

                            @if($cours->logo)
                                <a href="{{ route('cours_one_show', ['alias' => $cours->alias ]) }}">
                                    <img class="company_avatar" src="{{ URL::asset('storage/companies/logos') }}/{{ $cours->logo }}" alt="avatar_tutor_{{ $cours->name }}">
                                </a>     
                            @endif
                            {{ mb_strimwidth(strip_tags($cours->description), 0, 150, "...") }} 
                               
                        </p>


  
                    </div>
                
        </div>

        @endforeach
        
        <div id="pagination_wrapper">
            {{ $courses->render() }}
        </div>    

@if(Auth::check())

<!-- Modal send mail -->
    <div class="modal fade" id="send_user_mail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <div class="modal-title" id="exampleModalLabel"><span id="get_user_name"></span> отправление сообщения<img  id="get_user_avatar" src="/" alt="user avatar"></div>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <textarea placeholder="Введите собщение" id="message_body" name="message" class="form-control"></textarea>
            <input id="user_number" type="hidden" name="recipients[]" value="">
          </div>
          <div class="modal-footer">
            <button id="send_mail_button" type="button" class="btn btn-primary">Отправить</button>
          </div>
        </div>
      </div>
    </div>
    
@endif

    </div>
</div>
@endsection

@section('style')

{{ Html::style('front/select2/select2.css') }}
{{ Html::style('front/css/flags.css') }}

<style>

</style>

@endsection

@section('script')

{{ Html::script('front/select2/select2.full.js') }}
<script type="text/javascript">

$('.more_courses').on('click', function(){
    $(this).next( ".this_company_courses").toggle(); 
}); 

//получаем текущий путь
var url_cur = new Url();
//выбираем параметры юрл
var params = '?'+window.location.search.replace( '?', ''); 


    //показываем или скрываем выбор города
    var subject_name_course = $('#subject_name_course');

    $("#subject_name_course").select2({
      tags: false,
      maximumSelectionLength: 20,
    });

    $("#city_id").select2({
      tags: false,
      maximumSelectionLength: 20,
    });

   
    //отправляет пользователя после выбора предмета
    $('select#subject_name_course').on('select2:select', function (evt) {

        var city = $('#city_id').val();
        if (city == null) {
            var city = 'city';
        }
        var subject = $('#subject_name_course').val();
        var url = '{{ route("courses_basic_search", [":subject", ":id"]) }}';
        url = url.replace(':id', city);
        url = url.replace(':subject', subject);

        if (params.length > 3) {
            url += params;
        }

        window.location.href = url;
    });

    //отправляет пользователя после выбора города
    $('select#city_id').on('select2:select', function (evt) {

        var url_cur = new Url();
        var city = $('#city_id').val();
        var subject = $('#subject_name_course').val();

           
        var url = '{{ route("courses_basic_search", [":subject", ":id"]) }}';
        url = url.replace(':id', city);
        if(subject != ''){
            url = url.replace(':subject', subject);
        }
        

        if (params.length > 3) {
            url += params;
            url = url.replace("online", 'local');
        }else{
            url += '?type_lessons=local';
        }
        

        window.location.href = url;
    });


$( document ).ready(function() {

    $.ajax({
      type: "POST",
      url: "{{ route('top_courses_block') }}",
      data: { "_token": "{{ csrf_token() }}",
                "type" : 'courser',
            },
      success: function(data){
            if(data != 0){
                $('#top_courses').before('<h3 id="top_courses_label">Топ-курсы</h3>');
                $('#top_courses').append(data);
                $('#top_courses').fadeIn();
            }


        }
    });

}); 



</script>

@endsection

