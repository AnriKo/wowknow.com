@extends('layouts/app_sidebar')

@section('title_page')
  Редактирование курса - "{{ $cours->course_name }}"
@endsection

@section('side_bar')

  @include('parts/edit_course_menu')

@endsection

@section('content')

@php $locale = App::getLocale() @endphp

{{-- dd($langs) --}}

<div id="tutor_block" class="panel panel-default">
    <div id="company_block" class="panel-body">

      <div id="second_title">
        <h2><a href="{{ route('company_update') }}">Информация о компании</a></h2>
        <h2><a href="{{ route('courses_all') }}">Курсы компании</a></h2>
        <h2 class="active">Редактирование курса</h2>
      </div>

        {!! Form::open(['route' => 'course_edit_save']) !!}

        <div class="col-lg-12 col-md-12 col-ms-12 col-xs-12 one_form_block">
          <label for="tutor_name" class="control-label col-lg-2 col-md-12 col-sm-12 col-xs-12">Название&nbsp; курса<span class="require"></span></label>
          <div class="col-md-12 col-lg-10 col-sm-12 col-xs-12">

              <!-- Tab panes -->
              <ul class="nav nav-pills">
                  @foreach($langs as $lang_code => $element)
                      <li @if($loop->first) class="active" @endif >
                          <a href="#conpany_name_{{$lang_code}}" data-toggle="tab">
                            <i class="fa fa-pencil" aria-hidden="true"></i>
                            {{App\MyHelpers\Helper::lang_name($lang_code)}}
                            <i class="fa fa-check-square"></i>
                          </a>
                      </li>
                  @endforeach
              </ul>

              <div class="tab-content clearfix">

                @foreach($langs as $lang_code => $element)

                  @if($element != null)
                    <div class="tab-pane @if($loop->first) active @endif" id="conpany_name_{{$lang_code}}">
                      <input @if($loop->first) required value="{{ $cours->course_name }}" @else value="{{ $element->course_name }}" @endif  id="id_conpany_name_{{$lang_code}}" name="lang_{{ $loop->iteration }}[name]" type="text" class="form-control input_lang"/>
                    </div>
                  @else
                    <div class="tab-pane @if($loop->first) active @endif" id="conpany_name_{{$lang_code}}">
                        <input @if($loop->first) required value="{{ $cours->course_name }}" @endif id="id_conpany_name_{{$lang_code}}" name="lang_{{ $loop->iteration }}[name]" type="text" class="form-control input_lang"/>
                    </div>
                  @endif  

                @endforeach

              </div>
            
          </div>  
        </div>

        <div class="col-lg-12 col-md-12 col-ms-12 col-xs-12 one_form_block">
          <label for="tutor_name" class="control-label col-lg-2 col-md-12 col-sm-12 col-xs-12">Категории&nbsp; курса<span class="require"></span></label>

          <div data-tooltip="Выбрите одну или несколько категорий к которым относятся ваши курсы" id="accordion" class="course_checbox_group col-md-10 col-lg-8 col-sm-12 col-xs-12">

            {{-- dump($saved_cat) --}}

            @foreach($courses_group as $group) 

            <div class="group">
              <div class="" id="headingOne_{{ $group->id }}">
                <div class="group_name" data-toggle="collapse" data-target="#collapseOne__{{ $group->id }}" aria-expanded="false" aria-controls="collapseOne_{{ $group->id }}"><i style="font-size: 90%;color: #b4d4d4;" class="fa fa-plus-square"></i> {{ $group->name_ru }} <span class="count_checked"></span></div>
              </div>
              <div id="collapseOne__{{ $group->id }}" class="collapse" aria-labelledby="headingOne_{{ $group->id }}" data-parent="#accordion">
                <div class="group-body">

                  @foreach($group->company_cours_cat as $one_cat)
                    <div class='course_cat '>
                      <input

                        @foreach($saved_cat as $one_saved_cat)
                          {{ $one_cat->id == $one_saved_cat->cat_id ? 'checked' : '' }}
                        @endforeach

                      class='css-checkbox course_cat_checkbox' name='course_cat[]' id="chk_{{ $one_cat->id }}" type='checkbox' value='{{ $one_cat->id }}' />
                      <label class='css-label' for='chk_{{ $one_cat->id }}'>{{ $one_cat->name_ru }}</label>
                    </div>
                  @endforeach

                </div>  
              </div> 
            </div>  

            @endforeach

          </div>  
        </div>

        <div style="padding-right: 14px;" id="tutor_education_text" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 one_form_block">
          <label class="control-label col-lg-2 col-md-12 col-sm-12 col-xs-12">Описание курса<span class="require"></span></label>
          <div class="col-md-12 col-lg-10 col-sm-12 col-xs-12">
            <ul class="nav nav-pills">

              @foreach($langs as $lang_code => $element)

                  <li @if($loop->first) class="active" @endif >
                      <a href="#conpany_desc_{{$lang_code}}" data-toggle="tab">
                        <i class="fa fa-pencil" aria-hidden="true"></i>
                        {{App\MyHelpers\Helper::lang_name($lang_code)}}
                        <i class="fa fa-check-square"></i>
                      </a>
                  </li>

              @endforeach

            </ul>

            <div class="tab-content clearfix">
                @foreach($langs as $lang_code => $element)
                  @if($element != null)

                    <div class="tab-pane @if($loop->first) active @endif" id="conpany_desc_{{$lang_code}}">
                      <textarea class="form-control input_lang" name="lang_{{ $loop->iteration }}[desc]" id="id_conpany_desc_{{$lang_code}}" cols="40" rows="16">{{ $element->course_desc }}</textarea>
                    </div>

                  @else

                    <div class="tab-pane @if($loop->first) active @endif" id="conpany_desc_{{$lang_code}}">
                      <textarea @if($loop->first) required @endif class="form-control input_lang" name="lang_{{ $loop->iteration }}[desc]" id="id_conpany_desc_{{$lang_code}}" cols="40" rows="16">@if($loop->first){{ $cours->course_desc }}@endif</textarea>
                    </div>

                  @endif  
                @endforeach
            </div>

          </div>
        </div>  

        <div class="col-lg-12 col-md-12 col-ms-12 col-xs-12 one_form_block bd-top">
          <label class="control-label col-lg-2 col-md-2 col-sm-12 col-xs-12">
            Возраст аудитории&nbsp;&nbsp;&nbsp;<span class="require"></span>
          </label>
          <div data-tooltip="Выбрите одно или несколько значений" class="checkbox_group pupil_age_checbox_group col-md-5 col-lg-5 col-sm-12 col-xs-12">
{{--             <div class="checkbox">
                <input id="a1" @if (stristr($cours->pupil_age, 'a1'))  checked="checked" @endif class="pupil_age_input css-checkbox" name="pupil_age[]" type="checkbox" value="a1">
                <label for="a1" class="css-label">Малыши до 3 лет</label>
            </div> --}}
            <div class="checkbox">
              <input id="a2" @if (stristr($cours->pupil_age, 'a2'))  checked="checked" @endif class="pupil_age_input css-checkbox" name="pupil_age[]" type="checkbox" value="a2">
              <label for="a2" class="css-label">Дети (до 6)</label>
            </div>
            <div class="checkbox">
              <input id="a3" @if (stristr($cours->pupil_age, 'a3'))  checked="checked" @endif class="pupil_age_input css-checkbox" name="pupil_age[]" type="checkbox" value="a3">
              <label for="a3" class="css-label">Младшие школьники (6-12)</label>
            </div>
            <div class="checkbox">
              <input id="a4" @if (stristr($cours->pupil_age, 'a4'))  checked="checked" @endif class="pupil_age_input css-checkbox" name="pupil_age[]" type="checkbox" value="a4">
              <label for="a4" class="css-label">Школьники (12-17)</label>
            </div>
            <div class="checkbox">
              <input id="a5" @if (stristr($cours->pupil_age, 'a5'))  checked="checked" @endif class="pupil_age_input css-checkbox" name="pupil_age[]" type="checkbox" value="a5">
              <label for="a5" class="css-label">Студенты (17-23)</label>
            </div>                            
            <div class="checkbox">
              <input id="a6" @if (stristr($cours->pupil_age, 'a6'))  checked="checked" @endif class="pupil_age_input css-checkbox" name="pupil_age[]" type="checkbox" value="a6">
              <label for="a6" class="css-label">Взрослые (23-40)</label>
            </div>
            <div class="checkbox">
              <input id="a7" @if (stristr($cours->pupil_age, 'a7'))  checked="checked" @endif class="pupil_age_input css-checkbox" name="pupil_age[]" type="checkbox" value="a7">
              <label for="a7" class="css-label">Взрослые (40+)</label>
            </div>
          </div>
        </div>

        <div style="margin-bottom: 15px;" id="price_block" class="col-lg-12 col-md-12 col-ms-12 col-xs-12 one_form_block bd-top">
          <label class="control-label col-lg-2 col-md-2 col-sm-12 col-xs-12">Стоимость</label>
          <div class="col-md-10 col-lg-10 col-sm-12 col-xs-12">
            <input value="{{ $cours->price }}" name="price_value" type="text" class="form-control"/>
            <select class="form-control" name="price_valute">
              <option @if ($cours->currency == 'UAH') selected="selected" @endif value="UAH">грн</option>
              <option @if ($cours->currency == 'RUB') selected="selected" @endif value="RUB">руб</option>
              <option @if ($cours->currency == 'USD') selected="selected" @endif value="USD">$</option>
              <option @if ($cours->currency == 'BYN') selected="selected" @endif value="BYN">белор. руб</option> 
              <option @if ($cours->currency == 'KZT') selected="selected" @endif value="KZT">теңге</option>
            </select>
            <input @if ($cours->free == "1") checked @endif class="css-checkbox" id="price_free" name="price_free" type="checkbox" value="1">
            <label for="price_free" class="price_free css-label" style="margin-left: 25px">Бесплатный курс</label>
          </div>  
        </div>

        <div class="col-lg-12 col-md-12 col-ms-12 col-xs-12 one_form_block">
          <label for="tutor_name" class="control-label col-lg-2 col-md-12 col-sm-12 col-xs-12">Тип курса<span class="require"></span></label>
          <div id="gender" class="col-md-12 col-lg-10 col-sm-12 col-xs-12">

            <input @if ($cours->type == "1") checked @endif id="local" class="css-checkbox" value="1" type="radio" name="type">
            <label for="local" class="css-label">
                Локальный <span style="color: #ccc">(занятия по адресу указаном в контактах компании)</span>
            </label> <br>

            <input @if ($cours->type == "0") checked @endif id="online" class="css-checkbox" value="0" type="radio" name="type">
            <label for="online" class="css-label">
                Онлайн <span style="color: #ccc"></span>
            </label>
            
          </div>  
        </div>

        <div class="col-lg-12 col-md-12 col-ms-12 col-xs-12 one_form_block">
          <label for="tutor_name" class="control-label col-lg-2 col-md-12 col-sm-12 col-xs-12">Статус<span class="require"></span></label>
          <div id="gender" class="col-md-12 col-lg-10 col-sm-12 col-xs-12">

            <input id="active" class="css-checkbox" @if ($cours->active == "1") checked @endif value="active" type="radio" name="status" required>
            <label for="active" class="css-label">
                Активный <span style="color: #ccc">(доступен для просмотра на сайте)</span>
            </label>

            <input id="no_active" class="css-checkbox" @if ($cours->active == "0") checked @endif value="no_active" type="radio" name="status">
            <label for="no_active" class="css-label">
              Не активный <span style="color: #ccc">(недоступен для просмотра а сайте)</span>
            </label>
            
          </div>  
        </div>

        @foreach($langs as $code => $val)
          <input type="hidden" name="lan_code_{{ $loop->iteration }}" value="{{ $code }}">
        @endforeach
        <input type="hidden" name="course" value="{{ $cours->id }}">

        <div id="save-course-info-wrap">
        
          <input id="save-course-info"  class="btn btn-success" value="Сохранить" type="submit">

        </div>

        {!! Form::close() !!}
        
    </div>
</div>

@endsection

@section('style')

{{-- Html::style('front/select2/select2.css') --}}
{{ Html::style('front/editor_html/ui/trumbowyg.min.css') }}

@endsection

@section('script')

{{-- Html::script('front/select2/select2.full.js') --}}
{{ Html::script('front/cropie_avatar/jquery.cropit.js') }}
{{ Html::script('front/editor_html/trumbowyg.min.js') }}
{{ Html::script('front/editor_html/langs/'. $locale .'.min.js') }}

<script type="text/javascript">

$(function() {

  $('textarea').trumbowyg({
      btns: [['customFormatting'], ['bold', 'italic'], ['link'], ['unorderedList', 'orderedList']],
      resetCss: true,
      lang: '{{ $locale }}',
      btnsDef: {
          customFormatting: {
              dropdown: ['h2','p'],
              text: 'Заголовок',
              title: 'Заголовок',
              hasIcon: false
          }
      },
      svgPath: false,
      hideButtonTexts: true,
  }).on('tbwchange', function(){ 

      if($(this).val() != ''){
        var attr = $(this).closest('.active').attr('id');
        $("a[href='#"+attr+"']").find('.fa-check-square').show();

      }else{
        var attr = $(this).closest('.active').attr('id');
        $("a[href='#"+attr+"']").find('.fa-check-square').hide();
      }
    
    }).on('tbwpaste', function(){ 

      if($(this).val() != ''){
        var attr = $(this).closest('.active').attr('id');
        $("a[href='#"+attr+"']").find('.fa-check-square').show();

      }else{
        var attr = $(this).closest('.active').attr('id');
        $("a[href='#"+attr+"']").find('.fa-check-square').hide();
      }

    });

  //заполненность инпутов языковых
  $('.input_lang').on('keyup', function(){
    if($(this).val() != ''){
      var attr = $(this).parent().attr('id');
      $("a[href='#"+attr+"']").find('.fa-check-square').show();
      console.log(attr);

    }else{
      var attr = $(this).parent().attr('id');
      $("a[href='#"+attr+"']").find('.fa-check-square').hide();
    }
  });

  //при загрузке страницы
  $( document ).ready(function() {
      //подсчет выбранных сохраненных категорий курсов 
      var group = $('.group');
      group.map(function() {
        var count = $(this).find('input:checked').length;
        if(count > 0){
          $(this).find('.count_checked').text("-"+count).show();
          console.log(count);
        }
      });

      //заполненность описания, названия на разных языках
      var tab_pane = $('.tab-pane');
      tab_pane.map(function() {
        var input = $(this).find('.form-control');
        if(input.val() != ""){
          var id = $(this).attr('id');
          $("a[href='#"+id+"']").find('.fa-check-square').show();
          console.log(id);
        }
      });

  });

  //подсчет выбранных категорий курсов
  $('.course_cat label').on('click', function(){
      var this_ = $(this);
      var count = this_.closest('.group').find('.count_checked');
     if(this_.siblings('input').prop('checked')){
        count.text(+count.text() + 1);
        count.show();
     }else{
        count.text(+count.text() - 1);
        count.show();
     }

    if(+count.text() == 0){
      count.text('');
      count.hide();
    }
     
  });

  // проверки обязательных чекбоксов на пустоту
$('#save-course-info').on('click', function(event){

  valthis(event);

  $('textarea').map(function(){
    var s = $(this).val();
    var result = s.replace(/style="[^"]*"/g, '');
    $(this).val(result); 
  });

}); 

function valthis(submit_button) {
  var checkBoxes = document.getElementsByClassName( 'course_cat_checkbox' );
  var isChecked = false;
  var countChecked = 0;
  for (var i = 0; i < checkBoxes.length; i++) {
      if ( checkBoxes[i].checked ) {
        isChecked = true;
        countChecked += 1;
      };
  };
  if( isChecked ){
    if(countChecked > 3){

        var course_checbox_group = $('.course_checbox_group').css('border', '1px solid #ffb23e');
        var course_checbox_group = $('.course_checbox_group');
        showTip(course_checbox_group, 'Выберите не больше 3 категорий вашего курса.');
        setTimeout( function(){
          course_checbox_group.css('border','none');
        },10000);
        $('html, body').animate({
            scrollTop: course_checbox_group.offset().top
        }, 1000);
        submit_button.preventDefault();

    } 

  }    
};  

  //функция подсказки создания
function showTip(current, tip_text) {
    console.log(current);
    var tipText = current.attr('data-tooltip');
    if(tip_text){
      var tipText = tip_text;
    }
    var element = document.createElement('div');
    element.className = 'my_tooltip';
    element.innerHTML = tipText;
    $(current).append(element);
    $(element).delay( 10000 ).fadeOut('slow');
}

  // $('.image-editor').cropit({
  //   imageState: {
      
  //   },
  //   imageBackground: true,
  //   imageBackgroundBorderWidth: 15, // Width of background border
  //   smallImage: 'stretch',
  // });

  // $('#save-course-info').click(function(event) {

  //   if ($('#upload').val() != '') {

  //     var image_crop = document.getElementById('image_crop');
  //     var imageData = $('.image-editor').cropit('export');
  //     image_crop.value = imageData;

  //   }else{

      // var label_for_upload_file = $('#label_for_upload_file').css('border', '2px solid #e74c3c');
      // showTip(label_for_upload_file);
      // setTimeout( function(){
      //   label_for_upload_file.css('border','none');
      // },5000);
      // $('html, body').animate({
      //     scrollTop: label_for_upload_file.offset().top-200
      // }, 1000);
      // event.preventDefault();
      // console.log( 'Please, check image' );

//     }
    
//   });

//   $('#upload').on('change', function(){
//     if ($(this).val() != '') {
//       $('#isset_image').hide();
//       $('.image-editor').show();
//       console.log('no empty');
//     }
//   });

//   if ($('#isset_image img').attr("alt") == 'exist-avatar') {
//     $('#label_for_upload_file span').text('Загрузить другое фото');
//   }

//   $('.prevent_default_link').on('click', function(e){
//     e.preventDefault();
//     $('#why_dont_use_link').modal('show');
//   });

 });

</script>

@endsection

      

