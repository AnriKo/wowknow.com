@extends('layouts/app_sidebar')

@section('title_page')
  Информация о курсах компании
@endsection

@section('side_bar')

@include('parts/create_course_menu')

@endsection

@section('content')

@php $locale = App::getLocale() @endphp

<div id="tutor_block" class="panel panel-default">
    <div id="company_block" class="panel-body">

      <div id="second_title">
        <h2><a href="{{ route('company_update') }}">Информация о компании</a></h2>
        <h2 ><a href="{{ route('courses_all') }}">Курсы компании</a></h2>
        <h2 class="active">Создание нового курса</h2>
      </div>

        {!! Form::open(['route' => 'course_new_save']) !!}

        <div class="col-lg-12 col-md-12 col-ms-12 col-xs-12 one_form_block">
          <label for="tutor_name" class="control-label col-lg-2 col-md-12 col-sm-12 col-xs-12">Название&nbsp; курса<span class="require"></span></label>
          <div class="col-md-12 col-lg-10 col-sm-12 col-xs-12">

              <!-- Tab panes -->
              <ul class="nav nav-pills">
                  @foreach($langs as $lang_code => $element)
                      <li @if($loop->first) class="active" @endif >
                          <a href="#conpany_name_{{$lang_code}}" data-toggle="tab">
                            <i class="fa fa-pencil" aria-hidden="true"></i>
                            {{App\MyHelpers\Helper::lang_name($lang_code)}}
                            <i class="fa fa-check-square"></i>
                          </a>
                      </li>
                  @endforeach
              </ul>

              <div class="tab-content clearfix">

                @foreach($langs as $lang_code => $element)

                    <div data-tooltip="Заполните название курса" class="tab-pane @if($loop->first) active @endif" id="conpany_name_{{$lang_code}}">
                      <input id="id_conpany_name_{{$lang_code}}" name="lang_{{ $loop->iteration }}[name]" type="text" class="form-control input_lang"/>
                    </div>

                @endforeach

              </div>
            
          </div>  
        </div>

        <div class="col-lg-12 col-md-12 col-ms-12 col-xs-12 one_form_block">
          <label for="tutor_name" class="control-label col-lg-2 col-md-12 col-sm-12 col-xs-12">Категории&nbsp; курса<span class="require"></span></label>

          <div data-tooltip="Выбрите одну или несколько категорий к которым относятся ваши курсы" id="accordion" class="course_checbox_group col-md-10 col-lg-8 col-sm-12 col-xs-12">

            {{-- dump($saved_cat) --}}

            @foreach($courses_group as $group) 

            <div class="group">
              <div class="" id="headingOne_{{ $group->id }}">
                <div class="group_name" data-toggle="collapse" data-target="#collapseOne__{{ $group->id }}" aria-expanded="false" aria-controls="collapseOne_{{ $group->id }}"><i style="font-size: 90%;color: #b4d4d4;" class="fa fa-plus-square"></i> {{ $group->name_ru }} <span class="count_checked"></span></div>
              </div>
              <div id="collapseOne__{{ $group->id }}" class="collapse" aria-labelledby="headingOne_{{ $group->id }}" data-parent="#accordion">
                <div class="group-body">

                  @foreach($group->company_cours_cat as $one_cat)
                    <div class='course_cat '>
                      <input class='css-checkbox course_cat_checkbox' name='course_cat[]' id="chk_{{ $one_cat->id }}" type='checkbox' value='{{ $one_cat->id }}' />
                      <label class='css-label' for='chk_{{ $one_cat->id }}'>{{ $one_cat->name_ru }}</label>
                    </div>
                  @endforeach

                </div>  
              </div> 
            </div>  

            @endforeach

          </div>  
        </div>

        <div style="padding-right: 14px;" id="tutor_education_text" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 one_form_block">
          <label class="control-label col-lg-2 col-md-12 col-sm-12 col-xs-12">Описание курса<span class="require"></span></label>
          <div class="col-md-12 col-lg-10 col-sm-12 col-xs-12">
            <ul class="nav nav-pills">

              @foreach($langs as $lang_code => $element)

                  <li @if($loop->first) class="active" @endif >
                      <a href="#conpany_desc_{{$lang_code}}" data-toggle="tab">
                        <i class="fa fa-pencil" aria-hidden="true"></i>
                        {{App\MyHelpers\Helper::lang_name($lang_code)}}
                        <i class="fa fa-check-square"></i>
                      </a>
                  </li>

              @endforeach

            </ul>

            <div class="tab-content clearfix">
                @foreach($langs as $lang_code => $element)

                    <div data-tooltip="Заполните описание курса" class="tab-pane @if($loop->first) active @endif" id="conpany_desc_{{$lang_code}}">
                      <textarea class="form-control input_lang" name="lang_{{ $loop->iteration }}[desc]" id="id_conpany_desc_{{$lang_code}}" cols="40" rows="16" placeholder="Как можно более подробно опишите свой курс"></textarea>
                    </div>
 
                @endforeach
            </div>

          </div>
        </div>  

        <div class="col-lg-12 col-md-12 col-ms-12 col-xs-12 one_form_block bd-top">
          <label class="control-label col-lg-2 col-md-2 col-sm-12 col-xs-12">
            Возраст аудитории&nbsp;&nbsp;&nbsp;<span class="require"></span>
          </label>
          <div data-tooltip="Выбрите одно или несколько значений" class="checkbox_group pupil_age_checbox_group col-md-5 col-lg-5 col-sm-12 col-xs-12">
{{--             <div class="checkbox">
                <input id="a1" class="pupil_age_input css-checkbox" name="pupil_age[]" type="checkbox" value="a1">
                <label for="a1" class="css-label">Малыши до 3 лет</label>
            </div> --}}
            <div class="checkbox">
              <input id="a2" class="pupil_age_input css-checkbox" name="pupil_age[]" type="checkbox" value="a2">
              <label for="a2" class="css-label">Дети (до 6)</label>
            </div>
            <div class="checkbox">
              <input id="a3" class="pupil_age_input css-checkbox" name="pupil_age[]" type="checkbox" value="a3">
              <label for="a3" class="css-label">Младшие школьники (6-12)</label>
            </div>
            <div class="checkbox">
              <input id="a4" class="pupil_age_input css-checkbox" name="pupil_age[]" type="checkbox" value="a4">
              <label for="a4" class="css-label">Школьники (12-17)</label>
            </div>
            <div class="checkbox">
              <input id="a5" class="pupil_age_input css-checkbox" name="pupil_age[]" type="checkbox" value="a5">
              <label for="a5" class="css-label">Студенты (17-23)</label>
            </div>                            
            <div class="checkbox">
              <input id="a6" class="pupil_age_input css-checkbox" name="pupil_age[]" type="checkbox" value="a6">
              <label for="a6" class="css-label">Взрослые (23-40)</label>
            </div>
            <div class="checkbox">
              <input id="a7" class="pupil_age_input css-checkbox" name="pupil_age[]" type="checkbox" value="a7">
              <label for="a7" class="css-label">Взрослые (40+)</label>
            </div>
          </div>
        </div>

        <div style="margin-bottom: 15px;" id="price_block" class="col-lg-12 col-md-12 col-ms-12 col-xs-12 one_form_block bd-top">
          <label class="control-label col-lg-2 col-md-2 col-sm-12 col-xs-12">Стоимость</label>
          <div class="col-md-10 col-lg-10 col-sm-12 col-xs-12">
            <input name="price_value" type="text" class="form-control"/>
            <select class="form-control" name="price_valute">
              <option value="UAH">грн</option>
              <option value="RUB">руб</option>
              <option value="USD">$</option>
              <option value="BYN">белор. руб</option> 
              <option value="KZT">теңге</option>
            </select>
            <input class="css-checkbox" id="price_free" name="price_free" type="checkbox" value="1">
            <label for="price_free" class="price_free css-label" style="margin-left: 25px">Бесплатный курс</label>
          </div>  
        </div>

        <div class="col-lg-12 col-md-12 col-ms-12 col-xs-12 one_form_block">
          <label for="tutor_name" class="control-label col-lg-2 col-md-12 col-sm-12 col-xs-12">Тип курса<span class="require"></span></label>
          <div id="gender" class="col-md-12 col-lg-10 col-sm-12 col-xs-12">

            <input checked id="local" class="css-checkbox" value="1" type="radio" name="type">
            <label for="local" class="css-label">
                Локальный <span style="color: #ccc">(занятия по адресу указаном в контактах компании)</span>
            </label> <br>

            <input id="online" class="css-checkbox" value="0" type="radio" name="type">
            <label for="online" class="css-label">
                Онлайн <span style="color: #ccc"></span>
            </label>
            
          </div>  
        </div>

        <div class="col-lg-12 col-md-12 col-ms-12 col-xs-12 one_form_block">
          <label for="tutor_name" class="control-label col-lg-2 col-md-12 col-sm-12 col-xs-12">Статус<span class="require"></span></label>
          <div id="gender" class="col-md-12 col-lg-10 col-sm-12 col-xs-12">

            <input checked id="active" class="css-checkbox" value="active" type="radio" name="status">
            <label for="active" class="css-label">
                Активный <span style="color: #ccc">(доступен для просмотра на сайте)</span>
            </label>

            <input id="no_active" class="css-checkbox" value="no_active" type="radio" name="status">
            <label for="no_active" class="css-label">
              Не активный <span style="color: #ccc">(недоступен для просмотра а сайте)</span>
            </label>
            
          </div>  
        </div>

        @foreach($langs as $code => $val)
          <input type="hidden" name="lan_code_{{ $loop->iteration }}" value="{{ $code }}">
        @endforeach


        <div id="save-course-info-wrap">
        
          <input id="save-course-info"  class="btn btn-success" value="Сохранить" type="submit">

        </div>

        {!! Form::close() !!}
        
    </div>
</div>

@endsection

@section('style')

{{-- Html::style('front/select2/select2.css') --}}
{{ Html::style('front/editor_html/ui/trumbowyg.min.css') }}

@endsection

@section('script')

{{-- Html::script('front/select2/select2.full.js') --}}
{{ Html::script('front/cropie_avatar/jquery.cropit.js') }}
{{ Html::script('front/editor_html/trumbowyg.min.js') }}
{{ Html::script('front/editor_html/langs/'. $locale .'.min.js') }}

<script type="text/javascript">

$(function() {

    $('textarea').trumbowyg({
      btns: [['customFormatting'], ['bold', 'italic'], ['link'], ['unorderedList', 'orderedList']],
      resetCss: true,
      lang: '{{ $locale }}',
      btnsDef: {
        customFormatting: {
            dropdown: ['h2','p'],
            text: 'Заголовок',
            title: 'Заголовок',
            hasIcon: false
        }
      },
      svgPath: false,
      hideButtonTexts: true,
    }).on('tbwchange', function(){ 

      if($(this).val() != ''){
        var attr = $(this).closest('.active').attr('id');
        $("a[href='#"+attr+"']").find('.fa-check-square').show();

      }else{
        var attr = $(this).closest('.active').attr('id');
        $("a[href='#"+attr+"']").find('.fa-check-square').hide();
      }
    
    }).on('tbwpaste', function(){ 

      if($(this).val() != ''){
        var attr = $(this).closest('.active').attr('id');
        $("a[href='#"+attr+"']").find('.fa-check-square').show();

      }else{
        var attr = $(this).closest('.active').attr('id');
        $("a[href='#"+attr+"']").find('.fa-check-square').hide();
      }

    });

  //заполненность инпутов языковых
  $('.input_lang').on('keyup', function(){
    if($(this).val() != ''){
      var attr = $(this).parent().attr('id');
      $("a[href='#"+attr+"']").find('.fa-check-square').show();
      console.log(attr);

    }else{
      var attr = $(this).parent().attr('id');
      $("a[href='#"+attr+"']").find('.fa-check-square').hide();
    }
  });

  //подсчет выбранных категорий курсов
  $('.course_cat label').on('click', function(){
      var this_ = $(this);
      var count = this_.closest('.group').find('.count_checked');
     if(this_.siblings('input').prop('checked')){
        count.text(+count.text() + 1);
        count.show();
     }else{
        count.text(+count.text() - 1);
        count.show();
     }

    if(+count.text() == 0){
      count.text('');
      count.hide();
    }
     
  });

  // проверки обязательных чекбоксов на пустоту
$('#save-course-info').on('click', function(event){

  valthis(event);
  pupil_age_required(event);

  var inputs_requared = $('.tab-content .tab-pane:first-child input, .tab-content .tab-pane:first-child textarea');
  inputs_requared.map( function(index, element) {

      if($(element).val() == ''){
        var first_child = $(element).closest('.tab-content').siblings('.nav-pills').find('li:first-child');
        first_child.find('a').tab('show');
        first_child.css('border', '1px solid orange').css('border-bottom', 'none');
        setTimeout( function(){
          first_child.css('border','none');
        },8000);
        var show_tip = $(element).closest('.tab-pane');
        showTip(show_tip);
        event.preventDefault();

          $('html, body').animate({
              scrollTop: show_tip.offset().top-200
          }, 1000);
      }

  });

  $('textarea').map(function(){
    var s = $(this).val();
    var result = s.replace(/style="[^"]*"/g, '');
    $(this).val(result); 
  });

}); 

function valthis(submit_button) {
  var checkBoxes = document.getElementsByClassName( 'course_cat_checkbox' );
  var isChecked = false;
  var countChecked = 0;
  for (var i = 0; i < checkBoxes.length; i++) {
      if ( checkBoxes[i].checked ) {
        isChecked = true;
        countChecked += 1;
      };
  };
  if( isChecked ){
    if(countChecked > 3){

        var course_checbox_group = $('.course_checbox_group').css('border', '1px solid #ffb23e');
        var course_checbox_group = $('.course_checbox_group');
        showTip(course_checbox_group, 'Выберите не больше 3 категорий вашего курса.');
        setTimeout( function(){
          course_checbox_group.css('border','none');
        },10000);
        $('html, body').animate({
            scrollTop: course_checbox_group.offset().top
        }, 1000);
        submit_button.preventDefault();

    } 

  }else{

        var course_checbox_group = $('.course_checbox_group').css('border', '1px solid #ffb23e');
        var course_checbox_group = $('.course_checbox_group');
        showTip(course_checbox_group, 'Выберите как минимум одну категорию вашего курса');
        setTimeout( function(){
          course_checbox_group.css('border','none');
        },10000);
        $('html, body').animate({
            scrollTop: course_checbox_group.offset().top
        }, 1000);
        submit_button.preventDefault();

    }    
};  

function pupil_age_required(submit_button) {
  var checkBoxes = document.getElementsByClassName( 'pupil_age_input' );
  var isChecked = false;
  var countChecked = 0;
  for (var i = 0; i < checkBoxes.length; i++) {
      if ( checkBoxes[i].checked ) {
        isChecked = true;
        countChecked += 1;
      };
  };
  if( !isChecked ){

        var pupil_age_checbox_group = $('.pupil_age_checbox_group').css('border', '1px solid #ffb23e');
        var pupil_age_checbox_group = $('.pupil_age_checbox_group');
        showTip(pupil_age_checbox_group, 'Выберите одно или несколько значений');
        setTimeout( function(){
          pupil_age_checbox_group.css('border','none');
        },10000);
        $('html, body').animate({
            scrollTop: pupil_age_checbox_group.offset().top
        }, 1000);
        submit_button.preventDefault();

  }   
};     

  //функция подсказки создания
function showTip(current, tip_text) {
    console.log(current);
    var tipText = current.attr('data-tooltip');
    if(tip_text){
      var tipText = tip_text;
    }
    var element = document.createElement('div');
    element.className = 'my_tooltip';
    element.innerHTML = tipText;
    $(current).append(element);
    $(element).delay( 8000 ).fadeOut('slow');
    setTimeout( function(){
      current.css('border','none');
    },8000);
}

 });

</script>

@endsection

      

