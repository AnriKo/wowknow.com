@extends('layouts/app')

@section('title_meta')
   Подписка на уведомления на почту. WowKnow &ndash; образовательный портал, поиск репетиторов.
@endsection

@section('description_meta')
    Страница подписки на уведомления которые приходят на почту.
@endsection

@section('title_page')
    Настройка уведомлений которые приходят на почту.
@endsection

@section('content')

        <div class="col-md-9">
            <div class="panel panel-default">
                <div class="panel-heading"><b>Уведомления о каких событиях присылать вам на почту?</b></div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}

                        <div class="col-lg-12 col-md-12 col-ms-12 col-xs-12 one_form_block">
                          <label class="control-label col-lg-3 col-md-3 col-sm-3">Пол<span class="require"></span></label>
                          <div class="col-md-5 col-lg-5 col-sm-5">
                            <label class="gender"><input value="man" type="radio" name="gender">Мужской</label>
                            <label class="gender"><input value="woman" type="radio" name="gender">Женский</label>
                          </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>

@endsection
@section('script')



@endsection

