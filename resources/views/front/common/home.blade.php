@extends('layouts/home')

@section('title_meta')
@lang('home.title')
@endsection

@section('description_meta')
@lang('home.descr')
@endsection

@section('title_og')
@lang('home.title')
@endsection

@section('description_og')
@lang('home.descr')
@endsection

@section('main_img')

    <div id="promo_img">
            <div id="form_find_tutor">
                <h2>Готові почати навчання?<br> Ми допоможемо знайти вчителя!</h2>
                <select class="form-control" name="subject_id" id="subject_name">
                    <option value="subjects">Що саме ви хочете вивчати?</option>
                    @foreach($subjects as $subjects)

                        <option value="{{$subjects->alias}}">
                                {{ $subjects->name }}
                        </option>
                        
                    @endforeach
                </select>
                <a href="{{ route('tutor_basic_search') }}" id="find_tutor" class="btn">Знайти вчителя</a>
            </div>
    </div>

@endsection

@section('content')

@php 
    $top_tuts = App\MyHelpers\Helper_two::top_tutors(); 
    $top_course = App\MyHelpers\Helper_two::top_company(); 
@endphp

<div class="container-fluid" id="process">
    <div class="container">

        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <h3>Всього лиш 3 простих кроки до навчання</h3>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <div class="icon">
                    <img src="/front/images/icon_search.png" alt="search tutor icon">
                </div>
                <h4 class="head">Пошук</h4>
                <p class="description">
                    Виберіть репетитора в нашій базі викладачів
                </p>

            </div>

            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <div class="icon" style="margin-top: -5px;">
                    <img src="/front/images/icon_request.png" alt="search tutor icon">
                </div>
                <h4 class="head">Зв'язок з репетитором</h4>
                <p class="description">
                    Контакти репетитора ви можете переглянути після того, як пройдете дуже коротку реєстрацію на сайті
                </p>

            </div>

            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <div class="icon">
                    <img src="/front/images/icon_teach.png" alt="search tutor icon">
                </div>
                <h4 class="head">Навчання</h4>
                <p class="description">
                    Обговоріть з репетитором всі деталі, виберіть перший день навчання і вперед до знать! 
                </p>

            </div>

{{--             <div style="margin-top: 25px" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                   <div class="ads_top_gorizont">
      <ins class="adsbygoogle"
           style="display:block"
           data-ad-client="ca-pub-3870576311603929"
           data-ad-slot="7892618142"
           data-ad-format="auto"
           data-full-width-responsive="true"></ins>
      <script>
           (adsbygoogle = window.adsbygoogle || []).push({});
      </script>
    </div>
            </div> --}}

        </div>
        
    </div>
</div>

<div class="container-fluid" id="for_tutors">
    <div class="container">
        <h3>Вакансії для репетиторів</h3>
        <p>Приєднуйтеся до кращої команди викладачів. Самі вигідні умови співпраці у нас.</p>
        <div class="icon">
            <img src="/front/images/icon_best.png" alt="search icon_best">
        </div>
        <a class="my_btn" data-next-page="{{ route('tutor_account_basic') }}" title="@lang('menus.tutor_reg')" href="#" data-toggle="modal" data-target="#basic_register">
            Реєстрація репетитора
        </a>    

    </div>    
</div>    


<div class="container-fluid" id="home_posts">
    <div class="container">
        <h3>@lang('home.new_blog_articles')</h3>
          @foreach($posts as $row)
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 one_post">
              <div class=" post_image" >
                <a href="{{ route('blog.one_page', $row->slug) }}">

                  <div class="img_wrapp"> 

                      @if($row->sm_image)

                        @if(strpos($row->sm_image, 'storage') !== false)
                          <img src="{{ $row->sm_image }}" alt="{{$row->title}}">
                        @else
                          <img src="/storage/posts/shares/post_main_images/{{ $row->sm_image }}" alt="{{$row->title}}">
                        @endif

                      @else

                        @if(strpos($row->image, 'storage') !== false)
                          <img src="{{ $row->image }}" alt="{{$row->title}}">
                        @else
                          <img src="/storage/posts/shares/post_main_images/{{ $row->image }}" alt="{{$row->title}}">
                        @endif
                        
                      @endif

                    <h5>@if($row->title) {{$row->title}} @else {{$row->title_b}} @endif</h5>
                  </div>  

                </a>
              </div>
            </div>
          @endforeach
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="all_posts">
            <a href="{{ route('posts.index') }}">
                <span class="my_btn">
                    @lang('home.show_all_articles')
                    <i class="fa fa-angle-right"></i>
                </span>
                <div class="icon">
                    <img src="/front/images/icon_blog.png" alt="search icon_best">
                </div>
            </a>
        </div>
    </div>
</div>

@endsection

@section('style')

<link href="https://fonts.googleapis.com/css?family=Roboto+Slab:700&amp;subset=cyrillic-ext" rel="stylesheet">
{{ Html::style('front/select2/select2.css') }}

@endsection

@section('script')

    {{ Html::script('front/select2/select2.full.js') }}

    <script>
        $("#subject_name").select2({
          tags: false,
          maximumSelectionLength: 20,
        });

        $('.navbar-static-top').css('background', 'rgba(103, 141, 189, 0.75)');

        //отправляет пользователя после выбора предмета
        $('select#subject_name').on('select2:select', function (evt) {

            var city = 'all-city';
            var subject = $('#subject_name').val();
            var url = '{{ route("tutor_search_subject_city", [":subject", ":id"]) }}';
            url = url.replace(':id', city);
            url = url.replace(':subject', subject);
            window.location.href = url;
        });
    </script>

@endsection

