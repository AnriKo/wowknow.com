@extends('layouts/app_sidebar_right')

@section('title_page')
 Напишіть нам
@endsection

@section('title_meta')
Сторінка контактів, напишіть нам на пошту
@endsection

@section('description_meta')
 Сторінка контактів, напишіть нам на пошту
@endsection

@section('keywords_meta')
контакти, пошта, зворотній вз'язок
@endsection

@section('side_bar')
{{--  @include('parts/adsense_sidebar')--}}
@endsection

@section('content')

<div id="blog_page">

    <div class="col-md-12 mt_30">

        <form id="message_form" class="form-horizontal check_captcha" method="POST" action="{{ route('send_message') }}">
            {{ csrf_field() }}

            <div class="form-group">
                <label for="message_name" class="col-md-3 control-label">@lang('auth.your_name')<span class="require"></span></label>
                <div class="col-md-5">
                    <input id="message_name" type="text" class="form-control" name="message_name" required autofocus>
                </div>
            </div>

            <div class="form-group">
                <label for="message_email" class="col-md-3 control-label">@lang('auth.your_mail')<span class="require"></span></label>
                <div class="col-md-5">
                    <input id="message_email" type="email" class="form-control" name="message_email" required>
                </div>
            </div>

            <div class="form-group">
                <label for="message_body" class="col-md-3 control-label">
                    Ваш лист
                    <span class="require"></span>
                </label>
                <div class="col-md-9">
                    <textarea class="form-control" name="message_body" id="message_body" cols="30" rows="7"></textarea>
                </div>
            </div>

            <div class="form-group">

{{--                <label for="message_jCaptcha" class="col-md-3 control-label">@lang('auth.captcha')<span class="require"></span></label>--}}
{{--                <div class="col-md-5">--}}
{{--                    <input id="message_jCaptcha" class="jCaptcha form-control" type="text" placeholder="Введіть тут результат">--}}
{{--                </div>--}}

                <label class="col-md-3 control-label">@lang('auth.captcha')<span class="require"></span></label>
                <div class="col-md-9">
                     {!! app('captcha')->display() !!}
                    <span style="display: none;" id="captcha_verify"></span>
                </div>

            </div>

            <div class="form-group">
                <div class="col-md-8 col-md-offset-3">
                    <button style="margin-bottom: 5px;" type="submit" class="btn btn-primary">
                        Відправити лист
                    </button>
                </div>
            </div>

        </form>

    </div>

</div>

@endsection

@section('script')
{{--        <script src="https://www.google.com/recaptcha/api.js" async defer></script>--}}
@endsection

      

