@extends('layouts/app_sidebar_no_big_title')

@section('title_meta')
    {{ $school->name }} @if(strripos($school->name, $school_info['city']) === false) в городе {{ $school_info['city'] }} @endif
@endsection

@section('description_meta')
    {{ strip_tags(mb_strimwidth($school->description, 0, 170, "...")) }}
@endsection

@section('description_og')
    {{ strip_tags(mb_strimwidth($school->description, 0, 170, "...")) }}
@endsection

@section('og_image_meta')
    @if($school->logo)
        {{ URL::asset('storage/companies/logos') }}/{{ $school->logo }}
    @else 
        {{ URL::asset('front/images/wowknow_img.png') }}
    @endif
@endsection

@section('title_og')
    {{ $school->name }} @if(strripos($school->name, $school_info['city']) === false) в городе {{ $school_info['city'] }} @endif
@endsection

@section('side_bar')
    @include('parts/adsense_sidebar')
@endsection

@section('content')

<?php $name = "name_".$local; ?>

<div id="search_page" class="panel-default">
    <div id="cours_page" class="panel-body">
        <div class="row one_row one_courses">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no_padding">
                <div class="name">
                    <h1>
                        {{ $school->name }}
                    </h1>
                </div>
            </div>
            <div class="col-lg-7 col-md-7 col-sm-6 col-xs-12">

                <div class="profile_info">

                <table id="datatable" class="table table-striped table-bordered company_detals">
                    <tbody>
                        <tr >
                            <td>
                               Страна 
                            </td>
                            <td>
                               {{ App\MyHelpers\Helper::country_name($school->country) }}
                               <span class="flag flag-{{$school->country}}"></span>
                            </td>
                        </tr>
                        <tr >
                            <td>
                               Город 
                            </td>
                            <td>
                                {{ $school_info['city'] }}
                            </td>
                        </tr>
                        <tr >
                            <td>
                                Адрес
                            </td>
                            <td>
                                 {{$school->street}}
                            </td>
                        </tr>
                        @if($school->phone)
                            <tr >
                                <td>
                                    Телефон
                                </td>
                                <td>
                                    {{ $school->phone }}
                                </td>
                            </tr>
                        @endif
                        @if($school->site)
                            <tr >
                                <td>
                                    Сайт 
                                </td>
                                <td>
                                    @if(strpos($school->site, 'http') !== false)
                                        <a target="_blank" href="{{ $school->site }}">{{ str_replace(array('http://','https://'), '', $school->site) }}</a>
                                    @else
                                        <a target="_blank" href="http://{{ $school->site }}">{{ $school->site }}</a>
                                    @endif
                                </td>
                            </tr>
                        @endif
                        @if($school->soc_page)
                            <tr >
                                <td>
                                    Страница в соц. сетях 
                                </td>
                                <td>
                                    @if(strpos($school->soc_page, 'http') !== false)
                                        <a target="_blank" href="{{ $school->soc_page }}">{{ str_limit(str_replace(array('http://','https://'), '', $school->soc_page), 40) }}</a>
                                    @else
                                        <a target="_blank" href="http://{{ $school->soc_page }}">{{ $school->soc_page }}</a>
                                    @endif
                                </td>
                            </tr>
                        @endif
                        <tr >
                            <td>
                                Классы школы 
                            </td>
                            <td>
                                {{ $school->class }}
                            </td>
                        </tr>
                    </tbody>
                  </table>
                </div> 
            </div> 
            <div class="col-lg-5 col-md-5 col-sm-6 col-xs-12 no_padding company_info"> 

                @if($school->logo)

                    <img class="company_avatar" src="{{ URL::asset('storage/companies/logos') }}/{{ $school->logo }}" alt="avatar_tutor_{{ $school->last_name }}">
                @endif

         </div>
         <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no_padding about_corse"> 
            <p> <span class="sub_title">Описание</span></p> 
             <div class="about_cours_text">
                {!! $school->description !!}
             </div>   
         </div> 

        @if($school_info['gallery'])
             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no_padding foto_gallery_block"> 
                <p> <span class="sub_title">Фотогалерея <i style="color: #78cced" class="fa fa-picture-o" aria-hidden="true"></i></span></p> 
                 <div class="foto_gallery">
                    @foreach($school_info['gallery'] as $one_gallery_link)

                        <a class="col-sm-4" href="/storage/companies/gallery/{{ $one_gallery_link }}" data-toggle="lightbox" data-gallery="example-gallery">
                            <img src="/storage/companies/gallery/{{ $one_gallery_link }}" class="img-fluid">
                        </a>

                    @endforeach
                 </div>   
             </div>   
         @endif

     </div>
 </div>
</div>

 <div id="reviews" class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> 
    <h2 class="sub_title">Отзывы <i style="color: #78cced" class="fa fa-comments"></i></h2> 

    @if($reviews)

        <div class="exist_reviews">
            @foreach($reviews as $item)

                <div class="one_review">
                    <div class="review_meta">
                        <span>{{$item->sender_name}}</span> <span>{{$item->created_at->format('d.m.Y') }}</span>
                    </div>
                    <div class="review_text">
                        <p>{!! nl2br($item->review) !!}</p>
                    </div>
                </div>

            @endforeach
        </div>

    @endif

    <div id="add_reviews_label"> <span>@lang('univers.page_add_rewievs') <i class="fa fa-pencil" aria-hidden="true"></i></span></div>

     <div id="add_reviews">

        <form class="form-horizontal" method="POST" action="{{ route('review_add') }}">
            {{ csrf_field() }}

            <div class="form-group{{ $errors->has('sender_name') ? ' has-error' : '' }}">
                <label for="sender_name" class="col-md-12 control-label no_padding">@lang('auth.your_name')<span class="require"></span></label>
                <div class="col-md-4 no_padding">
                    <input id="sender_name" type="text" class="form-control" name="sender_name" value="{{ old('sender_name') }}" required>
                    @if ($errors->has('sender_name'))
                        <span class="help-block">
                            <strong>{{ $errors->first('sender_name') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group">
                <label for="password_register" class="no_padding col-md-12 control-label">@lang('univers.page_rewiev')<span class="require"></span></label>
                <div class="col-md-8 no_padding">
                    <textarea class="form-control" required name="review_text" id="" cols="40" rows="5"></textarea>
                    @if ($errors->has('review_text'))
                        <span class="help-block">
                            <strong>{{ $errors->first('review_text') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group">
                <label for="password-confirm" class="col-md-12 no_padding control-label">@lang('auth.captcha')<span class="require"></span></label>
                <div class="no_padding col-md-6">
                    {!! app('captcha')->display() !!}
                </div>
                <input type="hidden" name="item_id" value="{{ $school->id }}">
                <input type="hidden" name="review_type" value="course">
            </div>            

            <div class="form-group">
                <div class="col-md-12 no_padding">
                    <button style="margin: 10px 0px;" type="submit" class="btn btn-primary">
                        Сохранить отзыв <i class="fa fa-plus-circle"></i>
                    </button>
                </div>
            </div>

        </form>


     </div>   
 </div>   
        
@endsection

@section('style')
    {{ Html::style('front/css/flags.css') }}
    {{ Html::style('front/lightbox/ekko-lightbox.css') }}
@endsection

@section('script')
    {{ Html::script('front/lightbox/ekko-lightbox.js') }}

<script type="text/javascript">
    $(document).on('click', '[data-toggle="lightbox"]', function(event) {
        event.preventDefault();
        $(this).ekkoLightbox();
    });

  //при загрузке страницы
  $( document ).ready(function() {

  }); 

</script>

@endsection

