@extends('layouts/app_sidebar_right')

@php

@endphp

@section('title_meta')
    Альтернативные школы Украины
@endsection

@section('title_page')
    Альтернативные школы в Украине
@endsection

@section('side_bar')
    @include('parts/adsense_sidebar')
@endsection

@section('content')

<?php $name = "name_".$local;  $title_loc = "title_".$local; ?>

<div id="search_page" class="panel-default">

    <div id="top_current_page_menu" class="no_margin row">

        <div style="padding-right: 15px" class="one_block_menu col-lg-6 col-md-6 col-sm-6 col-xs-12 no_padding">
            <select style="width: 100%" name="city_id" id="city_id">
                <option value="city">Выберите город</option>
                @foreach($cities_school as $one_city)
                    <option @if($one_city->city == $data_schools['city']) selected @endif value="{{$one_city->city}}">
                        @if($one_city->country == 'ua')
                            {{ $one_city->city_ua->title_ru }}
                        @elseif($one_city->country == 'ru')
                            {{ $one_city->city_ru->title_ru }}
                        @elseif($one_city->country == 'kz')
                            {{ $one_city->city_kz->title_ru }}
                        @endif
                    </option>
                @endforeach
            </select>
            
        </div>

        <div class="one_block_menu col-lg-6 col-md-6 col-sm-6 col-xs-12 no_padding">

        </div>

    </div>

    <div class="panel-body">



        @foreach($schools as $school)

                <div class="@if($school->top == '1') top_item @endif row one_row one_courses">
                    <div class="col-lg-7 col-md-7 col-sm-6 col-xs-12 no_padding">
                        <div class="name">
                            <a href="{{ route('schools_one_show', ['alias' => $school->alias ]) }}">
                                {{ $school->name }}
                            </a> 
                        </div>
                        <div class="cours_location"><i class="fa fa-map-marker" aria-hidden="true"></i> 
                                @if( $school->country == 'ru')
                                    @if(isset($school->city_ru->{$title_loc}))
                                        <span class="city">
                                            {{-- <span class="flag flag-ru"></span> --}}
                                            {{ $school->city_ru->{$title_loc} }}
                                        </span>   
                                    @endif    
                                @elseif( $school->country == 'ua')
                                    @if(isset($school->city_ua->{$title_loc}))
                                        <span class="city">
                                            {{ $school->city_ua->{$title_loc} }}
                                            {{-- <span class="flag flag-ua"></span> --}}
                                        </span>
                                    @endif     
                                @elseif( $school->country == 'kz')
                                    @if(isset($school->city_kz->{$title_loc}))
                                        <span class="city">
                                            {{-- <span class="flag flag-kz"></span> Казахстан<br> --}}
                                            {{ $school->city_kz->{$title_loc} }}
                                        </span>
                                    @endif    
                                @endif

                                @if($school->street)
                                    <span class="street">
                                        - {{ $school->street }}
                                    </span><br>
                                @endif
                        </div>
                        @if($school->class)
                            <div class="school_class">
                                <i style="color: orange" class="fa fa-child" aria-hidden="true"></i> Классы обучения - ({{$school->class}})
                            </div>
                        @endif
                    </div> 
                    <div class="col-lg-5 col-md-5 col-sm-6 col-xs-12 no_padding company_info"> 

                        <p>
                            @if($school->logo)
                                <a href="{{ route('cours_one_show', ['alias' => $school->alias ]) }}">
                                    <img class="company_avatar" src="{{ URL::asset('storage/companies/logos') }}/{{ $school->logo }}" alt="avatar_tutor_{{ $school->name }}">
                                </a>    
                            @endif
                            {{ mb_strimwidth(strip_tags($school->description), 0, 150, "...") }} 
                        </p>


  
                    </div>
                
        </div>

        @endforeach
        
        <div id="pagination_wrapper">
            {{ $schools->render() }}
        </div>    

@if(Auth::check())

<!-- Modal send mail -->
    <div class="modal fade" id="send_user_mail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <div class="modal-title" id="exampleModalLabel"><span id="get_user_name"></span> отправление сообщения<img  id="get_user_avatar" src="/" alt="user avatar"></div>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <textarea placeholder="Введите собщение" id="message_body" name="message" class="form-control"></textarea>
            <input id="user_number" type="hidden" name="recipients[]" value="">
          </div>
          <div class="modal-footer">
            <button id="send_mail_button" type="button" class="btn btn-primary">Отправить</button>
          </div>
        </div>
      </div>
    </div>
    
@endif

    </div>
</div>
@endsection

@section('style')

{{ Html::style('front/select2/select2.css') }}
{{ Html::style('front/css/flags.css') }}

@endsection

@section('script')

{{ Html::script('front/select2/select2.full.js') }}
<script type="text/javascript">

  //при загрузке страницы
  $( document ).ready(function() {
        $('.pupil_age_course').map(function() {
            var ages = $(this).text();
            var arr = ages.split(',');
            var new_age = '';
            for (var i = 0; i < arr.length; i++) {
              switch (arr[i]) {
                  case 'a2':
                    new_age += 'Дети, ';
                    break;
                  case 'a3':
                    new_age += 'Младшие школьники, ';
                    break;
                  case 'a4':
                    new_age += 'Школьники, ';
                    break;
                  case 'a5':
                    new_age += 'Студенты, ';
                    break;
                  case 'a6':
                    new_age += 'Взрослые, ';
                    break;
                  case 'a7':
                    new_age += 'Пенсионеры';
                    break;
                  default:
                    
                }
            }
            $(this).text(new_age);
            console.log(arr);
          })

  }); 
$('.more_courses').on('click', function(){
    $(this).next( ".this_company_courses").toggle(); 
});
    

//получаем текущий путь
var url_cur = new Url();
//выбираем параметры юрл
var params = '?'+window.location.search.replace( '?', ''); 

    //function tips before select subject red border
    function show_tips_red_border() {
        subject_name_course.next().addClass("border_red");
        subject_name_course.next().next('.tips_hide').show();
    }


    //показываем или скрываем выбор города
    var subject_name_course = $('#subject_name_course');
    // var city_id = $('#city_id');

    //     $('#city_id').select2({
    //         placeholder: "Выберите город",
    //         minimumInputLength: 1,
    //         ajax: {
    //             url: '{{-- url('/') --}}/city-search-all',
    //             delay: 100,
    //             dataType: 'json',
    //             data: function (params, locale) {
    //                 var query = {
    //                     city: $.trim(params.term),
    //                     locale: $('#country_teach_id').val(),
    //                 };
    //                 return query;
    //             },
    //             processResults: function (data) {
    //                 return {
    //                     results: data
    //                 };
    //             },
    //             cache: true
    //         }
    //     });


    $("#subject_name_course").select2({
      tags: false,
      maximumSelectionLength: 20,
    });

    $("#city_id").select2({
      tags: false,
      maximumSelectionLength: 20,
    });

   
    //отправляет пользователя после выбора предмета
    $('select#subject_name_course').on('select2:select', function (evt) {

        var city = $('#city_id').val();
        if (city == null) {
            var city = 'city';
        }
        var url = '{{ route("schools_search", [":id"]) }}';
        url = url.replace(':id', city);

        if (params.length > 3) {
            url += params;
        }

        window.location.href = url;
    });

    //отправляет пользователя после выбора города
    $('select#city_id').on('select2:select', function (evt) {

        var url_cur = new Url();
        var city = $('#city_id').val();
        var url = '{{ route("schools_search", [":id"]) }}';
        url = url.replace(':id', city);

        if (params.length > 3) {
            url += params;
            url = url.replace("online", 'local');
        }else{
            url += '?type_lessons=local';
        }
        

        window.location.href = url;
    });
    //отправляет пользователя после выбора типа урока online

    $('#type_lessons #online').click( function (evt) {

        var subject_name_course = $('#subject_name_course');
        if (subject_name_course.val() == null) {
            show_tips_red_border();
            return false;
        }

        var url_cur = new Url();

        var params = $("input:checked").val();

        if (params == 'online') {

            var x  = url_cur.paths();
            url_cur.paths([x[0], x[1], 'online']);

        }

        url_cur.query.type_lessons = params;
        var url = url_cur.toString();
        window.location.href = url;
        
    });  
    //отправляет пользователя после выбора пола репетитора
    $('#gender input').click( function (evt) {
        var url_cur = new Url();
        var params = $("#gender input:checked").val();
        url_cur.query.gender = params;
        if (params == 'mw') {
            delete url_cur.query.gender;
        }
        var url = url_cur.toString();
        window.location.href = url;
        
    });    
    //отправляет пользователя после выбора возраста ученика
    $('#pupil_age input').click( function (evt) {

        var url_cur = new Url();

        var inputs = $('#pupil_age input');

        var params = '';

        $.each(inputs, function(index, value){
            if ($(this).prop("checked")) {
                params += ($(this).val());
            }
            
        });

        if (params != '') {
            url_cur.query.p_age = params;
        }else{
            delete url_cur.query.p_age
        }

        var url = url_cur.toString();
        console.log(url);
        window.location.href = url;
        
    });     


//отправка сообщения репетитору
var send_mail = $('.send_mail');
send_mail.on('click', function(){
    var user_id = $(this).attr('id');
    $('body').append('<div class="send_mail_wrapp_block"><div class="form-group send_mail_inner_block"><label class="control-label">Сообщение</label><textarea name="message" class="form-control"></textarea><input type="checkbox" name="recipients[]" value="'+user_id+'"></div></div>');
});

$('#send_user_mail').on('show.bs.modal', function (e) {


    var $button = $(e.relatedTarget); 
    var avatar_user = $(e.relatedTarget).closest('.one_teacher').find('.user_avatar').attr('src');
    var name_user = $(e.relatedTarget).closest('.one_teacher').find('.name a').text();

    $('#user_number').val($button.attr('id'));
    $('#get_user_name').text(name_user);
    $('#get_user_avatar').attr('src', avatar_user);
})

$('#send_user_mail').on('hide.bs.modal', function (e) {
    $('#message_body').val('');
    $('.success_message').remove();
})

$('#send_mail_button').on('click', function(e){
    var $user_id = $('#user_number').val();
    var $message = $('#message_body').val();
    var modal_footer = $('#send_user_mail').find('.modal-footer')
    $.ajax({
      type: "POST",
      url: "{{ route('message.store') }}",
      data: { "_token": "{{ csrf_token() }}",
                "recipient_id" : $user_id,
                "message" : $message,
            },
      success: function(data){
            $('#message_body').val('');
            modal_footer.append('<div class="success_message">'+data+'</div>');
        }
    });
});



</script>

@endsection

