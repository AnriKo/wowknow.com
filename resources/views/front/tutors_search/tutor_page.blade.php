@extends('layouts/app_sidebar_right')


@php 
// собираем meta title страницы
$tutor_meta_title = $teacher->name .' '. $teacher->last_name . ' - ' . $teacher->profile_title ;
$tutor_meta_title = $teacher->profile_title. ' - ' .$teacher->name .' '. $teacher->last_name ;
$keywords = __('meta.tutor_key');
if($teacher->teach_city_en) {
    $keywords .= $teach_city->name . ', ';
    if(mb_stripos($teacher->profile_title, $teach_city->name) === false){
       $tutor_meta_title .= " | " . $teach_city->name ;
    }
}
if($teacher->teach_online == '1') {
    $keywords .= __('meta.tutors_key_2');
    if(mb_stripos($teacher->profile_title, 'Онлайн') === false){
       $tutor_meta_title .= __('meta.tutor_title') ;
    }
}
//
@endphp

@section('title_meta')
{{ $tutor_meta_title }}
@endsection

@section('keywords_meta')
{{ $keywords }} 
@endsection

@section('title_page')

    {{ $teacher->profile_title }}
    @if($teacher->teach_city_en)
        @if(mb_stripos($teacher->profile_title, $teach_city->name) === false)
            <span class="tutor_page_subtitle"> {{ $teach_city->name }}.</span>
        @endif
    @endif
    @if($teacher->teach_online == '1') 
        @if(mb_stripos($teacher->profile_title, 'Онлайн') === false)
            <span class="tutor_page_subtitle"> @lang('meta.tutor_online').<span>
        @endif
    @endif

@endsection

@section('description_meta')
{{ mb_strimwidth($teacher->tutor_edu_text, 0, 150, "...") }}
@endsection

@section('description_og')
{{ mb_strimwidth($teacher->tutor_edu_text, 0, 150, "...") }}
@endsection

@section('og_image_meta')
@if($teacher->avatar != ''){{ URL::asset('storage/users/') }}/{{ $teacher->avatar }}
@else {{ URL::asset('front/images/wowknow_img.png') }}
@endif
@endsection

@section('title_og')
{{ $teacher->name }} {{ $teacher->last_name }} &ndash; {{ $teacher->profile_title }}
@endsection

@section('side_bar')
    @include('parts/adsense_sidebar')
@endsection

@section('content')

{{-- dd($teach_city) --}}

<nav aria-label="breadcrumb">
<ol class="breadcrumb">
  <li class="breadcrumb-item"><a href="/">Домашняя</a></li>
  <li class="breadcrumb-item"><a href="{{ route('tutor_basic_search') }}">Все репетиторы</a></li>
    @if($teacher->teach_city_en)
        <li class="breadcrumb-item"><a href="{{ route("tutor_search_subject_city", ["subjects", $teach_city->title_en ]) }}">Репетиторы в городе {{ $teach_city->name }}</a></li>
    @endif
  <li class="breadcrumb-item active" aria-current="page">{{ mb_strimwidth($teacher->profile_title, 0, 50, '...') }}</li>
</ol>
</nav>

<div id="tutor_page" class="panel panel-default">
    <div class="panel-body">
           <div class="row">
               <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                    <div class="col-md-3 col-lg-3 col-sm-4 col-xs-12 no_padding">
                        @if($teacher->avatar != '')
                            <div id="tutor_avatar_sidebar">
                               <img src="{{ URL::asset('storage/users/') }}/{{ $teacher->avatar }}" alt="{{ $teacher->profile_title }}">
                            </div>
                        @else
                            <div id="tutor_avatar_sidebar">
                               <img src="{{ URL::asset('front/images/tutor_avatar_crop/') }}/default.jpg" alt="{{ $teacher->profile_title }}">
                            </div>
                        @endif
                    </div>
                    <div class="col-md-9 col-lg-9 col-sm-8 col-xs-12">
                        <div id="tut_name" class="name">
                            <h2>{{ $teacher->name }} {{ $teacher->last_name }}</h2>
                            <span class="flag flag-{{ $teacher->country_teach }}"></span>
                            
                            @if($teacher->teach_city_en)
                            <span class="tutor_country_sity"> - @lang('tutors.page_city') {{ $teach_city->name }}</span>
                            @endif
                        </div>

                        <div id="tut_info">
                            <div id="subjects" class="row">
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 no_padding_left info_label">
                                    <i class="fa fa-book" aria-hidden="true"></i>
                                    @lang('tutors.page_subjects')
                                </div> 
                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 no_padding_right info_value">
                                    @foreach($teacher->subjects as $subject)
                                        <span>{{ $subject->{'name_'.$local} }},</span>
                                    @endforeach
                                </div>
                            </div> 

                            <div id="teach_place" class="row">
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 no_padding_left info_label">
                                    <i class="fa fa-map-marker" aria-hidden="true"></i>
                                    @lang('tutors.page_tut_place')
                                </div> 
                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 no_padding_right info_value">
                                    @if($teacher->teach_online == '1') 
                                        <p>@lang('tutors.page_tut_online')</p>
                                    @endif
                                    @if($teacher->teach_local == '1')
                                        <p>
                                        @if($teacher->teach_city_en)
                                            @lang('tutors.in_city') {{ $teach_city->name }} - 
                                        @endif
                                        @if($teacher->teach_local_me == '1')  
                                            <span>@lang('tutors.page_place_tut'),</span>
                                        @endif
                                        @if($teacher->teach_local_pupil == '1')  
                                            <span>@lang('tutors.page_place_pup')</span>
                                        @endif
                                        </p>
                                    @endif
                                </div>
                            </div> 

                            @if($teacher->teacher_price)
                                <div id="teacher_price" class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 no_padding_left info_label">
                                        <i class="fa fa-money" aria-hidden="true"></i>
                                        @lang('tutors.page_cost')
                                    </div> 
                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 no_padding_right info_value">
                                        <p><b>{{ $teacher->teacher_price }} {{ App\MyHelpers\Helper::value_cur($teacher->cur_def) }}</b> / @lang('tutors.search_class')</p>
                                    </div>
                                </div> 
                            @endif 
                            @if($teacher->is_group_teach == '1' )
                                <div id="country_born" class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 no_padding_left info_label">
                                        <i class="fa fa-users" aria-hidden="true"></i>
                                        @lang('tutors.group_teach')
                                    </div> 
                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 no_padding_right info_value">
                                        <span>@lang('tutors.group_to') {{ $teacher->group_size }} @lang('tutors.mens')</span>
                                    </div>
                                </div> 
                            @endif 
                            @if($teacher->teach_exp != '' )
                                <div id="teach_exp" class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 no_padding_left info_label">
                                        <i class="fa fa-tree" aria-hidden="true"></i>
                                        @lang('tutors.expirience')
                                    </div> 
                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 no_padding_right info_value">
                                        <span>{{ $teacher->teach_exp }} {!! App\MyHelpers\Helper::GetYear($teacher->teach_exp, $local) !!}</span>
                                    </div>
                                </div> 
                            @endif 
                            @if($teacher->pupil_age != '' )
                                <div id="teach_exp" class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 no_padding_left info_label">
                                        <i class="fa fa-male" aria-hidden="true"></i>
                                        @lang('tutors.pupil_age')
                                    </div> 
                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 no_padding_right info_value">
                                        
                                        @php $age = explode(',', $teacher->pupil_age) ;  @endphp
                                        @foreach($age as $one)
                                            <span> {!! App\MyHelpers\Helper::pupil_age(str_replace(".", '', $one)) !!}, </span>
                                        @endforeach
                                    </div>
                                </div> 
                            @endif 

                            <div id="country_born" class="row">
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 no_padding_left info_label">
                                    <i class="fa fa-address-book-o" aria-hidden="true"></i>
                                    @lang('tutors.contact_info')
                                </div> 
                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 no_padding_right info_value">

                                    @if(Auth::user())
                                        <p id="show_contact_dates">
                                            <span style="letter-spacing: 0.03em; font-size: 17px; background-color: #30abd0;" class="btn btn-info">@lang('tutors.contact_show')</span>
                                        </p>  
                                    @else
                                        <p id="">
                                            <span data-toggle="modal" data-target="#go_to_register" style="letter-spacing: 0.03em; font-size: 17px; background-color: #30abd0;" class="btn btn-info">
                                                @lang('tutors.contact_show')
                                            </span>
                                        </p> 
                                    @endif
                                </div>
                            </div> 

{{--                             <div class="row">
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 no_padding_left info_label">
                                    <i class="fa fa-envelope" aria-hidden="true"></i>
                                    Сообщение
                                </div> 
                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 no_padding_right info_value">
                                        <button id="{{ $teacher->user_id }}" type="button" class="btn btn-info send_mail_open_modal" data-toggle="modal" data-target="#send_user_mail">Отправить сообщение / заявку <i class="fa fa-envelope" aria-hidden="true" style="color: #fff"></i>
                                        </button>
                                </div>
                            </div>  --}}

                                        
                        </div>
                    </div>    

                    <div class="profile_about_me col-lg-12 col-md-12 col-sm-12 col-xs-12 no_padding block_info">
                        <h3>@lang('tutors.about_tut')</h3>
                        <p>
                            @if($video_link != '')
                                <div id="tutor_video">
                                    <iframe width="100%" height="210px" src="http://www.youtube.com/embed/{{ $video_link }}?autoplay=0" frameborder="0"></iframe>
                                    <h3 style="margin-top: 0;">@lang('tutors.page_video')</h3>
                                </div>
                            @endif
                            {!! nl2br($teacher->tutor_edu_text) !!}
                        </p>
                    </div>
                    <div id="subject_detail" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 block_info no_padding">
                        <h3><i style="color: #FD773D;" class="fa fa-bookmark"></i> @lang('tutors.page_subjects')</h3>
                        <!-- Nav tabs -->
                          <ul class="nav nav-pills">
                            @foreach($teacher->subjects as $index => $subject)
                                <li @if($index == 0) class="active" @endif >
                                    <a href="#{{ $subject->alias }}" data-toggle="tab"><i class="fa fa-check-circle" aria-hidden="true"></i></i>{{ $subject->{'name_'.$local} }}</a>
                                </li>
                            @endforeach
                          </ul>

                        <!-- Tab panes -->
                          <div class="tab-content clearfix">
                            @foreach($teacher->subjects as $index => $subject)
                                <div class="tab-pane @if($index == 0) active @endif" id="{{ $subject->alias }}">

                                    <h4>@lang('tutors.directions') "{{ $subject->{'name_'.$local} }}" @lang('tutors.directions_2')</h4>
                        
                                        @foreach($subject->subject_direction as $x)

                                            @foreach($teacher->subjects_dir as $y)
                                           
                                                @if($x->id == $y->subject_direction_id )
                                                    <div class="subject_dir">
                                                        
                                                        <h6><i class="fa fa-circle"></i> {{ $x->{'name_'.$local} }}</h6>
                                                        <p class="teacher_exp">
                                                            {{ $y->subject_direction_exp }}
                                                        </p>
                                                    </div>
                                                @endif
                                            
                                            @endforeach  

                                        @endforeach    

                                </div>
                            @endforeach
                          </div>
                    </div>
                    @if($teacher->tutor_hobie_text != '' )
                        <div id="tutor_hobie">
                            <h3>@lang('tutors.hobbi')</h3>
                            <p>{{ $teacher->tutor_hobie_text }}</p>
                        </div>
                    @endif

                    @if($teacher->education->count() > 0)
                        <div id="tutor_education" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 block_info no_padding">
                            <h3><i class="fa fa-graduation-cap" aria-hidden="true"></i> @lang('tutors.page_edu')</h3>
                            @foreach($teacher->education as $institut)

                                <div class="one_institut">
                                    <div class="institut_name">{{ $institut->name_institut }}</div>
                                    <div class="institut_degree">@lang('tutors.page_edu_spec'): {{ $institut->degree }}</div>
                                    <div class="institut_time">@lang('tutors.page_edu_time'): {{ $institut->year_enter_edu }} - {{ $institut->year_finish_edu }}</div>
                                </div>

                            @endforeach
                        </div>
                    @endif

                    <div id="tutor_reviews" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 block_info no_padding">
{{--                        <h3><i class="fa fa-commenting-o" aria-hidden="true"></i> @lang('tutors.page_rewievs') </h3>--}}

                        @if($reviews)

                            <div class="exist_reviews">
                                @foreach($reviews as $item)

                                    <div class="one_review">
                                        <div class="review_meta">
                                            <span>{{$item->sender_name}}</span> <span>{{$item->created_at->format('d.m.Y  H:i') }}</span>
                                                                                    @if($item->rating)
                                            <span class="review_like">
                                                @if($item->rating == 2)
                                                    <i class="fa fa-smile-o" aria-hidden="true"></i>
                                                @elseif($item->rating == 1)
                                                    <i class="fa fa-frown-o" aria-hidden="true"></i>
                                                @endif
                                            </span>
                                        @endif
                                        </div>
                                        <div class="review_text">
                                            <p>{!! nl2br($item->review) !!}</p>
                                        </div>
                                    </div>

                                @endforeach
                            </div>

                        @endif

{{--                        <button style="color: #2379c4; border-radius: 2px" id="{{ $teacher->user_id }}" type="button" class="btn btn-default" data-toggle="modal" data-target="#set_review_teacher"><i style="color: #2379c4;" class="fa fa-edit"></i> Написать отзыв учителю</button> --}}
                    </div>
               </div>
           </div> 
    </div>
{{--    <div id="share_page_block_tutor">--}}
{{--        <div id="soc_icons_block" class="btn-group">--}}
{{--            <a class="btn btn-default"--}}
{{--            style="background-color: #3b5998" --}}
{{--            target="_blank"--}}
{{--            title="On Facebook"--}}
{{--            href="http://www.facebook.com/sharer.php?u={{url()->current()}}">--}}
{{--                <i style="color: #fff" class="fa fa-facebook fa-lg fb"></i>--}}
{{--            </a>--}}

{{--            <a class="btn btn-default"--}}
{{--            style="background-color: #3b5998" --}}
{{--            target="_blank"--}}
{{--            title="Like On Facebook"--}}
{{--            href="http://www.facebook.com/plugins/like.php?href={{url()->current()}}">--}}
{{--                <i style="color: #fff" class="fa fa-thumbs-o-up fa-lg fb"></i>--}}
{{--            </a>--}}

{{--            <a class="btn btn-default"--}}
{{--            style="background-color: #45668e"--}}
{{--            target="_blank"--}}
{{--            title="On VK.com" --}}
{{--            href="http://vk.com/share.php?url={{url()->current()}}">--}}
{{--                <i style="color: #fff" class="fa fa-vk fa-lg vk"></i>--}}
{{--            </a>--}}

{{--            <a class="btn btn-default"--}}
{{--            style="background-color: #dd4b39"--}}
{{--            target="_blank"--}}
{{--            title="On Google Plus"--}}
{{--            href="https://plusone.google.com/_/+1/confirm?hl=en&url={{url()->current()}}">--}}
{{--                <i style="color: #fff" class="fa fa-google-plus fa-lg google"></i>--}}
{{--            </a>--}}

{{--            <a class="btn btn-default"--}}
{{--            style="background-color: #55acee"--}}
{{--            target="_blank"--}}
{{--            title="On Twitter"--}}
{{--            href="http://twitter.com/share?url={{url()->current()}}">--}}
{{--                <i style="color: #fff" class="fa fa-twitter fa-lg tw"></i>--}}
{{--            </a>--}}
{{--        </div>--}}
{{--        <p>@lang('menus.share_page')</p>--}}
{{--    </div>--}}
</div>


<!-- Modals windows -->

<!-- Modal register -->
<div class="modal fade" id="go_to_register" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="panel-heading">

        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div style="padding: 25px 40px;" class="modal-body">

            <div class="row">
                <div style="margin-bottom: 30px" class="col-md-12">
                    <b>Щоб переглянути контактні данні репетитора, потрібно зареєструватися. <br></b>
                    У нас це дуже швидко і незайме багато часу.
                </div>

                <div class="col-md-12">
                    <a class="btn btn-primary" href="/register?next={{ url()->current() }}">
                        @lang('auth.register_go')
                    </a>
                    <div style="float: right;" >
                        <span style="font-size: 70%;">@lang('auth.register_already')</span>
                        <a style="border-color: #337ab7; color: #084d88; background-color: #f1f1f1;" id="log_from_mod_reg" class="btn btn-default" href="#" data-toggle="modal" data-target="#basic_login">@lang('auth.enter')</a>
                    </div>
                </div>
            </div>

      </div>
    </div>
  </div>
</div>


<!-- Modal send mail -->
    <div class="modal fade" id="send_user_mail" tabindex="-1" role="dialog" aria-labelledby="send_request_title" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
 
                <span id="get_user_name">
                    <strong>{{ $teacher->name }} {{ $teacher->last_name }}</strong> - {{ $teacher->profile_title }}
                </span> 
                <p><strong>Отправление заявки репетитору</strong></p>

                <img  id="get_user_avatar" src="{{ URL::asset('storage/users/') }}/{{ $teacher->avatar }}" alt="user avatar">

            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          {!! Form::open(['route' => 'send_tutor_request']) !!}
              <div class="modal-body">

                <div id="part_1" class="part_element">
                    
                    <div class="form-group row">
                      <label for="select_subject" class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                          Выберите предмет
                      </label>

                      <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">

                        <div data-tooltip="Отметьте предмет обучения" class="checkbox_block">

                            @if($teacher->subjects->count() > 1)

                                @foreach($teacher->subjects as $subject)

                                    <div class="form-check">
                                      <input name="subject_id[]" class="form-check-input" type="checkbox" value="{{ $subject->id }}" id="subject_{{ $subject->id }}">
                                      <label class="form-check-label" for="subject_{{ $subject->id }}">
                                        {{ $subject->{'name_'.$local} }}
                                      </label>
                                    </div>

                                @endforeach

                            @elseif($teacher->subjects->count() == 1)

                                <div class="form-check">
                                  <input name="subject_id[]" checked class="form-check-input" type="checkbox" value="{{ $teacher->subjects[0]->id }}" id="subject_{{ $teacher->subjects[0]->id }}">
                                  <label class="form-check-label" for="subject_{{ $teacher->subjects[0]->id }}">
                                    {{ $teacher->subjects[0]->{'name_'.$local} }}
                                  </label>
                                </div>

                            @endif

                        </div>

                      </div>

                    </div>

                    <div class="form-group row">

                      <label for="pupil_level" class="col-lg-3 col-md-3 col-sm-3 col-xs-12">Уровень знаний ученика</label>
                      <div data-tooltip="Укажите уровень знаний" class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                        <select class="form-control" name="level" id="pupil_level">
                            <option value="">Виберите уровень ученика</option>
                            <option value="children">Для детей</option>
                            <option value="beginner">Репетитор для начинающих</option>
                            <option value="middle">Средний уровень</option>
                        </select>
                      </div>

                    </div>

                    <div class="form-group row">

                      <label for="teach_place_select" class="col-lg-3 col-md-3 col-sm-3 col-xs-12">Место проведения занятий</label>
                      <div data-tooltip="Выберите место занятий" class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                        <select class="form-control" name="teach_place" id="teach_place_select">
                            <option value="">Где удобно заниматься?</option>
                            <option value="pupil_teacher">У меня или репетитора</option>
                            <option value="teacher">У репетитора</option>
                            <option value="pupil">У меня</option>
                            <option value="online">Онлайн (по скайпу)</option>
                        </select>
                      </div>

                    </div>

                    <div id="pupil_adress_block" class="form-group row">
                      
                    </div>

                    <div class="form-group row">

                      <label for="teach_time_on_week" class="col-lg-3 col-md-3 col-sm-3 col-xs-12">Кто будет заниматься?</label>
                      <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">

                        <div class="form-check">
                          <input checked class="form-check-input" type="radio" name="age" id="child" value="child">
                          <label class="form-check-label" for="child">
                            Ребёнок/школьник
                          </label>
                        </div>
                        <div class="form-check">
                          <input class="form-check-input" type="radio" name="age" id="student" value="student">
                          <label class="form-check-label" for="student">
                            Студент
                          </label>
                        </div>
                        <div class="form-check">
                          <input class="form-check-input" type="radio" name="age" id="big" value="big">
                          <label class="form-check-label" for="big">
                            Взрослый
                          </label>
                        </div>

                      </div>

                    </div>

                    <div class="form-group row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div data-close="part_1" data-open="part_2" class="float-right btn btn-primary step_button check_fields">Перейти дальше <i class="fa fa-angle-right" aria-hidden="true"></i></div>
                        </div>    
                    </div>

                </div>   

                <div id="part_2" class="part_element">

                    <div class="form-group row">

                      <label for="times_a_week" class="col-lg-3 col-md-3 col-sm-3 col-xs-12">Количество занятий в неделю</label>
                      <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">

                        <div class="form-check">
                          <input class="form-check-input" type="radio" name="times_a_week" id="times_1" value="1">
                          <label class="form-check-label" for="times_1">
                            1 раз в неделю
                          </label>
                        </div>
                        <div class="form-check">
                          <input class="form-check-input" type="radio" name="times_a_week" id="times_2" value="2" checked>
                          <label class="form-check-label" for="times_2">
                            2 раза в неделю
                          </label>
                        </div>
                        <div class="form-check">
                          <input class="form-check-input" type="radio" name="times_a_week" id="times_3" value="3">
                          <label class="form-check-label" for="times_3">
                            3 и больше раз в неделю
                          </label>
                        </div>

                      </div>  
                    </div>

                    <div class="form-group row">

                      <label for="times_a_week" class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                          Как долго планируете заниматься
                      </label>
                      <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">

                        <div class="form-check">
                          <input class="form-check-input" type="radio" name="course_length" id="long" value="long" checked>
                          <label class="form-check-label" for="long">
                            Больше 7 занятий
                          </label>
                        </div>
                        <div class="form-check">
                          <input class="form-check-input" type="radio" name="course_length" id="short" value="short">
                          <label class="form-check-label" for="short">
                            Несколько раз (до 7 занятий)
                          </label>
                        </div>

                      </div>

                    </div>

                    <div class="form-group row">
                      <label for="message" class="col-lg-3 col-md-3 col-sm-3 col-xs-12">Цель занятий</label>
                      <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                        <textarea placeholder="Укажите цель обучения, любую уточняющую информацию" id="message" name="message" class="form-control"></textarea>
                      </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div data-close="part_2" data-open="part_1" class="float-left btn btn-default step_button"><i class="fa fa-angle-left" aria-hidden="true"></i> Вернуться назад</div>
                            <div data-close="part_2" data-open="part_3" class="float-right btn btn-primary step_button">Перейти дальше <i class="fa fa-angle-right" aria-hidden="true"></i></div>
                        </div>    
                    </div>

                </div>    

                <div id="part_3" class="part_element"> 

                    <div class="form-group row">
                      <label for="client_name" class="col-lg-3 col-md-3 col-sm-3 col-xs-12">Ваше имя</label>
                      <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                        <input required type="text" class="form-control" id="client_name" name="client_name">
                      </div>
                    </div>

                

                    <div class="form-group row">
                      <label for="phone" class="col-lg-3 col-md-3 col-sm-3 col-xs-12">Ваш телефон</label>
                      <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                        <input required type="tel" class="form-control" id="phone" name="phone">
                      </div>
                    </div>

                    <div class="form-group row">
                      <label for="staticEmail" class="col-lg-3 col-md-3 col-sm-3 col-xs-12">@lang('auth.your_mail')</label>
                      <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                        <input required type="email" class="form-control" id="staticEmail" name="email">
                      </div>
                    </div>

                    <div class="form-group row captcha_row" data-tooltip="Забыли отметить капчу, укажите что вы не робот )">
                        <label for="password-confirm" class="col-lg-3 col-md-3 col-sm-3 col-xs-3">@lang('auth.captcha')<span class="require"></span></label>
                        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                           {{--  {!! app('captcha')->display() !!} --}}
                           <div class="g-recaptcha" data-sitekey="{{ env('CAPTCHA_SITEKEY', 'default_sitekey') }}"></div>
                        </div>
                    </div> 

                    <div class="form-group row">

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <input type="hidden" name="tutor_id" value="{{ $teacher->user_id }}">
                            <div data-close="part_3" data-open="part_2" class="float-left btn btn-default step_button"><i class="fa fa-angle-left" aria-hidden="true"></i> Вернуться назад</div>
                            <button id="send_mail_button" type="submit" class="float-right btn btn-success">@lang('auth.send') <i class="fa fa-paper-plane" aria-hidden="true"></i></button>
                        </div>

                    </div>

                </div> 

            </div>

           {!! Form::close() !!}   
        </div>
      </div>
    </div>

<!-- Modal share profile -->
<div class="modal fade" id="share_profile" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel_share" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <div class="modal-title" id="exampleModalLabel_share">
            <p style="font-weight: bold; font-size: 150%;">Поделиться профилем в соц сетях.</p>
        </div>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

            <p style="text-align: center;">Выберите социальную сеть.</p>

            <div style="text-align: center;" id="soc_icons_block" class="btn-group">

                <a class="btn btn-default"
                style="background-color: #3b5998" 
                target="_blank"
                title="Share on Facebook"
                href="http://www.facebook.com/sharer.php?u={{url()->current()}}&t={{ mb_strimwidth($teacher->tutor_edu_text, 0, 150, "...") }}">
                    <i style="color: #fff" class="fa fa-facebook fa-lg fb"></i>
                </a>

                <a class="btn btn-default"
                style="background-color: #3b5998" 
                target="_blank"
                title="Like On Facebook"
                href="http://www.facebook.com/plugins/like.php?href={{url()->current()}}">
                    <i style="color: #fff" class="fa fa-thumbs-o-up fa-lg fb"></i>
                </a>

                <a class="btn btn-default"
                style="background-color: #45668e"
                target="_blank"
                title="Share on VK.com" 
                href="http://vk.com/share.php?url={{url()->current()}}&title={{ $teacher->profile_title }}&description={{ mb_strimwidth($teacher->tutor_edu_text, 0, 150, "...") }}">
                    <i style="color: #fff" class="fa fa-vk fa-lg vk"></i>
                </a>

                <a class="btn btn-default"
                style="background-color: #dd4b39"
                target="_blank"
                title="Share on Google Plus"
                href="https://plusone.google.com/_/+1/confirm?hl=en&url={{url()->current()}}">
                    <i style="color: #fff" class="fa fa-google-plus fa-lg google"></i>
                </a>

                <a class="btn btn-default"
                style="background-color: #55acee"
                target="_blank"
                title="Share on Twitter"
                href="http://twitter.com/share?url={{url()->current()}}&text=WowKnow &ndash; Образовательный портал. Поиск репетиторов, учеников, учебных заведений.">
                    <i style="color: #fff" class="fa fa-twitter fa-lg tw"></i>
                </a>

            </div>
      </div>
    </div>
  </div>
</div>

<!-- Modal send review to teacher-->
{{--<div class="modal fade" id="set_review_teacher" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">--}}
{{--  <div class="modal-dialog" role="document">--}}
{{--    <div class="modal-content">--}}
{{--      <div class="modal-header">--}}
{{--        <div class="modal-title" id="exampleModalLabel"><span id="get_user_name">Создание отзыва учителю "{{ $teacher->name }} {{ $teacher->last_name }}"</span> <img  id="get_user_avatar" src="{{ URL::asset('storage/users/') }}/{{ $teacher->avatar }}" alt="user avatar"></div>--}}
{{--        <button type="button" class="close" data-dismiss="modal" aria-label="Close">--}}
{{--          <span aria-hidden="true">&times;</span>--}}
{{--        </button>--}}
{{--      </div>--}}
{{--      <form method="POST" action="{{ route('review.store') }}">--}}
{{--        {{ csrf_field() }}--}}
{{--        <div class="modal-body">--}}

{{--                <div class="col-md-6 no_padding">--}}
{{--                    <input placeholder="Ваше имя" id="sender_name" type="text" class="form-control" name="sender_name" value="{{ old('sender_name') }}" required>--}}
{{--                    @if ($errors->has('sender_name'))--}}
{{--                        <span class="help-block">--}}
{{--                            <strong>{{ $errors->first('sender_name') }}</strong>--}}
{{--                        </span>--}}
{{--                    @endif--}}
{{--                </div>--}}

{{--                <div class="col-md-6 no_padding">--}}
{{--                    <input placeholder="Ваш email" id="sender_email" type="email" class="form-control" name="sender_email" value="{{ old('sender_email') }}" required>--}}
{{--                    @if ($errors->has('sender_email'))--}}
{{--                        <span class="help-block">--}}
{{--                            <strong>{{ $errors->first('sender_email') }}</strong>--}}
{{--                        </span>--}}
{{--                    @endif--}}
{{--                </div>    --}}

{{--                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no_padding">--}}
{{--                    <textarea required="required" placeholder="Напишите свой отзыв" id="review_body" name="review_text" class="form-control"></textarea>--}}
{{--                </div>    --}}

{{--            --}}
{{--                <div style="margin-top: 15px;" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no_padding">--}}
{{--                    --}}{{-- {!! app('captcha')->display() !!} --}}
{{--                    <div class="g-recaptcha" data-sitekey="{{ env('CAPTCHA_SITEKEY', 'default_sitekey') }}"></div>--}}
{{--                </div>--}}
{{--                <input type="hidden" name="teacher_id" value="{{ $teacher->id }}">--}}

{{--                <div class="clear_both"></div>--}}

{{--        </div>--}}
{{--          <div class="modal-footer">--}}
{{--            <div id="review_raiting">--}}
{{--                <div id="raiting_title"><span class="require"></span>Оцените преподавателя</div>--}}

{{--                <div id="raiting_buttons">--}}

{{--                    <input type="radio" id="review_rait_3" name="like" value="2">--}}
{{--                    <label for="review_rait_3">--}}
{{--                        <i style="color: #5BC963;" title="Отлично! Очень понравилось заниматься с учителем" class="fa fa-smile-o" aria-hidden="true"></i>--}}
{{--                    </label>--}}

{{--                    <input type="radio" id="review_rait_1" name="like" value="1">--}}
{{--                    <label for="review_rait_1">--}}
{{--                        <i style="color: #d9534f;" title="Не понравилось! Отрицательная оценка" class="fa fa-frown-o" aria-hidden="true"></i>--}}
{{--                    </label>--}}

{{--                </div>--}}

{{--            </div>--}}
{{--            <button id="send_review_button" type="submit" class="btn btn-primary">--}}
{{--                <i class="loading fa fa-spinner fa-pulse fa-fw"></i>Отправить--}}
{{--            </button>--}}
{{--          </div>--}}
{{--      </form>--}}
{{--    </div>--}}
{{--  </div>--}}
{{--</div>--}}

@endsection

@section('style')

{{ Html::style('front/css/flags.css') }}

<style>
.breadcrumb {
    margin-bottom: 7px;
    padding: 4px 15px;
}
.btn-default {
    color: #767676;
    background-color: transparent;
    border: 1px solid #9d9d9d26;
}
</style>

@endsection

@section('script')


<script type="text/javascript">

$(function() {

//отправление заявки
    $('#teach_place_select').on('change', function(){
        var pupil_adress_block = $('#pupil_adress_block');
        pupil_adress_block.empty();
        if($(this).val() == 'pupil_teacher' || $(this).val() == 'pupil'){

            pupil_adress_block.append('<label for="pupil_adress" class="col-lg-3 col-md-3 col-sm-3 col-xs-12">Адресс</label><div class="col-lg-9 col-md-9 col-sm-9 col-xs-12"><input placeholder="город, район, метро, улица (квартиру не нужно)" type="text" class="form-control" id="pupil_adress" name="pupil_adress"></div>');
            
        }else{

        }

    });
    // переключение шагов в отправке заявки учителю
    $('.step_button').on('click', function(){

        var step_button = $(this);
        if(step_button.hasClass('check_fields')){
            var required_fields = step_button.closest('.part_element').find('[data-tooltip]');
            var check = 0;
            required_fields.map(function() {
                var current_field = $(this).find('select');
                if (current_field.val() == '') {
                    showTip($(this));
                    check += 1;
                }

            })

            if (step_button.closest('.part_element').find('.checkbox_block :checkbox:checked').length == 0) {
                    showTip($('.checkbox_block'));
                    check += 1;
            }

            if(check == 0){
                var close_block = step_button.attr("data-close");
                var open_block = step_button.attr("data-open");
                $('#'+close_block).hide();
                $('#'+open_block).show();
            }
        }else{
            var close_block = step_button.attr("data-close");
            var open_block = step_button.attr("data-open");
            $('#'+close_block).hide();
            $('#'+open_block).show();
        }


    });
// end


    $(".tab_item").not(":first").hide();
$(".wrapper .tab").click(function() {
    $(".wrapper .tab").removeClass("active").eq($(this).index()).addClass("active");
    $(".tab_item").hide().eq($(this).index()).fadeIn()
}).eq(0).addClass("active");

    //преобразование предметов
    var subjects = $('.teach_subjects');
    subjects.each(function(ind, el){
        var text = $(el).text().trim();
        var arr = text.split(',');

        var res = '';

        for (var i = 0; i < arr.length; i++) {

             res = res + '<span class="one_subject">' + arr[i] + '</span>';

       }

        $(el).html(res);

   });

//показ телефона
  $('#show_contact_dates').click(function(){

        $.ajax({
            type: "GET",
            url: "{{ route('show_contact_dates') }}",
            data: {id: {{ $teacher->id }} },
            success: function(data) {
                $('#show_contact_dates').html(data);

            }
        });

  });

  $("#send_user_mail form").submit(function(event) {

       var recaptcha = $("#g-recaptcha-response").val();
       
       if (recaptcha === "") {

            var recaptcha_block = $(".captcha_row");
            recaptcha_block.css('border', '1px solid #ffb23e');
            showTip(recaptcha_block);
            setTimeout( function(){
            recaptcha_block.css('border','none');
            },5000);
            event.preventDefault();

       }
    });

  function showTip(current) {
    console.log(current);
    var tipText = current.attr('data-tooltip');
    var element = document.createElement('div');
    element.className = 'my_tooltip';
    element.innerHTML = tipText;
    $(current).append(element);
    //$(element).delay( 4000 ).fadeOut('slow');
    $(current).on('click', function(){
        $(element).fadeOut();
    });
    

    
}


if(window.location.hash) {

    $("#share_profile").modal("show");

}

if ($(window).width() < 768) {
    var side_bar = $('#side_bar').detach();
    $( "#main_content_inner" ).after( side_bar );

    var tutor_avatar_sidebar = $('#tutor_avatar_sidebar').detach();
    $( "#tut_name" ).after( tutor_avatar_sidebar );
}

    
});

</script>

@endsection

