@extends('layouts/app_sidebar_right')

@php

    $title = '';
    $keywords = __('meta.tutors_key');
    $description = '';

    if( $data_teachers['subject'] != 'subjects'){
        $title .= __('meta.tutors_from_subject') . $data_teachers['subject']->name;
        $keywords .= $data_teachers['subject']->name . ', ';
        $description .=  __('meta.tutors_descr_1'). $data_teachers['subject']->name .__('meta.tutors_descr_2');

        if($data_teachers['teach_city_en'] != 'all-city' && $data_teachers['teach_city_en'] != 'online'){
          $title .= __('meta.tutors_title_2') . $data_teachers['teach_city_current'];
          $keywords .= $data_teachers['teach_city_current'] . ', ';
          $description = __('meta.tutors_descr_3') . $data_teachers['teach_city_current'] . '. ' . $description;
        }
        if(isset($data_teachers['teach_online']) && $data_teachers['teach_online'] == 'online'){
          $title .= __('meta.tutors_title_3');
          $keywords .=  __('meta.tutors_key_2');
          $description = __('meta.tutors_descr_4') . '. ' . $description;
        }
    }else{
      if($data_teachers['teach_city_en'] != 'all-city'){

        $title .= __('meta.tutors_descr_3') . $data_teachers['teach_city_current'];
        $keywords .= $data_teachers['teach_city_current'] . ', ';
        $description .=  __('meta.tutors_descr_5') . $data_teachers['teach_city_current'] .__('meta.tutors_descr_6');

      }elseif(isset($data_teachers['teach_online']) && $data_teachers['teach_online'] == 'online'){

          $title .= __('meta.tutors_title_4');
          $keywords .=  __('meta.tutors_key_2');
          $description .=  __('meta.tutors_descr_7');

      }else{

        $title .= __('meta.tutors_title_5');
        $keywords .=  __('meta.tutors_key_3');
        $description .=  __('meta.tutors_descr_8');

      }
    }

    if(is_object($data_teachers['sub_dir']) && $data_teachers['sub_dir']->count() != 0){
      $sub_dir_exist = '1';
    }else{
      $sub_dir_exist = null;
    }

@endphp

@section('title_meta')
{{ $title }} | WowKnow.com @if( $teachers->currentPage() > 1) @lang('meta.tutors_page') {{ $teachers->currentPage() }} @endif
@endsection

@section('description_meta')
{{ $description }} @lang('meta.tutors_descr_9') @if( $teachers->currentPage() > 1) @lang('meta.tutors_page') {{ $teachers->currentPage() }} @endif
@endsection

@section('keywords_meta')
{{ $keywords }}
@endsection

@section('title_page')

    {{ $title }} 

@endsection

@section('side_bar')
  @include('parts/adsense_sidebar')
@endsection

@section('content')

<div id="search-tutor" class="panel panel-default">

    <div id="top_current_page_menu" class="no_margin row">
    <div id="accordion" >

    <div class="col-md-3 col-lg-3 col-sm-6 col-xs-6 no_padding mob_padding">
          <div title="Предмет обучения" class="panel-body">

            <select class="form-control" name="subject_id" id="subject_name">
                <option value="subjects">Предмет обучения</option>
                @foreach($subjects as $subjects)


                    <option @if( $data_teachers['subject'] != 'subjects') @if($subjects->alias == $data_teachers['subject']->alias) selected @endif @endif value="{{$subjects->alias}}">
                            {{ $subjects->name }}
                    </option>

                    
                @endforeach
            </select>

          </div>
     
    </div>

    @if($sub_dir_exist)

        <div class="col-md-3 col-lg-3 col-sm-6 col-xs-6 no_padding relative mob_padding mob_none">
          <div class="card-header panel-body" id="headingTwo">
            <label  data-toggle="collapse" aria-controls="collapseTwo" aria-expanded="false" data-target="#collapseTwo" class="control-label side-bar-label label_button">
              @lang('menus.sub_subject')
            </label>
          </div>  
          <div id="collapseTwo" class="collapse panel-body" aria-labelledby="headingTwo" data-parent="#accordion" >
            <div class="">
                <div class="one-block-menu">
                    <div class="wrap">
                        <div id="subject_direction" class="checkbox_group">

                            @foreach($data_teachers['sub_dir'] as $sub_dir_1)
                              <div class="checkbox">
                                  <input id="sub_dir_{{ $sub_dir_1->id }}" @if (strripos(Request::query('sub_dir'), "$sub_dir_1->id" ))  checked="checked" @endif name="sub_dir[]" type="checkbox" value=".{{ $sub_dir_1->id }}" class="css-checkbox"><label for="sub_dir_{{ $sub_dir_1->id }}" class="css-label">{{ $sub_dir_1->name_ru }}</label>
                              </div>
                            @endforeach

                        </div>
                    </div>
                </div>
            </div>
          </div>
        </div>

    @endif

    <div class="col-md-4 col-lg-4 col-sm-6 col-xs-6 no_padding @if($sub_dir_exist) exist_sub_dir @endif">
        <div title="@lang('menus.teach_place')" class="panel-body">

            <div class="one-block-menu">
                 <div class="wrap" id="type_lessons">

                    <div id="select_citu_block">

                      <select class="form-control" name="city_id" id="city_id">
                          <option value="all-city">Город</option>
                          @foreach($cities_teach as $one_city)

                            @if($one_city->teach_city_en != null)
                              <option @if($one_city->teach_city_en == $data_teachers['teach_city_en']) selected @endif value="{{$one_city->teach_city_en}}">
                                  @if($one_city->country_teach == 'ua')
                                      {{ $one_city->city_ua->title_ru }}
                                  @elseif($one_city->country_teach == 'ru')
                                      {{ $one_city->city_ru->title_ru }}
                                  @elseif($one_city->country_teach == 'by')
                                      {{ $one_city->city_by->title_ru }}
                                  @elseif($one_city->country_teach == 'kz')
                                      {{ $one_city->city_kz->title_ru }}
                                  @endif
                              </option>
                             @endif
                              
                          @endforeach
                      </select>

                    </div>
                    <div id="select_online_block">
                      <input type="radio" id="online" value="online" name="online_local" @if(Request::query('type_lessons') === 'online' ) checked @endif />
                      <label for="online">
                          @lang('menus.place_online')
                      </label> 
                    </div>
                </div>
            </div>
     
        </div>
    </div>

{{--     <div class="col-md-5 col-lg-5 col-sm-6 col-xs-6 no_padding relative tutor_select_gender">
          <div class="card-header panel-body" id="headingThree">
            <label class="label_button" data-toggle="collapse" aria-controls="collapseThree" aria-expanded="false" data-target="#collapseThree" class="control-label side-bar-label">
              @lang('menus.tutor_gen')
            </label>
          </div> 
      <div id="collapseThree" class="collapse panel-body" aria-labelledby="headingThree" data-parent="#accordion" >
        <div class="">
            <div class="one-block-menu">
                <div class="wrap" id="gender">
                    <div>
                        <input class="css-checkbox" type="radio" id="gender_mw" value="mw" name="gender_teacher" @if(Request::query('gender') === 'mw' or Request::query('gender') == '') checked @endif  />
                        <label class="css-label" for="gender_mw">
                            @lang('menus.not_matter')
                        </label>
                    </div>
                    <div>
                        <input class="css-checkbox" type="radio" id="gender_m" value="m" name="gender_teacher" @if(Request::query('gender') === 'm') checked @endif />
                        <label class="css-label" for="gender_m">
                             @lang('menus.male')
                        </label> 
                    </div>
                    <div>
                        <input class="css-checkbox" type="radio" id="gender_w" value="w" name="gender_teacher" @if(Request::query('gender') === 'w') checked @endif />
                        <label class="css-label" for="gender_w">
                            @lang('menus.female')
                        </label> 
                    </div>
                </div>
            </div>
        </div>
      </div>
    </div> --}}

    </div>
    </div>
    <div class="panel-body panel">

      <?php $rand_fid = rand ( 4 , 8 ); ?>

        @foreach($teachers as $index => $teacher)

{{--            @if($index == $rand_fid )--}}

{{--              <div style="" class="row one_row one_teacher" >--}}

{{--<ins class="adsbygoogle"--}}
{{--     style="display:block"--}}
{{--     data-ad-format="fluid"--}}
{{--     data-ad-layout-key="-fv+64+31-d5+c4"--}}
{{--     data-ad-client="ca-pub-3870576311603929"--}}
{{--     data-ad-slot="2755841537"></ins>--}}
{{--<script>--}}
{{--     (adsbygoogle = window.adsbygoogle || []).push({});--}}
{{--</script>--}}

{{--              </div>--}}

{{--            @else--}}

            <div class="row one_row one_teacher">
                <div class="col-lg-10 col-md-10 col-sm-9 col-xs-12 no_padding">
                    <div class="teacer_avatar_block">
                        @if($teacher->avatar)
                            <a href="{{ route('tutor_page', ['slug' => $teacher->slug ]) }}">
                                <img class="user_avatar" src="{{ URL::asset('storage/users/') }}/{{ $teacher->avatar }}" alt="репетитор {{ $teacher->name }}">
                            </a>
                        @else
                            <a href="{{ route('tutor_page', ['slug' => $teacher->slug ]) }}">
                                <img class="user_avatar" src="{{ URL::asset('front/images/tutor_avatar_crop/') }}/default.jpg" alt="репетитор {{ $teacher->name }}">
                            </a>
                        @endif
                    </div>
                    <h5 class="profile_title">
                      <a href="{{ route('tutor_page', ['slug' => $teacher->slug ]) }}">{{ $teacher->profile_title }}</a>
                    </h5>
                    
                    <div class="profile_info">

                        <div class="name">
                            {{ $teacher->name }} {{ $teacher->last_name }}
{{--                            <span class="flag flag-{{ $teacher->country_teach }}"></span>--}}
                        </div>
                        
                        <div class="teach_subjects">
                          <i class="fa fa-tag" aria-hidden="true"></i>  
                            @foreach($teacher->subjects as $subject)

                               <span class="one_subject"> {{ $subject->{'name_'.$local} }} </span>

                            @endforeach
                        </div>
                        <div class="profile_about">

                                <?php // echo str_limit($teacher->tutor_edu_text, 150); ?>

                            <i style="color: #3fc9d0; font-size: 14px; margin-top: 0px;" class="fa fa-map-marker" aria-hidden="true"></i>    

                            Место преподавания: 
                            
                            @if($teacher->teach_online)
                             <span style="font-weight: bold;"> онлайн </span> / 
                            @endif
                            @if($teacher->country_teach == 'ua' && $teacher->teach_city_en != null && $teacher->city_ua)
                              <span style="font-weight: bold;"> {{ $teacher->city_ua->title_ru }} </span>
                            @elseif($teacher->country_teach == 'ru' && $teacher->teach_city_en != null && $teacher->city_ru)
                              <span style="font-weight: bold;"> {{ $teacher->city_ru->title_ru }} </span>
                            @endif
                        </div>

                    </div>
                </div> 
                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12 no_padding user_profile_info_block">
                    @if($teacher->teacher_price)
                        <div class="price">
                            
                            <span data-valute="{{ $teacher->teacher_price }}" class="price_value">{{ $teacher->teacher_price }}</span>  <span class="price_detale">{{ App\MyHelpers\Helper::value_cur($teacher->cur_def) }}/@lang('tutors.search_class')</span>

                        </div>
                    @endif    
                    <div class="view_profile">
                        <a href="{{ route('tutor_page', ['slug' => $teacher->slug ]) }}" class="btn btn-info">Связяться с репетитором</a>
                    </div>


 {{--                    <button id="{{ $teacher->user_id }}" type="button" class="btn btn-info send_mail_open_modal" data-toggle="modal" data-target="#send_user_mail">@lang('tutors.search_write') <i class="fa fa-envelope" aria-hidden="true"></i></button> --}}

                </div>
            </div>

{{--            @endif--}}

        @endforeach
        
        <div id="pagination_wrapper">
            {{ $teachers->render() }}
        </div>    

<!-- Modal send mail -->
{{--     <div class="modal fade" id="send_user_mail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <div class="modal-title" id="exampleModalLabel"><span id="get_user_name"></span> @lang('tutors.search_send_letter')<img  id="get_user_avatar" src="/" alt="user avatar"></div>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          {!! Form::open(['route' => 'send_tutor_mail']) !!}
              <div class="modal-body">
                <div class="form-group row">
                  <label for="staticName" class="col-lg-2 col-md-2 col-sm-2 col-xs-2"> @lang('auth.your_name')</label>
                  <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
                    <input required type="text" class="form-control" id="staticName" name="your_name">
                  </div>
                </div>
                <div class="form-group row">
                  <label for="staticEmail" class="col-lg-2 col-md-2 col-sm-2 col-xs-2">@lang('auth.your_mail')</label>
                  <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
                    <input required type="email" class="form-control" id="staticEmail" name="your_email">
                  </div>
                </div>
                <div class="form-group row">
                  <label for="staticMessage" class="col-lg-2 col-md-2 col-sm-2 col-xs-2">@lang('auth.message')</label>
                  <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
                    <textarea required placeholder="@lang('auth.enter_message')" id="message_body" name="your_message" class="form-control"></textarea>
                  </div>
                </div>
                <div class="form-group row captcha_row" data-tooltip="Забыли отметить капчу, укажите что вы не робот )">
                    <label for="password-confirm" class="col-lg-2 col-md-2 col-sm-2 col-xs-2">@lang('auth.captcha')<span class="require"></span></label>
                    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
                        {!! app('captcha')->display() !!}
                    </div>
                </div>  
              </div>
              <div class="modal-footer">
                <input type="hidden" name="user_id">
                <button id="send_mail_button" type="submit" class="btn btn-primary">@lang('auth.send')</button>
              </div>
           {!! Form::close() !!}   
        </div>
      </div>
    </div> --}}



    </div>
</div>
@endsection

@section('style')

{{ Html::style('front/select2/select2.css') }}
{{ Html::style('front/css/flags.css') }}

@endsection

@section('script')

{{ Html::script('front/select2/select2.full.js') }}

<script type="text/javascript">

//получаем текущий путь
var url_cur = new Url();
//выбираем параметры юрл
var params = '?'+window.location.search.replace( '?', ''); 

//function tips before select subject red border
function show_tips_red_border() {
    subject_name.next().addClass("border_red");
    subject_name.next().next('.tips_hide').show();
}

$("#city_id").select2({
  tags: false,
  maximumSelectionLength: 20,
});

$("#subject_name").select2({
  tags: false,
  maximumSelectionLength: 20,
});

//скрываем выбор города если уроки онлайн
$('#local').on('click', function(){
    if ($(this).is( ":checked" )) {
        $('#city_block').show();
    }else{
        $('#city_block').hide();
    }
});
if ($('#local').is( ":checked" )) {
    $('#city_block').show();
}else{
    $('#city_block').hide();
}

$(function() {

    //отправляет пользователя после выбора предмета
    $('select#subject_name').on('select2:select', function (evt) {

        var city = $('#city_id').val();
        if (city == null) {
            var city = 'all-city';
        }
        var subject = $('#subject_name').val();
        var url = '{{ route("tutor_search_subject_city", [":subject", ":id"]) }}';
        url = url.replace(':id', city);
        url = url.replace(':subject', subject);
        if (params.length > 3) {
            url += params;
        }
        window.location.href = url;
    });

    //отправляет пользователя после выбора города
    $('select#city_id').on('select2:select', function (evt) {

        var url_cur = new Url();
        var city = $('#city_id').val();
        @if($data_teachers['subject'] != 'subjects')
            var subject = "{{ $data_teachers['subject']->alias }}";
        @else
            var subject = 'subjects';
        @endif
        var url = '{{ route("tutor_search_subject_city", [":subject", ":id"]) }}';
        url = url.replace(':id', city);
        url = url.replace(':subject', subject);
        if (params.length > 3) {
            url += params;
            url = url.replace("online", 'local');
        }else{
            url += '?type_lessons=local';
        }
        window.location.href = url;
    });

    //отправляет пользователя после выбора типа урока online
    $('#type_lessons #online').click( function (evt) {

        var subject_name = $('#subject_name');
        var url_cur = new Url();
        var params = $("input:checked").val();
        
        if (params == 'online') {
            var x  = url_cur.paths();
            url_cur.paths([x[0], x[1], 'online']);
        }
        url_cur.query.type_lessons = "online";
        console.log(url_cur);
        var url = url_cur.toString();
        url = url.replace("undefined", "subjects");
        //console.log(url);
        window.location.href = url;
        
    });  
    //отправляет пользователя после выбора пола репетитора
    $('#gender input').click( function (evt) {
        var url_cur = new Url();
        var params = $("#gender input:checked").val();
        url_cur.query.gender = params;
        if (params == 'mw') {
            delete url_cur.query.gender;
        }
        var url = url_cur.toString();
        window.location.href = url;
        
    });    
    //отправляет пользователя после выбора возраста ученика
    $('#pupil_age input').click( function (evt) {
        var url_cur = new Url();
        var inputs = $('#pupil_age input');
        var params = '';
        $.each(inputs, function(index, value){
            if ($(this).prop("checked")) {
                params += ($(this).val());
            }
            
        });
        if (params != '') {
            url_cur.query.p_age = params;
        }else{
            delete url_cur.query.p_age
        }
        var url = url_cur.toString();
        console.log(url);
        window.location.href = url;
        
    });     
    //отправляет пользователя после выбора подкатегории (направления) предмета обучения
    $('#subject_direction input').click( function (evt) {

        var url_cur = new Url();
        var inputs = $('#subject_direction input');
        var params = '';
        $.each(inputs, function(index, value){
            if ($(this).prop("checked")) {
                params += ($(this).val());
            }
            
        });
        if (params != '') {
            url_cur.query.sub_dir = params;
        }else{
            delete url_cur.query.sub_dir
        }
        var url = url_cur.toString();
        console.log(url);
        window.location.href = url;
        
    });    
});

//отправка сообщения репетитору
var send_mail = $('.send_mail_open_modal');
send_mail.on('click', function(){
    var user_id = $(this).attr('id');
    $("input[name='user_id']").val(user_id);
});

$('#send_user_mail').on('show.bs.modal', function (e) {

    var $button = $(e.relatedTarget); 
    var avatar_user = $(e.relatedTarget).closest('.one_teacher').find('.user_avatar').attr('src');
    var name_user = $(e.relatedTarget).closest('.one_teacher').find('.name a').text();
    $('#user_number').val($button.attr('id'));
    $('#get_user_name').text(name_user);
    $('#get_user_avatar').attr('src', avatar_user);
})

$("#send_user_mail form").submit(function(event) {

   var recaptcha = $("#g-recaptcha-response").val();
   
   if (recaptcha === "") {

        var recaptcha_block = $(".captcha_row");
        recaptcha_block.css('border', '1px solid #ffb23e');
        showTip(recaptcha_block);
        setTimeout( function(){
        recaptcha_block.css('border','none');
        },5000);
        event.preventDefault();

   }
});

function showTip(current) {
    console.log(current);
    var tipText = current.attr('data-tooltip');
    var element = document.createElement('div');
    element.className = 'my_tooltip';
    element.innerHTML = tipText;
    $(current).append(element);
    $(element).delay( 5000 ).fadeOut('slow');
}

$('#send_user_mail').on('hide.bs.modal', function (e) {
    $('#message_body').val('');
    $('.success_message').remove();
    $("input[name='user_id']").val('');
})

</script>

@endsection

