@extends('layouts/app_sidebar_right')

@section('title_page')
@lang('meta.articles_page_title')
@endsection

@section('title_meta')
@lang('meta.articles_page_title') @if( $posts->currentPage() > 1) @lang('articles_meta_title') {{ $posts->currentPage() }} @endif
@endsection

@section('description_meta')
@lang('articles_meta_descr')
@endsection

@section('keywords_meta')
@lang('articles_page_key')
@endsection

@section('side_bar')
  @include('parts/adsense_sidebar')
@endsection

@section('content')

<div id="blog_page">
    <div style="text-align: center; margin-bottom: 18px" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <a class="btn btn-info" href="{{ route('posts_user.add') }}">Додати статтю</a>
    </div>
  @foreach($posts as $row)
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 one_post">
      <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4 post_image" >
        <a href="{{ route('blog.one_page', $row->slug) }}">

          @if($row->sm_image)

            @if(strpos($row->sm_image, 'storage') !== false)
              <img src="{{ $row->sm_image }}" alt="{{$row->title}}">
            @else
              <img src="/storage/posts/shares/post_main_images/{{ $row->sm_image }}" alt="{{$row->title}}">
            @endif

          @else

            @if(strpos($row->image, 'storage') !== false)
              <img src="{{ $row->image }}" alt="{{$row->title}}">
            @else
              <img src="/storage/posts/shares/post_main_images/{{ $row->image }}" alt="{{$row->title}}">
            @endif
            
          @endif

        </a>
      </div>
      <div class="col-lg-9 col-md-9 col-sm-9 col-xs-8 post_text">
        <div class="post_title">
            <h3>
              <a href="{{ route('blog.one_page', $row->slug) }}">
                @if($row->title) {{$row->title}} @else {{$row->title_b}} @endif
              </a>
            </h3>
        </div>
        <div class="post_body">
          @if($row->excerpt) {{$row->excerpt}} @else {{$row->excerpt_b}} @endif
        </div>
      </div>
      <div class="post_tags">
          @foreach($row->tag_name as $row)
            <div class="post_tag"><a href="{{ route('posts.one_category', $row->slug) }}">{{ $row->name }}</a></div>
          @endforeach
      </div>  
    </div>
  @endforeach
  
  <div id="pagination_wrapper">
      {{ $posts->render() }}
  </div>    
</div>

@endsection

@section('script')

@endsection

      

