@extends('layouts/app_sidebar_no_big_title')

@section('title_meta')
  @if($post->title) {{ $post->title }} @else {{ $post->title_b }} @endif | Wowknow.com - блог
@endsection

@section('title_og')
  @if($post->title) {{ $post->title }} @else {{ $post->title_b }} @endif | Wowknow.com - блог
@endsection

@section('description_meta')
@if($post->excerpt) 
  {{ mb_strimwidth(strip_tags($post->excerpt), 0, 150, "...") }} 
@else 
  {{ mb_strimwidth(strip_tags($post->excerpt_b), 0, 150, "...") }}
@endif
@endsection

@section('description_og')
@if($post->excerpt) 
  {{ mb_strimwidth(strip_tags($post->excerpt), 0, 150, "...") }} 
@else 
  {{ mb_strimwidth(strip_tags($post->excerpt_b), 0, 150, "...") }}
@endif
@endsection

@section('og_image_meta')
@if($post->image)
http://wowknow.com{{ $post->image }}
@else 
{{ URL::asset('front/images/wowknow_img.png') }}
@endif
@endsection

@section('keywords_meta')
@foreach($post->tag_name as $tag){{ $tag->{'name_'.$local} }}, @endforeach
@endsection

@section('title_page')

<div id="one_post_container_title">

  <nav aria-label="breadcrumb">
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="/">Домашняя</a></li>
      <li class="breadcrumb-item"><a href="{{ route('posts.index') }}">Все статьи</a></li>
      <li class="breadcrumb-item active" aria-current="page">{{ $post->title }}</li>
    </ol>
  </nav>

  @if( isset($post->sm_image) || isset($post->image) )
    <div id="post_wrapp_img">
      
      <div id="post_wrapp_title">
        <h1>
          @if($post->title) {{ $post->title }} @else {{ $post->title_b }} @endif
        </h1>
      </div>

      @if($post->image)

        @if(strpos($post->image, 'storage') !== false)
          <img src="{{ $post->image }}" alt="{{$post->title}}">
        @else
          <img src="/storage/posts/shares/post_main_images/{{ $post->image }}" alt="{{$post->title}}">
        @endif

      @else

        @if(strpos($post->sm_image, 'storage') !== false)
          <img src="{{ $post->sm_image }}" alt="{{$post->title}}">
        @else
          <img src="/storage/posts/shares/post_main_images/{{ $post->sm_image }}" alt="{{$post->title}}">
        @endif
        
      @endif

    </div>  
  @else
    <div id="post_wrapp_title">
      <h1>
        @if($post->title) {{ $post->title }} @else {{ $post->title_b }} @endif
      </h1>
    </div>
  @endif  


  
</div>

@endsection

@section('side_bar')
  @include('parts/adsense_sidebar')
@endsection

@section('content')

{{-- @foreach($post->tag_name as $tag)
   {{$tag->name}}, 
@endforeach --}}

{{-- dump($post->tag_name) --}}

<div id="one_page_blog">
  <div id="text_body">
    @if($post->content) {!! $post->content !!} @else {!! $post->content_b !!} @endif
  </div>

  <div id="similar_posts">
    <p class="similar_title">Схожі статті</p>
    <div class="row">
      @foreach($similar_posts as $post)

        <div class="col-md-4 col-sm-6 col-xs-6 one_similar_post">
          <a href="{{ route('blog.one_page', $post->slug) }}">

            <div class="img_wrapp">
              @if(strpos($post->sm_image, 'storage') !== false)
                <img src="{{ $post->sm_image }}" alt="{{$post->title}}">
              @else
                <img src="/storage/posts/shares/post_main_images/{{ $post->sm_image }}" alt="{{$post->title}}">
              @endif
            </div>

            <span class="post_title">{{ $post->title }}</span>
          </a>
        </div>

      @endforeach
    </div>
  </div>

  <div id="share_block_wrapp">
      <div id="share_page_block">
          <p>@lang('menus.share_page')</p>
          <div id="soc_icons_block" class="btn-group">

          <a class="btn btn-default"
          style="background-color: #3b5998" 
          target="_blank"
          title="On Facebook"
          href="http://www.facebook.com/sharer.php?u={{url()->current()}}">
              <i style="color: #fff" class="fa fa-facebook fa-lg fb"></i>
          </a>

          <a class="btn btn-default"
          style="background-color: #3b5998" 
          target="_blank"
          title="Like On Facebook"
          href="http://www.facebook.com/plugins/like.php?href={{url()->current()}}">
              <i style="color: #fff" class="fa fa-thumbs-o-up fa-lg fb"></i>
          </a>

          <a class="btn btn-default"
          style="background-color: #45668e"
          target="_blank"
          title="On VK.com" 
          href="http://vk.com/share.php?url={{url()->current()}}">
              <i style="color: #fff" class="fa fa-vk fa-lg vk"></i>
          </a>

          <a class="btn btn-default"
          style="background-color: #dd4b39"
          target="_blank"
          title="On Google Plus"
          href="https://plusone.google.com/_/+1/confirm?hl=en&url={{url()->current()}}">
              <i style="color: #fff" class="fa fa-google-plus fa-lg google"></i>
          </a>

          <a class="btn btn-default"
          style="background-color: #55acee"
          target="_blank"
          title="On Twitter"
          href="http://twitter.com/share?url={{url()->current()}}">
              <i style="color: #fff" class="fa fa-twitter fa-lg tw"></i>
          </a>

      </div>
    </div>
  </div>  
</div>

<div id="reviews">

  <label for="review_text"><div id="review_title">Комментарии</div></label>

    @if($reviews)

      <div class="exist_reviews">
          @foreach($reviews as $item)

              <div class="one_review">
                  <div class="review_text">
                      <p>{!! nl2br($item->review) !!}</p>
                  </div>
                  <div class="review_meta">
                      <span>{{$item->sender_name}}</span> <span>{{$item->created_at->format('d.m.Y') }}</span>
                  </div>
              </div>

          @endforeach
      </div>

  @endif

  <form class="form-horizontal" method="POST" action="{{ route('review_add') }}">
      {{ csrf_field() }}

      <div class="form-group">
          <div class="col-md-8">
              <textarea id="review_text" placeholder="Добавить свой комментарий" class="form-control" required name="review_text" id="" cols="40" rows="3"></textarea>
              @if ($errors->has('review_text'))
                  <span class="help-block">
                      <strong>{{ $errors->first('review_text') }}</strong>
                  </span>
              @endif
          </div>
      </div>

      <div class="form-group margin_b_0 {{ $errors->has('sender_name') ? ' has-error' : '' }}">
          <div class="col-md-4">
              <input placeholder="@lang('auth.your_name')" id="sender_name" type="text" class="form-control" name="sender_name" value="{{ old('sender_name') }}" required>
          </div>
      </div>

      <div class="form-group">
          <label for="password-confirm" class="col-md-12 control-label">@lang('auth.captcha')<span class="require"></span></label>
          <div class="col-md-6">
              {!! app('captcha')->display() !!}
          </div>
          <input type="hidden" name="item_id" value="{{ $post->id }}">
          <input type="hidden" name="review_type" value="post">
      </div>            

      <div class="form-group">
          <div class="col-md-12">
              <button type="submit" class="btn btn-success">
                  Сохранить комментарий <i class="fa fa-plus-circle"></i>
              </button>
          </div>
      </div>

  </form>


</div>     

@endsection

@section('script')

@endsection

      

