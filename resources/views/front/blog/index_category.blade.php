@extends('layouts/app_sidebar_right')

@section('title_page')
@lang('meta.articles_category_title'): {{ $tag->{'name_'.$local} }}.
@endsection

@section('title_meta')
@lang('meta.articles_category_title'): {{ $tag->{'name_'.$local} }}. | Wowknow - блог
@endsection

@section('description_meta')
@lang('meta.articles_category_title') {{ $tag->{'name_'.$local} }} @lang('meta.articles_category_descr') @if( $posts->currentPage() > 1) | Страница - {{ $posts->currentPage() }} @endif
@endsection

@section('keywords_meta')
{{ $tag->{'name_'.$local} }}, @lang('meta.articles_category_key') {{ $tag->{'name_'.$local} }}
@endsection

@section('side_bar')
 @include('parts/adsense_sidebar')
@endsection

@section('content')

<div id="blog_page">
  @foreach($posts as $row)
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 one_post">
      <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4 post_image" >
        <a href="{{ route('blog.one_page', $row->slug) }}">
          <img src="/storage/posts/shares/post_main_images/{{ $row->sm_image ? $row->sm_image : $row->image }}" alt="{{$row->title}}">
        </a>
      </div>
      <div class="col-lg-9 col-md-9 col-sm-9 col-xs-8 post_text">
        <div class="post_title">
            <h3>
              <a href="{{ route('blog.one_page', $row->slug) }}">
                @if($row->title) {{$row->title}} @else {{$row->title_b}} @endif
              </a>
            </h3>
        </div>
        <div class="post_body">
          @if($row->excerpt) {{$row->excerpt}} @else {{$row->excerpt_b}} @endif
        </div>
      </div>
      <div class="post_tags">
          @foreach($row->tag_name as $row)
            <div class="post_tag"><a href="{{ route('posts.one_category', $row->slug) }}">{{ $row->name }}</a></div>
          @endforeach
      </div>  
    </div>
  @endforeach
</div>

@endsection

@section('script')

@endsection

      

