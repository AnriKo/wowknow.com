@extends('layouts/app_sidebar_no_big_title')

@section('content')

    <h1 class="col-md-offset-2">Додати статтю в базу сайту</h1>

    <form method="POST" action="{{ route('posts_user.save') }}" class="form-horizontal check_captcha" enctype="multipart/form-data">

        {{ csrf_field() }}

        <div class="form-group">
            <label class="col-sm-2 control-label" for="title">Заголовок</label>
            <div class="col-sm-10">
                <input required type="text" class="form-control" id="title" name="title">
            </div>
        </div>

{{--        <div class="form-group">--}}
{{--            <label class="col-sm-2 control-label" for="tags">Категорія</label>--}}
{{--            <div class="col-sm-10">--}}
{{--                <select multiple class="form-control" name="tags[]" id="tags">--}}
{{--                  @foreach($tags as $row_id => $row_name)--}}
{{--                    <option value="{{$row_id}}">{{ $row_name }}</option>--}}
{{--                  @endforeach--}}
{{--                </select>--}}
{{--            </div>--}}
{{--        </div>--}}

        <div class="form-group">
            <label class="col-sm-2 control-label" for="title">Головне зображення статті</label>
            <div class="col-sm-10">

                <div class="input-group">
                    <input class="form-control" required id="thumbnail" class="form-control" type="file" name="image">
                </div>

            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-2 control-label" for="content">Текст статті</label>
            <div class="col-sm-10">
                <textarea id="content" name="post_content" class="ckeditor form-control"></textarea>
                <input style="display: none" checked value="1" type="radio" name="post_or_page">
                <input style="display: none" class="form-control" type="text" value="1" name="user_create">
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-2 control-label">@lang('auth.captcha')<span class="require"></span></label>
            <div class="col-md-10">
                {!! app('captcha')->display() !!}
                <span style="display: none;" id="captcha_verify"></span>
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-2 control-label"></label>
            <div class="col-sm-10 col-md-offset-2">
                <input class=" btn btn-primary" type="submit" value="Зберегти статтю">
            </div>
        </div>

      </form>

@endsection

@section('script')

<script src="{{ URL::to('admin/js/new_package/tinymce/js/tinymce/tinymce.min.js') }}"></script>

<script>
    var editor_config = {
        path_absolute: "/",
        selector: '#content',
        menubar: false,
        plugins: [
            "autolink lists link charmap hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars code fullscreen",
            "insertdatetime media nonbreaking save table contextmenu directionality",
            "emoticons template paste textcolor colorpicker textpattern"
        ],
        toolbar: "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent ",
    };

    tinymce.init(editor_config);

</script>

@endsection