@extends('layouts/app_sidebar_right')

@php

@endphp

@section('title_meta')
   {{ $current_subject }} - все вузы, университеты, институты в Украинне по данному направлению обучения
@endsection

@section('title_page')
    "{{ $current_subject }}" - поиск вузов, университетов, институтов в Украине
@endsection

@section('side_bar')


@endsection

@section('content')

<div id="search_page" class="panel panel-default">
    <div id="univer_search_page" class="panel-body">

        @foreach($univers as $univer)

        <div class="row one_row one_courses">
            <div class="col-lg-8 col-md-8 col-sm-6 col-xs-12 no_padding">
                @if($univer->logo)
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no_padding univer_logo_block">
                        <a href="{{ route('univer_page', ['slug' => $univer->slug]) }}">
                            <img class="univer_avatar" src="http://wowknow.io/storage/universities/{{ $univer->logo }}" alt="{{ $univer->title }}">
                        </a>
                        <div class="adress">
                            <i class="fa fa-map-marker"></i>
                            <span class="city">
                                {{ $univer->city }}
                            </span>
                        </div> 
                    </div>
                @endif

                <div class="name">
                    <a href="{{ route('univer_page', ['slug' => $univer->slug]) }}">
                        {{ $univer->title }}
                    </a>
                </div>
                <div class="grey_color">

                    @if($univer->type == 'state')
                        Государственный
                    @elseif($univer->type == 'private')
                        Частный
                    @endif

               </div> 
           </div> 
           <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 no_padding univer_subject"> 
               <ul>
                    @foreach($univer->subjects_name as $item)

                        <li>
                            <span class="defis_ul">–</span>{{ $item->{'name_'.$local} }}
                        </li>

                        @if($loop->index == 6)

                            <li class="show_all">Все направления <i class="fa fa-angle-down"></i></li>

                        @endif

                    @endforeach
                </ul>
            </div>
            {{-- dump($univer->subjects_name) --}}

        </div>

        @endforeach
        
        <div id="pagination_wrapper">
            {{ $univers->render() }}
        </div>    

    </div>
</div>
@endsection

@section('style')

{{ Html::style('front/select2/select2.css') }}
{{-- Html::style('front/nouislider/nouislider.css') --}}
{{ Html::style('front/css/flags.css') }}

@endsection

@section('script')

</script>

@endsection

