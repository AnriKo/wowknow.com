@extends('layouts/app_sidebar_no_big_title')

@section('title_meta')
{{ $meta['meta_title'] }}
@endsection

@section('description_meta')
{{ $meta['meta_description'] }}
@endsection

@section('description_og')
{{ $meta['meta_description'] }}
@endsection

@section('keywords_meta')
{{ $meta['meta_keywords'] }}
@endsection

@section('og_image_meta')
@if($univer->logo)
{{ URL::asset('storage/universities/') }}/{{ $univer->logo }}
@else 
{{ URL::asset('front/images/wowknow_img.png') }}
@endif
@endsection

@section('title_og')
{{ $meta['meta_title_og'] }}
@endsection

@section('side_bar')
    @include('parts/adsense_sidebar')
@endsection

@section('content')

<div id="one_page" class="panel-default">
{{--    <div class="ads_top_gorizont">--}}
{{--      <ins class="adsbygoogle"--}}
{{--           style="display:block"--}}
{{--           data-ad-client="ca-pub-3870576311603929"--}}
{{--           data-ad-slot="7892618142"--}}
{{--           data-ad-format="auto"--}}
{{--           data-full-width-responsive="true"></ins>--}}
{{--      <script>--}}
{{--           (adsbygoogle = window.adsbygoogle || []).push({});--}}
{{--      </script>--}}
{{--    </div>--}}
    <div id="univer_page" class="panel-body">
        <div class="row one_row one_courses">

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no_padding univer_title_block">
                @if($univer->logo)
                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-3 univer_logo no_padding">
                        <img class="univer_avatar" src="{{ URL::asset('storage/universities') . '/' . $univer->country . '/' . $univer->logo }}" alt="{{ $univer->title }}">
                    </div>
                @endif
                <div class="col-lg-10 col-md-10 col-sm-10 col-xs-9 no_padding univer_name">
                    <h1>
                        {{$univer->title}}
                    </h1>
                </div>
            </div> 

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no_padding univer_menu_block">
                <ul>
                    <li><label for="review_text">@lang('univers.page_rewievs')</label></li>
                    <li><label for="review_text">@lang('univers.page_add_rewievs') <i class="fa fa-plus-circle"></i></label></li>
                </ul>
            </div> 

            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 no_padding univer_adress"> 
                {!! $univer->adress_info !!}
            </div>

            <div class="col-lg-5 col-md-5 col-sm-6 col-xs-12 no_padding univer_aditional_info">
                <div class="univer_subjects">
                    <h3 class="sub_title"><i class="fa fa-graduation-cap"></i>@lang('univers.page_direction_study')</h3>
                    <div class="teach_subjects">
                        <ul>
                            @foreach($univer->subjects_name as $item)
                                <li>
                                    <a href="{{ route('univer_city_subject', ['subject' => $item->slug, 'city' => 'all']) }}">{{ $item->{'name_'.$local} }}</a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>   
                @if($univer->short_info)
                    <div class="univer_short_info">
                        {!! $univer->short_info !!}
                    </div>
                @endif 
            </div> 

             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no_padding univer_description"> 
                <h2 class="sub_title"><i class="fa fa-university"></i>@lang('univers.univer_descr')</h2> 
                 <div class="about_cours_text">
                    {!! $univer->description !!}
                 </div>   
             </div> 

            <div id="share_block_wrapp"> 
                <div id="share_page_block">
                    <p>@lang('menus.share_page')</p>
                    <div id="soc_icons_block" class="btn-group">

                        <a class="btn btn-default"
                        style="background-color: #3b5998" 
                        target="_blank"
                        title="On Facebook"
                        href="http://www.facebook.com/sharer.php?u={{url()->current()}}">
                            <i style="color: #fff" class="fa fa-facebook fa-lg fb"></i>
                        </a>

                        <a class="btn btn-default"
                        style="background-color: #3b5998" 
                        target="_blank"
                        title="Like On Facebook"
                        href="http://www.facebook.com/plugins/like.php?href={{url()->current()}}">
                            <i style="color: #fff" class="fa fa-thumbs-o-up fa-lg fb"></i>
                        </a>

                        <a class="btn btn-default"
                        style="background-color: #45668e"
                        target="_blank"
                        title="On VK.com" 
                        href="http://vk.com/share.php?url={{url()->current()}}">
                            <i style="color: #fff" class="fa fa-vk fa-lg vk"></i>
                        </a>

                        <a class="btn btn-default"
                        style="background-color: #dd4b39"
                        target="_blank"
                        title="On Google Plus"
                        href="https://plusone.google.com/_/+1/confirm?hl=en&url={{url()->current()}}">
                            <i style="color: #fff" class="fa fa-google-plus fa-lg google"></i>
                        </a>

                        <a class="btn btn-default"
                        style="background-color: #55acee"
                        target="_blank"
                        title="On Twitter"
                        href="http://twitter.com/share?url={{url()->current()}}">
                            <i style="color: #fff" class="fa fa-twitter fa-lg tw"></i>
                        </a>

                    </div>
                </div>
            </div>
            </div>

        <div id="reviews" class="row one_row no_padding"> 
            <label for="review_text">
                <div id="review_title">
                    @lang('univers.page_rewievs_about')
                </div>
            </label>

            @if($reviews)

                <div class="exist_reviews">
                  @foreach($reviews as $item)

                      <div class="one_review">
                          <div class="review_text">
                              <p>{!! nl2br($item->review) !!}</p>
                          </div>
                          <div class="review_meta">
                              <span>{{$item->sender_name}}</span> <span>{{$item->created_at->format('d.m.Y') }}</span>
                          </div>
                      </div>

                  @endforeach
                </div>

            @endif


{{--            <form class="form-horizontal" method="POST" action="{{ route('review_add') }}">--}}
{{--              {{ csrf_field() }}--}}

{{--              <div class="form-group">--}}
{{--                  <div class="col-md-8">--}}
{{--                      <textarea id="review_text" placeholder="Добавить свой отзыв" class="form-control" required name="review_text" id="" cols="40" rows="3"></textarea>--}}
{{--                      @if ($errors->has('review_text'))--}}
{{--                          <span class="help-block">--}}
{{--                              <strong>{{ $errors->first('review_text') }}</strong>--}}
{{--                          </span>--}}
{{--                      @endif--}}
{{--                  </div>--}}
{{--              </div>--}}

{{--              <div class="form-group margin_b_0 {{ $errors->has('sender_name') ? ' has-error' : '' }}">--}}
{{--                  <div class="col-md-4">--}}
{{--                      <input placeholder="@lang('auth.your_name')" id="sender_name" type="text" class="form-control" name="sender_name" value="{{ old('sender_name') }}" required>--}}
{{--                  </div>--}}
{{--              </div>--}}

{{--              <div class="form-group">--}}
{{--                  <label for="password-confirm" class="col-md-12 control-label">@lang('auth.captcha')<span class="require"></span></label>--}}
{{--                  <div class="col-md-6">--}}
{{--                      {!! app('captcha')->display() !!}--}}
{{--                  </div>--}}
{{--                  <input type="hidden" name="item_id" value="{{ $univer->id }}">--}}
{{--                  <input type="hidden" name="review_type" value="univer">--}}
{{--              </div>            --}}

{{--              <div class="form-group">--}}
{{--                  <div class="col-md-12">--}}
{{--                      <button type="submit" class="btn btn-success">--}}
{{--                          @lang('univers.page_add_rewievs') <i class="fa fa-plus-circle"></i>--}}
{{--                      </button>--}}
{{--                  </div>--}}
{{--              </div>--}}

{{--            </form>--}}


        </div>  

    </div>
</div>
        
@endsection

@section('style')
    {{ Html::style('front/css/flags.css') }}
    {{ Html::style('front/css/star_rating_plugin.css') }}
@endsection

@section('script')
    {{ Html::script('front/js/star_rating_plugin.js') }}

<script type="text/javascript">

    $('.univer_aditional_info a, .univer_description a').attr('target','_blank');
    $("a[name='faculties'], a[name='entrant'], a[name='about']").next('div').attr('class','univer_descr_label');
    $(function(){                   // Start when document ready
        $('#star-rating').rating(); // Call the rating plugin
    });     
    $(function(){                   // Start when document ready
        $('.star-rating').rating(); // Call the rating plugin
    }); 
  //при загрузке страницы
  $( document ).ready(function() {
        

  }); 

</script>

@endsection

