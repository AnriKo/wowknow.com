@extends('layouts/app_sidebar_right')

@php

@endphp

@section('title_meta')
{{ $meta['meta_title'] }}
@endsection

@section('description_meta')
{{ $meta['meta_description'] }}
@endsection

@section('keywords_meta')
{{ $meta['meta_keywords'] }}
@endsection

@section('title_og')
{{ $meta['meta_title_og'] }}
@endsection

@section('title_page')
{{ $meta['page_title'] }}
@endsection

@section('side_bar')
    @include('parts/adsense_sidebar')
@endsection

@section('content')

<div id="top_current_page_menu" class="no_margin row">

    <div style="padding-right: 15px" class="one_block_menu col-lg-6 col-md-6 col-sm-6 col-xs-12 no_padding">
          <select name="city" id="select_cities_univer" class="js-example-basic-single form-control">
            <option value="all">@lang('univers.all_cities')</option>
            @foreach(App\MyHelpers\Helper_two::univers_cities() as $city_one)
              <option @if($city_alias == $city_one->title_en) selected @endif value="{{ $city_one->title_en }}">{{ $city_one->name }}</option>
            @endforeach
          </select> 
    </div>

    <div class="one_block_menu col-lg-6 col-md-6 col-sm-6 col-xs-12 no_padding">
          <select name="subject" id="select_subject_univer" class="js-example-basic-single form-control">
            <option value="all">@lang('univers.all_subjects')</option>
            @foreach(App\MyHelpers\Helper_two::univers_subject() as $subject_one)
              <option @if($subject == $subject_one->slug) selected @endif value="{{ $subject_one->slug }}">{{ $subject_one->name }}</option>
            @endforeach
          </select>
    </div>

</div>

<div id="search_page" class="panel panel-default">
    <div id="univer_search_page" class="panel-body">

        @foreach($univers as $univer)

        <div class="row one_row one_courses">
            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 no_padding">
                @if($univer->logo)
                    <div class="univer_logo_block">
                        <a href="{{ route('univer_page', ['slug' => $univer->slug]) }}">
                            <img class="univer_avatar" src="{{ URL::asset('storage/universities') . '/' . $univer->country . '/' . $univer->logo }}" alt="{{ $univer->title }}">
                        </a>
                    </div>
                @endif

                <div class="adress">
                    <i class="fa fa-map-marker"></i>
                    <span class="city">
                        {{ $univer->city }}
                    </span>
                </div> 

                <div class="name">
                    <a href="{{ route('univer_page', ['slug' => $univer->slug]) }}">
                        {{ $univer->title }}
                    </a>
                </div>
                <div class="grey_color">

                    @if($univer->type == 'state')
                        Государственный
                    @elseif($univer->type == 'private')
                        Частный
                    @endif

               </div> 
           </div> 
           <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 no_padding univer_subject"> 
               <ul>
                    @foreach($univer->subjects_name as $item)

                        <li>
                            <span class="defis_ul">–</span>{{ $item->{'name_'.$local} }}
                        </li>

                        @if($loop->index == 6)

                            <li class="show_all">Все направления <i class="fa fa-angle-down"></i></li>

                        @endif

                    @endforeach
                </ul>
            </div>
            {{-- dump($univer->subjects_name) --}}

        </div>

        @endforeach
        
        <div id="pagination_wrapper">
            {{ $univers->render() }}
        </div>    

    </div>
</div>
@endsection

@section('style')

{{ Html::style('front/select2/select2.css') }}
{{-- Html::style('front/nouislider/nouislider.css') --}}
{{ Html::style('front/css/flags.css') }}

@endsection

@section('script')

{{ Html::script('front/select2/select2.full.js') }}

<script type="text/javascript">

    $("select").select2({
      tags: false,
      maximumSelectionLength: 20
    });

    //отправляет пользователя после выбора города или предмета
    $('select#select_cities_univer, select#select_subject_univer').on('select2:select', function (evt) {

        var url_cur = new Url();
        var city = $('#select_cities_univer').val();
        var subject = $('#select_subject_univer').val();
           
        var url = '{{ route("univer_city_subject", [":city", ":subject"]) }}';
        url = url.replace(':city', city);
        url = url.replace(':subject', subject);
        window.location.href = url;
    });

</script>

@endsection

