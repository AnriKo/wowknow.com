@extends('layouts/app_sidebar_right')

@section('title_meta')
    @lang('meta.univer_cities_meta_title')
@endsection

@section('title_page')
    @lang('meta.univer_cities_page_title')
@endsection

@section('side_bar')
    @include('parts/adsense_sidebar')
@endsection

@section('content')

<div id="top_current_page_menu" class="no_margin row">

    <div style="padding-right: 15px" class="one_block_menu col-lg-6 col-md-6 col-sm-6 col-xs-12 no_padding">
      <select name="city" id="select_cities_univer" class="js-example-basic-single form-control">
        <option value="all">@lang('univers.all_cities')</option>
        @foreach(App\MyHelpers\Helper_two::univers_cities() as $city_one)
          <option value="{{ $city_one->title_en }}">{{ $city_one->name }}</option>
        @endforeach
      </select> 
    </div>

    <div class="one_block_menu col-lg-6 col-md-6 col-sm-6 col-xs-12 no_padding">
      <select name="subject" id="select_subject_univer" class="js-example-basic-single form-control">
        <option value="all">@lang('univers.all_subjects')</option>
        @foreach(App\MyHelpers\Helper_two::univers_subject() as $subject_one)
          <option value="{{ $subject_one->slug }}">{{ $subject_one->name }}</option>
        @endforeach
      </select>
    </div>

</div>

<div id="cities_univer" class="panel panel-default">
    <div class="panel-body"> 

        <ul class="ul_3_column">

        @foreach($cities as $city)

            <li><a href="{{ route('univer_city_subject', ['city' => $city->title_en, 'subject' => "all"]) }}">{{ $city->{'title_'.$local} }} <span class="count">{{ $city->total }}</span></a></li>

        @endforeach

        </ul>

    </div>
</div>
@endsection

@section('style')

{{ Html::style('front/select2/select2.css') }}

@endsection

@section('script')

{{ Html::script('front/select2/select2.full.js') }}

<script type="text/javascript">

    $("select").select2({
      tags: false,
      maximumSelectionLength: 20
    });

//отправляет пользователя после выбора города или предмета
    $('select#select_cities_univer, select#select_subject_univer').on('select2:select', function (evt) {

        var url_cur = new Url();
        var city = $('#select_cities_univer').val();
        var subject = $('#select_subject_univer').val();
           
        var url = '{{ route("univer_city_subject", [":city", ":subject"]) }}';
        url = url.replace(':city', city);
        url = url.replace(':subject', subject);
        window.location.href = url;
    });

</script>   

@endsection

