@extends('layouts/app_sidebar')

@section('title_page')
  Редактирование информации профиля
@endsection

@section('side_bar')

@include('parts/user_menu')

@endsection

@section('content')

<div id="tutor_block" class="panel panel-default">
    <div class="panel-body">
{{-- dump($data_user) --}}


        {!! Form::open(['route' => 'update_profile']) !!}

        <div class="col-md-12 no_padding">

          <div id="foto_content" class="col-lg-12 col-md-12 col-ms-12 col-xs-12 one_form_block">
            <label class="control-label col-lg-3 col-md-3 col-sm-3">Ваше фото</label>
            <div class="col-md-4 col-lg-3 col-sm-4">
              @if (!empty(Auth::user()->avatar))
              <div id="isset_image">
                <img src="{{ URL::asset('storage/users/') }}/{{ Auth::user()->avatar }}" alt="exist-avatar">
              </div>
              @else
              <div id="isset_image">
                <img src="{{ URL::asset('front/images/tutor_avatar_crop/default.jpg') }}" alt="default-avatar">
              </div>
              @endif
              <div class="image-editor">
                <div class="cropit-preview"></div>
                <div class="image-size-label">
                  <input type="range" class="cropit-image-zoom-input">
                </div>
                <input id="upload" type="file" class="cropit-image-input">
                <input type="hidden" id="image_crop" name="image_crop">
              </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12" >
              <label id="label_for_upload_file" for="upload">
                <span>Загрузить свое фото</span><i class="fa fa-upload" aria-hidden="true"></i>
              </label>
            </div>  
          </div>

          <div class="col-lg-12 col-md-12 col-ms-12 col-xs-12 one_form_block">
            <label for="tutor_name" class="control-label col-lg-3 col-md-3 col-sm-3">Ваше имя<span class="require"></span></label>
            <div class="col-md-5 col-lg-5 col-sm-5">
              <input required id="tutor_name" value="{{ Auth::user()->name }}" name="name" type="text" class="form-control"/>
            </div>  
          </div>

          <div class="col-lg-12 col-md-12 col-ms-12 col-xs-12 one_form_block">
            <label for="tutor_email" class="control-label col-lg-3 col-md-3 col-sm-3">Почта<span class="require"></span></label>
            <div class="col-md-5 col-lg-5 col-sm-5">
              <input id="tutor_email" required value="{{ Auth::user()->email }}" name="email" type="email" class="form-control"/>
            </div>  
          </div>

        </div>

        <div id="save-tutor-info-wrap">
        
          <input id="save-tutor-info"  class="btn btn-success" value="Сохранить" type="submit">

        </div>

        {!! Form::close() !!}
        
    </div>
</div>

@endsection

@section('style')

{{ Html::style('front/select2/select2.css') }}

<style>
  #tutor_block {
    margin-top: 0px;
}
</style>

@endsection

@section('script')

{{ Html::script('front/select2/select2.full.js') }}
{{ Html::script('front/cropie_avatar/jquery.cropit.js') }}

<script type="text/javascript">

      $(function() {
        $('.image-editor').cropit({
          imageState: {
            
          },
          imageBackground: true,
          imageBackgroundBorderWidth: 15, // Width of background border
        });

        $('#save-tutor-info').click(function() {

          if ($('#upload').val() != '') {

            var image_crop = document.getElementById('image_crop');
            var imageData = $('.image-editor').cropit('export');
            image_crop.value = imageData;

          }
          
        });


        $('#upload').on('change', function(){
          if ($(this).val() != '') {
            $('#isset_image').hide();
            $('.image-editor').show();
            console.log('no empty');
          }
        });

        if ($('#isset_image img').attr("alt") == 'exist-avatar') {
          $('#label_for_upload_file span').text('Загрузить другое фото');
        }


      });



</script>

@endsection

      

