@extends('layouts/app_sidebar')

@section('title_page')
Редактирование учебных заведений
@endsection

@section('title_meta')
 Редактирование учебных заведений
@endsection

@section('side_bar')

@include('parts/user_menu')

@endsection

@section('content')

<div id="tutor_block" class="panel panel-default">
    <div id="company_block" class="panel-body">
      
      <p style="text-align: center; margin: 25px 0px">
        <a class="btn-lg btn-success" href="{{ route('company_dasboard') }}">Добавить учебное заведение<i class="fa fa-plus-circle"></i></a>
      </p>

      @if($notes->count() > 0)

      <table id="datatable" class="table table-striped table-bordered">
        <tbody>
            @foreach($notes as $cours)
            <tr style="">
                <td>
                  <div class="course_name">
                      {{ $cours->name }}
                  </div>
                  <p class="course_adress_link">Страница на сайте: <br> 
                    @if($cours->type == 'cours')

                      <a class="a_little" target="_blank" href="{{ route('cours_one_show', ["alias" => $cours->alias]) }}">
                        {{ route('cours_one_show', ['alias' => $cours->alias ]) }} <i class="fa fa-external-link" aria-hidden="true"></i>
                      </a>

                    @elseif($cours->type == 'school')

                      <a class="a_little" target="_blank" href="{{ route('schools_one_show', ["alias" => $cours->alias]) }}">
                        {{ route('schools_one_show', ['alias' => $cours->alias ]) }} <i class="fa fa-external-link" aria-hidden="true"></i>
                      </a>

                    @endif
                    
                  </p>
                  <p class="little_grey">Дата создания {{ date('d.m.Y', $cours->created_at->timestamp) }}</p>
                </td>
                <td>
                  <div class='course_active_block'>
                    <input disabled @if($cours->active == "1") checked @endif class='css-checkbox course_active' name='course_active' id="chk_{{ $cours->id }}" type='checkbox' value='{{ $cours->id }}' />
                    <label style="cursor: default;" class='css-label' for='chk_{{ $cours->id }}'>@if($cours->active == "1") Активный @else Не активный @endif</label>
                  </div>
                </td>
                <td>
                  <p>
                    @if($cours->type == 'cours')
                      <a href="{{ route('course_edit', ["id" => $cours->id]) }}"><i style="color: #027ce8;" class="fa fa-edit"></i> Изменить</a>
                    @elseif($cours->type == 'school')
                      <a href="{{ route('sclool_edit', ["id" => $cours->id]) }}"><i style="color: #027ce8;" class="fa fa-edit"></i> Изменить</a>
                    @endif
                  </p>
                  <p> <a href="{{ route('one_company_vip', ["id" => $cours->id, "type" => $cours->type ]) }}"> <i class="fa fa-level-up" aria-hidden="true"></i> Рекламировать (VIP статус)</p></a>
                  <p> <a class="course_delete" href="{{ route('course_delete', ["id" => $cours->id]) }}"> <i class="fa fa-trash"></i> Удалить</p></a>
                  
                </td>
            </tr>
            @endforeach
        </tbody>
      </table>

      @else
        <div>
          <p>Здесь будут отображаться все созданные вами учебные заведения</p>
          <p>В бесплатном варианте аккаунта вы можете добавить до 3 страниц различных учебных заведений. Если вам нужно добавить больше учебных заведений на сайт - напишите нам на почту <a href="mailto:info.wowknow@gmail.com">info.wowknow@gmail.com</a></p>
          <p>Добавить курсы или школу на сайт очень просто. Нажмите на кнопку "Добавить учебное заведение" и заполните все необходимые поля. </p>
          <p>Увеличивайте свою популярность и находите учеников вместе с нами.</p>
          <div style="text-align: center;">
            <img style="margin: 20px 0px; max-width: 350px" src="front/images/let_go.jpg" alt="">
          </div>
        </div>
      @endif

    </div>    
</div>
@endsection

@section('style')

{{ Html::style('front/select2/select2.css') }}
<style>
  #tutor_block {
    margin-top: 0px;
}
</style>

@endsection

@section('script')

{{ Html::script('front/select2/select2.full.js') }}
{{ Html::script('front/cropie_avatar/jquery.cropit.js') }}

<script type="text/javascript">

$(function() {
  $('.course_delete').on('click', function(){
    var question = confirm("Вы действительно хотите удалить этот курс? Это действие нельзя отменить");
    if(question){
      return true;
    }else{
      return false;
    }

  });
});

</script>

@endsection

      

