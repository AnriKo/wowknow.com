@extends('layouts/app_sidebar')

@section('title_page')
  Продвижение страницы - {{ $company_name }}
@endsection

@section('side_bar')

@include('parts/user_menu')

@endsection

@section('content')

<div id="tutor_block" class="panel panel-default">
    <div class="panel-body">
        <div class="col-md-12 no_padding">

          <p>Ваши ученики уже ищут вас, помогите им в этом.</p>

          <h3>Что дает <strong style="color: orange;">TOП</strong> статус учебного заведения на нашем сайте?</h3>

          <div style=" margin-top: 15px" class="col-md-12 no_padding one_promo_block">
            <div class="col-md-6 col-sm-6">
              <p class="promo_text after_right">Улучшение внешнего вида страницы. Вы сможете добавить фотогалерею изображений своего учебного заведения.</p>
            </div>   
            <div class="col-md-6 col-sm-6">
              <img class="promo_img" src="/front/images/show_gallery.png" alt=""> 
            </div> 
          </div>

          <div class="col-md-12 no_padding one_promo_block">
            <div class="col-md-6 col-sm-6">
               <img class="promo_img" src="/front/images/promo_company.png" alt=""> 
            </div>   
            <div class="col-md-6 col-sm-6">
               <p class="promo_text after_left">Реклама в правой колонке. На всех страницах сайта в правой колонке ваше заведение будет отображаться как <strong>TOP</strong> учебное заведение</p>
            </div>  
          </div> 

          <div class="col-md-12 no_padding one_promo_block">
            <div class="col-md-6 col-sm-6">
               <p class="promo_text after_right">Реклама в поиске. В общем поиске ваше заведение будет в разделе топ на первых позициях страницы. Также в поиске заведение получает значок <strong>TOП</strong> что выделяет его на общем фоне</p>
            </div>   
            <div class="col-md-6 col-sm-6">
               <img class="promo_img" src="/front/images/promo_company_2.png" alt=""> 
            </div>  
          </div> 

          <h3>Стоимость размещения учебного заведения в TOП всего <strong style="color: orange;">50грн/месяц</strong></h3>
          <p>Минимальний строк размещения в топ - 4 месяца (200грн)</p>
          <p>Сейчас оплата производится на карту Приват банка. <br>
            Номер карты 5168 7573 4858 1333 <br>
            После проведения оплати сообщите нам название обьявления которое вы оплатили
          </p>
          <p>Остались вопросы? Будем рады ответить. Наши контакты: <br> 
            почта - <a href="mailto:info.wowknow@gmail.com">info.wowknow@gmail.com</a> <br> 
            телефон/Viber - 050 695 64 78 <br>
            менеджер - Андрей</p>
        </div>

        
    </div>
</div>

@endsection

@section('style')

{{ Html::style('front/select2/select2.css') }}

<style>
  #tutor_block {
    margin-top: 0px;
}
.promo_img{
  width: 100%;
}
.promo_text{
border-radius: 22px;
    color: #fff;
    background-color: #58bc58;
    padding: 9px 15px;
    position: relative;
    margin-top: 16%;
}
.promo_text.after_right:after{
    border-radius: 50%;
    color: #fff;
    background-color: #6ee1a2;
    width: 18px;
    height: 18px;
    content: ' ';
    position: absolute;
    top: 41%;
    right: -21px;
}
.promo_text.after_left:before{
    border-radius: 50%;
    color: #fff;
    background-color: #6ee1a2;
    width: 18px;
    height: 18px;
    content: ' ';
    position: absolute;
    top: 41%;
    left: -21px;
}
.one_promo_block{
  margin-bottom: 20px;
}
</style>

@endsection

@section('script')

{{ Html::script('front/select2/select2.full.js') }}

<script type="text/javascript">


</script>

@endsection

      

