@extends('layouts/app_sidebar')

@section('title_page')
  Регистрация учебного заведения
@endsection

@section('side_bar')

  @include('parts/create_comp_menu')

@endsection

@section('content')

@php $locale = App::getLocale() @endphp

<div id="tutor_block" class="panel panel-default">
    <div id="company_block" class="panel-body">

      @php $contry_code = mb_strtolower(Location::get()->countryCode); @endphp

        {!! Form::open(['route' => 'company_register_save', 'files' => true]) !!}

        <h3 style="margin-bottom: 25px; margin-top: 0px" class="col-lg-12 col-md-12 col-ms-12 col-xs-12 one_form_block ">Основная информация</h3>

        <div class="col-lg-12 col-md-12 col-ms-12 col-xs-12 one_form_block">
          <label for="company_type" class="control-label col-lg-2 col-md-2 col-sm-12">Тип учебного заведения<span class="require"></span></label>
          <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
            <select id="company_type" required class="form-control js-example-basic-single" name="company_type">
              <option value="">Выберите тип учебного заведения</option>
              <option value="cours">Учебные курсы</option>
              <option value="school">Школы</option>
            </select>
          </div>
        </div> 

        <div class="col-lg-12 col-md-12 col-ms-12 col-xs-12 one_form_block">
          <label for="company_name" class="control-label col-lg-2 col-md-2 col-sm-12 col-xs-12">Название<span class="require"></span></label>
          <div class="col-md-10 col-lg-10 col-sm-12 col-xs-12">
              <input required id="company_name" name="company_name" type="text" class="form-control"/>
          </div>  
        </div>

        <div id="courses_cat" class="col-lg-12 col-md-12 col-ms-12 col-xs-12 one_form_block">
          <label class="control-label col-lg-2 col-md-12 col-sm-12 col-xs-12">Категории&nbsp; курсов<span class="require"></span><br><span class="little_grey">(до 3 категорий)</span></label>

          <div data-tooltip="Выбрите одну или несколько категорий к которым относятся ваши курсы" id="accordion" class="course_checbox_group col-md-10 col-lg-8 col-sm-12 col-xs-12">

            @foreach($courses_group as $group) 

            <div class="group">
              <div class="" id="headingOne_{{ $group->id }}">
                <div class="group_name" data-toggle="collapse" data-target="#collapseOne__{{ $group->id }}" aria-expanded="false" aria-controls="collapseOne_{{ $group->id }}"><i style="font-size: 90%;color: #b4d4d4;" class="fa fa-plus-square"></i> {{ $group->name_ru }} <span class="count_checked"></span></div>
              </div>
              <div id="collapseOne__{{ $group->id }}" class="collapse" aria-labelledby="headingOne_{{ $group->id }}" data-parent="#accordion">
                <div class="group-body">

                  @foreach($group->company_cours_cat as $one_cat)
                    <div class='course_cat '>
                      <input class='css-checkbox course_cat_checkbox' name='course_cat[]' id="chk_{{ $one_cat->id }}" type='checkbox' value='{{ $one_cat->id }}' />
                      <label class='css-label' for='chk_{{ $one_cat->id }}'>{{ $one_cat->name_ru }}</label>
                    </div>
                  @endforeach

                </div>  
              </div> 
            </div>  

            @endforeach

          </div>  
        </div>

        <div style="padding-right: 14px;" id="tutor_education_text" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 one_form_block">
          <label for="company_desc" class="control-label col-lg-2 col-md-2 col-sm-12 col-xs-12">Описание<span class="require"></span><br><span class="little_grey">минимум 300 символов</span></label>
          <div style="padding-right: 0px" class="col-md-10 col-lg-10 col-sm-12 col-xs-12">
            <textarea {{-- minlength="300" --}} id="company_desc" class="form-control input_lang" name="company_desc" cols="40" rows="16" placeholder="Подробно опишите вашу компанию, сферу деятельности, какие методики применяються, принцыпы обучения"></textarea>
          </div>  
        </div>

        <div id="school_class_block" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 one_form_block">
          <label for="school_class" class="control-label col-lg-2 col-md-2 col-sm-12 col-xs-12">Школьные клаcсы<span class="require"></span><br><span class="little_grey">В каких классах проходит обучение в школе</span></label>
          <div style="padding-right: 0px" class="col-md-10 col-lg-10 col-sm-12 col-xs-12">
            @for($i = 1; $i < 13; $i++)
            <div class="checkbox">
              <label><input class="pupil_age_input" name="school_class[]" type="checkbox" value="{{ $i }}">{{ $i }} класс</label>
            </div>
            @endfor
          </div>  
        </div>

        <div style="padding-right: 14px;" id="price_block_company" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 one_form_block">
          <label for="price" class="control-label col-lg-2 col-md-2 col-sm-12 col-xs-12">Цена <br><span class="little_grey">не обязательно</span></label>
          <div style="padding-right: 0px" class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
            <input id="price" placeholder="Например: 3000грн/месяц" name="price" type="text" class="form-control"/>
            <label id="price_free"><input name="price_free" type="checkbox" value="yes">бесплатно</label>
          </div>  
        </div>

        <h3 class="col-lg-12 col-md-12 col-ms-12 col-xs-12 one_form_block ">Контакты</h3>

        <div class="col-lg-12 col-md-12 col-ms-12 col-xs-12 one_form_block">
          <label for="country_teach_id" class="control-label col-lg-2 col-md-2 col-sm-2">Страна<span class="require"></span></label>
          <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
            <select required class="form-control js-example-basic-single" name="company_country" id="country_teach_id">
              <option value="">Cтрана</option>
                <option @if($contry_code ==  "ua") selected @endif value="ua">Украина</option>
                <option @if($contry_code ==  "ru") selected @endif value="ru">Россия</option>
                <option @if($contry_code ==  "by") selected @endif value="by">Белорусь</option>
                <option @if($contry_code ==  "kz") selected @endif value="kz">Казахстан</option>
            </select>
          </div>  
          <div class="tips col-lg-4 col-md-4 col-sm-4 col-xs-12">Страна в которой компания ведет деятельность</div>
        </div> 

        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 one_form_block">
          <label for="city_id" class="control-label col-lg-2 col-md-2 col-sm-2 col-xs-12">Город<span class="require"></span></label>
          <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
            <select required name="company_city" data-ajax--cache="true" id="city_id" class="form-control js-data-example-ajax">
            </select>
          </div>  
        </div> 

        <div class="col-lg-12 col-md-12 col-ms-12 col-xs-12 one_form_block">
          <label for="company_street" class="control-label col-lg-2 col-md-2 col-sm-2">Адрес<span class="require"></span></label>
          <div class="col-md-6 col-lg-6 col-sm-12">
              <input placeholder="Например: ул. Набережная 7" name="company_street" type="text" class="form-control input_lang"/>
          </div> 
        </div>  

        <div class="col-lg-12 col-md-12 col-ms-12 col-xs-12 one_form_block">
          <label class="control-label col-lg-2 col-md-2 col-sm-2">Номер телефона<span class="require"></span></label>
          <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
           <input required placeholder="Введите номер компании" name="phone" type="text" class="form-control"/>
          </div> 
        </div>  

        <div class="col-lg-12 col-md-12 col-ms-12 col-xs-12 one_form_block">
          <label class="control-label col-lg-2 col-md-2 col-sm-2">Сайт</label>
          <div class="col-md-6 col-lg-6 col-sm-12">
            <input placeholder="Ссылка на ваш сайт" name="site" type="text" class="form-control"/>
          </div>  
          <div class="tips col-lg-3 col-md-3 col-sm-12 col-xs-12"><a target="_blank" href="http://friendband.com.ua/">Нужен сайт? Напишите</a></div>
        </div> 

        <div class="col-lg-12 col-md-12 col-ms-12 col-xs-12 one_form_block">
          <label class="control-label col-lg-2 col-md-2 col-sm-2">Группа в facebook <br> <span class="little_grey">страница в соц. сетях</span> </label>
          <div class="col-md-6 col-lg-6 col-sm-12">
            <input placeholder="Ссылка на страницу в социальной сети" name="soc_page" type="text" class="form-control"/>
          </div>  
          <div class="tips col-lg-3 col-md-3 col-sm-12 col-xs-12">Если у вас есть страничка, группа в социальной сети, например в facebook, вставьте ее сдесь </div>
        </div>  

        <h3 class="col-lg-12 col-md-12 col-ms-12 col-xs-12 one_form_block ">Логотип, фотогалерея</h3>

        <div class="col-lg-12 col-md-12 col-ms-12 col-xs-12 one_form_block">
          <label class="control-label col-lg-2 col-md-3 col-sm-3">Логотип компании</label>
          <div class="col-md-3 col-lg-3 col-sm-8">

            <input name="company_logo" id="upload" type="file" onchange="previewFile()">
            <img style="max-width: 150px; max-height: 100px" id="preview_file" src="{{ URL::asset('front/images/default_logo_company.png') }}" alt="Image preview...">

            <div style="margin-top: 7px" >
              <label class="btn btn-default" data-tooltip="Выберите свое фото"  id="" for="upload">
                <span>Загрузить логотип</span><i class="fa fa-upload" aria-hidden="true"></i>
              </label>
            </div> 

          </div> 



        </div>
        <p class="col-lg-12 col-md-12 col-ms-12 col-xs-12 one_form_block " id="label_foto_content"><strong>Добавление изображений в фотогалерею доступно для Топ страниц.</strong> <br> <a target="_blank" href="{{ route('one_company_vip', ["id" => 'all', "type" => 'all' ]) }}">Какие возможности Топ страницы, как получить статус Топ?</a></p>
        <div data-toggle="modal" data-target="#error_to_try_add_gallery" id="foto_content" class="col-lg-12 col-md-12 col-ms-12 col-xs-12 one_form_block">

          <label class="control-label col-lg-2 col-md-2 col-sm-3">Фотогалерея учебного заведения<br><span class="little_grey">(до 12 изображений)</span></label>
          <div class="col-md-10 col-lg-10 col-sm-9">

            <div>  

              <div style="margin-right: 25px;" id="basic_input" class="hdtuto control-group lst input-group this_image">
                <label >
                  <img class="blah previews" src="/front/images/add_image.png" alt="your image" />
                </label>  
              </div>  
              <div id="end_block_images"></div>

            </div>

            <div class="add_image_block" style="margin-top: 7px"> 
              <button class="btn btn-default" type="button">
                <i class="fa fa-plus-circle" aria-hidden="true"></i>Добавить изображения
              </button>
            </div>

          </div>
        </div>

        <div  id="save-tutor-info-wrap">
          <input id="save-tutor-info"  class="btn btn-success" value="Сохранить" type="submit">
        </div>

        {!! Form::close() !!}
        
    </div>
</div>

<!-- Modal top gallery error -->
<div class="modal fade" id="error_to_try_add_gallery" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="panel-heading"><b>Добавление изображений в фотогалерею доступно для Топ страниц</b>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <div class="modal-body">

            <p>Топ статус для страницы учебного заведения это возможность добавить фотогалерею, а также реклама вашего заведения практически на всех страницах нашего сайта в разделах Топ.</p>

            <a target="_blank" href="{{ route('one_company_vip', ["id" => 'all', "type" => 'all' ]) }}">Более подробно, какие возможности Топ страницы, как получить статус Топ?</a>

            <p style="text-align: center; margin: 10px 0px;">
              <a class="btn btn-success" target="_blank" href="{{ route('one_company_vip', ["id" => 'all', "type" => 'all' ]) }}">Рекламировать свою компанию</a>
            </p>

            

      </div>
    </div>
  </div>
</div>

@endsection

@section('style')

{{ Html::style('front/select2/select2.css') }}
{{ Html::style('front/editor_html/ui/trumbowyg.min.css') }}

<style>
#tutor_block {
  margin-top: 0px;
}
#label_for_upload_file {
    margin-top: 0px !important;
}
#courses_cat{
    display: none;
}
#school_class_block{
    display: none;
}

</style>

@endsection

@section('script')

{{ Html::script('front/select2/select2.full.js') }}
{{ Html::script('front/cropie_avatar/jquery.cropit.js') }}
{{ Html::script('front/editor_html/trumbowyg.min.js') }}
{{ Html::script('front/editor_html/langs/'. $locale .'.min.js') }}

<script type="text/javascript">

  //подсчет выбранных категорий курсов
  $('.course_cat label').on('click', function(){

      var this_ = $(this);
      var count = this_.closest('.group').find('.count_checked');
     if(this_.siblings('input').prop('checked')){
        count.text(+count.text() - 1);
        count.show();
     }else{
        var max_count = $('.group').find('input[type="checkbox"]:checked').length;
        console.log(max_count);
        if(max_count > 2){
          alert('Максимальное количество категорий одного курса 3. Вы уже выбрали 3 категории');
          return false;
        }
        count.text(+count.text() + 1);
        count.show();
     }

    if(+count.text() == 0){
      count.text('');
      count.hide();
    }
     
  });

  $('select[name="company_type"]').change(function() {
    var val = $(this).val();
    var courses_cat = $('#courses_cat');
    var school_class_block = $('#school_class_block');
    if(val == 'cours'){
      courses_cat.fadeIn('slow');
      school_class_block.fadeOut('slow');
    }else if(val == 'school'){
      courses_cat.fadeOut('slow');
      school_class_block.fadeIn('slow');
    }else if(val == ''){
      courses_cat.fadeOut('slow');
      school_class_block.fadeOut('slow');
    }
  });

{{-- $(document).ready(function() {
  $(".add_image").click(function(){ 
    var count = $('.input_image').length;
    if (count <= 10) {
      var lsthmtl = $(".clone").html();
      $("#end_block_images").before(lsthmtl);
    }

  });

  $("body").on("click",".delete_file",function(){ 
      $(this).parents(".this_image").remove();
  });
}); --}}

   function readURL(input) {
      if (input.files && input.files[0]) {
          var reader = new FileReader();

          reader.onload = function (e) {
            console.log(input);
              $(input).parents(".this_image").find( ".blah" )
                  .attr('src', e.target.result);
          };

          reader.readAsDataURL(input.files[0]);
      }
  }


  function previewFile() {
    var preview = document.getElementById('preview_file');
    var file    = document.querySelector('input[type=file]').files[0];
    var reader  = new FileReader();

    reader.onloadend = function () {
      preview.src = reader.result;
    }

    if (file) {
      reader.readAsDataURL(file);
    } else {
      preview.src = "";
    }
  }

$(function() {

  $('textarea').trumbowyg({
      btns: [['customFormatting'], ['bold', 'italic'], ['link'], ['unorderedList', 'orderedList']],
      resetCss: true,
      lang: '{{ $locale }}',
      btnsDef: {
          customFormatting: {
              dropdown: ['h2','p'],
              text: 'Заголовок',
              title: 'Заголовок',
              hasIcon: false
          }
      },
      svgPath: false,
      hideButtonTexts: true,
  }).on('tbwchange', function(){ 
    if($(this).val() != ''){
      var attr = $(this).closest('.active').attr('id');
      $("a[href='#"+attr+"']").find('.fa-check-square').show();

    }else{
      var attr = $(this).closest('.active').attr('id');
      $("a[href='#"+attr+"']").find('.fa-check-square').hide();
    }
  }).on('tbwpaste', function(){ 

    if($(this).val() != ''){
      var attr = $(this).closest('.active').attr('id');
      $("a[href='#"+attr+"']").find('.fa-check-square').show();

    }else{
      var attr = $(this).closest('.active').attr('id');
      $("a[href='#"+attr+"']").find('.fa-check-square').hide();
    }

  }); 

$('#city_id').select2({
    placeholder: "Выберите город",
    minimumInputLength: 2,
    ajax: {
        url: '{{ url('/') }}/city-search',
        delay: 100,
        dataType: 'json',
        data: function (params, locale) {
            var query = {
                city: $.trim(params.term),
                locale: $('#country_teach_id').val(),
            };
            return query;
        },
        processResults: function (data) {
            return {
                results: data
            };
        },
        cache: true
    }
});

  //функция подсказки создания
function showTip(current) {
    console.log(current);
    var tipText = current.attr('data-tooltip');
    var element = document.createElement('div');
    element.className = 'my_tooltip';
    element.innerHTML = tipText;
    $(current).append(element);
    $(element).delay( 8000 ).fadeOut('slow');
    setTimeout( function(){
      current.css('border','none');
    },8000);
      
}

  var ua = $('#country_live_id').find('option[value="ua"]');
  var ru = $('#country_live_id').find('option[value="ru"]');
  var by = $('#country_live_id').find('option[value="by"]');
  var kz = $('#country_live_id').find('option[value="kz"]');
  var first = $('#country_live_id').find('option[value=""]');
  $('#country_live_id').prepend(kz).prepend(by).prepend(ru).prepend(ua).prepend(first);

  $('#save-tutor-info').click(function(event) {

    $('textarea').map(function(){
      var s = $(this).val();
      var result = s.replace(/style="[^"]*"/g, '');
      $(this).val(result); 
    });
    
  });

  if ($('#isset_image img').attr("alt") == 'exist-avatar') {
    $('#label_for_upload_file span').text('Загрузить другое фото');
  }

});

</script>

@endsection

      

