@extends('layouts/app_sidebar')

@section('title_page')
  Работа с учениками
@endsection

@section('side_bar')

@include('parts/user_menu')

@endsection

@section('content')

<div id="tutor_block" class="panel panel-default">
    <div class="panel-body">
{{-- dump($requests) --}}

      <div id="second_title">
        <h2><a href="{{ route('requests') }}">Мои ученики</a></h2><h2><a href="{{ route('response_to_my_task') }}">Я ученик, предложения от репетиторов</a></h2>
      </div>

        <div id="requests_page" class="col-md-12 no_padding">

          <table id="datatable" class="table table-striped table-bordered">
              <thead>
                  <tr>
                      <th>Ваша заявка</th>
                      <th>Статус</th>
                      <th>Действия</th>
                  </tr>
              </thead>
              <tbody>
                  @if (count($requests))
                  @foreach($requests as $request)
                  <tr class="one_conversation {{ $request->status == "sent" ? 'unread_message' : '' }}" >
                      <td>
                        @if($request->sender_info != null)
                          <span class="bold">Имя:</span> {{ $request->sender_info->name }}
                        @endif 
                        <span title="Дата создания заявки" style="float: right;" class="small_text">{{ date('d.m.Y - H:i', $request->created_at->timestamp) }}</span>
                        @if($request->conversation_id->message_attach_to_request->reply != null)
                          <br> <span class="bold">Детали:</span>
                          {{$request->conversation_id->message_attach_to_request->reply}}
                        @endif  
                      </td>
                      <td >
                        @if($request->status == 'sent')
                          Ожидает вашего ответа
                        @elseif($request->status == 'accept')
                         <i class="fa fa-check-circle" aria-hidden="true"></i> 
                         Принята <br>
                         <div class="user_contacts">
                            <div class="user_contacts_title">Контакты ученика:</div>
                            <i class="fa fa-envelope-o" aria-hidden="true"></i>- {{ $request->sender_info->email }} <br>

                            {!! $request->sender_info->phone ? '<div class="user_contact_one"><i class="fa fa-phone" aria-hidden="true"></i> - ' . $request->sender_info->phone . '</div>' : '' !!}
                            {!! $request->sender_info->skype ? '<div class="user_contact_one"><i class="fa fa-skype" aria-hidden="true"></i> - ' . $request->sender_info->skype . '<div>' : '' !!}

                          </div> 
                        @elseif($request->status == 'no_accept')
                          <i class="fa fa-minus-circle" aria-hidden="true"></i> Отклонена
                        @endif
                      </td>
                      <td>
                          <?php $dialog = $request->conversation_id; ?>
                          @if($dialog != null)
                            <a id="write_message" title="Написать сообщение ученику" href="{{ route('show_conversation', ['id' => $dialog->id]) }}" class="btn btn-default btn-xs">
                              @if($request->conversation_id->unread_message_for_one_user->count() > 0)
                                <span title="Непрочитанные сообщения" class="label label-danger">{{$request->conversation_id->unread_message_for_one_user->count()}}</span>
                              @endif
                              <i style="color: #5bc0de;" class="fa fa-envelope" aria-hidden="true"></i>
                              Написать сообщение
                            </a> <br>
                          @else
                            <a title="Написать сообщение" href="#" class="btn btn-default btn-xs"><i style="color: #5bc0de;" class="fa fa-envelope" aria-hidden="true"></i>Написать сообщение</a> <br>
                          @endif

                          <a title="Принять заявку и начать обучение" style="" href="{{ route('accept_request', ['id' => $request->id]) }}" class="btn btn-default btn-xs"><i class="fa fa-check-square-o" aria-hidden="true"></i>Принять заявку</a>
                          <a style="" href="{{ route('no_accept_request', ['id' => $request->id]) }}" class="btn btn-default btn-xs"><i class="fa fa-minus-circle" aria-hidden="true"></i> Отклонить</a>
                          <a title="Удалить" style="" href="{{ route('delete_request', ['id' => $request->id]) }}" class="btn btn-default  btn-xs"><i class="fa fa-trash" aria-hidden="true"></i></a>
                      </td>
                  </tr>
                  @endforeach
                  @endif
              </tbody>
          </table>

        </div>

    </div>
</div>

@endsection

@section('style')
  <link href="{{asset('admin/css/dataTables.bootstrap.min.css')}}" rel="stylesheet">
@endsection

@section('script')

  <script src="{{asset('admin/js/jquery.dataTables.min.js')}}"></script>
  <script src="{{asset('admin/js/dataTables.bootstrap.min.js')}}"></script>

  <script>
    $(document).ready(function() {

      $('#datatable').dataTable({
        "order": [[2, 'desc']],
        "language": {
          "emptyTable": "У вас пока нету личных заявок. <br /> В разделе <a href='{{ route('task_search_subject_city', ['subject'=> 'subject','city'=> 'all-city']) }}'>Ученики</a> пользователи оставляют заявки на поиск учителя. Найдите учеников сами и отправьте им заявку, воспользовавшись этим разделом<br /> <a href='{{ route('task_search_subject_city', ['subject'=> 'subject','city'=> 'all-city']) }}'>Найти учеников</a>",
        },
        "bLengthChange": false
      });

    });  
  </script>
  
@endsection

      

