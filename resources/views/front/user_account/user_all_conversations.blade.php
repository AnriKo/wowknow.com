@extends('layouts/app_sidebar')

@section('title_page')
  Работа с учениками
@endsection

@section('side_bar')

@include('parts/user_menu')

@endsection

@section('content')

<div id="tutor_block" class="panel panel-default">
    <div class="panel-body">
{{-- dd($messages) --}}

      <div id="second_title">
        <h2>Все сообщения</h2>
      </div>

        <div style="margin-bottom:15px;" class="col-md-12 no_padding">

          <table id="datatable" class="table table-striped table-bordered">
              <thead>
                  <tr>
                      <th>Пользователь</th>
                      <th>Сообщение</th>
                      <th>Дата</th>
                      <th>Действия</th>
                  </tr>
              </thead>
              <tbody>
                  @if (count($messages))
                  @foreach($messages as $message)
                  <tr class="one_conversation {{ $message->conversation_reply->status == "0" && $message->conversation_reply->user_id != Auth::user()->id ? 'unread_message' : '' }}" >
                      <td>
                        @if($message->recipient_id == Auth::user()->id)
                          {{ $message->sender_info->name }}
                        @elseif($message->sender_id == Auth::user()->id)
                          {{ $message->recipient_info->name }}
                        @endif
                      </td>
                      <td>{{ $message->conversation_reply->reply }}</td>
                      <td>
                        <div title="Дата отправки последнего сообщения" class="small_text">
                          {{ date('d.m.Y - H:i', $message->conversation_reply->created_at->timestamp) }}
                        </div>
                      </td>
                      <td>
                          <a style="width: 100%;" href="{{ route('show_conversation', ['id' => $message->id]) }}" class="btn btn-info btn-xs">Прочитать, Ответить</a>
                      </td>
                  </tr>
                  @endforeach
                  @endif
              </tbody>
          </table>

        </div>

    </div>
</div>

@endsection

@section('style')
  <link href="{{asset('admin/css/dataTables.bootstrap.min.css')}}" rel="stylesheet">
@endsection

@section('script')

  <script src="{{asset('admin/js/jquery.dataTables.min.js')}}"></script>
  <script src="{{asset('admin/js/dataTables.bootstrap.min.js')}}"></script>

  <script>
    $(document).ready(function() {

      $('#datatable').dataTable({
        "order": [[2, 'desc']],
        "language": {
          "emptyTable": "Сдесь будут отображаться ваши личные собщения",
        },
        "bLengthChange": false
      });

    });  
  </script>
  
@endsection

      

