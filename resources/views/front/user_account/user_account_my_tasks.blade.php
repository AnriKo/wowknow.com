@extends('layouts/app_sidebar')

@section('title_page')
  Мои обьявления на поиск учителя
@endsection

@section('side_bar')

@include('parts/user_menu')

@endsection

@section('content')

<div id="tutor_block" class="panel panel-default">
    <div class="panel-body">
{{-- dump($tasks) --}}

      <div id="second_title">
        <h2><a href="{{ route('user_tasks') }}">Активные</a></h2><h2><a href="{{ route('user_tasks_no_active') }}">Неактивные</a></h2>
      </div>

        <div style="margin-bottom:15px;" class="col-md-12 no_padding">

          <table id="datatable" class="table table-striped table-bordered">
              <thead>
                  <tr>
                      <th>Детали</th>
                      <th>Ответы учителей</th>
                      <th>Выбор действия</th>
                  </tr>
              </thead>
              <tbody>
                  @if (count($tasks))
                  @foreach($tasks as $task)
                  <tr>
                      <td>
                        <span title="Дата создания заявки" style="float: right;" class="small_text">
                          {{ date('d.m.Y - H:i', $task->created_at->timestamp) }}
                        </span>
                        <b>{{$task->subject_name->name_ru }}</b><br>
                        {{$task->text }} 
                      </td>
                      <td></td>
                      <td>
                          <a style="margin-bottom: 3px;" href="{{ route('edit_task', ['id' => $task->id]) }}" class="btn btn-default btn-xs">Редактировать обьявление</a>
                          <!--a style="margin-bottom: 3px;" href="{{-- route('tutor_account_tasks_no_accept', ['id' => $task->id]) --}}" class="btn btn-default btn-xs">Обновить заявку </a-->
                          @if($task->active == '1')
                            <a style="margin-bottom: 3px;" href="{{ route('change_user_task_active', ['id' => $task->id]) }}" class="btn btn-danger btn-xs">Деактивировать</a>
                          @else
                            <a style="margin-bottom: 3px;" href="{{ route('change_user_task_active', ['id' => $task->id]) }}" class="btn btn-success btn-xs">Активировать</a>
                          @endif
                          
                      </td>
                  </tr>
                  @endforeach
                  @endif
              </tbody>
          </table>

        </div>

    </div>
</div>

@endsection

@section('style')
  <link href="{{asset('admin/css/dataTables.bootstrap.min.css')}}" rel="stylesheet">
@endsection

@section('script')

  <script src="{{asset('admin/js/jquery.dataTables.min.js')}}"></script>
  <script src="{{asset('admin/js/dataTables.bootstrap.min.js')}}"></script>

  <script>
    $(document).ready(function() {

      $('#datatable').dataTable({
        "bLengthChange": false,
        "language": {
          "emptyTable": "У вас нету обьявлений на поиск учителя <br /> Создайте новое обьявление по предмету который вас интересует и учителя сами вас найдут отправив заявку или сообщение, это займет всего несколько минут <br /> <a href='{{ route('create_new_task') }}'>Создать обьявление</a>",
        },
      });

    });  
  </script>
  
@endsection

      

