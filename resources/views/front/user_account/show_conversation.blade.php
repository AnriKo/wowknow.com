@extends('layouts/app_sidebar')

@section('title_page')
  Заявки от пользвателя
@endsection

@section('side_bar')

@include('parts/user_menu')

@endsection

@section('content')

<div id="tutor_block" class="panel panel-default">
    <div class="panel-body">
{{-- dump($requests) --}}

      <div id="second_title">
        <h2>{{$friend->name}} переписка с пользователем</h2>
      </div>

        <div id="one_conversation" style="margin-bottom:15px;" class="col-md-12">

          @if($friend->avatar != null)
            <div id="friend_avatar"><img src="{{ URL::asset('storage/users/') }}/{{ $friend->avatar }}" alt="{{$friend->name}}_image"></div>
          @endif
          
          @foreach($messages as $message)

            @if(Auth::user()->id == $message->user_id)

              <div class="one_message_wrapp" >
                <div class="col-md-8 col-md-offset-1 ">
                  <div class="message_name">Ваше сообщение</div>
                  <div title="Отправлено {{ date('d.m.Y - H:i', $message->created_at->timestamp) }}" class="message_create "><i class="fa fa-clock-o" aria-hidden="true"></i></div>
                  <p class="one_message my_message">
                    {{ $message->reply }}
                  </p>
                  <div class="read_status">
                    @if($message->status == 0)
                      Cообщение еще не прочитано
                    @else
                      Прочитано {{ date('d.m.Y - H:i', $message->updated_at->timestamp) }}
                    @endif
                  </div>
                </div>
              </div>  

            @else

              <div class="one_message_wrapp" >
                <div class="col-md-8 ">
                  <div class="message_name">
                    @if($friend->type == 'user_teach')
                      <a href="{{ route('tutor_page', ['slug' => $friend->user_teacher->slug ]) }}">{{$friend->name}}</a>
                    @else
                      {{$friend->name}}
                    @endif  
                  </div>

                  <div title="Отправлено {{ date('d.m.Y - H:i', $message->created_at->timestamp) }}" class="message_create "><i class="fa fa-clock-o" aria-hidden="true"></i></div>
                  <p class="one_message companion_message">
                    {{ $message->reply }}
                  </p>
                </div>
              </div>  

            @endif

          @endforeach  
          <div class="col-md-9">

            {!! Form::open(['route' => 'message.new']) !!}
            <div class="form-group  ">
              <textarea placeholder="Введите ваше ссобщение" class="form-control" name="reply" id="" cols="" rows="5"></textarea>
            </div>
            <div id="save-tutor-info-wrap">
              <input id="save-tutor-info" class="btn btn-success" value="Отправить письмо" type="submit">
            </div>
              <input type="hidden" name="recip" value="{{$friend->id}}">
              <input id="conv" type="hidden" name="conv_id" value="{{ $conv_id }}">
            {!! Form::close() !!}

          </div>

        </div>

    </div>
</div>

@endsection

@section('style')

@endsection

@section('script')

  <script>
    $(document).ready(function() {

        var conv = $('#conv').val();
        var modal_footer = $('#send_request_teacher').find('.modal-footer')
        $.ajax({
          type: "POST",
          url: "{{ route('read_conversation') }}",
          data: { "_token": "{{ csrf_token() }}",
                    "rec_id" : {{ $friend->id }},
                    "conv" : conv,
                },
          success: function(data){
                    console.log(data);
            }
        });

        var send_message = $('#save-tutor-info');
        send_message.on('click', function(e){
          var textarea = $('textarea');
          if(textarea.val() == ''){
            textarea.css('border', '1px solid #ff7d7d')
            e.preventDefault();
          }
        });

    });  
  </script>
  
@endsection

      

