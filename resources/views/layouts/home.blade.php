@include('parts/head')

<body id="app-layout">

@include('parts/header')

<div id="front_top_img" class="container-fluid">
    <div class="container">
        @yield('main_img')
    </div>
</div>

<div id="home_page_content">

    @yield('content')

</div>

@include('parts/footer')

@include('parts/auth_modals')

<!-- JavaScripts -->

{{-- Html::script('front/js/jQuery2_2_3.js') --}}

{{ Html::script('front/js/bootstrap.min.js') }}
{{ Html::script('front/js/common.js') }}
{{ Html::script('front/js/parse_url.js') }}
{{ Html::script('front/js/jCaptcha.js') }}

@yield('script')
@yield('script_partials')

</body>
</html>
