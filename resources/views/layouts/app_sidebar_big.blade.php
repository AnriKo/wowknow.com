@include('parts/head')

<body id="app-layout">

@include('parts/header')

<div id="page_title" class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<h1>
				@yield('title_page')
			</h1>
		</div>
	</div>
</div>

<div id="main_content" class="container">
	<div class="row">
		<div class="col-md-12">
			<div id="side_bar" class="col-md-4 col-lg-4 col-sm-4 col-xs-12">

				@yield('side_bar')

			</div>
			<div id="main_content_inner" style="padding:0;" class="col-md-8 col-lg-8 col-sm-8 col-xs-12">

				@yield('content')

			</div>
		</div>
	</div>
</div>

@include('parts/footer')

@include('parts/auth_modals')

<!-- JavaScripts -->

{{-- Html::script('front/js/jQuery2_2_3.js') --}}

{{ Html::script('front/js/bootstrap.min.js') }}
{{ Html::script('front/js/common.js') }}
{{ Html::script('front/js/parse_url.js') }}

@yield('script')

</body>
</html>
