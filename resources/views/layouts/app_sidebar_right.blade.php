@include('parts/head')

<body id="app-layout">

@include('parts/header')

<div id="page_title" class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<h1>
				@yield('title_page')
			</h1>
		</div>
	</div>
</div>

<div id="main_content" class="container">
	<div class="row">
		<div class="col-md-12">
			<div id="content_app_sidebar" class="col-lg-9 col-md-9 col-sm-9 col-xs-12">

				@yield('content')

			</div>
			<div id="side_bar" class="col-lg-3 col-md-3 col-sm-3 col-xs-12 no_padding">

				@yield('side_bar')

			</div>
		</div>
	</div>
</div>

@include('parts/footer')

@include('parts/auth_modals')

<!-- JavaScripts -->

{{-- Html::script('front/js/jQuery2_2_3.js') --}}

{{ Html::script('front/js/bootstrap.min.js') }}
{{ Html::script('front/js/common.js') }}
{{ Html::script('front/js/parse_url.js') }}
{{ Html::script('front/js/jCaptcha.js') }}

@yield('script')
@yield('script_partials')

</body>
</html>
