<?php

use App\Teachers;
use App\Post;
use App\University;
use App\Subjects;
use Illuminate\Support\Facades\Config;
//use Config;

//------------------------------ простые ссылки доступные всем -----------------------------//

//парсер education.ua
 Route::get('university', [ 'as' => 'parser_university' , 'uses' => 'DomCrawlerController@university'] );
 Route::get('universities_in_city', [ 'as' => 'parser_universities_in_city' , 'uses' => 'DomCrawlerController@universities_in_city'] );
// Route::get('cities', [ 'as' => 'parser_cities' , 'uses' => 'DomCrawlerController@cities'] );
// Route::get('pages_from_city_hand', [ 'as' => 'pages_from_city_hand' , 'uses' => 'DomCrawlerController@pages_from_city_hand'] );

Route::get('education_ua', [ 'as' => 'education_ua' , 'uses' => 'DomCrawlerController@education_ua'] );
Route::get('eduget', [ 'as' => 'eduget' , 'uses' => 'DomCrawlerController@eduget'] );

Route::get('fix_univer_table', [ 'as' => 'fix_univer_table' , 'uses' => 'DomCrawlerController@fix_univer_table'] );

Route::get('domain_price', [ 'as' => 'domain_price' , 'uses' => 'DomCrawlerController@domain_price'] );

//------------------------------ ссылки разных языков -----------------------------//
$language = Request::segment(1);
if (isset(config('app.locales')[$language])) { 
    App::setLocale($language); 
} else{
	$language = '';
}
//главная
Route::prefix($language)->group(function () {

//главная
Route::get('/', [ 'as' => 'home' , 'uses' => 'HomeController@index'] );

//контакти
Route::get('contacts', [ 'as' => 'contacts', 'uses' => 'HomeController@contacts'] );
Route::post('send_message', [ 'as' => 'send_message', 'uses' => 'HomeController@send_message'] );

//Університети Пошук міста
Route::get('c-ua/universities/cities', [ 'as' => 'univer_look_cities' , 'uses' => 'UniverController@lookCities'] );
//Університети Пошук предмета
Route::get('c-ua/universities/subjects', [ 'as' => 'univer_look_subjects' , 'uses' => 'UniverController@lookSubjects'] );
Route::get('c-ua/universities/all/all', function () {
    return redirect()->route('univer_look_cities');
});
//Універи в місті
Route::get('c-ua/universities/{city}/{subject}', [ 'as' => 'univer_city_subject' , 'uses' => 'UniverController@searchCitySubject'] );
//Сторінка універу
Route::get('c-ua/universities/{slug}', [ 'as' => 'univer_page' , 'uses' => 'UniverController@univerPage'] );
Route::post('review_add', [ 'as' => 'review_add' , 'uses' => 'ReviewController@review_add' ] );

Route::auth();
Auth::routes();
//поиск репетитора
Route::get('tutors', [ 'as' => 'tutor_basic_search' , 'uses' => 'TutorSearchController@tutor_basic_search'] );
//страница репетитора
Route::get('tutors/{slug}', [ 'as' => 'tutor_page' , 'uses' => 'TutorSearchController@tutor_page'] );
//Отзывы репетитору
Route::post('review', ['as' => 'review.store', 'uses' => 'ReviewController@tutor_review_add']);

//Блог
Route::get('articles', [ 'as' => 'posts.index' , 'uses' => 'PostController@index'] );
Route::get('articles/category/{id}', [ 'as' => 'posts.one_category' , 'uses' => 'PostController@index_category'] );
Route::get('articles/{slug}', [ 'as' => 'blog.one_page' , 'uses' => 'PostController@one_page'] );
Route::get('add-articles', [ 'as' => 'posts_user.add' , 'uses' => 'PostController@add_article'] );
Route::post('add-articles', [ 'as' => 'posts_user.save' , 'uses' => 'PostController@store_user_post'] );

//Головний роут пошука репетитора по місту і предмету із базовими показниками
Route::get('tutors/{subject}/{city}', [ 'as' => 'tutor_search_subject_city' , 'uses' => 'TutorSearchController@tutor_search_subject_city'] );
//поиск заявок учеников
Route::get('job-for-tutors/{subject}/{city}', [ 'as' => 'task_search_subject_city' , 'uses' => 'TaskController@task_search_subject_city'] );

//----------------------- для авторизованных пользователей ссылки -----------------//

//-----страницаы создания и редактирования профиля репетитора
Route::group(['middleware' => 'auth', 'prefix' => 'tutor-dasboard'], function () {
    //базовая информация
	Route::get('/', [ 'as' => 'tutor_account_basic' , 'uses' => 'TutorController@tutor_account_basic' ] );
	Route::post('/save-basic', [ 'as' => 'tutor_save_basic' , 'uses' => 'TutorController@tutor_save_basic' ] );
	Route::post('/update-basic', [ 'as' => 'tutor_update_basic' , 'uses' => 'TutorController@tutor_update_basic' ] );
	//основная информация
	Route::get('main-info', [ 'as' => 'tutor_account_main_info' , 'uses' => 'TutorController@tutor_account_main_info' ] );
	Route::post('main-info', [ 'as' => 'tutor_account_main_info_save' , 'uses' => 'TutorController@tutor_account_main_info_save' ] );
	//страница предметов репетитора
	Route::get('subjects-info', [ 'as' => 'tutor_account_subjects_info' , 'uses' => 'TutorController@tutor_account_subjects_info' ] );
	Route::post('subjects-info', [ 'as' => 'tutor_account_subjects_info_save' , 'uses' => 'TutorController@tutor_account_subjects_info_save' ] );
	//страница репетитора обо мне
	Route::get('about-me-info', [ 'as' => 'tutor_account_about_me_info' , 'uses' => 'TutorController@tutor_account_about_me_info' ] );
	Route::post('about-me-info', [ 'as' => 'tutor_account_about_me_info_save' , 'uses' => 'TutorController@tutor_account_about_me_info_save' ] );
    // создание профиля на разных языках
    Route::get('tutor_langs', [ 'as' => 'tutor_langs' , 'uses' => 'TutorController@tutor_langs' ] );
    Route::post('tutor_langs', [ 'as' => 'tutor_langs_save' , 'uses' => 'TutorController@tutor_langs_save' ] );
	//создание нового предмета
	Route::get('create-new-subject', [ 'as' => 'create_new_subject' , 'uses' => 'TutorController@create_new_subject' ] );


    //страница заявок репетитору
    Route::get('tutor-requests', [ 'as' => 'requests_index' , 'uses' => 'UserController@requests_index' ]);
    //страница одной заявки репетитору
    Route::get('tutor-requests/{requestid}', [ 'as' => 'requests_one' , 'uses' => 'UserController@request_one' ]);
	//страница отправки отчета если репетитор отказался от заявки
    Route::post('tutor-noassept_report', [ 'as' => 'tutor_noassept_report' , 'uses' => 'UserController@tutor_noassept_report' ]);

	//принять заявку	
	Route::get('request-accept/{id}', [ 'as' => 'tutor_account_requests_accept' , 'uses' => 'TutorController@requests_accept' ] );
	//отклонить заявку	
	Route::get('request-no-accept{id}', [ 'as' => 'tutor_account_requests_no_accept' , 'uses' => 'TutorController@requests_no_accept' ] );	
	Route::get('my-review', ['as' => 'my.review.show', 'uses' => 'ReviewController@my_review_show']);
});

//страница автоматической авторизации для просмотра заявки из письма
Route::get('tutor-auth/{id}', [ 'as' => 'tutor_auth' , 'uses' => 'UserController@tutor_auth' ]);

// топ курсы ajax
Route::post('top_courses_block', [ 'as' => 'top_courses_block' , 'uses' => 'CourseSearchController@top_courses_block' ] );
// курсы просмотр
Route::get('courses/{alias}', [ 'as' => 'cours_one_show' , 'uses' => 'CourseSearchController@cours_one_show' ] );
Route::get('courses/{subject}/{city}', [ 'as' => 'courses_basic_search' , 'uses' => 'CourseSearchController@courses_basic_search' ] );
Route::get('courses', function () {
    return redirect()->route('courses_basic_search', ['subject' => 'subject', 'city' => 'city']);
});
// школы просмотр
Route::get('schools/{alias}', [ 'as' => 'schools_one_show' , 'uses' => 'SchoolSearchController@one_show' ] );
Route::get('search-schools/{city}', [ 'as' => 'schools_search' , 'uses' => 'SchoolSearchController@search' ] );
Route::get('search-schools', function () {
    return redirect()->route('schools_search', ['city' => 'city']);
});
Route::get('schools', function () {
    return redirect()->route('schools_search', ['city' => 'city']);
});
//страница рекламирования курсов школ
Route::get('one_vip_company/{type}/{id}', [ 'as' => 'one_company_vip' , 'uses' => 'CompanyController@one_company_vip' ] );



Route::group(['middleware' => 'auth'], function () {

//страницы создания и редактирования компаний
    Route::get('company-create', [ 'as' => 'company_dasboard' , 'uses' => 'CompanyController@company_register' ] );
    Route::post('register-company-new', [ 'as' => 'company_register_save' , 'uses' => 'CompanyController@company_register_save' ] );
    // редактирование информации профиля в компании
    Route::get('edit-profile', [ 'as' => 'edit_profile' , 'uses' => 'CompanyController@editProfile' ]);    
    Route::post('edit-profile', [ 'as' => 'update_profile' , 'uses' => 'CompanyController@updateProfile' ]);
    Route::get('my-notes', [ 'as' => 'company_all_my_notes' , 'uses' => 'CompanyController@companyAllMyNotes' ] );
    Route::get('course-edit/{id}', [ 'as' => 'course_edit' , 'uses' => 'CompanyController@courseEdit' ] );
    Route::get('sclool-edit/{id}', [ 'as' => 'sclool_edit' , 'uses' => 'CompanyController@scloolEdit' ] );
    Route::post('course_edit_save', [ 'as' => 'course_edit_save' , 'uses' => 'CompanyController@courseEditSave' ] );
    Route::post('school_edit_save', [ 'as' => 'school_edit_save' , 'uses' => 'CompanyController@schoolEditSave' ] );
    // удаление курсов
    Route::get('course-delete/{id}', [ 'as' => 'course_delete' , 'uses' => 'CourseSearchController@delete' ] );

//страница пользователя простого
	//создание заявки на обучение
	Route::get('create-new-task', [ 'as' => 'create_new_task' , 'uses' => 'TaskController@create_new_task_get'] );
	//сохранение новой заявки
	Route::post('create-new-task', [ 'as' => 'create_new_task_post' , 'uses' => 'TaskController@save_new_task' ] );
	//заявки созданные учеником к репетиторам
	Route::get('user-tasks', [ 'as' => 'user_tasks' , 'uses' => 'TaskController@show_active_user_tasks' ] );
	Route::get('edit-task/{id}', [ 'as' => 'edit_task' , 'uses' => 'TaskController@edit_task' ] );
	Route::post('update-task', [ 'as' => 'update_task' , 'uses' => 'TaskController@update_task' ] );
	Route::get('user-tasks-no-active', [ 'as' => 'user_tasks_no_active' , 'uses' => 'TaskController@show_no_active_user_tasks' ] );
	Route::get('change-active-tasks/{id}', [ 'as' => 'change_user_task_active' , 'uses' => 'TaskController@change_user_task_active' ] );
	//подписка на уведомления на почту
	Route::get('user-subscription', [ 'as' => 'user_subscription' , 'uses' => 'UserController@user_subscription' ]);	
});

//сообщения
Route::group(['middleware' => 'auth', 'prefix' => 'messages'], function () {

    Route::post('message', ['as' => 'message.store', 'uses' => 'ConversationController@messageStore']);
    Route::post('new_message', ['as' => 'message.new', 'uses' => 'ConversationController@messageNew']);
    Route::get('/', [ 'as' => 'conversations' , 'uses' => 'ConversationController@show_all_conversations' ] );
    Route::get('conversation/{id}', ['as' => 'show_conversation', 'uses' => 'ConversationController@showConversation']);
    Route::post('read_conversation', ['as' => 'read_conversation', 'uses' => 'ConversationController@read_conversation']);
});

//Заявки
Route::group(['middleware' => 'auth', 'prefix' => 'requests'], function () {
    Route::post('request', ['as' => 'request.store', 'uses' => 'RequestController@requestStore']);
    Route::post('request_response_task', ['as' => 'request.response.task', 'uses' => 'RequestController@response_to_pupil_task']);
    Route::get('/', [ 'as' => 'requests' , 'uses' => 'RequestController@show_all_requests' ] );
    Route::get('accept/{id}/{sender_id}', [ 'as' => 'accept_request' , 'uses' => 'RequestController@accept_request' ] );
    Route::get('no_accept/{id}', [ 'as' => 'no_accept_request' , 'uses' => 'RequestController@no_accept_request' ] );
    Route::get('delete/{id}', [ 'as' => 'delete_request' , 'uses' => 'RequestController@delete_request' ] );
});

    
});

//отправление заявки репетитору от клиента
Route::post('send-tutor-request', [ 'as' => 'send_tutor_request' , 'uses' => 'UserController@send_tutor_request' ]);


//Ajax запросы
//поиск города
Route::get('city-search', [ 'as' => 'city_search' , 'uses' => 'SearchTagsAjaxController@city_search'] );
Route::get('city-search-id', [ 'as' => 'city_search_id' , 'uses' => 'SearchTagsAjaxController@city_search_id'] );
Route::get('city-search-all', [ 'as' => 'city_search_all' , 'uses' => 'SearchTagsAjaxController@city_search_all'] );
//поиск предмета
Route::get('subject-search', [ 'as' => 'subject_search' , 'uses' => 'SearchTagsAjaxController@subject_search' ] );
//поиск направлений предмета
Route::get('sub-dir', [ 'as' => 'sub_dir' , 'uses' => 'SearchTagsAjaxController@sub_dir_search' ] );//Подстановка от поднаправления предмета главного предмета
Route::get('change_sub_to_main_subject', [ 'as' => 'change_sub_to_main_subject' , 'uses' => 'SearchTagsAjaxController@change_sub_to_main_subject' ] );
//конвертация валюты
Route::get('convert-valute', [ 'as' => 'convert-valute' , 'uses' => 'SearchTagsAjaxController@convert_valute' ] );
//поиск предмета ( для страницы поиска преподавателя ) возврат имени инглиш
Route::get('subject-search-name', [ 'as' => 'subject_search_name' , 'uses' => 'SearchTagsAjaxController@subject_search_name' ] );
//поиск предмета ( по id для страницы предметов преподавателя ) возврат имени инглиш
Route::get('subject-search-name-for-id', [ 'as' => 'subject_search_name_for_id' , 'uses' => 'SearchTagsAjaxController@subject_search_name_for_id' ] );
//поиск страны рождения
Route::get('country-search', [ 'as' => 'country_search' , 'uses' => 'SearchTagsAjaxController@country_search' ] );
//показ всех контактных данных 
Route::get('show_contact_dates', [ 'as' => 'show_contact_dates' , 'uses' => 'TutorSearchController@show_contact_dates' ] );


//-------------------------------- Админка ------------------------------//

Route::group(['prefix' => 'admin','namespace' => 'Admin', 'middleware' => ['auth', 'admin'] ],function(){
    Route::resource('customers', 'CustomersController');
    Route::resource('brands', 'BrandsController');
    Route::resource('product-categories', 'ProductCategoriesController');
    Route::resource('products', 'ProductsController');
    Route::resource('users', 'UsersController');

    Route::get('edit_sub_categories/{id}', [ 'as' => 'edit_sub_categories' , 'uses' => 'ProductCategoriesController@edit_sub_categories'] );

    Route::get('orders',[
        'uses' => 'OrdersController@index',
        'as' => 'orders.index',
        ]);

	//blog
    Route::get('blog', ['as' => 'blog.index', 'uses' => 'BlogController@index_blog'] );
    Route::get('post_create_new', ['as' => 'blog_post.create', 'uses' => 'BlogController@create_post'] );
    Route::post('post_store_new', ['as' => 'blog_post.store', 'uses' => 'BlogController@store_post'] );

    Route::get('post_edit/{id}', ['as' => 'blog_post.edit', 'uses' => 'BlogController@edit_post'] );
    Route::post('post_update', ['as' => 'blog_post.update', 'uses' => 'BlogController@update_post'] );

    Route::get('blog_tag', ['as' => 'blog_tag.index', 'uses' => 'BlogController@index_tag'] );
    Route::get('blog_tag_create_new', ['as' => 'blog_tag.create', 'uses' => 'BlogController@create_tag'] );
    Route::post('blog_tag_save', ['as' => 'blog_tag.store', 'uses' => 'BlogController@store_tag'] );

	//учебные заведения
    Route::get('university', ['as' => 'university.index', 'uses' => 'UniversityController@index_university'] );
    Route::get('university_create_new', ['as' => 'university.create', 'uses' => 'UniversityController@create_university'] );
    Route::post('university_store_new', ['as' => 'university.store', 'uses' => 'UniversityController@store_university'] );

    Route::get('university_edit/{id}', ['as' => 'university.edit', 'uses' => 'UniversityController@edit_university'] );
    Route::get('university_delete/{id}', ['as' => 'university.delete', 'uses' => 'UniversityController@delete_university'] );
    Route::post('university_update', ['as' => 'university.update', 'uses' => 'UniversityController@update_university'] );

    Route::get('university_subjects', ['as' => 'university_subjects.index', 'uses' => 'UniversityController@index_subjects'] );
    Route::get('university_subjects_create_new', ['as' => 'university_subjects.create', 'uses' => 'UniversityController@create_subjects'] );
    Route::post('university_subjects_save', ['as' => 'university_subjects.store', 'uses' => 'UniversityController@store_subjects'] );

});

//locale
//Переключение языков
Route::get('setlocale/{lang}', function ($lang) {

    $referer = Redirect::back()->getTargetUrl(); //URL предыдущей страницы
    $parse_url = parse_url($referer, PHP_URL_PATH); //URI предыдущей страницы

    //разбиваем на массив по разделителю
    $segments = explode('/', $parse_url);

    //Если URL (где нажали на переключение языка) содержал корректную метку языка
    if (in_array($segments[1], config('app.locales'))) {

        unset($segments[1]); //удаляем метку
    } 
    
    //Добавляем метку языка в URL (если выбран не язык по-умолчанию)
    if ($lang != config('app.locale')){ 
        array_splice($segments, 1, 0, $lang); 
    }

    //формируем полный URL
    $url = Request::root().implode("/", $segments);
    
    //если были еще GET-параметры - добавляем их
    if(parse_url($referer, PHP_URL_QUERY)){    
        $url = $url.'?'. parse_url($referer, PHP_URL_QUERY);
    }
    return redirect($url); //Перенаправляем назад на ту же страницу                            

})->name('setlocale');


//sitemap
Route::get('sitemap', function() {

	// create new sitemap object
	$sitemap = App::make('sitemap');

	// set cache key (string), duration in minutes (Carbon|Datetime|int), turn on/off (boolean)
	// by default cache is disabled
	//    $sitemap->setCache('laravel.sitemap', 60);

	// check if there is cached sitemap and build new only if is not
	//    if (!$sitemap->isCached()) {

	// add item to the sitemap (url, date, priority, freq)
	$sitemap->add(URL::to('/'), '2018-02-01T05:10:00+02:00', '0.9', 'monthly');
	$sitemap->add(route('tutor_basic_search'), '2018-02-01T05:10:00+02:00', '0.6', 'daily');

    $posts = Post::where('post_status', '=', 'published')->where('lang', '=', 'ua')->get();

    // add posts
    foreach ($posts as $post) {
        $images = [];
        $images[0]['url'] = URL::to('/storage/posts/shares/post_main_images/').$post->sm_image;
        $images[0]['title'] = $post->title;
        $images[0]['caption'] = $post->title;
        $sitemap->add(route('blog.one_page', $post->slug), $post->updated_at, '0.7', 'monthly', $images);
    }

	// города

    $cities_teach = Teachers::groupBy('teach_city_en')->select('teach_city_en')->get();

    foreach ($cities_teach as $one_city) {
        $sitemap->add(route('tutor_search_subject_city', ['city' => $one_city->teach_city_en, 'subject' => 'subjects']), '2018-04-14T05:12:00+02:00', '0.7', 'weekly');
    }

    $subjects = Subjects::select('alias')->get();

    foreach ($subjects as $subject) {
        $sitemap->add(route('tutor_search_subject_city', ['city' => 'all-city', 'subject' => $subject->alias]), '2018-03-14T05:12:00+02:00', '0.7', 'weekly');
    }

	// английский
		// Киев
		$sitemap->add(route('tutor_search_subject_city', ['city' => 'Kiev', 'subject' => 'english-language']), '2018-03-14T05:12:00+02:00', '0.7', 'daily');
		// Онлайн
		$sitemap->add(route('tutor_search_subject_city', ['city' => 'online', 'subject' => 'english-language']), '2018-03-14T05:12:00+02:00', '0.7', 'daily');
	// немецкий 
		// Киев
		$sitemap->add(route('tutor_search_subject_city', ['city' => 'Kiev', 'subject' => 'german-language']), '2018-03-14T05:12:00+02:00', '0.7', 'daily');
		// Онлайн
		$sitemap->add(route('tutor_search_subject_city', ['city' => 'online', 'subject' => 'german-language']), '2018-03-14T05:12:00+02:00', '0.7', 'daily');
	// математика 
		// Онлайн
		$sitemap->add(route('tutor_search_subject_city', ['city' => 'online', 'subject' => 'mathematics']), '2018-03-14T05:12:00+02:00', '0.7', 'weekly');
		// Киев
		$sitemap->add(route('tutor_search_subject_city', ['city' => 'Kiev', 'subject' => 'mathematics']), '2018-03-14T05:12:00+02:00', '0.7', 'weekly');
	// музыка 
		// Киев
		$sitemap->add(route('tutor_search_subject_city', ['city' => 'Kiev', 'subject' => 'music-subject']), '2018-03-14T05:12:00+02:00', '0.7', 'weekly');		
		// Онлайн
		$sitemap->add(route('tutor_search_subject_city', ['city' => 'online', 'subject' => 'music-subject']), '2018-03-14T05:12:00+02:00', '0.7', 'weekly');
	// химия 
		// онлайн
		$sitemap->add(route('tutor_search_subject_city', ['city' => 'online', 'subject' => 'chemistry-subject']), '2018-03-14T05:12:00+02:00', '0.7', 'weekly');
		// Киев
		$sitemap->add(route('tutor_search_subject_city', ['city' => 'Kiev', 'subject' => 'chemistry-subject']), '2018-03-14T05:12:00+02:00', '0.7', 'weekly');
	// украинский 
		// Киев
		$sitemap->add(route('tutor_search_subject_city', ['city' => 'Kiev', 'subject' => 'ukrainian-language-subject']), '2018-03-14T05:12:00+02:00', '0.7', 'weekly');
		// Онлайн
		$sitemap->add(route('tutor_search_subject_city', ['city' => 'online', 'subject' => 'ukrainian-language-subject']), '2018-03-14T05:12:00+02:00', '0.7', 'weekly');
		// Кривой рог
		$sitemap->add(route('tutor_search_subject_city', ['city' => 'Kryvyy-Rih', 'subject' => 'ukrainian-language-subject']), '2018-03-14T05:12:00+02:00', '0.7', 'weekly');
	// история
		// онлайн
		$sitemap->add(route('tutor_search_subject_city', ['city' => 'online', 'subject' => 'history-subject']), '2018-03-14T05:12:00+02:00', '0.7', 'weekly');
		// киев
		$sitemap->add(route('tutor_search_subject_city', ['city' => 'Kiev', 'subject' => 'history-subject']), '2018-03-14T05:12:00+02:00', '0.7', 'weekly');
	// физика
		// онлайн
		$sitemap->add(route('tutor_search_subject_city', ['city' => 'online', 'subject' => 'physics-subject']), '2018-03-14T05:12:00+02:00', '0.7', 'weekly');
		// киев
		$sitemap->add(route('tutor_search_subject_city', ['city' => 'Kiev', 'subject' => 'physics-subject']), '2018-03-14T05:12:00+02:00', '0.7', 'weekly');
	// изобраз исскуство
		// киев
		$sitemap->add(route('tutor_search_subject_city', ['city' => 'Kiev', 'subject' => 'visual-arts']), '2018-03-14T05:12:00+02:00', '0.7', 'weekly');	
	// информатика
		// онлайн
		$sitemap->add(route('tutor_search_subject_city', ['city' => 'online', 'subject' => 'computer-science']), '2018-03-14T05:12:00+02:00', '0.7', 'weekly');
	// ит программирование
		// онлайн
		$sitemap->add(route('tutor_search_subject_city', ['city' => 'online', 'subject' => 'programming-subjec']), '2018-03-14T05:12:00+02:00', '0.7', 'weekly');

		// когда появятся задания учеников для репетиторов - Включить!
		//$sitemap->add(route('task_search_subject_city', ['subject', 'all-city']), '2018-02-01T05:10:00+02:00', '0.6', 'daily');

		// get all teachers from db
		$teachers = Teachers::where('active', '=', '1')->get();
		// get all posts from db
		
		// get all univers from db
		$univers = University::where('country', '=', 'ua')->get();

		// add teachers
		foreach ($teachers as $teacher) {
			$images = [];
			$images[0]['url'] = URL::asset('storage/users/') .'/'. $teacher->teacher_user->avatar;
			$images[0]['title'] = $teacher->profile_title;
			$images[0]['caption'] = $teacher->profile_title;
			$sitemap->add(route('tutor_page', $teacher->slug), $teacher->updated_at, '0.8', 'monthly', $images);
		}
		
		// add univers pages
		foreach ($univers as $univer) {
			$images = [];
			$images[0]['url'] = URL::asset('storage/universities/') .'/'. $univer->logo;
			$images[0]['title'] = $univer->title;
			$images[0]['caption'] = $univer->title;
            $sitemap->add(route('univer_page', $univer->slug), $univer->updated_at, '0.7', 'monthly', $images);
			$sitemap->add(route('home').'/c-ua/universities/'.$univer->slug, $univer->updated_at, '0.7', 'monthly');
		}
		
	return $sitemap->render('xml');
});