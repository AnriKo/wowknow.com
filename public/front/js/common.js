$(function() {

    var url_cur = new Url();
	$('.selec_valute').on( "change", function(){
		
		if (url_cur.query.price_min == undefined && url_cur.query.price_max == undefined) {
			var url = window.location.origin+'/convert-valute?valute='+$(this).val();
			window.location.href = url;
		}else{
			var url = window.location.origin+'/convert-valute?valute='+$(this).val();
			delete url_cur.query.price_min;
			delete url_cur.query.price_max;
			var url_this = url_cur.toString();
			window.history.pushState("", "", url_this);
			window.location.href = url;
		}

	});

	var flash_message = $('#flash_message');
	if (flash_message.length > 0) {
		flash_message.delay(7000).fadeOut('slow');
	}

	var flash_message_long_time = $('#flash_message_long_time');
	var close_flash_message = $('#close_flash_message');
	close_flash_message.on('click', function(){
		flash_message_long_time.fadeOut('slow');
	});

	var register = $('#reg_from_mod_login');
	if(register){
		register.on('click', function(){
			$('#basic_login').modal('hide');
		});
	}

	var register = $('#log_from_mod_reg');
	if(register){
		register.on('click', function(event){
			$('#basic_register').modal('hide');
		});
	}


	$('#basic_login').add('#basic_register').on('show.bs.modal', function (event) {

		$(this).find("input[name='next_page']").val($(event.relatedTarget).attr('data-next-page'));

	})


	//show left part top menu
	$('#button_to_right_top_menu').on('click', function(){
		$('#right_top_menu ul').toggle("slow");
	});

	$('#button_header_menu').on('click', function(){
		$('#user_profile_menu').addClass('open'); // Opens the dropdown
	});

	$('#select_type_of_register a').on('click', function(){
		$('#modal_question_what_register').modal('hide');
	});


	//captcha
	// var check_auth = $('meta[property="auth"]').attr('content');
	// if(check_auth == 'false'){
	// 	var myCaptcha = new jCaptcha({
	//
	// 		// set callback function
	// 		requiredValue: '=',
	// 		canvasFillStyle: '#00000',
	// 		callback: function(response, $captchaInputElement) {
	//
	// 			if (response == 'success') {
	// 				$captchaInputElement[0].classList.remove('error');
	// 				$captchaInputElement[0].classList.add('success');
	// 				$captchaInputElement[0].placeholder = 'Це перемога!';
	// 				return true;
	//
	// 			}
	//
	// 			if (response == 'error') {
	// 				$captchaInputElement[0].classList.remove('success');
	// 				$captchaInputElement[0].classList.add('error');
	// 				$captchaInputElement[0].placeholder = 'Не вірно, спробуйте знову!';
	//
	// 			}
	//
	// 		}
	//
	// 	});
	//
	// 	// $('form').on('submit',function(e) {
	// 	//
	// 	// 	var res = myCaptcha.validate();
	// 	// 	if(res === false){
	// 	// 		e.preventDefault();
	// 	// 	}
	// 	//
	// 	// });
	// }



    $('form.check_captcha').on('submit',function(e) {

		var response = grecaptcha.getResponse();
		if(response.length == 0)
		{
			//reCaptcha not verified
			//alert("please verify you are humann!");
			e.preventDefault();
			$(this).find('#captcha_verify').text('Натисніть на каптчу, пройдіть перевірку що ви не робот!').show();
			return false;
		}

    });


})