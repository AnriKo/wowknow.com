<?php 
namespace App\MyHelpers;

use App\Teachers;
use App\Courses;
use App\School;
use App\TeacherPrice;
use App\TeacherSubject;
use App\TeacherEducation;
use App\University;
use App\UnivSubject;
use Auth;
use App\Review;
use App;
use App\Mail\CongratulateNewTutor;

class Helper_two {

	static function check_tutor_active(){
		$tutor_id = Auth::user()->id;
		$tutor = Teachers::where('user_id', '=', $tutor_id)->first();
		$active = 0;
		//базовая информация
		if(
			$tutor->last_name != null &&
			$tutor->slug != null &&
			$tutor->gender != null &&
			$tutor->country_teach != null 
		){
			$active += 1;
		}
		if($active == 1){
			//основная информация
			if(
				$tutor->profile_title != null &&
				$tutor->tutor_edu_text != null &&
				($tutor->teach_online == "1" || $tutor->teach_local == "1")
			){
				$active += 1;
			}

		}
		if($active == 2){
			//предметы преподавания
			$teacher_subject = TeacherSubject::where('teach_id', '=', $tutor->id)->first();
			if(
				$teacher_subject != null
			){
				$active += 1;
			}
		}
		if($active == 3){
			//образование интересы
			$teacher_education = TeacherEducation::where('teach_id', '=', $tutor->id)->first();
			if(
				$teacher_education != null ||
				$tutor->tutor_hobie_text != null

			){
				$active += 1;
			}
		}
		if($active == 4){
			$tutor->active = 1;
			if($tutor->raiting < 100){
				\Mail::to(Auth::user()->email)->send(new CongratulateNewTutor(Auth::user()->name, $tutor->slug) );
				$tutor->raiting = 100;
			}
			$tutor->save();
			return 1;
		}else{
			$tutor->active = 0;
			$tutor->save();
			return 0;
		}
	}

	static function count_teacher_review(){
		$reviews = Review::where('item_id', '=', Auth::user()->user_teacher->id)
		->where('type', '=', 'tutor')
		->count();
		if($reviews > 0){
			return $reviews;
		}
		return '';
	}

	static function univers_cities(){
		$local = App::getLocale();
		$name = "title_".$local;
		$univers_cities = University::groupBy('universities.city')
		->where('country', '=', 'ua')
		->leftJoin('cities_ua', 'cities_ua.title_en', '=', 'universities.city')
		->select("cities_ua.$name As name", 'cities_ua.title_en')
		->get();
		return $univers_cities;
	}

	static function univers_subject(){
		$local = App::getLocale();
		$name = "name_".$local;
		$univers_subject = UnivSubject::
		select("$name As name", 'slug')
		->get();
		return $univers_subject;
	}

	static function top_tutors(){
		$top_tutors = Teachers::
		where('top', '=', '1')
		->leftJoin('users', 'users.id',  '=', 'teachers.user_id')
		  ->select(
		    'teachers.id', 
		    'teachers.user_id', 
		    'teachers.last_name', 
		    'teachers.country_teach',
		    'teachers.profile_title', 
		    'teachers.slug', 
		    'users.name',
		    'users.avatar'
		  )
		->take(5)
		->inRandomOrder()
		->get();
		return $top_tutors;
	}

	static function top_company(){
		$top_course = Courses::
		where('top', '=', '1')
		->leftJoin('cities_ua', 'cities_ua.title_en',  '=', 'courses.city')
		  ->select(
		    'courses.id', 
		    'courses.alias', 
		    'courses.name', 
		    'courses.street',
		    'cities_ua.title_ru',
		    'courses.city'
		  )
		->take(4)
		->inRandomOrder()
		->get();
		$top_school = School::
		where('top', '=', '1')
		->leftJoin('cities_ua', 'cities_ua.title_en',  '=', 'schools.city')
		  ->select(
		    'schools.id', 
		    'schools.alias', 
		    'schools.name', 
		    'schools.street',
		    'schools.class',
		    'cities_ua.title_ru',
		    'schools.city'
		  )
		->take(4)
		->inRandomOrder()
		->get();

		$top_course = $top_course->merge($top_school);
		$top_course = $top_course->shuffle();
		return $top_course;
	}

}	