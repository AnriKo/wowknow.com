<?php 
namespace App\MyHelpers;

use App\Conversation;
use App\ConversationReply;
use Auth;
use DB;
use Carbon\Carbon;

 
class Helper {

    static function convert_cur($amount, $from, $to){
        // $get = file_get_contents("https://finance.google.com/finance/converter?a=$amount&from=$from&to=$to");

        // $get = explode("<span class=bld>",$get);

        // $get = explode("</span>",$get[1]);
        // dd($get);
        // $converted_currency = preg_replace("/[^0-9\.]/", null, $get[0]);
        // return $converted_currency;
    }

    static function lang_name($lang_code) {

      switch ($lang_code) {
        case 'ua':
            return 'Українською';
          break;
        case 'ru':
            return 'на Русском';
          break;
        case 'en':
            return 'in English';
          break;
      }
    }

    static function country_name($country_code) {

      switch ($country_code) {
        case 'ua':
            return 'Украина';
          break;
        case 'ru':
            return 'Россия';
          break;
        case 'kz':
            return 'Казахстан';
          break;
      }
    }

    static function value_cur($cur) {

              switch ($cur) {
        case 'UAH':
            return 'грн';
          break;
        case 'RUB':
            return 'руб';
          break;
        case 'USD':
            return '$';
          break;
        case 'KZT':
            return 'теңге';
          break;
        default:
            return 'руб. белор';
          break;
      }
    }

    static function pupil_age($age) {

              switch ($age) {
        case 'a1':
            return 'ребенок до 3 лет';
          break;
        case 'a2':
            return 'ребенок до 6 лет';
          break;
        case 'a3':
            return 'младшие школьники';
          break;
        case 'a4':
            return 'школьники';
          break;
        case 'a5':
            return 'студенты';
          break;
        case 'a6':
            return 'взрослые 23-40';
          break;
        case 'a7':
            return 'взрослые 40+';
          break;
        default:
            return '';
          break;
      }

    }

    static function get_years_born() {
    $years = [            
        '2004','2003','2002','2001','2000','1999','1998','1997','1996','1995','1994','1993','1992','1991','1990','1988','1987','1986','1985','1984','1983','1982','1981','1980','1979','1978','1977','1976','1975','1974','1973','1972','1971','1970','1969','1968','1967','1966','1965','1964','1963','1962','1961','1960','1959','1958','1957','1956','1955','1954','1953','1952','1951','1950','1949','1948','1947','1946','1945','1944','1943','1942','1941','1940','1939','1938','1937','1936','1935','1934','1933','1932','1930','1929','1928','1927','1926','1925','1924','1923','1922','1921','1920',
        ];

        return $years;

    }

    static function get_years_study($year_begin) {

        $now_year = date("Y");
        $years = [];
        for($i = $year_begin; $i <=55; $i++){
          $years[] = $now_year - $i;
        }

      return $years;

    }

    static function get_native_language() {
    $languages = [ 'Українська','Русский','English','Deutsch','Azərbaycanca','Български','Català','Česky','Dansk','Ελληνικά','Español','Eesti','Euskara','Français','Galego','Hrvatski','Bahasa','Indonesia','Italiano','Latina','Lietuvių','Magyar','Bahasa','Melayu','Bahaso','Minangkabau','Nederlands','Norsk (bokmål)','Norsk (nynorsk)','Oʻzbekcha / Ўзбекча','Polski','Português','Қазақша / Qazaqşa / قازاقشا','Română','Саха','Тыла','Sinugboanong','Binisaya','Srpskohrvatski / Српскохрватски','Slovenčina','Shqipërisë / Albanian','Српски / Srpski','Suomi','Svenska','Türkçe','Qırımtatar','tili','Volapük','Winaray','فارسی','Հայերեն','한국어','हिन्दी','עברית','日本語','Tiếng','Việt','中文','العربية','اُردُو',
        ];

        return $languages;

    }
    static function pupils_age() {
    $languages = [ 
            '-3', '4-6', '6-12', '12-17', '17-23', '23-40', '40+'
        ];

        return $languages;
    }

    static function ru2lat($str)
    {
        if (mb_strlen($str) > 200 ) {
            $str = mb_strimwidth($str, 0, 200);
        }
        $tr = array(
        "А"=>"a", "Б"=>"b", "В"=>"v", "Г"=>"g", "Д"=>"d",
        "Е"=>"e", "Ё"=>"yo", "Ж"=>"zh", "З"=>"z", "И"=>"i", 
        "Й"=>"j", "К"=>"k", "Л"=>"l", "М"=>"m", "Н"=>"n", 
        "О"=>"o", "П"=>"p", "Р"=>"r", "С"=>"s", "Т"=>"t", "Є"=>"e",
        "У"=>"u", "Ф"=>"f", "Х"=>"kh", "Ц"=>"ts", "Ч"=>"ch", 
        "Ш"=>"sh", "Щ"=>"sch", "Ъ"=>"", "Ы"=>"y", "Ь"=>"", 
        "Э"=>"e", "Ю"=>"yu", "Я"=>"ya", "а"=>"a", "б"=>"b", 
        "в"=>"v", "г"=>"g", "д"=>"d", "е"=>"e", "ё"=>"yo", "є"=>"e",
        "ж"=>"zh", "з"=>"z", "и"=>"i", "і"=>"i", "й"=>"j", "к"=>"k", 
        "л"=>"l", "м"=>"m", "н"=>"n", "о"=>"o", "п"=>"p", 
        "р"=>"r", "с"=>"s", "т"=>"t", "у"=>"u", "ф"=>"f", 
        "х"=>"kh", "ц"=>"ts", "ч"=>"ch", "ш"=>"sh", "щ"=>"sch", 
        "ъ"=>"", "ы"=>"y", "ь"=>"", "э"=>"e", "ю"=>"yu", 
        "я"=>"ya", " "=>"-", "."=>"", ","=>"", "/"=>"-", "’"=>"", "?"=>"", "ї"=>"i", 
        ":"=>"", ";"=>"","—"=>"", "–"=>"-", "«"=>"", "»"=>"", "("=>"", ")"=>""
        );
    return strtr($str,$tr);
    }

    static function GetYear($int, $local) {

      if ( $int > 20 ) {
        $int = substr( $int, -1 );
      }
      if($local == 'ru'){
        switch ($int) {
          case 0:
              return 'лет';
            break;
          case 1:
              return 'год';
            break;
          case ( $int >= 2 && $int <= 4 ):
              return 'года';
            break;
          case ( $int >= 10 && $int <= 20 ):
              return 'лет';
            break;
          default:
              return 'лет';
            break;
        }
      }elseif($local == 'ua'){
          switch ($int) {
          case 0:
              return 'років';
            break;
          case 1:
              return 'рік';
            break;
          case ( $int >= 2 && $int <= 4 ):
              return 'роки';
            break;
          case ( $int >= 10 && $int <= 20 ):
              return 'років';
            break;
          default:
              return 'років';
            break;
        }
      }else{
        return 'years';
      }
    }


    static function local_teach_country($code_country) {
              switch ($code_country) {
        case 'ua':
            return false;
          break;
        case 'ru':
            return false;
          break;
        case 'by':
            return false;
          break;
        case 'kz':
            return false;
          break;
        default:
            return true;
          break;
      }

    }

    // получение ID видео из URL
    static function getYoutubeVideoID($url){
     
        // допустимые доменые имена в ссылке
        $names = array('www.youtube.com','youtube.com');
     
        // разбор адреса
        $up = parse_url($url);
     
        // проверка параметров
        if (isset($up['host']) && in_array($up['host'],$names) &&
            isset($up['query']) && strpos($up['query'],'v=') !== false){
     
            // достаем параметр ID
            $lp = explode('v=',$url);
     
            // отсекаем лишние параметры
            $rp = explode('&',$lp[1]);
     
            // возвращаем строку, либо false
            return (!empty ($rp[0]) ? $rp[0] : false);
        }
        return false;
    }

    static function unreadMessage(){

      $user_id = Auth::user()->id;
      $conversation1 = DB::table('conversation')
        ->where('recipient_id', '=', $user_id)
        ->leftJoin('conversation_reply', 'conversation_reply.c_id',  '=', 'conversation.id')
        ->where('conversation_reply.status', '=', '0')
        ->groupBy('conversation_reply.c_id')
        ->get();

        $filtered1 = $conversation1->whereNotIn('user_id', [$user_id]);

      $conversation2 = DB::table('conversation')
        ->where('sender_id', '=', $user_id)
        ->leftJoin('conversation_reply', 'conversation_reply.c_id',  '=', 'conversation.id')
        ->where('conversation_reply.status', '=', '0')
        ->groupBy('conversation_reply.c_id')
        ->get();

        $filtered2 = $conversation2->whereNotIn('user_id', [$user_id]);
        $filtered = $filtered1->merge($filtered2);

      $count = $filtered->count();

      return $count;

      
    }

    static function unread_erquests(){

      $user_id = Auth::user()->id;
      $requests = DB::table('requests')
        ->where('teacher_id', '=', $user_id)
        //->orWhere('pupil_id', '=', $user_id)
        ->where('status', '=', 'sent')
        ->where('sender', '!=', $user_id)
        ->count();

      return $requests;

    }

    static function set_review($teacher_id){

      $user_id = Auth::user()->id;
      $requests = DB::table('requests')
        ->where('teacher_id', '=', $teacher_id)
        ->where('pupil_id', '=', $user_id)
        ->where('status', '=', 'accept')
        ->where('created_at', '<=', Carbon::now()->subDays(7)->toDateTimeString())
        ->count();

      return $requests;

    }

}