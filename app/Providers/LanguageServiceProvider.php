<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Request;
use App;
use Config;

class LanguageServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->setRouteLang();
    }

    public function setRouteLang(){
        // $language = Request::segment(1); 
        // $routeLang = '';

        // if (isset(config('app.locales')[$language])) { 
        //     App::setLocale($language); 
        //     $routeLang = $language; 
        // } 

        // //Config::set('routeLang', $routeLang);
        // config(['app.routeLang' => $routeLang]);
        //dump(App::getLocale());
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
