<?php

namespace App\Modules;

use Symfony\Component\DomCrawler\Crawler;
use App\CityUa;

class DomCrawler
{

	public function domain_price($link){

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch,CURLOPT_URL,$link);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);

		curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/525.13 (KHTML, like Gecko) Chrome/0.A.B.C Safari/525.13");
		$html = curl_exec($ch);
		curl_close($ch);

	    // Create new instance for parser.
	    $crawler = new Crawler(null, $link);
	    $crawler->addHtmlContent($html, 'UTF-8');

	    $post = [];

	    // Get title text.
	    $post = $crawler->filter('.domain-price-scroller table tr')->each(function ($node, $i) {
	    	$res = [];
	    	$res['domain'] = $node->filter('td')->eq(0)->text();
	    	$price = $node->filter('td')->eq(1)->text();
	    	$price = str_replace(' .', '', $price);
	    	$res['price'] = $price;
		    return $res;
		});

	    return $post;

	}

	public function eduget_post($link){

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch,CURLOPT_URL,$link);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);

		curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/525.13 (KHTML, like Gecko) Chrome/0.A.B.C Safari/525.13");
		$html = curl_exec($ch);
		curl_close($ch);

	    // Create new instance for parser.
	    $crawler = new Crawler(null, $link);
	    $crawler->addHtmlContent($html, 'UTF-8');

	    $post = [];

	    // Get title text.
	    $post['title'] = $crawler->filter('.container-main h1')->text();

	   	// Get image
	    try {
	    	$post['image'] = $crawler->filter('.container-main .news-content figure img')->eq(0)->attr('src');
		} catch (\InvalidArgumentException $e) {
		    $post['image'] = null;
		}

	   	// Get tag
	    try {

	    	$post['tag'] = $crawler->filter('.container-main .label-group span')->text();
	    	$post['tag'] = trim($post['tag']);

		} catch (\InvalidArgumentException $e) {
		    $post['tag'] = null;
		}

	    $post['body'] = $crawler->filter('.container-main .news-content .clearfix')->last()->html();
	    $post['body'] = strip_tags($post['body'], '<p><br><strong><b><div><h2><h3><h4><ul><ol><li>');

	    $post['body'] = str_replace('  ', ' ', $post['body']);
	    $post['body'] = str_replace('\n\n', '\n', $post['body']);
	    $post['body'] = trim($post['body']);
	    return $post;

	}

	public function education_ua_post($link){

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch,CURLOPT_URL,$link);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);

		curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/525.13 (KHTML, like Gecko) Chrome/0.A.B.C Safari/525.13");
		$html = curl_exec($ch);
		curl_close($ch);

	    // Create new instance for parser.
	    $crawler = new Crawler(null, $link);
	    $crawler->addHtmlContent($html, 'UTF-8');

	    $post = [];

	    // Get title text.
	    $post['title'] = $crawler->filter('#content h1')->text();

	   	// Get image
	    try {
	    	$post['image'] = $crawler->filter('#content .picMain .picCenter img')->eq(0)->attr('src');
		} catch (\InvalidArgumentException $e) {
		    $post['image'] = null;
		}

	   	// Get tag
	    try {

	    	$post['tag'] = $crawler->filter('#content .nf a')->text();
	    	$post['tag'] = trim($post['tag']);

		} catch (\InvalidArgumentException $e) {
		    $post['tag'] = null;
		}

	    $post['body'] = $crawler->filter('#content #paddContent')->html();
	    
	    $post['body'] = $this->strip_selected_tags_content($post['body'] , ['h1', 'style', 'script', ]);


	    

	    $post['body'] = str_replace('<div class="picMain">', '', $post['body']);
	    
	    $post['body'] = preg_replace('/<div class="picTop">(.*?)<\/div>/', "", $post['body']);
	    $post['body'] = preg_replace('/<div class="nf" (.*?)<\/div>/s', "", $post['body']);
	    $post['body'] = preg_replace('/<div class="nf grey"(.*?)<\/div>/s', "", $post['body']);
	    $post['body'] = preg_replace('/<div class="picCenter">(.*?)<\/div>/', "", $post['body']);
	    $post['body'] = str_replace(array("\r", "\t", "\n"), '', $post['body']);
	    $post['body'] = str_replace('</p>', "</p>\n", $post['body']);
	    $post['body'] = trim($post['body']);
	    $post['body'] = strip_tags($post['body'], '<p><br><strong><b><h2><h3><h4><h5><ul><ol><li>');
	    $post['body'] = preg_replace('/Інші статті в(.*)/', "", $post['body']);
	    $post['body'] = preg_replace('/<p>Джерело:(.*)/', "", $post['body']);
	    return $post;

	}

	public function strip_selected_tags_content($text, $tags)
	{
		foreach ($tags as $tag) {
			$text = preg_replace('~<'.$tag.'(.*?)</'.$tag.'>~Usi', "", $text);
		}
		

		return $text;
	}

	public function education_ua($link){

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch,CURLOPT_URL,$link);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);

		curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/525.13 (KHTML, like Gecko) Chrome/0.A.B.C Safari/525.13");
		$html = curl_exec($ch);
		curl_close($ch);

	    // Create new instance for parser.
	    $crawler = new Crawler(null, $link);
	    $crawler->addHtmlContent($html, 'UTF-8');

	    // Get title text.
	    $link_author = $crawler->filter('#content .artShort a')->extract(array( 'href'));
	    return $link_author;

	}

	public function eduget($link){

		$ch = curl_init();
		//$proxy = '177.52.48.225:4145';
		$proxyauth = 'user:password';
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch,CURLOPT_URL,$link);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);

		//curl_setopt($ch, CURLOPT_PROXY, $proxy);
		//curl_setopt ($ch, CURLOPT_PROXYTYPE, CURLPROXY_SOCKS5); 

		curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/525.13 (KHTML, like Gecko) Chrome/0.A.B.C Safari/525.13");
		$html = curl_exec($ch);
		curl_close($ch);

	    // Create new instance for parser.
	    $crawler = new Crawler(null, $link);
	    $crawler->addHtmlContent($html, 'UTF-8');

	    // Get title text.
	    $link_author = $crawler->filter('.news-item a.news-item-thumbnail')->extract(array( 'href'));
	    return $link_author;

	}


	public function getFromOnePageLinkUniver($parser, $link)
	{
	    // Get html remote text.
	    $ch = curl_init();
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch,CURLOPT_URL,$link);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/525.13 (KHTML, like Gecko) Chrome/0.A.B.C Safari/525.13");
		$html = curl_exec($ch);
		curl_close($ch);
	    //$html = file_get_contents($link);

	    // Create new instance for parser.
	    $crawler = new Crawler(null, $link);
	    $crawler->addHtmlContent($html, 'UTF-8');

	    // Get title text.
	    $cities = $crawler->filter('article .details .title a')->extract(array( 'href'));

	    $content = [
	        'cities' => $cities,
	    ];

	    return $content;
	}

	public function get28PagesUnivers($parser, $link)
	{
	    // Get html remote text.
	    $html = file_get_contents($link);

	    // Create new instance for parser.
	    $crawler = new Crawler(null, $link);
	    $crawler->addHtmlContent($html, 'UTF-8');

	    // Get title text.
	    $link_univers_pages = $crawler->filter('.pagination li.active + li a')->extract(array( 'href'));

	    $content = $link_univers_pages;

	    return $content;
	}

	public function getUniversity($parser, $link)
		{
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch,CURLOPT_URL,$link);
			curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
			curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/525.13 (KHTML, like Gecko) Chrome/0.A.B.C Safari/525.13");
			$html = curl_exec($ch);
			curl_close($ch);
		    // Get html remote text.
		    //$html = file_get_contents($link);

		    // Create new instance for parser.
		    $crawler = new Crawler(null, $link);
		    $crawler->addHtmlContent($html, 'UTF-8');

		    // Get title text.
		    try {
			   $title = $crawler->filter($parser['title'])->text();
			} catch (\InvalidArgumentException $e) {
			    $title = null;
			}

		    // Get table table_adress
		   	try {
			   $table_adress = $crawler->filter($parser['table_adress'])->html();
			} catch (\InvalidArgumentException $e) {
			    $table_adress = null;
			}

		    // Get image
		    try {
			   $image = $crawler->selectImage($title)->image()->getUri();
			} catch (\InvalidArgumentException $e) {
			    $image = null;
			}

		    // Get table table_adress
		    try {
			   $bodies = $crawler->filter($parser['body'])->html();
			} catch (\InvalidArgumentException $e) {
			    $bodies = null;
			}

		    // Get gallery
		    try {
			   $gallery = $crawler->filter($parser['gallery'])->html();
			} catch (\InvalidArgumentException $e) {
			    $gallery = null;
			}

		    // Get city
		    try {
			   $city = $crawler->filter($parser['city'])->text();
			} catch (\InvalidArgumentException $e) {
			    $city = null;
			}
		    // Get all
		    try {
			   $city_all = $crawler->filter($parser['city_all'])->text();
			} catch (\InvalidArgumentException $e) {
			    $city_all = null;
			}

			//фильтруем полученные данные table_adress таблицы адресса
	   		$table_adress = str_replace(array("\r", "\t", "\n"), '', $table_adress);
	   		$table_adress = preg_replace('/(<li><a href="\/ru\/form\/vstup").*/', '', $table_adress);
	   		$table_adress = preg_replace('/(<li><a href="\/form\/vstup").*/', '', $table_adress);
	   		$table_adress = preg_replace('|[\s]+|s', ' ', $table_adress);
	   		$table_adress = preg_replace('/(li>)/', 'tr>', $table_adress);
	   		$table_adress = preg_replace('/(li>)/', 'tr>', $table_adress);
	   		$table_adress = preg_replace('/(strong>)/', 'td>', $table_adress);
	   		$table_adress = preg_replace('/(em>)/', 'td>', $table_adress);
	   		$table_adress = preg_replace('/(<em)/', '<td', $table_adress);
	   		$table_adress = '<table id="adress_univer" class="table table-striped table-bordered company_detals"><tbody>'.$table_adress.'</tbody></table>';

			//фильтруем полученные данные gallery таблицы адресса
	   		$gallery = str_replace(array("\r", "\t", "\n"), '', $gallery);
	   		$gallery = preg_replace('/(<li><a href="\/ru\/form\/vstup").*/', '', $gallery);
	   		$gallery = preg_replace('|[\s]+|s', ' ', $gallery);
	   		preg_match_all('/<a[^>]+href=([\'"])(?<href>.+?)\1[^>]*>/i', $gallery, $gallery);

	   		////фильтруем полученные данные данные body основного текста 
		    $bodies = str_replace(array("\r", "\t", "\n"), '', $bodies);

	   		////фильтруем город на рус укр
			$city_all = explode(' ',trim($city_all));
			$city_all = $city_all[0];

			$content = [
		        'title' => $title,
		        'image' => $image,
		        'city' => $city,
		        'city_all' => $city_all,
		        'table_adress' => $table_adress,
		        'bodies' => $bodies,
		        'gallery' => $gallery
		    ];

		    return $content;

			//dd($content);
		    
////фильтруем полученные данные данные body основного текста 
		    $bodies = str_replace(array("\r", "\t", "\n"), '', $bodies);
			$bodies = preg_replace('/.*?(<a name="faculty"><\/a>)/s', '', $bodies);
		    $bodies = preg_replace('/(<a name="gallery").*/', '', $bodies);
		    $bodies = preg_replace('/(<div style="border:1px solid #D6D8DD).*/', '', $bodies);
		    $tags = array("table", "script", "input", "form", "meta", "h1", "i");
	   		$bodies = preg_replace('#<(' . implode( '|', $tags) . ')(?:[^>]+)?>.*?</\1>#s', '', $bodies);
	   		$bodies = preg_replace( '/(<meta|<input|<img).*?>/', "", $bodies );
	   		$bodies = preg_replace('/(<a href="#up).*?<\/a>/', '', $bodies);
	   		$bodies = preg_replace( '/( class| style)=".*?"/', "", $bodies );
	   		$bodies = preg_replace('/(b>)/', 'strong>', $bodies);
	   		
	   		$bodies = preg_replace('|[\s]+|s', ' ', $bodies);
	   		$bodies = preg_replace('/(<div id="menu-inside">|<div id="js-map").*?<\/div>/', '', $bodies);

	   		$bodies = preg_replace('/(<div)\s*>\s*<\/div>/s', '', $bodies);
	   		$bodies = preg_replace('/(<div)\s*>\s*<\/div>/s', '', $bodies);
	   		$bodies = preg_replace('/(<div)\s*>\s*<\/div>/s', '', $bodies);
	   		$bodies = '<a name="faculties"></a>'.$bodies;

//фильтруем полученные данные table_adress таблицы адресса
	   		$table_adress = str_replace(array("\r", "\t"), '', $table_adress);
	   		$table_adress = preg_replace('|[\s]+|s', ' ', $table_adress);
	   		$tags_2 = array("script");
	   		$table_adress = preg_replace('#<(' . implode( '|', $tags_2) . ')(?:[^>]+)?>.*?</\1>#s', '', $table_adress);
	   		$table_adress = preg_replace( '/(style)=".*?"/', "", $table_adress );
	   		$table_adress = preg_replace('/(td1)/', 'adress_label', $table_adress);
	   		$table_adress = preg_replace('/(<tr><td align="right").*?<\/tr>/', '', $table_adress);
	   		//находим id города в своей базе данных
	   		$city_in_my_bd = CityUa::where('title_ru', '=', $city)->first();
	   		if($city_in_my_bd){
	   			$city_in['title_en'] = $city_in_my_bd->title_en;
	   		}else{
	   			$city_in['title_en'] = null;
	   		}
	   		//формируем ссылку на страницу свех университетов этого города.
			$table_adress = preg_replace( '/(href="\/universities\/).*?"/', 'href="/c-ua/universities/'.$city_in['title_en'].'"', $table_adress );
			$table_adress = '<table id="adress_univer">'.$table_adress.'</table>';

//фильтруем полученные данные short_info таблицы дополнит инфо			
	   		$table_descr = str_replace(array("\r", "\t"), '', $table_descr);
	   		$table_descr = preg_replace('|[\s]+|s', ' ', $table_descr);
	   		$table_descr = preg_replace('/("ok")/', '"check"', $table_descr);
	   		$table_descr = preg_replace('/("no")/', '"uncheck"', $table_descr);
	   		$table_descr = preg_replace('/("r")/', '"heavy"', $table_descr);
	   		$table_descr = preg_replace('/( class="bor_no")/', '', $table_descr);
	   		$table_descr = preg_replace('/(width="240" class="tbl_grey_center nf")/', '', $table_descr);
	   		$table_descr = preg_replace('/(<div)\s*>\s*<\/div>/s', '', $table_descr);
	   		$table_descr = preg_replace('/(<tr)\s*>\s*<\/tr>/s', '', $table_descr);
	   		$table_descr = preg_replace('/(<div class="div_grey_top">|<div class="div_grey_bottom").*?<\/div>/', '', $table_descr);


		    $content = [
		        'title' => $title,
		        'image' => $image,
		        'city_en' => $city_in['title_en'],
		        'table_adress' => $table_adress,
		        'table_descr' => $table_descr,
				'direct' => array_merge($direct1, $direct2),
		        'body' => $bodies
		    ];

		    return $content;
		}

}
