<?php

namespace App\Modules;

use Symfony\Component\DomCrawler\Crawler;
use App\CityUa;

class DomCrawler_copy
{

	public function getCities($parser, $link)
	{
	    // Get html remote text.
	    $html = file_get_contents($link);

	    // Create new instance for parser.
	    $crawler = new Crawler(null, $link);
	    $crawler->addHtmlContent($html, 'UTF-8');

	    // Get title text.
	    $cities = $crawler->filter('#paddContent li a')->extract(array( 'href'));

	    $content = [
	        'cities' => $cities,
	    ];

	    return $content;
	}

	public function getLinkUniversities($parser, $link)
	{
	    // Get html remote text.
	    $html = file_get_contents($link);

	    // Create new instance for parser.
	    $crawler = new Crawler(null, $link);
	    $crawler->addHtmlContent($html, 'UTF-8');

	    // Get title text.
	    $link_univers = $crawler->filter('#paddContent .f_bor a')->extract(array( 'href'));

	    $content = [
	        'link_univers' => $link_univers,
	    ];

	    return $content;
	}

	public function getUniversity($parser, $link)
		{
		    // Get html remote text.
		    $html = file_get_contents($link);

		    // Create new instance for parser.
		    $crawler = new Crawler(null, $link);
		    $crawler->addHtmlContent($html, 'UTF-8');

		    // Get title text.
		    try {
			   $title = $crawler->filter($parser['title'])->text();
			} catch (\InvalidArgumentException $e) {
			    $title = '';
			}

		    // Get table table_adress
		   	try {
			   $table_adress = $crawler->filter($parser['table_adress'])->html();
			} catch (\InvalidArgumentException $e) {
			    $table_adress = '';
			}

		    // Get table table_descr
		    try {
			   $table_descr = $crawler->filter($parser['table_descr'])->html();
			} catch (\InvalidArgumentException $e) {
			    $table_descr = '';
			}

		    // Get image
		    try {
			   $image = $crawler->selectImage($title)->image()->getUri();
			} catch (\InvalidArgumentException $e) {
			    $image = '';
			}
		    
		    // Get direct1
		    try {
			   $direct1 = $crawler->filter($parser['direct1'])->extract(array( '_text'));
			} catch (\InvalidArgumentException $e) {
			    $direct1 = [];
			}
		        
		    // Get direct2
		    try {
			   $direct2 = $crawler->filter($parser['direct2'])->extract(array( '_text'));
			} catch (\InvalidArgumentException $e) {
			    $direct2 = [];
			}

		    // Get table table_adress
		    try {
			   $bodies = $crawler->filter($parser['body'])->html();
			} catch (\InvalidArgumentException $e) {
			    $bodies = '';
			}

		    // Get city
		    try {
			   $city = $crawler->filter('.tbl_info a')->text();
			} catch (\InvalidArgumentException $e) {
			    $city = '';
			}
		    
////фильтруем полученные данные данные body основного текста 
		    $bodies = str_replace(array("\r", "\t", "\n"), '', $bodies);
			$bodies = preg_replace('/.*?(<a name="faculty"><\/a>)/s', '', $bodies);
		    $bodies = preg_replace('/(<a name="gallery").*/', '', $bodies);
		    $bodies = preg_replace('/(<div style="border:1px solid #D6D8DD).*/', '', $bodies);
		    $tags = array("table", "script", "input", "form", "meta", "h1", "i");
	   		$bodies = preg_replace('#<(' . implode( '|', $tags) . ')(?:[^>]+)?>.*?</\1>#s', '', $bodies);
	   		$bodies = preg_replace( '/(<meta|<input|<img).*?>/', "", $bodies );
	   		$bodies = preg_replace('/(<a href="#up).*?<\/a>/', '', $bodies);
	   		$bodies = preg_replace( '/( class| style)=".*?"/', "", $bodies );
	   		$bodies = preg_replace('/(b>)/', 'strong>', $bodies);
	   		
	   		$bodies = preg_replace('|[\s]+|s', ' ', $bodies);
	   		$bodies = preg_replace('/(<div id="menu-inside">|<div id="js-map").*?<\/div>/', '', $bodies);

	   		$bodies = preg_replace('/(<div)\s*>\s*<\/div>/s', '', $bodies);
	   		$bodies = preg_replace('/(<div)\s*>\s*<\/div>/s', '', $bodies);
	   		$bodies = preg_replace('/(<div)\s*>\s*<\/div>/s', '', $bodies);
	   		$bodies = '<a name="faculties"></a>'.$bodies;

//фильтруем полученные данные table_adress таблицы адресса
	   		$table_adress = str_replace(array("\r", "\t"), '', $table_adress);
	   		$table_adress = preg_replace('|[\s]+|s', ' ', $table_adress);
	   		$tags_2 = array("script");
	   		$table_adress = preg_replace('#<(' . implode( '|', $tags_2) . ')(?:[^>]+)?>.*?</\1>#s', '', $table_adress);
	   		$table_adress = preg_replace( '/(style)=".*?"/', "", $table_adress );
	   		$table_adress = preg_replace('/(td1)/', 'adress_label', $table_adress);
	   		$table_adress = preg_replace('/(<tr><td align="right").*?<\/tr>/', '', $table_adress);
	   		//находим id города в своей базе данных
	   		$city_in_my_bd = CityUa::where('title_ru', '=', $city)->first();
	   		if($city_in_my_bd){
	   			$city_in['title_en'] = $city_in_my_bd->title_en;
	   		}else{
	   			$city_in['title_en'] = null;
	   		}
	   		//формируем ссылку на страницу свех университетов этого города.
			$table_adress = preg_replace( '/(href="\/universities\/).*?"/', 'href="/c-ua/universities/'.$city_in['title_en'].'"', $table_adress );
			$table_adress = '<table id="adress_univer">'.$table_adress.'</table>';

//фильтруем полученные данные short_info таблицы дополнит инфо			
	   		$table_descr = str_replace(array("\r", "\t"), '', $table_descr);
	   		$table_descr = preg_replace('|[\s]+|s', ' ', $table_descr);
	   		$table_descr = preg_replace('/("ok")/', '"check"', $table_descr);
	   		$table_descr = preg_replace('/("no")/', '"uncheck"', $table_descr);
	   		$table_descr = preg_replace('/("r")/', '"heavy"', $table_descr);
	   		$table_descr = preg_replace('/( class="bor_no")/', '', $table_descr);
	   		$table_descr = preg_replace('/(width="240" class="tbl_grey_center nf")/', '', $table_descr);
	   		$table_descr = preg_replace('/(<div)\s*>\s*<\/div>/s', '', $table_descr);
	   		$table_descr = preg_replace('/(<tr)\s*>\s*<\/tr>/s', '', $table_descr);
	   		$table_descr = preg_replace('/(<div class="div_grey_top">|<div class="div_grey_bottom").*?<\/div>/', '', $table_descr);


		    $content = [
		        'title' => $title,
		        'image' => $image,
		        'city_en' => $city_in['title_en'],
		        'table_adress' => $table_adress,
		        'table_descr' => $table_descr,
				'direct' => array_merge($direct1, $direct2),
		        'body' => $bodies
		    ];

		    return $content;
		}

}
