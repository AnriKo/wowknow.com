<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class TeacherSubject extends Authenticatable
{
    protected $table = 'teacher_subject';
    public $timestamps = false;
    protected $fillable = [ 'subject_id'];

    public function sub_dir_saved()
    {
        return $this->hasMany('App\TeacherSubjectDirection', 'teachers_id' , 'teach_id');
    }
    public function sub_dir_all()
    {
        return $this->hasMany('App\SubjectDirection', 'subject_id' , 'subject_id');
    }

}
