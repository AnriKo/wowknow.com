<?php 

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model {
    protected $table = 'blog_posts';
    protected $fillable = ['status', 'title', 'slug', 'chapo', 'content', 'published_at', 'category_id'];

    function tag_name() {
        return $this->belongsToMany('App\Tags', 'blog_posts_tags', 'post_id', 'tag_id');
    }

}
