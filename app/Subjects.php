<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subjects extends Model
{
    protected $table = 'subjects';
    public $timestamps = false;
    protected $fillable = ['id'];

    public function subject_direction()
    {
        return $this->hasMany('App\SubjectDirection', 'subject_id', 'id');
    }

    public function try() {
    $instance =$this->hasMany('Video');
    $instance->getQuery()->where('available','=', 1);
    return $instance;
}

    public function teacher_count()
    {
        return $this->hasMany('App\TeacherSubject', 'subject_id', 'id');
    }
}
