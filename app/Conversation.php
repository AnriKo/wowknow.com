<?php

namespace App;
use Auth;
use Illuminate\Database\Eloquent\Model;

class Conversation extends Model
{
	protected $table = 'conversation';
    
    public function sender_info(){
        return $this->hasOne('App\User', 'id', 'sender_id');
    } 	
    
    public function recipient_info(){
        return $this->hasOne('App\User', 'id', 'recipient_id');
    }   

    public function conversation_reply(){
        return $this->hasOne('App\ConversationReply', 'c_id', 'id')->orderBy('created_at', 'desc');
    } 

    public function unread_message_for_one_user(){
        return $this->hasMany('App\ConversationReply', 'c_id', 'id')->where('user_id', '!=', Auth::user()->id)->where('status', '=', '0');
    }

    public function message_attach_to_request(){
        return $this->hasOne('App\ConversationReply', 'c_id', 'id')->where('user_id', '!=', Auth::user()->id);
    }


}
