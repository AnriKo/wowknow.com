<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TeacherPrice extends Model
{
    protected $table = 'teacher_prices';
}
