<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class CompanyCoursCat extends Model
{
    protected $table = 'company_cours_cat';
    public $timestamps = false;
}
