<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class University extends Model
{
    function subjects_name() {
        return $this->belongsToMany('App\UnivSubject', 'university_subjects', 'university_id', 'univ_subject_id');
    }
    
    public function city_ru(){
        return $this->hasOne('App\CityRu', 'title_en', 'city');
    }
    public function city_ua(){
        return $this->hasOne('App\CityUa', 'title_en', 'city');
    }
    public function city_kz(){
        return $this->hasOne('App\CityKz', 'title_en', 'city');
    }
}
