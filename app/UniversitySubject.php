<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UniversitySubject extends Model
{
    public $timestamps = false;
    protected $table = 'university_subjects';
}
