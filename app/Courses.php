<?php

namespace App;
use App;
use Illuminate\Database\Eloquent\Model;

class Courses extends Model
{
    protected $table = 'courses';

    public function city_ru(){
        return $this->hasOne('App\CityRu', 'title_en', 'city');
    }
    public function city_ua(){
        return $this->hasOne('App\CityUa', 'title_en', 'city');
    }
    public function city_kz(){
        return $this->hasOne('App\CityKz', 'title_en', 'city');
    }
    public function city_by(){
        return $this->hasOne('App\CityBy', 'title_en', 'city');
    }
    public function company_langs(){
    	$local = App::getLocale();
        return $this->hasOne('App\CompaniesLang', 'company_id', 'id')->where('lang', '=', $local);
    }
    public function cats(){
        return $this->belongsToMany('App\CompanyCoursCat', 'company_cours_cat_rel', 'course_id', 'cat_id' );
    }
}
