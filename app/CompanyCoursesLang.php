<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyCoursesLang extends Model
{
    protected $table = 'company_cours_langs';
    public $timestamps = false;
}
