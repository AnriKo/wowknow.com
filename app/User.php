<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'type',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function teacher_request(){
        return $this->hasMany('App\Conversation', 'recipient_id', 'id')->orWhere('sender_id','=','sent');
    } 

    public function user_teacher(){
        return $this->hasOne('App\Teachers', 'user_id', 'id');
    } 

}
