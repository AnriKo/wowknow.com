<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CityKz extends Model
{
    protected $table = 'cities_kz';
    public $timestamps = false;
}