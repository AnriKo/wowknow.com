<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    public function review_author()
    {
        return $this->hasOne('App\User', 'id' , 'pupil_id');
    }
}
