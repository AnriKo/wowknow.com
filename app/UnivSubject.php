<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UnivSubject extends Model
{
    public $timestamps = false;
    protected $table = 'univ_subjects';
}
