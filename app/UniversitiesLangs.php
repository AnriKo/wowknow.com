<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UniversitiesLangs extends Model
{
    protected $table = 'universities_langs';
    public $timestamps = false;
}
