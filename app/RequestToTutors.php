<?php

namespace App;

use Auth;
use Illuminate\Database\Eloquent\Model;

class RequestToTutors extends Model
{
    protected $table = 'requests_to_tutors';
    
    public function sender_info(){
        return $this->hasOne('App\User', 'id', 'sender');
    }
    public function teacher_info(){
        return $this->hasOne('App\Teachers', 'user_id', 'sender');
    } 
    public function teacher_id_relative(){
        return $this->hasOne('App\User', 'id', 'teacher_id');
    } 
    public function pupil_id_relative(){
        return $this->hasOne('App\User', 'id', 'pupil_id');
    }  

    public function conversation_id(){
        $recipient_teacher = $this->hasOne('App\Conversation', 'recipient_id', 'pupil_id')->where('sender_id', $this->teacher_id)->get();
        if ($recipient_teacher->count() == 0) {
        	$recipient_teacher = $this->hasOne('App\Conversation', 'sender_id', 'pupil_id')->where('recipient_id', $this->teacher_id);
        }else{
        	$recipient_teacher = $this->hasOne('App\Conversation', 'recipient_id', 'pupil_id')->where('sender_id', $this->teacher_id);
        }
        return $recipient_teacher;
        return $this->hasOne('App\Conversation', 'recipient_id', 'teacher_id')->where('sender_id', $this->pupil_id);
    }  
}
