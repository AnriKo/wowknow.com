<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CityUa extends Model
{
    protected $table = 'cities_ua';
    public $timestamps = false;
}
