<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CityPl extends Model
{
    protected $table = 'cities_pl';
    public $timestamps = false;
}
