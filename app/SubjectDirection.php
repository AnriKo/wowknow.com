<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubjectDirection extends Model
{
    protected $table = 'subject_direction';
    public $timestamps = false;

    public function sub_dir_saved_new()
    {
        return $this->hasMany('App\TeacherSubjectDirection', 'subject_id' , 'subject_id');
    }

}