<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyCoursesCatRel extends Model
{
    protected $table = 'company_cours_cat_rel';
    public $timestamps = false;
}
