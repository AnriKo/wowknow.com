<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class School extends Model
{
    protected $table = 'schools';

    public function city_ru(){
        return $this->hasOne('App\CityRu', 'title_en', 'city');
    }
    public function city_ua(){
        return $this->hasOne('App\CityUa', 'title_en', 'city');
    }
    public function city_kz(){
        return $this->hasOne('App\CityKz', 'title_en', 'city');
    }
    public function city_by(){
        return $this->hasOne('App\CityBy', 'title_en', 'city');
    }
}
