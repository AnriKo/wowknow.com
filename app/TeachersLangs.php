<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TeachersLangs extends Model
{
    protected $table = 'teachers_langs';
    public $timestamps = false;
}
