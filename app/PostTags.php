<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostTags extends Model
{
    protected $table = 'blog_posts_tags';
    public $timestamps = false;
}
