<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyCoursGroup extends Model
{
    protected $table = 'company_cours_group';
    public $timestamps = false;

    public function company_cours_cat()
    {
        return $this->hasMany('App\CompanyCoursCat', 'group_id' , 'id');
    }
}
