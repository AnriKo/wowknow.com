<?php

namespace App\Http\Middleware;

use Closure;

class SetGlobalCookie
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //dump('hello');
    if($request->hasCookie('cod_valute'))
    {
        return $next($request);
    }
    else
    {
        $response = $next($request);
        $ip  = $_SERVER['REMOTE_ADDR'];
        //$ip  = '188.231.136.151';
        $currencyCode  = "USD";
        $ip_data = @json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=".$ip));
        if($ip_data && $ip_data->geoplugin_currencyCode != null) {
            $currencyCode = $ip_data->geoplugin_currencyCode;
        }
        //dd($currencyCode);
        //dd($ip_data);

        //return $currencyCode;
        return $response->cookie('cod_valute', $currencyCode, 60);
    }

    }
}
