<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\User;
use App\Tags;
use App\TeachTags;
use App\TeacherSubject;
use App\LearnTags;
use Auth;
use App\Learn;
use App\Teachers;
use App\Subjects;
use App\PupilTask;
use App\Money\Conversion;
use App\MyHelpers\Helper;
use Image;
use Cookie;
use App;
use App\Review;
use App\Country;


class TutorSearchController extends Controller
{
   
    public function tutor_basic_search(Request $query) {

      $local = App::getLocale();
      $data_teachers = [];
      $data_teachers['teach_city_en'] = 'all-city';
      $data_teachers['teach_city_current'] = 'Выбрать город';
      $data_teachers['sub_dir'] = 0;
      $data_teachers['subject'] = 'subjects';

      $cities_teach = Teachers::groupBy('teach_city_en')->select('teach_city_en', 'country_teach')->get();
      $subjects = Subjects::select('name_'.$local.' AS name', 'alias')->get();

      $teachers = Teachers::
        where('active', '=', '1')
        ->leftJoin('users', 'users.id',  '=', 'teachers.user_id')
        ->leftJoin('teacher_prices', 'teacher_prices.teach_id',  '=', 'teachers.id')
      
      ->select(
        'teachers.id', 
        'teachers.user_id', 
        'teachers.last_name', 
        'teachers.country_teach', 
        'teachers.teach_exp', 
        'teachers.profile_title', 
        'teachers.slug', 
        'teachers.teach_online', 
        'teachers.teach_city_en', 
        'users.name',
        'users.avatar',
        'teacher_prices.price_def AS teacher_price',
        'teacher_prices.cur_def'
      )
      ->orderBy('id', 'desc')
      ->groupBy('teachers.id')
      ->paginate(10);

      return view('front/tutors_search/search_tutors', ['teachers'=>$teachers, 'data_teachers'=>$data_teachers, 'local'=> $local, 'cities_teach'=> $cities_teach, 'subjects'=> $subjects ]);

    }
   
    public function tutor_search_subject_city(Request $query, $subject, $city) {

      $local = App::getLocale();
      $data_teachers = [];
      $data_teachers['teach_city_en'] = 'all-city';
      $data_teachers['teach_city_current'] = 'Выбрать город';
      $data_teachers['sub_dir'] = 0;
      $data_teachers['subject'] = 'subjects';

      $cities_teach = Teachers::groupBy('teach_city_en')->select('teach_city_en', 'country_teach')->get();

      $subjects = Subjects::select('name_'.$local.' AS name', 'alias')->get();

      $teachers = Teachers::
      where('active', '=', '1')
      ->join('teacher_subject as teacher_subject2', DB::raw('teacher_subject2.teach_id'),  '=', DB::raw('teachers.id') )
      ->join('subjects as subjects2', DB::raw('subjects2.id'),  '=', DB::raw('teacher_subject2.subject_id') )
      ->leftJoin('users', 'users.id',  '=', 'teachers.user_id')
      ->leftJoin('teacher_prices', 'teacher_prices.teach_id',  '=', 'teachers.id')
      ->leftJoin('subject_direction_teachers', 'subject_direction_teachers.teachers_id',  '=', 'teachers.id');


      if($subject != 'subjects'){
        $subject = Subjects::where('alias', '=', $subject)->first()->id;
        $teachers = $teachers->where('subjects2.id', '=', $subject);
        $data_teachers['subject'] = DB::table('subjects')
        ->where('id', '=', $subject)
        ->select("name_$local As name", "alias", "id")
        ->first();
      }

//обработка параметров юрл

      if ($city == "online") {
          $teachers = $teachers->where('teachers.teach_online', '=', '1');
          $data_teachers['teach_online'] = "online";
      }

      //пол преподавателя
      if (isset($query->gender)) {
        if ($query->gender == 'm') {
          $teachers = $teachers->where('teachers.gender', '=', 'man' );
        }
        if ($query->gender == 'w') {
          $teachers = $teachers->where('teachers.gender', '=', 'woman' );
        }
      }

      //возраст ученика
      if (isset($query->p_age)) {
          $p_age = preg_replace('/[^0-9]/', '', $query->p_age);
          $teachers = $teachers->whereRaw("teachers.pupil_age REGEXP '[".$p_age."]'" );
      }

      //подкатегории предметов
      if (isset($query->sub_dir)) {
          $sub_dir = preg_replace('/[^0-9]/', '', $query->sub_dir);
          $sub_dir = explode('.', $query->sub_dir);
          $teachers = $teachers->whereIn('subject_direction_teachers.subject_direction_id', $sub_dir );
      }

      if ($city != "all-city" && $city != "online") {
        $teachers = $teachers->where('teachers.teach_city_en', '=', $city);
        $data_teachers['teach_city_en'] = $city;

        $city_ru = DB::table('cities_ru')
          ->where('title_en', '=', $city);

        $city_by = DB::table('cities_by')
          ->where('title_en', '=', $city);

        $city_kz = DB::table('cities_kz')
          ->where('title_en', '=', $city);

        $city_current = DB::table('cities_ua')
          ->where('title_en', '=', $city)
          ->union($city_ru)  
          ->union($city_by)
          ->union($city_kz)
          ->first();

        $data_teachers['teach_city_current'] = $city_current->{"title_$local"};
        
      }

      $teachers = $teachers->select(
        'teachers.id', 
        'teachers.last_name', 
        'teachers.user_id',
        'teachers.country_teach', 
        'teachers.teach_exp', 
        'teachers.profile_title', 
        'users.avatar',
        'teachers.teach_online', 
        'teachers.teach_city_en', 
        'teachers.slug',
        'users.name',
        'teacher_prices.price_def AS teacher_price',
        'teacher_prices.cur_def'
      )
      ->orderBy('id', 'desc')
      ->groupBy('teachers.id')
      ->paginate(10);

      //выбираем дополнительные направления в предмете если есть
      $sub_dir = DB::table('subject_direction')
        ->where('subject_id', '=', $subject)
        ->get();

      if (count($sub_dir)) {
        $data_teachers['sub_dir'] = $sub_dir;

      }

      return view('front/tutors_search/search_tutors', ['teachers'=>$teachers, 'data_teachers'=>$data_teachers, 'local'=> $local , 'cities_teach'=> $cities_teach, 'subjects'=> $subjects ]);

    }   

    public function tutor_page($slug) {

      $local = App::getLocale();
      $video_link = '';

      $teacher = Teachers::where('teachers.slug', '=', $slug)
      ->leftJoin('users', 'users.id',  '=', 'teachers.user_id')
      ->leftJoin('teacher_prices', 'teacher_prices.teach_id',  '=', 'teachers.id');

      $teacher = $teacher->select(
        'teachers.*',
        'users.name',
        'users.avatar',
        'teacher_prices.price_def AS teacher_price',
        'teacher_prices.cur_def'
      );

      $teacher = $teacher->first();

      if($local != "ru"){
        $teacher_langs = $teacher->teacher_langs;
        if($teacher_langs){
          if($teacher_langs->name){
            $teacher->name = $teacher_langs->name;
          }
          if($teacher_langs->last_name){
            $teacher->last_name = $teacher_langs->last_name;
          }
          if($teacher_langs->profile_title){
            $teacher->profile_title = $teacher_langs->profile_title;
          }
          if($teacher_langs->tutor_edu_text){
            $teacher->tutor_edu_text = $teacher_langs->tutor_edu_text;
          }
          if($teacher_langs->tutor_hobie_text){
            $teacher->tutor_hobie_text = $teacher_langs->tutor_hobie_text;
          }
          
        }
      }

      $teach_city = '';

      if($teacher->teach_city_en){
        if($teacher->country_teach == 'ua'){
          $teach_city = DB::table('cities_ua')
          ->where('title_en', '=', $teacher->teach_city_en)
          ->select("title_$local As name", "title_en", "id")
          ->first();
        }else{
          $teach_city = DB::table('cities_'.$teacher->country_teach)
          ->where('title_en', '=', $teacher->teach_city_en)
          ->select("title_ru As name", "title_en", "id")
          ->first();
        }

      }

      //video link
      if ($teacher->video_link != '') {
        $video_link = unserialize($teacher->video_link);
        $video_link = $video_link['video_id'];
      }

      //отзывы
      $reviews = Review::where("item_id", '=', $teacher->id)
      ->where('type', "=", 'tutor')
      ->orderBy('created_at', "DESC")
      ->get();

      $params = [
          'teacher' => $teacher,
          'teach_city' => $teach_city,
          'video_link' => $video_link,
          'local' => $local,
          'reviews' => $reviews,
      ];

      // dd($teacher->subjects);

      return view('front/tutors_search/tutor_page')->with($params);

    }

      public function show_contact_dates(Request $query) {

      $teacher_info = DB::table('teachers')
      ->leftJoin('users', 'users.id',  '=', 'teachers.user_id')
      ->where('teachers.id', '=', $query->query('id'))
      ->select('teachers.phone', 'teachers.skype_login', 'users.email')
      ->first();

      $datas = '';

      if ($teacher_info->phone != '') {
        $datas .= '<p><i class="fa fa-phone" aria-hidden="true"></i>Телефон - '.$teacher_info->phone.'</p>';
      }
      if ($teacher_info->email != '') {
        $datas .= '<p><i class="fa fa-envelope-o" aria-hidden="true"></i>Пошта - '.$teacher_info->email.'</p>';
      }
      if ($teacher_info->skype_login != '') {
        $datas .= '<p><i class="fa fa-skype" aria-hidden="true"></i>Skype - '.$teacher_info->skype_login.'</p>';
      }

      $datas = html_entity_decode($datas);

      return $datas;
    }

}
