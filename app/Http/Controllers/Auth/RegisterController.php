<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
//use App\MyHelpers\ReCaptcha;
use App\Mail\CongratulateNewUser;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';
    protected $name = '';
    protected $email = '';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6',
            'g-recaptcha-response' => 'required|captcha'
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        if($data['next_page'] != ''){
            $this->redirectTo = $data['next_page'];
        }else{
            $this->redirectTo = "/";
        }
        $this->name = $data['name'];
        $this->email = $data['email'];

        if(stristr($data['next_page'], 'tutor-dasboard') === 'tutor-dasboard'){
            return User::create([
                'name' => $data['name'],
                'email' => $data['email'],
                'password' => bcrypt($data['password']),
                'type' => 'user_teach',
            ]); 
        }else{
            return User::create([
                'name' => $data['name'],
                'email' => $data['email'],
                'password' => bcrypt($data['password']),
                'type' => 'user',
            ]); 
        }

    }

    protected function redirectTo()
    {
        
        if($this->redirectTo != "home"){
            if(stristr($this->redirectTo, 'tutor-dasboard') === 'tutor-dasboard'){
                \Mail::to($this->email)->send(new CongratulateNewUser($this->name) );
                session()->flash('success', 'Вы успешно зарегистрировались. Для активации вашего профиля репетитора, заполните все разделы из меню "Редактирование профиля, начните из этой страницы, базовой информации о вас"');
                return url('tutor-dasboard');
            }
            if(stristr($this->redirectTo, 'create-new-task') === 'create-new-task'){
                session()->flash('success', 'Вы успешно зарегистрировались. Для создания обьявления о поиске учителя заполните необходимую информацию и сохраните обьявления. Желаем хороших занятий! )');
                return url($this->redirectTo);
            }
            session()->flash('success', 'Ви успішно зареєструвалися. Тепер ви можете переглядати контактні данні репетиторів.');
            return url($this->redirectTo);
        }else{
            session()->flash('success', 'Вы успешно зарегистрировались. Теперь вы можете создать свой профиль учителя либо оставить обьявление о поиске учителя');
            return url()->previous();
        }
        return url()->previous();
    }
}
