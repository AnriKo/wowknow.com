<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\University;
use App\UnivSubject;
use App\CityUa;
use App\CityPl;
use App\CityKz;
use App\CityRu;
use App\CityBy;
use App\Review;
use DB;
use App;

class UniverController extends Controller
{
    public function lookCities(){
        $local = App::getLocale();
    	$cities = University::where('country', '=', 'ua')
            ->leftJoin('cities_ua', 'cities_ua.title_en',  '=', 'universities.city')
    		->groupBy('universities.city')
    		->orderBy('cities_ua.title_ru', 'asc')
    		->select("universities.id", "cities_ua.title_ru", "cities_ua.title_en", "cities_ua.title_ua", 'universities.city', DB::raw('count(*) as total'))
    		->get();
    	return view('front/univers/cities', ['cities'=>$cities, 'local' => $local]);
    }

    public function searchCitySubject($city, $subject) {

        $local = App::getLocale();
        $city_alias = $city;
        $current_city =  '';
        $current_subject =  '';
        $meta = [];
        $current_url = url()->current();

        if($city != 'all'){
            if(stristr($current_url, 'c-ua')){
                $current_city = CityUa::where('title_en', '=', $city)->first();
                $current_city = $current_city->{'title_'.$local};
            }elseif(stristr($current_url, 'c-ru')){
                $current_city = CityRu::where('title_en', '=', $city)->first();
                $current_city = $current_city->{'title_'.$local};
            }elseif(stristr($current_url, 'c-kz')){
                $current_city = CityKz::where('title_en', '=', $city)->first();
                $current_city = $current_city->{'title_'.$local};
            }elseif(stristr($current_url, 'c-by')){
                $current_city = CityBy::where('title_en', '=', $city)->first();
                $current_city = $current_city->{'title_'.$local};
            }
        }

        if($subject != 'all'){
            $current_subject = UnivSubject::where('slug', '=', $subject)->first();
            $current_subject = $current_subject->{'name_'.$local};
        }

        if($city != 'all' && $subject == 'all'){
            $univers = University::where('city', '=', $city)
            ->join('cities_ua', DB::raw('universities.city'),  '=', DB::raw('cities_ua.title_en') )
            ->select('universities.slug', 'universities.logo', 'universities.country', 'universities.title', 'universities.type', 'universities.id', 'cities_ua.title_'.$local.' as city');

            //metadata
            if($local == 'ua'){
                $meta['meta_title'] = "Пошук ВНЗ, університетів, інститутів в місті " . $current_city;
                $meta['meta_title_og'] = "Пошук ВНЗ, університетів, інститутів в місті " . $current_city;
                $meta['page_title'] = "ВНЗ: університети, інститути в місті " . $current_city;
                $meta['meta_keywords'] = "ВНЗ " . $current_city .", університети, інститути, академії, навчання в місті ". $current_city;
                $meta['meta_description'] = "На цій сторінці ви можете знайти всі вищі навчальні заклади: університети, інститути які розташовані в місті " . $current_city. ". ". "На кожній сторінці закладу ви можете знайти спеціальності навчання, адресу, телефон, прочитати докладну інформацію, подивитися та написати власні відгуки";
            }elseif($local == 'en'){
                $meta['meta_title'] = "Search for universities, universities institutes in the city " . $current_city;
                $meta['meta_title_og'] = "Search for universities, universities institutes in the city " . $current_city;
                $meta['page_title'] = "Universities, institutes in the cityі " . $current_city;
                $meta['meta_keywords'] = "Higher educational institutions ". $current_city.", universities, institutes, academies, education in the ".$current_city." city";
                $meta['meta_description'] = "On this page you can find all higher education institutions: universities, institutes that are located in the city ".$current_city.". On each page of the institution you can find education specialties, address, phone, read detailed information, see and write your own testimonials";
            }else{
                $meta['meta_title'] = "Поиск вузов, университетов институтов в городе " . $current_city;
                $meta['meta_title_og'] = "Поиск вузов, университетов институтов в городе " . $current_city;
                $meta['page_title'] = "Вузы, университеты, институты в городе " . $current_city;
                $meta['meta_keywords'] = "ВУЗЫ ". $current_city.", университеты, институты, академии, обучение в городе ". $current_city;
                $meta['meta_description'] = "На этой странице вы можете найти все высшие учебные заведения: университеты, институты которые находятся в городе ". $current_city. ". "."На каждой странице заведения можно найти специальности обучения, адрес, телефон, прочитать подробную информацию, посмотреть и написать собственные отзывы";
            }

        }elseif($city == 'all' && $subject != 'all'){

            $univers = University::
              leftJoin('university_subjects as teacher_subject2', DB::raw('teacher_subject2.university_id'),  '=', DB::raw('universities.id') )
              ->leftJoin('univ_subjects as subjects2', DB::raw('subjects2.id'),  '=', DB::raw('teacher_subject2.univ_subject_id') )
              ->leftJoin('cities_ua', DB::raw('universities.city'),  '=', DB::raw('cities_ua.title_en') )
              ->where('subjects2.slug', '=', $subject)
              ->select('universities.slug', 'universities.logo', 'universities.country', 'universities.title', 'universities.type', 'universities.id', 'cities_ua.title_'.$local.' as city');

            //metadata
            if($local == 'ua'){
                $meta['meta_title'] = "Пошук ВНЗ, університетів, інститутів по спеціальності, напрямку " . $current_subject;
                $meta['meta_title_og'] = "Пошук ВНЗ, університетів, інститутів по спеціальності " . $current_subject;
                $meta['page_title'] = $current_subject. ", навчальні заклади в Україні";
                $meta['meta_keywords'] = "ВНЗ " . $current_subject .", університети, предмети, інститути, навчання зі спеціальності ". $current_subject;
                $meta['meta_description'] = "На цій сторінці ви можете знайти всі вищі навчальні заклади: університети, інститути де проходить навчання за спеціальністю, напрямком" . $current_subject. ". ";
            }elseif($local == 'en'){
                $meta['meta_title'] = "Search for universities, universities institutes in the city " . $current_subject;
                $meta['meta_title_og'] = "Search for universities, universities institutes in the city " . $current_subject;
                $meta['page_title'] = $current_subject. ", higher education in Ukraine";
                $meta['meta_keywords'] = "Higher educational institutions ". $current_subject.", universities, institutes, academies, education in the ".$current_subject." city";
                $meta['meta_description'] = "On this page you will find higher educational institutions: universities, institutes where you study in a specialty, direction ".$current_subject;
            }else{
                $meta['meta_title'] = "Специальность " . $current_subject . ", поиск учебных заведений в Украине по данному направлению";
                $meta['meta_title_og'] = "Специальность " . $current_subject . ", поиск учебных заведений в Украине по данному направлению";
                $meta['page_title'] =  $current_subject. ", учебные заведения в Украине";
                $meta['meta_keywords'] = "ВУЗЫ ". $current_subject.", университеты, институты, обучение по специальности ". $current_subject;
                $meta['meta_description'] = "На этой странице вы найдете высшие учебные заведения: университеты, институты в Украине, где проходит обучение по специальности, направлению ". $current_subject;
            }

        }elseif($city != 'all' && $subject != 'all'){

            $univers = University::
              join('university_subjects as teacher_subject2', DB::raw('teacher_subject2.university_id'),  '=', DB::raw('universities.id') )
              ->join('univ_subjects as subjects2', DB::raw('subjects2.id'),  '=', DB::raw('teacher_subject2.univ_subject_id') )
              ->join('cities_ua', DB::raw('universities.city'),  '=', DB::raw('cities_ua.title_en') )
              ->where('subjects2.slug', '=', $subject)
              ->where('universities.city', '=', $city)
              ->select('universities.slug', 'universities.logo', 'universities.country', 'universities.title', 'universities.type', 'universities.id', 'cities_ua.title_'.$local.' as city');

            //metadata
            if($local == 'ua'){
                $meta['meta_title'] = $current_subject . " пошук ВНЗ, університетів, інститутів в місті ".$current_city." зі спеціальності ";
                $meta['meta_title_og'] = $current_subject . " пошук ВНЗ, університетів, інститутів в місті ".$current_city." зі спеціальності ";
                $meta['page_title'] = $current_subject. ", навчання в місті ". $current_city;
                $meta['meta_keywords'] = "ВНЗ, " . $current_subject .", університети, інститути, навчання в місті ". $current_city;
                $meta['meta_description'] = "На цій сторінці ви знайдете вищі навчальні заклади: університети, інститути в місті ".$current_city." де проходить навчання за, напрямком" . $current_subject. ". ";
            }elseif($local == 'en'){
                $meta['meta_title'] = $current_subject. " search for universities, universities institutions in the city " . $current_city;
                $meta['meta_title_og'] = $current_subject. " search for universities, universities institutions in the city " . $current_city;
                $meta['page_title'] = $current_subject. ", education in " . $current_city;
                $meta['meta_keywords'] = "Higher educational ". $current_subject.", universities, institutes, academies, education from the ".$current_subject." ";
                $meta['meta_description'] = "On this page you will find higher educational institutions: universities, institutes where you study in a specialty, direction ".$current_subject;
            }else{
                $meta['meta_title'] = $current_subject. ", поиск Вузов, университетов институтов в городе ".$current_city." по направлению ";
                $meta['meta_title_og'] = $current_subject. ", поиск Вузов, университетов институтов в городе ".$current_city." по направлению ";
                $meta['page_title'] =  $current_subject. ", обучение в городе ". $current_city;
                $meta['meta_keywords'] = "ВУЗЫ ". $current_subject.", университеты, институты, обучение в городе ".$current_city;
                $meta['meta_description'] = "На этой странице вы найдете высшие учебные заведения: университеты, институты в городе ".$current_city." где проходит обучение по специальности, направлению ". $current_subject;
            }

        }

        if($local == 'ua'){
            $univers = $univers->leftJoin('universities_langs', 'universities_langs.universities_id', '=', 'universities.id');
            $univers = $univers->select('universities_langs.title As title', 'universities.slug', 'universities.logo', 'universities.type', 'universities.id', 'universities.country', 'cities_ua.title_'.$local.' as city');
        }

        $univers = $univers->paginate(10);

        $city_loc = 'title_'.$local;
        return view('front/univers/search_city_subject', ['univers'=>$univers, 'meta'=> $meta, 'city_alias' => $city_alias, 'local' => $local, 'city_loc' => $city_loc, 'subject'=> $subject]);
    }

    public function showUniversFromSubject($subject) {
        $local = App::getLocale();
        $subject = UnivSubject::where('slug', '=', $subject)->first();

        $current_country =  '';

        $current_url = url()->current();

        if(stristr($current_url, 'c-ua')){
            $current_country = 'ua';
        }

    	$univers = University::
          join('university_subjects as teacher_subject2', DB::raw('teacher_subject2.university_id'),  '=', DB::raw('universities.id') )
          ->join('univ_subjects as subjects2', DB::raw('subjects2.id'),  '=', DB::raw('teacher_subject2.univ_subject_id') )
          ->join('cities_ua', DB::raw('universities.city'),  '=', DB::raw('cities_ua.title_en') )
          ->where('subjects2.slug', '=', $subject->slug);

        //dd($univers);

        if($local == 'ua'){
            $univers = $univers->leftJoin('universities_langs', 'universities_langs.universities_id', '=', 'universities.id');
            $univers = $univers->select('universities_langs.title As title', 'universities.slug', 'universities.logo', 'universities.type', 'universities.id', 'cities_ua.title_'.$local.' as city');
        }else{
            $univers = $univers->select('universities.slug', 'universities.logo', 'universities.title', 'universities.type', 'universities.id', 'cities_ua.title_'.$local.' as city');
        }

        $univers = $univers->orderBy('universities.id', 'desc');
        $univers = $univers->paginate(10);

        $current_subject = $subject->{'name_'.$local};

        //dump($subject);

    	return view('front/univers/univers_in_subject', ['univers'=>$univers, 'current_subject'=> $current_subject, 'local' => $local]);
    }

    public function univerPage($slug){

        $local = App::getLocale();
        $meta = [];
        $univer = University::where( 'slug', '=', $slug);

        if($local == 'ua'){
            $univer = $univer->leftJoin('universities_langs', 'universities_langs.universities_id', '=', 'universities.id');
            $univer = $univer->select('universities_langs.title As title', 'universities_langs.adress_info As adress_info', 'universities_langs.short_info As short_info', 'universities_langs.description As description', 'universities.slug', 'universities.logo', 'universities.type', 'universities.id', 'universities.city', 'universities.country');
        }

        $univer = $univer->first();
        $current_city =  '';
        $current_url = url()->current();

        if ($univer->country == 'ua') {
            $current_city = CityUa::where('title_en', '=', $univer->city)->first();
        }elseif($univer->country == 'pl'){
            $current_city = CityPl::where('title_en', '=', $univer->city)->first();
        }
        
        $current_city = $current_city->{'title_'.$local};

        //отзывы
        $reviews = Review::where("item_id", '=', $univer->id)
        ->where('type', "=", 'univer')
        ->orderBy('created_at', "DESC")
        ->get();

        //metadata
        if($local == 'ua'){
            $meta['meta_title'] = $univer->title . " | опис, відгуки, фото, спеціальності";
            $meta['meta_title_og'] = $univer->title . " | опис, відгуки, фото, спеціальності";
            $meta['meta_keywords'] = "ВНЗ, університет, предмет, навчання, ". $current_city;
            $meta['meta_description'] = "Інформація про " . $univer->title. ". Опис навчального закладу, відгуки, фото, спеціальності, тут ви можете залишити свій відгук про цей навчальний заклад";
        }elseif($local == 'en'){
            $meta['meta_title'] = "Search for universities, universities institutes in the city " . $current_subject;
            $meta['meta_title_og'] = "Search for universities, universities institutes in the city " . $current_subject;
            $meta['page_title'] = $current_subject. ", higher education in Ukraine";
            $meta['meta_keywords'] = "Higher educational institutions ". $current_subject.", universities, institutes, academies, education in the ".$current_subject." city";
            $meta['meta_description'] = "On this page you will find higher educational institutions: universities, institutes where you study in a specialty, direction ".$current_subject;
        }else{
            $meta['meta_title'] = $univer->title . " | контакты, описание, фото, отзывы | wowknow.com";
            $meta['meta_title_og'] = $univer->title . " | контакты, описание, фото, отзывы | wowknow.com";
            $meta['meta_keywords'] = "Вуз, университет, предмет, специальности, учеба, " . $current_city;
            $meta['meta_description'] = $univer->title. ". Описание учебного заведения, информация, отзывы, фото, специальности, контакты, здесь вы можете оставить свой отзыв об этом учебном заведении";
        }


        return view('front/univers/univer_page', ['univer'=>$univer, 'current_city'=> $current_city, 'local' => $local, 'meta' => $meta, 'reviews' => $reviews]);

    }

}
