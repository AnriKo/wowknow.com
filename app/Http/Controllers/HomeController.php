<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;
use App\Post;
use App\Subjects;
use App\Mail\ContactMessage;
//use App\MyHelpers\ReCaptcha;

class HomeController extends Controller
{

    public function index()
    {
        $local = App::getLocale();

	   $posts = Post::where('post_status', '=', 'published')
        ->where('type', '=', '1')
        ->where('lang', '=', $local)
        ->orderBy('id', 'desc')
        ->limit(6)
    	->select(
	        'blog_posts.id', 
	        'blog_posts.title',
            'blog_posts.sm_image',
	        'blog_posts.image',
	        'blog_posts.excerpt',
	        'blog_posts.slug'
	    );

		$posts = $posts->get();

        $subjects = Subjects::select('name_'.$local.' AS name', 'alias')->get();

    	$params = [
            'posts' => $posts,
            'local' => $local,
    		'subjects' => $subjects,
    	];

        return view('front/common/home')->with($params);
    }

    public function contacts(){
        return view('front/common/contacts');
    }

    public function send_message(Request $request){

        $this->validate($request, [
            'g-recaptcha-response' => 'required|captcha'
        ]);

        $user_name = $request->message_name;
        $user_email = $request->message_email;
        $user_message = $request->message_body;

        if(
            strpos($user_message, 'href') !== false
            || strpos($user_message, '<a') !== false
            || strpos($user_message, 'http') !== false
        ){
            session()->flash('success', '<span style="color: red;">Не відправлено!</span> В вашому листі є посилання, листи з ссилками не відправляються!');
            return redirect(route('contacts'));
        }else{
            $to = explode(',', env('ADMIN_EMAILS'));
            \Mail::to($to)->send(new ContactMessage($user_name, $user_email, $user_message));

            session()->flash('success', 'Ваш лист успішно відправлено. Дякуємо!');
            return redirect(route('contacts'));
        }



    }
}
