<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Courses;
use App\Review;
use App\CompanyCoursGroup;
use App\CompanyCoursCat;
use App\CompanyCoursesCatRel;
use App;
use DB;
use URL;

class CourseSearchController extends Controller
{
    public function courses_basic_search($subject, $city){

		$local = App::getLocale();
		$data_courses['subject'] = '';
		$data_courses['city'] = '';
		$cities_course = [];
		$data_courses['title'] = 'Образовательные курсы в Украине';
		$cities_course = Courses::groupBy('city')->select('city', 'country')->get();

      $courses = Courses::
        where('active', '=', '1')
        ->orderBy('updated_at', 'desc')
	    ->select(
	    	'courses.id',
        'courses.top',
	        'courses.alias', 
	        'name', 
	        'logo',
	        'city',
	        'street',
	        'description',
	        'country'
	      );
	    if($city != 'city'){
	    	$courses = $courses->where('city', '=', $city);
	    	$data_courses['city'] = $city;

		    $city_ru = DB::table('cities_ru')
	          ->where('title_en', '=', $city);

	        $city_by = DB::table('cities_by')
	          ->where('title_en', '=', $city);

	        $city_kz = DB::table('cities_kz')
	          ->where('title_en', '=', $city);

	        $city_current = DB::table('cities_ua')
	          ->where('title_en', '=', $city)
	          ->union($city_ru)  
	          ->union($city_by)
	          ->union($city_kz)
	          ->first();

	        $data_courses['title'] = 'Образовательные курсы в городе - ' . $city_current->title_ru;
	        $data_courses['city'] = $city;

	    }
	    if($subject != 'subject'){
	    	$courses->join('company_cours_cat_rel as company_cours_cat_rel2', DB::raw('company_cours_cat_rel2.course_id'),  '=', DB::raw('courses.id') )
      				->join('company_cours_cat as company_cours_cat2', DB::raw('company_cours_cat2.id'),  '=', DB::raw('company_cours_cat_rel2.cat_id') )
      				->where('company_cours_cat2.alias', '=', $subject);
      		$data_courses['subject'] = $subject;	
      		//dd($subject);
      		$subject_name = CompanyCoursCat::where('alias', '=', $subject)->first();
      		$data_courses['title'] = $subject_name->name_ru. ' в Украине';	
      		if($city != 'city'){
      			$data_courses['title'] = $subject_name->name_ru. ' в городе '. $city_current->title_ru;	
      		}
	    }

        $courses = $courses->paginate(10);
        $courses_group = CompanyCoursGroup::all();


       return view('front/courses/index', ['courses'=>$courses, 'local' => $local, 'courses_group' => $courses_group, 'data_courses' => $data_courses, 'cities_course' => $cities_course ]);

    }

    public function cours_one_show($alias) {

    	$local = App::getLocale();
    	$title_loc = "title_".$local;
    	$course_info = [];
    	
    	$cours = Courses::where('alias', '=', $alias)->first();
        if($cours->country == 'ua'){
        	$course_info['city'] = $cours->city_ua->title_ru;
        }elseif($cours->country == 'ru'){
        	$course_info['city'] = $cours->city_ru->title_ru;
        }elseif($cours->country == 'kz'){
        	$course_info['city'] = $cours->city_kz->title_ru;
        }elseif($cours->country == 'by'){
        	$course_info['city'] = $cours->city_by->title_ru;
        }
        if($cours->gallery){
        	$course_info['gallery'] = unserialize($cours->gallery);
        }else{
        	$course_info['gallery'] = null;
        }

        //отзывы
        $reviews = Review::where("item_id", '=', $cours->id)
        ->where('type', "=", 'course')
        ->orderBy('created_at', "DESC")
        ->get();

		return view('front/courses/show_one', ['cours'=>$cours,'reviews'=>$reviews, 'local' => $local, 'course_info' => $course_info]);

    }

    public function delete($id) {

    	$cours = CompanyCourses::find($id);
    	$name = $cours->course_name;
    	$cours->delete();
    	$coursCatRel = CompanyCoursesCatRel::where('course_id', '=', $id)->delete();
    	$coursLangs = CompanyCoursesLang::where('course_id', '=', $id)->delete();

    	session()->flash('success', 'Все данные о курсе "'. $name .'" успешно удалены');
        return redirect()->back();

    }

    public function top_courses_block(Request $query){

      $courses = Courses::
        where('top', '=', '1')
        ->orderBy('updated_at', 'desc')
        ->select(
          'courses.id',
            'courses.alias', 
            'name', 
            'logo',
            'city',
            'street',
            'description',
            'country'
          )
        ->take(3)
        ->inRandomOrder()
        ->get();

      if($courses){
        $result = '';
        foreach ($courses as $cours) {
          $result .= '
          <div class="row one_row one_courses">
                    <div class="col-lg-7 col-md-7 col-sm-6 col-xs-12 no_padding">
                        <div class="name">
                            <a href="' . route('cours_one_show', ['alias' => $cours->alias ]) . '">
                                ' . $cours->name . '
                            </a>
                        </div>
                        <div class="cours_location"><i class="fa fa-map-marker" aria-hidden="true"></i>    
                          <span class="city">
                            ' . $cours->city_ua->title_ru . '
                          </span>    

                          <span class="street">
                              - ( ' . $cours->street . ')
                          </span><br>

                        </div>
                        <div class="profile_info">

                            <i style="color: orange" class="fa fa-tag" aria-hidden="true"></i>
                            <div class="teach_subjects"> ' ;

      
                                foreach($cours->cats as $cat){
                                    $result .=  '<span class="one_subject"> ' . $cat->name_ru . ' </span>' ;
                                }
                            $result .='    
                            </div>
                        </div> 
                    </div> 
                    <div class="col-lg-5 col-md-5 col-sm-6 col-xs-12 no_padding company_info"> 

                        <p>


                                <a href="' . route('cours_one_show', ['alias' => $cours->alias ]) .'">
                                    <img class="company_avatar" src="' . URL::asset('storage/companies/logos') .'/'. $cours->logo .'" alt="avatar_tutor_'. $cours->name .'">
                                </a>     

                            '. mb_strimwidth(strip_tags($cours->description), 0, 150, "...") .' 
                               
                        </p>
                    </div>
        </div>';
        }
        return $result;
      }else{
        return '0';
      }

      

    }
}
