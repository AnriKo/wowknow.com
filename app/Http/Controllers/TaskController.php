<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PupilTask;
use App\Http\Requests;
use Auth;
use App\MyHelpers\Helper;
use Cookie;
use DB;

class TaskController extends Controller
{
    //создание заявки на обучение
    public function create_new_task_get() {
        return view('front/task_views/create_new_task');
    }

    //сохранение заявки на обучение
    public function save_new_task(Request $query) {

        $this->validate($query, [
            'subject_id' => 'required',
            'task_text' => 'required',
            'teach_place' => 'required',
            'price_value' => 'required',
            'name' => 'required',
            'pupil_name' => 'required',
            'email' => 'required'
        ]);

        $task = new PupilTask;
        $task->user_id = Auth::user()->id;
        $task->name = $query->name;
        $task->pupil_name = $query->pupil_name;
        $task->text = $query->task_text;
        $task->subject_id = $query->subject_id;
        $task->skype_login = $query->skype_login;
        $task->country_id = $query->country_teach;
        $task->teach_city_en = $query->teach_city_en;
        $task->price_def = $query->price_value;
        $task->cur_def = $query->price_valute;
        $task->email = $query->email;
        $task->phone = $query->phone;
        $task->gender = $query->gender;
        $task->pupil_age = $query->pupil_age;

        //обрабатываем слаг
        $slug = mb_strimwidth(Helper::ru2lat($query->task_text), 0, 40);
        $check_slug = PupilTask::where( 'slug', 'like', $slug.'%')->get();
        if ((int) $check_slug->count() > 0) {
        	$slug = $slug.'_'.((int) $check_slug->count() + 1);
        }
        $task->slug = $slug;

        //сохраняем место проведения уроков
        if(isset($query->teach_place['online'])){
            $task->teach_online = '1';
        }else{
            $task->teach_online = '0';
        }
        if(isset($query->teach_place['local'])){
            $task->teach_local = '1';
        }else{
            $task->teach_local = '0';
        }
        if(isset($query->teach_local_pupil)){
            $task->teach_local_pupil = '1';
        }else{
            $task->teach_local_pupil = '0';
        }
        if(isset($query->teach_local_teacher)){
            $task->teach_local_teacher = '1';
        }else{
            $task->teach_local_teacher = '0';
        }

        //конвертируем сохраняем валюты разные
        if (isset($query->price_value) && isset($query->price_valute)) {
	        if ($query->price_valute == 'UAH') {
	            $task->price_UAH = $query->price_value;
	            $task->price_RUB = Helper::convert_cur($query->price_value, $query->price_valute, 'RUB'); 
	            $task->price_USD = Helper::convert_cur($query->price_value, $query->price_valute, 'USD');
	            $task->price_KZT = Helper::convert_cur($query->price_value, $query->price_valute, 'KZT');
	            $task->price_BYN = Helper::convert_cur($query->price_value, $query->price_valute, 'BYN');
	        }elseif ($query->price_valute == 'RUB') {
	            $task->price_RUB = $query->price_value;
	            $task->price_UAH = Helper::convert_cur($query->price_value, $query->price_valute, 'UAH'); 
	            $task->price_USD = Helper::convert_cur($query->price_value, $query->price_valute, 'USD');
	            $task->price_KZT = Helper::convert_cur($query->price_value, $query->price_valute, 'KZT');
	            $task->price_BYN = Helper::convert_cur($query->price_value, $query->price_valute, 'BYN');
	        }elseif ($query->price_valute == 'USD') {
	            $task->price_USD = $query->price_value;
	            $task->price_UAH = Helper::convert_cur($query->price_value, $query->price_valute, 'UAH'); 
	            $task->price_RUB = Helper::convert_cur($query->price_value, $query->price_valute, 'RUB');
	            $task->price_KZT = Helper::convert_cur($query->price_value, $query->price_valute, 'KZT');
	            $task->price_BYN = Helper::convert_cur($query->price_value, $query->price_valute, 'BYN');
	        }elseif ($query->price_valute == 'KZT') {
	            $task->price_KZT = $query->price_value;
	            $task->price_UAH = Helper::convert_cur($query->price_value, $query->price_valute, 'UAH'); 
	            $task->price_RUB = Helper::convert_cur($query->price_value, $query->price_valute, 'RUB');
	            $task->price_USD = Helper::convert_cur($query->price_value, $query->price_valute, 'USD');
	            $task->price_BYN = Helper::convert_cur($query->price_value, $query->price_valute, 'BYN');
	        }elseif ($query->price_valute == 'BYN') {
	            $task->price_BYN = $query->price_value;
	            $task->price_UAH = Helper::convert_cur($query->price_value, $query->price_valute, 'UAH'); 
	            $task->price_RUB = Helper::convert_cur($query->price_value, $query->price_valute, 'RUB');
	            $task->price_USD = Helper::convert_cur($query->price_value, $query->price_valute, 'USD');
	            $task->price_KZT = Helper::convert_cur($query->price_value, $query->price_valute, 'KZT');
	        }
	     }

        $task->save();
        return redirect()->back()->with('success_long_time', 'Обьявление сохранено и доступно учителям на сайте. Изменить свое объявление вы можете в меню слева в разделе "Мои обьявления".');
    }

    //поиск заявок учеников
    public function task_search_subject_city(Request $query, $subject, $city) {

      //определение валюты на сайте
      $cookie_cod_valute = Cookie::get('cod_valute');
      if (!$cookie_cod_valute) {
        $cookie_cod_valute = 'UAH';
      }
      $valut_conv = Helper::value_cur($cookie_cod_valute);

      $data_tasks = [];
      $data_tasks['task_city_en'] = 'all-city';
      $data_tasks['teach_city_current'] = 'занятия по Skype (online)';
      $data_tasks['task_city_current'] = 'Выберите свой город';

      $tasks = DB::table('pupil_task')
      ->where('active', '=', '1')
      ->leftJoin('subjects', 'subjects.id',  '=', 'pupil_task.subject_id');

// поиск по предмету
      if ($subject != 'subject') {
      	$tasks = $tasks->where('pupil_task.subject_id', '=', $subject );
      	$data_tasks['subject'] = DB::table('subjects')
	      ->where('id', '=', $subject)
	      ->first();
      }

// поиск по городу
      if ($city != 'all-city') {

      	$tasks = $tasks->where('pupil_task.teach_city_en', '=', $city);
        $data_tasks['task_city_en'] = $city;

        $city_ru = DB::table('cities_ru')
          ->where('title_en', '=', $city);

        $city_by = DB::table('cities_by')
          ->where('title_en', '=', $city);

        $city_kz = DB::table('cities_kz')
          ->where('title_en', '=', $city);

        $city_current = DB::table('cities_ua')
          ->where('title_en', '=', $city)
          ->union($city_ru)  
          ->union($city_by)
          ->union($city_kz)
          ->first();

        $data_tasks['task_city_current'] = $city_current->title_ru;
      }

//обработка параметров юрл
      // тип уроков
      if (isset($query->type_lessons)) {
        if ($query->type_lessons == 'online') {
          $tasks = $tasks->where('pupil_task.teach_online', '=', '1');
          $data_tasks['teach_online'] = "online";
        }
      }

      //пол преподавателя
      if (isset($query->gender)) {
        if ($query->gender == 'm') {
          $tasks = $tasks->where('pupil_task.gender', '=', 'man' );
        }
        if ($query->gender == 'w') {
          $tasks = $tasks->where('pupil_task.gender', '=', 'woman' );
        }
      }

      //цена урока
      if (isset($query->price_min) && isset($query->price_max) ) {
          $tasks = $tasks->whereBetween('pupil_task.price_'.$cookie_cod_valute, [$query->price_min, $query->price_max] );
      }

      //возраст ученика
      if (isset($query->p_age)) {
          $p_age = preg_replace('/[^0-9]/', '', $query->p_age);
          $tasks = $tasks->whereRaw("pupil_task.pupil_age REGEXP '[".$p_age."]'" );
      }
    
      $tasks = $tasks->select(
        'pupil_task.id', 
        'pupil_task.name', 
        'pupil_task.user_id', 
        'pupil_task.text', 
        'pupil_task.price_'.$cookie_cod_valute.' AS task_price',
        'pupil_task.email', 
        'pupil_task.slug', 
        'pupil_task.gender', 
        'pupil_task.pupil_age',  
        'pupil_task.teach_online', 
        'pupil_task.teach_local', 
        'pupil_task.teach_local_pupil', 
        'pupil_task.teach_local_teacher', 
        'pupil_task.pupil_name', 
        'pupil_task.teach_city_en', 
        'pupil_task.country_id', 
        'subjects.name_ru AS task_subject'
      )
      ->orderBy('id', 'desc')
      ->paginate(20);

      return view('front/task_views/search_task', ['tasks'=>$tasks, 'data_tasks'=>$data_tasks, 'valut_conv'=>$valut_conv]);

    }   

    public function show_active_user_tasks(){

        $tasks = PupilTask::where('user_id', '=', Auth::user()->id)
        ->where('active', '=', "1")
        ->get();

        $params = [
              'tasks' => $tasks,
        ];

        return view('front/user_account/user_account_my_tasks' )->with($params);
    }

    public function show_no_active_user_tasks(){

        $tasks = PupilTask::where('user_id', '=', Auth::user()->id)
        ->where('active', '=', "0")
        ->get();

        $params = [
              'tasks' => $tasks,
        ];

        return view('front/user_account/user_account_my_tasks' )->with($params);
    }

    public function change_user_task_active($id){

        $task = PupilTask::find($id);

        if($task->active == "0"){
            $task->active = "1";
            $task->save();
            session()->flash('success', 'Ваше обьявление Активировано. Учителя могут его видеть и отвечать на него');
        }elseif($task->active == "1"){
            $task->active = "0";
            $task->save();
            session()->flash('success', 'Ваше обьявление Деактивировано, оно помещено в раздел "Неактивные" и не учавствует в поиске. Вы сможете его активировать если возникнет необходимость');
        }  

        return back();
        
    }

    public function edit_task($id){

      $task = PupilTask::find($id);

      $params = [
        'task' => $task,
      ];

      return view('front/task_views/edit_task' )->with($params);

    }

    //сохранение заявки на обучение
    public function update_task(Request $query) {

        $this->validate($query, [
            'subject_id' => 'required',
            'task_text' => 'required',
            'teach_place' => 'required',
            'price_value' => 'required',
            'name' => 'required',
            'email' => 'required'
        ]);

        $task = PupilTask::find($query->task_id);
        $user_id = Auth::user()->id;
        if($task->user_id != $user_id){
          session()->flash('success', 'Вы не являетесь автором данного обьявления!');
          return back();
        }
        $task->name = $query->name;
        $task->text = $query->task_text;
        $task->subject_id = $query->subject_id;
        $task->skype_login = $query->skype_login;
        $task->country_id = $query->country_teach;
        $task->teach_city_en = $query->teach_city_en;
        $task->price_def = $query->price_value;
        $task->cur_def = $query->price_valute;
        $task->email = $query->email;
        $task->phone = $query->phone;
        $task->gender = $query->gender;
        $task->pupil_age = $query->pupil_age;

        //сохраняем место проведения уроков
        if(isset($query->teach_place['online'])){
            $task->teach_online = '1';
        }else{
            $task->teach_online = '0';
        }
        if(isset($query->teach_place['local'])){
            $task->teach_local = '1';
        }else{
            $task->teach_local = '0';
        }
        if(isset($query->teach_local_pupil)){
            $task->teach_local_pupil = '1';
        }else{
            $task->teach_local_pupil = '0';
        }
        if(isset($query->teach_local_teacher)){
            $task->teach_local_teacher = '1';
        }else{
            $task->teach_local_teacher = '0';
        }

        //конвертируем сохраняем валюты разные
        if (isset($query->price_value) && isset($query->price_valute)) {
          if ($query->price_valute == 'UAH') {
              $task->price_UAH = $query->price_value;
              $task->price_RUB = Helper::convert_cur($query->price_value, $query->price_valute, 'RUB'); 
              $task->price_USD = Helper::convert_cur($query->price_value, $query->price_valute, 'USD');
              $task->price_KZT = Helper::convert_cur($query->price_value, $query->price_valute, 'KZT');
              $task->price_BYN = Helper::convert_cur($query->price_value, $query->price_valute, 'BYN');
          }elseif ($query->price_valute == 'RUB') {
              $task->price_RUB = $query->price_value;
              $task->price_UAH = Helper::convert_cur($query->price_value, $query->price_valute, 'UAH'); 
              $task->price_USD = Helper::convert_cur($query->price_value, $query->price_valute, 'USD');
              $task->price_KZT = Helper::convert_cur($query->price_value, $query->price_valute, 'KZT');
              $task->price_BYN = Helper::convert_cur($query->price_value, $query->price_valute, 'BYN');
          }elseif ($query->price_valute == 'USD') {
              $task->price_USD = $query->price_value;
              $task->price_UAH = Helper::convert_cur($query->price_value, $query->price_valute, 'UAH'); 
              $task->price_RUB = Helper::convert_cur($query->price_value, $query->price_valute, 'RUB');
              $task->price_KZT = Helper::convert_cur($query->price_value, $query->price_valute, 'KZT');
              $task->price_BYN = Helper::convert_cur($query->price_value, $query->price_valute, 'BYN');
          }elseif ($query->price_valute == 'KZT') {
              $task->price_KZT = $query->price_value;
              $task->price_UAH = Helper::convert_cur($query->price_value, $query->price_valute, 'UAH'); 
              $task->price_RUB = Helper::convert_cur($query->price_value, $query->price_valute, 'RUB');
              $task->price_USD = Helper::convert_cur($query->price_value, $query->price_valute, 'USD');
              $task->price_BYN = Helper::convert_cur($query->price_value, $query->price_valute, 'BYN');
          }elseif ($query->price_valute == 'BYN') {
              $task->price_BYN = $query->price_value;
              $task->price_UAH = Helper::convert_cur($query->price_value, $query->price_valute, 'UAH'); 
              $task->price_RUB = Helper::convert_cur($query->price_value, $query->price_valute, 'RUB');
              $task->price_USD = Helper::convert_cur($query->price_value, $query->price_valute, 'USD');
              $task->price_KZT = Helper::convert_cur($query->price_value, $query->price_valute, 'KZT');
          }
       }

        $task->save();
        return redirect()->back()->with('success', 'Ваше обьявление успешно отредактировано');
    }
}
