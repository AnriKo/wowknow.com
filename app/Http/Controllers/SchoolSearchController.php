<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\School;
use App\Review;
use App;
use DB;

class SchoolSearchController extends Controller
{
    public function search($city){

		$local = App::getLocale();
		$data_schools['subject'] = '';
		$data_schools['city'] = '';
		$cities_school = [];

		$cities_school = School::groupBy('city')->select('city', 'country')->get();

      $schools = School::
        where('active', '=', '1')
        ->orderBy('updated_at', 'desc')
	    ->select(
	    	'id',
	        'alias',
            'name', 
	        'top', 
	        'logo',
	        'street',
	        'class',
	        'city',
	        'description',
	        'country'
	      );
	    if($city != 'city'){
	    	$schools = $schools->where('city', '=', $city);
	    	$data_schools['city'] = $city;

		    $city_ru = DB::table('cities_ru')
	          ->where('title_en', '=', $city);

	        $city_by = DB::table('cities_by')
	          ->where('title_en', '=', $city);

	        $city_kz = DB::table('cities_kz')
	          ->where('title_en', '=', $city);

	        $city_current = DB::table('cities_ua')
	          ->where('title_en', '=', $city)
	          ->union($city_ru)  
	          ->union($city_by)
	          ->union($city_kz)
	          ->first();

	        $title_ = 'title_'. $local;

	        //$data_schools['city'] = $city_current->{$title_};
	        $data_schools['city'] = $city;

	    }

        $schools = $schools->paginate(10);


       return view('front/school/index', ['schools'=>$schools, 'local' => $local, 'data_schools' => $data_schools, 'cities_school' => $cities_school ]);

    }

    public function one_show($alias) {

    	$local = App::getLocale();
    	$title_loc = "title_".$local;
    	$school_info = [];
    	$school = School::where('alias', '=', $alias)->first();
        if($school->country == 'ua'){
        	$school_info['city'] = $school->city_ua->title_ru;
        }elseif($school->country == 'ru'){
        	$school_info['city'] = $school->city_ru->title_ru;
        }elseif($school->country == 'kz'){
        	$school_info['city'] = $school->city_kz->title_ru;
        }elseif($school->country == 'by'){
        	$school_info['city'] = $school->city_by->title_ru;
        }

        if($school->gallery){
        	$school_info['gallery'] = unserialize($school->gallery);
        }else{
        	$school_info['gallery'] = null;
        }
        
        //отзывы
        $reviews = Review::where("item_id", '=', $school->id)
        ->where('type', "=", 'course')
        ->orderBy('created_at', "DESC")
        ->get();

		return view('front/school/show_one', ['school'=>$school,'reviews'=>$reviews, 'local' => $local, 'school_info' => $school_info]);

    }

    public function delete($id) {

    	$cours = CompanySchool::find($id);
    	$name = $cours->course_name;
    	$cours->delete();
    	$coursCatRel = CompanyCoursesCatRel::where('course_id', '=', $id)->delete();
    	$coursLangs = CompanyCoursesLang::where('course_id', '=', $id)->delete();

    	session()->flash('success', 'Все данные о курсе "'. $name .'" успешно удалены');
        return redirect()->back();

    }
}
