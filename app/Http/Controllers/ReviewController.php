<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Review;
use App\Teachers;

class ReviewController extends Controller
{

    public function review_add(Request $query){
        $this->validate($query, [
            'sender_name' => 'required',
            'review_text' => 'required'
        ]);

        $review = new Review;
        $review->item_id = $query->item_id;
        $review->sender_name = $query->sender_name;
        $review->review = $query->review_text;
        $review->type = $query->review_type;
        $review->status = "0";
        $review->save();

        $flash_text = 'Спасибо, ваш отзыв добавлен';

        if($review->type == 'post'){
            $flash_text = 'Спасибо, ваш комментарий добавлен';
        }

        session()->flash('success', $flash_text );
        return redirect()->back();
    }

    public function tutor_review_add(Request $query){
        $this->validate($query, [
            'sender_name' => 'required',
            'review_text' => 'required',
        ]);

        $review = new Review;
        $review->item_id = $query->teacher_id;
        $review->sender_name = $query->sender_name;
        $review->review = $query->review_text;
        $review->type = "tutor";
        $review->status = "0";
        $review->save();

        session()->flash('success', 'Спасибо, ваш отзыв добавлен');
        return redirect()->back();
    }
 
    public function my_review_show (){
        $reviews = Review::where('teacher_id', '=', Auth::user()->id)->get();
        //отзывы
          $reviews = Review::where("item_id", '=', Auth::user()->user_teacher->id)
          ->where('type', "=", 'tutor')
          ->orderBy('created_at', "DESC")
          ->get();
        return view('front/tutor_account/my_review_show', ['reviews'=>$reviews]);
    }
}
