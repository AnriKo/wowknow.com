<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Subjects;
use App\Teachers;
use App\PupilTask;
use App\Conversation;
use App\RequestToTutors;
use DB;
use App\Http\Requests;
//use Auth;
use Session;
use App\MyHelpers\Helper;
use Image;
//use App\Mail\FromUserToTutor;
use App\Mail\ToAdminNewRequest;
use App\Mail\ToAdminTutorReportRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;


class UserController extends Controller
{
    public function send_tutor_request(Request $query) { 

        //dd($query);

        $request = new RequestToTutors;

        $request->subject_id = implode(",", $query->subject_id);
        $request->level = $query->level;
        $request->teach_place = $query->teach_place;
        $request->pupil_adress = preg_replace('/[0-9]+/', '', $query->pupil_adress);
        $request->age = $query->age;
        $request->times_a_week = $query->times_a_week;
        $request->course_length = $query->course_length;
        $request->message = preg_replace('/[0-9]+/', '', $query->message );
        $request->client_name = $query->client_name;
        $request->phone = $query->phone;
        $request->email = $query->email;
        $request->tutor_id = $query->tutor_id;
        $request->status = 'sent';
        $request->type = 'personal';
        $request->save();

        //обработка данных для письма репетитору
        $for_tutor_mail = [];

        $level = '';
        if($query->level == 'children'){
            $level = 'Для ребенка';
        }elseif($query->level == 'beginner'){
            $level = 'Репетитор для начинающих';
        }elseif($query->level == 'middle'){
            $level = 'Средний уровень';
        }

        $teach_place = '';
        if($query->teach_place == 'pupil_teacher'){
            $teach_place = 'Возможно и на територии репетитора и на територии ученика';
        }elseif($query->teach_place == 'teacher'){
            $teach_place = 'На територии репетитора';
        }elseif($query->teach_place == 'pupil'){
            $teach_place = 'На територии ученика';
        }elseif($query->teach_place == 'online'){
            $teach_place = 'Онлайн (по скайпу)';
        }

        $age = '';
        if($query->age == 'child'){
            $age = 'Ребёнок/школьник';
        }elseif($query->age == 'student'){
            $age = 'Студент';
        }elseif($query->age == 'big'){
            $age = 'Взрослый';
        }

        $times_a_week = '';
        if($query->times_a_week == '1'){
            $times_a_week = '1 раз в неделю';
        }elseif($query->times_a_week == '2'){
            $times_a_week = '2 раза в неделю';
        }elseif($query->times_a_week == '3'){
            $times_a_week = '3 и больше раз в неделю';
        }

        $course_length = '';
        if($query->course_length == 'short'){
            $course_length = 'Несколько раз (до 7 занятий)';
        }elseif($query->course_length == 'long'){
            $course_length = 'Больше 7 занятий';
        }

        $subjects = [];
        foreach($query->subject_id as $subject){
            $subject = Subjects::find($subject);
            $subjects[] = $subject->name_ru;
        }

        $for_tutor_mail['level'] = $level;
        $for_tutor_mail['teach_place'] = $teach_place;
        $for_tutor_mail['pupil_adress'] = preg_replace('/[0-9]+/', '', $query->pupil_adress);
        $for_tutor_mail['age'] = $age;
        $for_tutor_mail['times_a_week'] = $times_a_week;
        $for_tutor_mail['course_length'] = $course_length;
        $for_tutor_mail['message'] = preg_replace('/[0-9]+/', '', $query->message);
        $for_tutor_mail['client_name'] = $query->client_name;
        $for_tutor_mail['subjects'] = $subjects;
        $for_tutor_mail['requestid'] = $request->id;

        $tutor = User::find($query->tutor_id);
        //dd($tutor);
        $for_tutor_mail['tutor_name'] = $tutor->name;
        $for_tutor_mail['tutor_id'] = Crypt::encryptString($tutor->id);


        //сообщение репетитору
        //$mail = \Mail::to($tutor->email)->send(new FromUserToTutor($for_tutor_mail));

        $for_tutor_mail['tutor_id'] = $tutor->user_teacher->id;
        $for_tutor_mail['tutor_phone'] = $tutor->user_teacher->phone;
        $for_tutor_mail['tutor_mail'] = $tutor->email;
        $for_tutor_mail['tutor_link'] = $tutor->user_teacher->slug;
        $for_tutor_mail['tutor_name'] = $tutor->name . ' ' .$tutor->user_teacher->last_name;

        $for_tutor_mail['client_phone'] = $query->phone;
        $for_tutor_mail['client_mail'] = $query->email;

        //сообщение администратору
        $admin_mail = explode(',', env('ADMIN_EMAILS'));
        //$mail_admin = \Mail::to($admin_mail)->send(new ToAdminNewRequest($for_tutor_mail));

        Session::flash('success', 'Ваша заявка успешно отправлена. Репетитор или наш менеджер свяжется с вами в ближайшее время. Спасибо');
        return redirect()->back();

    }

    public function tutor_auth(Request $query, $requestid){

        $user_id = Crypt::decryptString($query->id);
        if($query->auth == 'need'){
            Auth::loginUsingId($user_id, true);
        }
        if($query->report == 'yes'){
            $status = 'accept';
        }elseif($query->report == 'no'){
            $status = 'no_accept';
        }
        return redirect()->route('requests_one', ['requestid' => $requestid, 'status' => $status]);
    }

    public function request_one(Request $query, $requestid){

        $request = RequestToTutors::find($requestid);
        $admin_mail = explode(',', env('ADMIN_EMAILS'));
        $status = '';

        //обработка перевод данных заявки
        $subjects_ids_array = explode(",", $request->subject_id);

        $request_detail = [];

        $level = '';
        if($request->level == 'children'){
            $level = 'Для ребенка';
        }elseif($request->level == 'beginner'){
            $level = 'Репетитор для начинающих';
        }elseif($request->level == 'middle'){
            $level = 'Средний уровень';
        }

        $teach_place = '';
        if($request->teach_place == 'pupil_teacher'){
            $teach_place = 'Возможно и на територии репетитора и на територии ученика';
        }elseif($request->teach_place == 'teacher'){
            $teach_place = 'На територии репетитора';
        }elseif($request->teach_place == 'pupil'){
            $teach_place = 'На територии ученика';
        }elseif($request->teach_place == 'online'){
            $teach_place = 'Онлайн (по скайпу)';
        }

        $age = '';
        if($request->age == 'child'){
            $age = 'Ребёнок/школьник';
        }elseif($request->age == 'student'){
            $age = 'Студент';
        }elseif($request->age == 'big'){
            $age = 'Взрослый';
        }

        $times_a_week = '';
        if($request->times_a_week == '1'){
            $times_a_week = '1 раз в неделю';
        }elseif($request->times_a_week == '2'){
            $times_a_week = '2 раза в неделю';
        }elseif($request->times_a_week == '3'){
            $times_a_week = '3 и больше раз в неделю';
        }

        $course_length = '';
        if($request->course_length == 'short'){
            $course_length = 'Несколько раз (до 7 занятий)';
        }elseif($request->course_length == 'long'){
            $course_length = 'Больше 7 занятий';
        }

        $subjects = [];
        foreach($subjects_ids_array as $subject){
            $subject = Subjects::find($subject);
            $subjects[] = $subject->name_ru;
        }

        $request_detail['level'] = $level;
        $request_detail['teach_place'] = $teach_place;
        $request_detail['pupil_adress'] = $request->pupil_adress;
        $request_detail['age'] = $age;
        $request_detail['times_a_week'] = $times_a_week;
        $request_detail['course_length'] = $course_length;
        $request_detail['message'] = $request->message;
        $request_detail['client_name'] = $request->client_name;
        $request_detail['subjects'] = $subjects;
        $request_detail['requestid'] = $request->id;

        //изменение статуса заявки
        $for_tutor_mail = [];
        $tutor = User::find($request->tutor_id);
        $for_tutor_mail['tutor_id'] = $tutor->user_teacher->id;
        $for_tutor_mail['tutor_link'] = $tutor->user_teacher->slug;
        $for_tutor_mail['tutor_name'] = $tutor->name . ' ' .$tutor->user_teacher->last_name;
        $for_tutor_mail['client_phone'] = $request->phone;
        $for_tutor_mail['client_mail'] = $request->email;
        $for_tutor_mail['client_name'] = $request->client_name;

        if($query->status == 'no_accept'){
            $request->status = 'no_accept';
            $for_tutor_mail['status'] = 'Репетитор отклонил заявку';
            $mail_admin = \Mail::to($admin_mail)->send(new ToAdminTutorReportRequest($for_tutor_mail));
            $request_detail['status'] = 'no_accept';
            $request->save();

        }elseif($query->status == 'accept'){
            $request->status = 'accept';
            $for_tutor_mail['status'] = 'Репетитор принял заявку';
            $mail_admin = \Mail::to($admin_mail)->send(new ToAdminTutorReportRequest($for_tutor_mail));
            $request_detail['status'] = 'accept';
            $request->save();
        }else{
            $request_detail['status'] = 'see';
        }
        

        return view('front/tutor_account/request_one', [ 'request' => $request, 'request_detail' => $request_detail]);

    }

    public function tutor_noassept_report(Request $query){

        $request = RequestToTutors::find($query->requestid);
        $request->tutor_report = $query->option . ' <br> ' . $query->review_text;
        $request->save();

        Session::flash('success', 'Спасибо что уделили нам время и указали причину. Хорошего дня!');
        return redirect()->back();

    }





    public function requests_index(){

        $user = Auth()->user();

        $requests = RequestToTutors::where( 'tutor_id', '=', $user->id)->get();

        return view('front/tutor_account/requests_index', ['requests' => $requests]);

    }

}
