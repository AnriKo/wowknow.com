<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Request as PupilRequest;
use Auth;
use App\Conversation;
use App\User;
use App\ConversationReply;
use App\PupilTask;
use App\Mail\NewRequest;
use App\Mail\AcceptRequest;
use App\Teachers;

class RequestController extends Controller
{
    public function requestStore(Request $query)
    {
    	if(!Auth::check()){
    		return 'login please';
    	}
        $user_id = Auth::user()->id;
        $teacher_id = $query->teacher_id;
    
        $check_exist_request = PupilRequest::where('pupil_id', '=', $user_id)
            ->where('teacher_id', '=', $teacher_id)
            ->where('status', '=', 'sent')->get();

        if($check_exist_request->count() <= 0 ){

            $request = new PupilRequest;
            $request->pupil_id = $user_id;
            $request->teacher_id = $teacher_id;
            $request->status = 'sent';
            $request->sender = $user_id;
            $request->save();

        }else{
            return '<i class="fa fa-exclamation-circle" aria-hidden="true" style="color:orange;"></i>Вы уже отправляли заявку этому учителю и учитель пока на нее не ответил, напишите личное сообщение или отправьте письмо учителю.';
        }

        if ($query->message != '') {

            $recipient_id = $teacher_id;

            $conversation = Conversation::where('sender_id', '=', $user_id)
                ->where('recipient_id', '=', $recipient_id)
                ->orWhere(function ($query) use ( $user_id, $recipient_id ) {
                    $query->where('recipient_id', '=', $user_id)
                            ->where('sender_id', '=', $recipient_id);
                })
                ->first();

            if($conversation == null ){

                $conversation = new Conversation;
                $conversation->sender_id = $user_id;
                $conversation->recipient_id = $recipient_id;
                $conversation->save();

            }

            $conversation_reply = new ConversationReply;
            $conversation_reply->reply = $query->message;
            $conversation_reply->user_id = $user_id;
            $conversation_reply->c_id = $conversation->id;
            $conversation_reply->status = '0';
            $conversation_reply->save();
            
        }

        //письмо учителю, уведомление о новой заявке
        $user_pupil = Auth::user();
        $user_teacher = User::find($teacher_id);
        $sent_letter_to = 'teacher';
        \Mail::to($user_teacher->email)->send(new NewRequest($user_pupil, $user_teacher, $sent_letter_to));

        return '<i class="fa fa-check-circle-o" aria-hidden="true" style="color:green;"></i>Заявка отправлена.';

    }

    public function response_to_pupil_task(Request $query)
    {

        $teacher_id = Auth::user()->id;
        $user_id = $query->pupil_id;
        if(!Auth::check()){
            return 'login please';
        }
    
        $check_exist_request = PupilRequest::where('pupil_id', '=', $user_id)
            ->where('teacher_id', '=', $teacher_id)
            ->where('status', '=', 'sent')->get();

        if($check_exist_request->count() <= 0 ){

            $request = new PupilRequest;
            $request->pupil_id = $user_id;
            $request->teacher_id = $teacher_id;
            $request->status = 'sent';
            $request->sender = $teacher_id;
            $request->attach_to_pupil_task = $query->pupil_task_id;
            $request->save();

        }else{
            return '<i class="fa fa-exclamation-circle" aria-hidden="true" style="color:orange;"></i>Вы уже отправляли заявку этому ученику и ученик пока на нее не ответил, напишите личное сообщение или отправьте письмо.';
        }

        if ($query->message != '') {

            $recipient_id = $user_id;

            $conversation = Conversation::where('sender_id', '=', $teacher_id)
                ->where('recipient_id', '=', $recipient_id)
                ->orWhere(function ($query) use ( $teacher_id, $recipient_id ) {
                    $query->where('recipient_id', '=', $teacher_id)
                            ->where('sender_id', '=', $recipient_id);
                })
                ->first();

            if($conversation == null ){

                $conversation = new Conversation;
                $conversation->sender_id = $teacher_id;
                $conversation->recipient_id = $recipient_id;
                $conversation->save();

            }

            $conversation_reply = new ConversationReply;
            $conversation_reply->reply = $query->message;
            $conversation_reply->user_id = $teacher_id;
            $conversation_reply->c_id = $conversation->id;
            $conversation_reply->status = '0';
            $conversation_reply->save();
            
        }

        //письмо ученику, уведомление о новой заявке
        $user_teacher = Auth::user();
        $user_pupil = User::find($user_id);
        $sent_letter_to = 'pupil';
        \Mail::to($user_pupil->email)->send(new NewRequest($user_pupil, $user_teacher, $sent_letter_to));

        return '<i class="fa fa-check-circle-o" aria-hidden="true" style="color:green;"></i>Заявка отправлена.';

    }

    public function show_all_requests(){

        $requests = PupilRequest::where('teacher_id', '=', Auth::user()->id)
            ->orWhere('pupil_id', '=', Auth::user()->id)
            ->get();

        $params = [
              'requests' => $requests,
        ];

        return view('front/user_account/user_all_requests' )->with($params);
    }

    public function accept_request($id, $sender_id){

        $request = PupilRequest::where('id', '=', $id)->first();
        if($request->status == 'accept'){
            session()->flash('success_long_time', 'Вы уже приняли эту заявку от учителя. Если учитель не отвечает напишите ему письмо или используйте другие доступные контакты.');
            return back();
        }
        $request->status = 'accept';
        $request->save();

        //добавляем учителю рейтинг
        $tutor = Teachers::where("user_id", '=', $request->teacher_id)->first();
        $raiting = intval($tutor->raiting) + 8;
        $tutor->raiting = $raiting;
        $tutor->save();

        //деактивируэм обьявление ученика о поиске репетитора если эта 
        //принятая заявка прикрепленная к этому обьявлению
        if($request->attach_to_pupil_task != null){
            $pupil_task = PupilTask::where('id', '=', $request->attach_to_pupil_task)->first();
            $pupil_task->active = 0;
            $pupil_task->save();
            session()->flash('success_long_time', 'Заявка принята, свяжитесь с учителем и начинайте обучение. Так как вы нашли учителя ваше обьявление мы делает не активное и убираем из поиска, в случае необходимости вы сможете его опять активировать.');
        }

        $who_accept_request = Auth::user();
        $who_create_request = User::find($sender_id);
        $sent_letter_to = '';
        if($request->teacher_id == $who_accept_request->id){
            $sent_letter_to = 'pupil';
        }elseif($request->pupil_id == $who_accept_request->id){
            $sent_letter_to = 'teacher';
        }
        \Mail::to($who_create_request->email)->send(new AcceptRequest($who_create_request, $who_accept_request, $sent_letter_to));

        return back();
        
    }

    public function no_accept_request($id){

        $request = PupilRequest::where('id', '=', $id)
            ->first();
        $request->status = 'no_accept';
        $request->save();
        return back();
        
    }

    public function delete_request($id){

        $request = PupilRequest::where('id', '=', $id)
            ->delete();
        return back();

    }
}
