<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;

class SearchTagsAjaxController extends Controller
{

 public function city_search(Request $query) {

        $city = $query->query('city');

        $locale = $query->query('locale');

        $tags = DB::table('cities_'.$locale)
            ->where('title_ru', 'like', $city.'%');
        if ($locale == 'ua') {
            $tags->orWhere('title_'.$locale, 'like', $city.'%');
        }
        $tags->take(5);
        $tags = $tags->get();

        $formatted_tags = [];
        foreach ($tags as $tag) {
            $formatted_tags[] = ['id' => $tag->title_en, 'text' => $tag->title_ru, ];
        }

        return \Response::json($formatted_tags);
    }

 public function city_search_id(Request $query) {

        $city = $query->query('city');

        $locale = $query->query('locale');

        $tags = DB::table('cities_'.$locale)
            ->where('title_ru', 'like', $city.'%');
        if ($locale == 'ua') {
            $tags->orWhere('title_'.$locale, 'like', $city.'%');
        }
        $tags->take(5);
        $tags = $tags->get();

        $formatted_tags = [];
        foreach ($tags as $tag) {
            $formatted_tags[] = ['id' => $tag->id, 'text' => $tag->title_ru, ];
        }

        return \Response::json($formatted_tags);
    }

 public function city_search_all(Request $query) {

        $city = $query->query('city');

        $tags_ru = DB::table('cities_ru')
          ->where('title_ru', 'like', $city.'%');

        $tags_by = DB::table('cities_by')
          ->where('title_ru', 'like', $city.'%');

        $tags_kz = DB::table('cities_kz')
          ->where('title_ru', 'like', $city.'%');

        $tags = DB::table('cities_ua')
          ->where('title_ru', 'like', $city.'%')
          ->union($tags_ru)  
          ->union($tags_by)
          ->union($tags_kz)
          ->take(5)   
          ->get();

        $formatted_tags = [];
        foreach ($tags as $tag) {
            $formatted_tags[] = ['id' => $tag->title_en, 'text' => $tag->title_ru, ];
        }

        return \Response::json($formatted_tags);
    }

    public function subject_search(Request $query) {

        $subject = $query->query('subject');
        $tags = DB::table('subjects')
            ->where('name_ru', 'like', '%'.$subject.'%')
            ->where('status', '=', 'good');

        $tags->take(6);
        $tags = $tags->get();

        $formatted_tags = [];
        foreach ($tags as $tag) {
            $formatted_tags[] = ['id' => $tag->id, 'text' => $tag->name_ru, ];
        }

        return \Response::json($formatted_tags);
    }

    /* Поиск предмета по алиасу (страница поиска репетитора)*/

    public function subject_search_name(Request $query) {

        $subject = $query->query('subject');
        $tags = DB::table('subjects')
            ->where('name_ru', 'like', '%'.$subject.'%')
            ->select('name_ru', "id", 'alias');

        $tags->take(6);
        $tags = $tags->get();

        if($tags->count() == 0){
            $tags = DB::table('subject_direction')
                ->where('subject_direction.name_ru', 'like', '%'.$subject.'%')
                ->leftJoin('subjects', 'subjects.id',  '=', 'subject_direction.subject_id')
                ->select('subject_direction.name_ru', 'subjects.alias');

            $tags->take(6);
            $tags = $tags->get();
            
        }elseif($tags->count() < 4 ){
            $tags_sub = DB::table('subject_direction')
                ->where('subject_direction.name_ru', 'like', '%'.$subject.'%')
                ->leftJoin('subjects', 'subjects.id',  '=', 'subject_direction.subject_id')
                ->select('subject_direction.name_ru', 'subjects.alias');
            $tags_sub->take(6);
            $tags_sub = $tags_sub->get();
            $tags = $tags->merge($tags_sub);
            $tags = $tags->take(6);
        }

        $formatted_tags = [];
        foreach ($tags as $tag) {
            $formatted_tags[] = ['id' => $tag->alias, 'text' => $tag->name_ru, ];
        }

        return \Response::json($formatted_tags);
    }

    /* поиск предмета по ID (для страницы предметов учителя)  */
    public function subject_search_name_for_id(Request $query) {

        $subject = $query->query('subject');
        $tags = DB::table('subjects')
            ->where('name_ru', 'like', '%'.$subject.'%')
            ->select('name_ru', 'id');

        $tags->take(6);
        $tags = $tags->get();

        if($tags->count() == 0){
            $subject = $query->query('subject');
            $tags = DB::table('subject_direction')
                ->where('name_ru', 'like', '%'.$subject.'%')
                ->select('name_ru', 'subject_id AS id');

            $tags->take(6);
            $tags = $tags->get();
            
        }elseif($tags->count() < 4 ){
            $subject = $query->query('subject');
            $tags_sub = DB::table('subject_direction')
                ->where('name_ru', 'like', '%'.$subject.'%')
                ->select('name_ru', 'subject_id AS id');
            $tags_sub->take(6);
            $tags_sub = $tags_sub->get();
            $tags = $tags->merge($tags_sub);
            $tags = $tags->take(6);
        }

        $formatted_tags = [];
        foreach ($tags as $tag) {
            $formatted_tags[] = ['id' => $tag->id, 'text' => $tag->name_ru, ];
        }

        return \Response::json($formatted_tags);
    }

    public function country_search(Request $query) {

        $country = $query->query('country');
        $tags = DB::table('countries')
            ->where('name_ru', 'like', $country.'%')
            ->orWhere('name_ua', 'like', $country.'%')
            ->orWhere('name_en', 'like', $country.'%');
        $tags->take(6);
        $tags = $tags->get();

        $formatted_tags = [];
        foreach ($tags as $tag) {
            $formatted_tags[] = ['id' => $tag->id, 'text' => $tag->name_ru, ];
        }

        return \Response::json($formatted_tags);
    }

    public function tag_search(Request $query) {

        $tag = $query->query('tag');
        $tags = DB::table('tags')
            ->where('name', 'like', $tag.'%')
            ->take(6)
            ->get();

        $formatted_tags = [];
        foreach ($tags as $tag) {
            $formatted_tags[] = ['id' => $tag->id, 'text' => $tag->name, ];
        }

        return \Response::json($formatted_tags);
    }
    public function converter(Request $query) {

        $prices_cur = $query->query('prices_cur');
        $to = $query->query('to');
        $prices_cur = explode('|',$prices_cur);
        $x = array_pop($prices_cur);
        $res = '';
        foreach ($prices_cur as $key => $one_prices_cur) {

            $one_prices_cur = explode('.',$one_prices_cur);
            $amount = $one_prices_cur[0];
            $from = $one_prices_cur[1];

            if ($from != $to) {

                $get = file_get_contents("https://www.google.com/finance/converter?a=$amount&from=$from&to=$to");
                $get = explode("<span class=bld>",$get);
                $get = explode("</span>",$get[1]);
                $converted_currency = preg_replace("/[^0-9\.]/", null, $get[0]);
                $converted_currency = round($converted_currency, -1);
                $res .= $converted_currency.'|';
            }else{
                $res .= $amount.'|';
            }

        }

        return $res;

    }

    public function convert_valute(Request $query) {
        return back()->cookie('cod_valute', $query->query('valute'), 60);
    }

    public function sub_dir_search(Request $query) {

        $sub_dir = DB::table('subject_direction')
            ->where('subject_direction.subject_id', '=', $query->query('subject_id'))
            ->select('subject_direction.*')
            ->get();
        if (count($sub_dir) > 0) {

            //return $sub_dir;
            return \Response::json($sub_dir);

        }else{
            return '0';
        }
        
    }

    public function change_sub_to_main_subject(Request $query) {

        $subject = DB::table('subjects')
            ->where('id', '=', $query->query('subject_id'))
            ->select('subjects.name_ru', 'subjects.id')
            ->first();
        if (count($subject) > 0) {

            //return $subject;
            return \Response::json([
                'name' => $subject->name_ru, 
                'id' => $subject->id
            ]);

        }else{
            return '0';
        }
        
    }

}