<?php 

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Post;
use App\Tags;
use App\PostTags;
use App\Option;
use App\MyHelpers\Helper;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Image;

class BlogController extends Controller {


    public function index_blog()
    {
        $posts = Post::orderBy('created_at','desc')->get();
        return view('admin.blog.post.index')
            ->withPosts($posts);
    }   

    public function index_tag()
    {

        $tags = Tags::all();
        return view('admin.blog.tags.index')
            ->withTags($tags);
    }

    public function create_tag() {
        return view('admin.blog.tags.create');
    }

    public function store_tag(Request $query) {

        $this->validate($query, [
            'name' => 'unique:blog_tags',
            'slug' => 'unique:blog_tags',
        ]);

        $tags = new Tags;
        $tags->name_ru = $query->name;
        $tags->slug = $query->slug;
        $tags->save();

        return redirect(route('blog_tags.index'));

    }

    public function create_post() {
        $tags = Tags::pluck('name_ru', 'id');

        return view('admin.blog.post.create')
            ->withTags($tags);
    }

    public function store_post(Request $query) {

        $this->validate($query, [
            'title' => 'required|unique:blog_posts',
            'slug' => 'unique:blog_posts',
        ]);

        $post = new Post;
        $post->title = $query->title;

        if(isset($query->slug)){
           $post->slug = Helper::ru2lat($query->slug);
        }else{
            $post->slug = Helper::ru2lat($query->title);
        }

        $post->excerpt = $query->excerpt;
        $post->content = $query->post_content;
        $post->post_status = $query->post_status;  
        $post->type = $query->post_or_page;  
        $post->image = $query->filepath;
        $post->sm_image = $query->sm_image;
        $post->save();

        if($query->tags){
            foreach ($query->tags as $row) {
                $tags = new PostTags;
                $tags->post_id = $post->id;
                $tags->tag_id = $row;
                $tags->save();
            }
        }

        session()->flash('success', 'Стаття успішно збережена');
        return redirect(route('blog.index'));


    }

    public function edit_post($id) {
        $tags = Tags::select('name_ru', 'id')->get();
        $post = Post::find($id);
        $post_tags = PostTags::where('post_id', '=', $id)->get();

        return view('admin.blog.post.edit')
            ->withTags($tags)
            ->withPostTags($post_tags)
            ->withPost($post);
    }

    public function update_post(Request $query) {

        $this->validate($query, [
            'title' => 'required',
        ]);

        $post = Post::find($query->post_id);
        $post->title = $query->title;
        $post->slug = $query->slug;
        $post->excerpt = $query->excerpt;
        $post->content = $query->post_content;
        $post->post_status = $query->post_status;
        $post->image = $query->filepath;
        $post->sm_image = $query->sm_image;

        $post->save();

        //удаляем теги старые этого поста
        $old_tags = PostTags::where('post_id', '=', $post->id)->delete();
        //добавляем новые теги
        if($query->tags){
            foreach ($query->tags as $row) {
                $tags = new PostTags;
                $tags->post_id = $post->id;
                $tags->tag_id = $row;
                $tags->save();
            }
        }

        session()->flash('success', 'Пост отредактирован');

        return redirect(route('blog.index'));
    }


}