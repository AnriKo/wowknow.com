<?php 

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\University;
use App\UniversitiesLangs;
use App\UnivSubject;
use App\UniversitySubject;
use App\Option;
use App\MyHelpers\Helper;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Image;

class UniversityController extends Controller {


    public function index_university()
    {
        $posts = University::orderBy('created_at','desc')->get();
        return view('admin.university.post.index')
            ->withPosts($posts);
    }   

    public function index_subjects()
    {

        $subject = UnivSubject::all();
        return view('admin.university.subject.index')
            ->withTags($subject);
    }

    public function create_subjects() {
        return view('admin.university.subject.create');
    }

    public function store_subjects(Request $query) {

        $this->validate($query, [
            'name' => 'unique:univ_subjects',
            'slug' => 'unique:univ_subjects',
        ]);

        $subject = new UnivSubject;
        $subject->name = $query->name;
        $subject->slug = $query->slug;
        $subject->save();

        return redirect(route('university_subjects.index'));

    }

    public function create_university() {
        $subject = UnivSubject::select('name_ru', 'id')->get();

        return view('admin.university.post.create')
            ->withTags($subject);
    }

    public function store_university(Request $query) {

        $this->validate($query, [
            'title' => 'required',
        ]);

        $post = new University;

        if ($query->logo_univer) {
            $filename = 'univer_'.time(). '_'. mt_rand(0,100). '.jpg';
            $location = public_path('storage/universities/'.$post->country.'/'.$filename);
            $img = Image::make($query->logo_univer); //->encode('jpg', 50);
            $img->resize(200, null, function ($constraint) {
                $constraint->aspectRatio();
            });
            $img->save($location);    

            $post->logo = $filename;
        }

        $post->title = $query->title;
        $post->slug = $query->slug;
        $post->description = $query->description;
        $post->adress_info = $query->adress_info;
        $post->save();

        //добавляем новые теги
        if ($query->subjects) {
            foreach ($query->subjects as $row) {
                $subject = new UniversitySubject;
                $subject->university_id = $post->id;
                $subject->univ_subject_id = $row;
                $subject->save();
            }
        }


        session()->flash('success', 'Универ создан и сохранен');

        return redirect(route('university.edit', ['id' => $post->id ]));

    }

    public function edit_university($id) {
        $subject = UnivSubject::select('name_ru', 'id')->get();
        $post = University::find($id);
        $post_tags = UniversitySubject::where('university_id', '=', $id)->get();

        return view('admin.university.post.edit')
            ->withTags($subject)
            ->withUniversitySubject($post_tags)
            ->withPost($post);
    }

    public function delete_university($id) {

        $post_tags = UniversitySubject::where('university_id', '=', $id)->delete();
        $post = University::where('id', '=', $id)->delete();
        $langs = UniversitiesLangs::where('universities_id', '=', $id)->delete();

        session()->flash('success', 'Универ удален');
        $posts = University::orderBy('created_at','desc')->get();
        return view('admin.university.post.index')
            ->withPosts($posts);
    }

    public function update_university(Request $query) {

        $this->validate($query, [
            'title' => 'required',
        ]);

        $post = University::find($query->post_id);

        if ($query->logo_univer) {
            $filename = 'univer_'.time(). '_'. mt_rand(0,100). '.jpg';
            $location = public_path('storage/universities/'.$post->country.'/'.$filename);
            $img = Image::make($query->logo_univer); //->encode('jpg', 50);
            $img->resize(200, null, function ($constraint) {
                $constraint->aspectRatio();
            });
            $img->save($location);    

            if ($post->logo != '') {
                $img = public_path('storage/universities/'.$post->country.'/'.$post->logo);
                if(file_exists($img)){
                    unlink($img);
                }
            }

            $post->logo = $filename;
        }

        $post->title = $query->title;
        $post->slug = $query->slug;
        $post->description = $query->description;
        $post->adress_info = $query->adress_info;
        $post->save();

        //удаляем теги старые этого поста
        $old_tags = UniversitySubject::where('university_id', '=', $post->id)->delete();
        //добавляем новые теги
        if ($query->subjects) {
            foreach ($query->subjects as $row) {
                $subject = new UniversitySubject;
                $subject->university_id = $post->id;
                $subject->univ_subject_id = $row;
                $subject->save();
            }
        }


        session()->flash('success', 'Универ отредактирован');

        return back();
    }


}