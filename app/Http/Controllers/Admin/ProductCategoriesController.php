<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Subjects as Category;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use DB;

class ProductCategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::all();

        $params = [
            'title' => 'Categories Listing',
            'categories' => $categories,
        ];

        return view('admin.categories.categories_list')->with($params);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $params = [
            'title' => 'Create Product Category',
        ];

        return view('admin.categories.categories_create')->with($params);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request);
        $this->validate($request, [
            'name' => 'required|unique:subjects,name_ru',
        ]);

        $category = new Category;

            $category->name_ru = $request->input('name');
            $category->alias = $request->input('alias');
            $category->name_en = $request->input('name_en');
            $category->name_pol = $request->input('name_pol');
            $category->status = $request->input('status');

            $category->save();
        //dd($category);

        return redirect()->route('product-categories.index')->with('success', "The product category <strong>$category->name</strong> has successfully been created.");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try
        {
            $category = Category::findOrFail($id);

            $params = [
                'title' => 'Edit Product Category',
                'category' => $category,
            ];

            return view('admin.categories.categories_delete')->with($params);
        }
        catch (ModelNotFoundException $ex) 
        {
            if ($ex instanceof ModelNotFoundException)
            {
                return response()->view('errors.'.'404');
            }
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try
        {
            $category = Category::findOrFail($id);
            $sub_category = DB::table('subject_direction')
                ->where('subject_id', '=', $category->id )
                ->get();

            $params = [
                'title' => 'Изменить предмет',
                'category' => $category,
                'sub_category' => $sub_category,
            ];

            return view('admin.categories.categories_edit')->with($params);
        }
        catch (ModelNotFoundException $ex) 
        {
            if ($ex instanceof ModelNotFoundException)
            {
                return response()->view('errors.'.'404');
            }
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try
        {
            $this->validate($request, [
                'name' => 'required',
            ]);

            //dd($request);

            $category = Category::findOrFail($id);

            $category->name_ru = $request->input('name');
            $category->alias = $request->input('alias');
            $category->name_en = $request->input('name_en');
            $category->name_pol = $request->input('name_pol');
            $category->status = $request->input('status');

            $category->save();

            return redirect()->route('product-categories.index')->with('success', "The product category <strong>Category</strong> has successfully been updated.");
        }
        catch (ModelNotFoundException $ex) 
        {
            if ($ex instanceof ModelNotFoundException)
            {
                return response()->view('errors.'.'404');
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try
        {
            $category = Category::findOrFail($id);

            $category->delete();

            return redirect()->route('product-categories.index')->with('success', "The product category <strong>Category</strong> has successfully been archived.");
        }
        catch (ModelNotFoundException $ex) 
        {
            if ($ex instanceof ModelNotFoundException)
            {
                return response()->view('errors.'.'404');
            }
        }
    }

    public function edit_sub_categories($id){

        try
        {
            $category = DB::table('subject_direction')
                ->find($id);

            $params = [
                'title' => 'Изменить подпредмет',
                'category' => $category,
            ];

            return view('admin.categories.sub_categories_edit')->with($params);
        }
        catch (ModelNotFoundException $ex) 
        {
            if ($ex instanceof ModelNotFoundException)
            {
                return response()->view('errors.'.'404');
            }
        }
        
    }
    public function update_sub_categories(Request $request, $id){

        dd('Hello');

        try
        {
            $this->validate($request, [
                'name' => 'required',
            ]);

            //dd($request);

            $category = DB::table('subject_direction')->find($id);
            dd($category);

            $category->name_ru = $request->input('name');
            $category->alias = $request->input('alias');
            $category->name_en = $request->input('name_en');
            $category->name_pol = $request->input('name_pol');
            $category->status = $request->input('status');

            $category->save();

            return redirect()->route('product-categories.index')->with('success', "The product category <strong>Category</strong> has successfully been updated.");
        }
        catch (ModelNotFoundException $ex) 
        {
            if ($ex instanceof ModelNotFoundException)
            {
                return response()->view('errors.'.'404');
            }
        }

    }
}
