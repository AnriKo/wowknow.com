<?php

namespace App\Http\Controllers;
use App\MyHelpers\Helper;
use App\Post;
use App\PostTags;
use App\Tags;
use App\Review;
use Illuminate\Http\Request;
use DB;
use App;
use Image;
use Storage;
use App\Mail\ToAdminNewPost;

class PostController extends Controller
{
	
    public function index() {
        $local = App::getLocale();
    	$posts = Post::where('post_status', '=', 'published')
            ->where('type', '=', '1')
            ->where('lang', '=', $local)
            ->orderBy('id', 'desc')
	    	->select(
		        'blog_posts.id', 
		        'blog_posts.title',
		        'blog_posts.sm_image',
		        'blog_posts.excerpt',
		        'blog_posts.slug'
		    );

        $posts = $posts->paginate(10);    
    	$params = [
    		'posts' => $posts,
    	];
    	return view('front/blog/index')->with($params);
    }	
    public function index_category($slug) {

        //dd('Hi');
        $local = App::getLocale();
    	$posts = Post::
    			  join('blog_posts_tags', DB::raw('blog_posts_tags.post_id'),  '=', DB::raw('blog_posts.id') )
    			->join('blog_tags', DB::raw('blog_tags.id'),  '=', DB::raw('blog_posts_tags.tag_id') )
    			->where('blog_tags.slug', '=', $slug)
    			->where('post_status', '=', 'published')
                ->where('lang', '=', $local)
                ->orderBy('id', 'desc')
    			->select(
			        'blog_posts.id', 
			        'blog_posts.title',
                    'blog_posts.image',
			        'blog_posts.sm_image',
			        'blog_posts.excerpt',
			        'blog_posts.slug'
			      );    
        
        $posts = $posts->paginate(10);

        $tag = Tags::where('slug', '=', $slug)->first();

    	$params = [
            'posts' => $posts,
            'tag' => $tag,
    		'local' => $local,
    	];
    	return view('front/blog/index_category')->with($params);
    }

    public function one_page($slug) {
        $local = App::getLocale();
    	$post = Post::where('slug', '=', $slug)->first();
        // if($local == 'ua'){
        //     $post = $post->leftJoin('blog_posts_langs', 'blog_posts_langs.post_id', '=', 'blog_posts.id');
        //     $post = $post->select('blog_posts_langs.title As title', 
        //         'blog_posts_langs.content As content', 
        //         'blog_posts_langs.excerpt As excerpt', 
        //         'blog_posts.id', 
        //         'blog_posts.image',
        //         'blog_posts.slug', 
        //         'blog_posts.excerpt As excerpt_b', 
        //         'blog_posts.title As title_b', 
        //         'blog_posts.content As content_b');
        // }

        $like_this_posts = $post->tag_name;
        // dump($post);
        // dd($post);
        $post_tags_id = [];
        foreach ($like_this_posts as $value) {
           $post_tags_id[] = $value->id;
        }
        //$similar_posts_id = DB::table('blog_posts_tags')->whereIn('tag_id', $post_tags_id)->where('post_id', '!=', $post->id)->take(6)->pluck('post_id')->toArray();
        //$similar_posts = Post::whereIn('id', $similar_posts_id)->pluck('title', 'slug', 'sm_image');
        $similar_posts = Post::inRandomOrder()
        ->limit(6)
        ->where('lang', '=', 'ua')
        ->select('title', 'slug', 'sm_image')
        ->get();

        $reviews = Review::where("item_id", '=', $post->id)
          ->where('type', "=", 'post')
          ->orderBy('created_at', "DESC")
          ->get();

    	$params = [
            'post' => $post,
    		'local' => $local,
            'similar_posts' => $similar_posts,
            'reviews' => $reviews,
    	];
    	return view('front/blog/one_page')->with($params);
    }

    public function add_article(){

        $tags = Tags::pluck('name_ru', 'id');
        return view('front/blog/add_article')->withTags($tags);

    }

    public function store_user_post(Request $query) {

        $this->validate($query, [
            'title' => 'required',
            'g-recaptcha-response' => 'required|captcha',
        ]);

        $slug = Helper::ru2lat($query->title) . '-' . rand(1, 100);

        $post = new Post;
        $post->title = $query->title;
        $post->slug = $slug;
        $post->excerpt = mb_strimwidth( strip_tags($query->post_content), 0, 200, "...");
        $post->content = strip_tags($query->post_content, '<br><p><a><ul><ol><li><b><strong><h1><h2><h3><h4><h5><h6><div><span>');
        $post->lang = 'ua';
        $post->post_status = 'no_published';
        $post->type = '1';
        
        $file = $query->file('image');
        $image = Image::make($file);
        $image->resize(800, 800, function ($constraint) {
            $constraint->aspectRatio();
        });
        $thumbnail_image_name = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME). '_' . rand(10,1000) . '.' .$file->getClientOriginalExtension();
        $image->save(storage_path( 'app/public/posts/shares/' . $thumbnail_image_name));
        $post->image = '/storage/posts/shares/' . $thumbnail_image_name;
        $post->sm_image = '/storage/posts/shares/' . $thumbnail_image_name;
        $post->save();

//        if($query->tags){
//            foreach ($query->tags as $row) {
//                $tags = new PostTags;
//                $tags->post_id = $post->id;
//                $tags->tag_id = $row;
//                $tags->save();
//            }
//        }

        $to = explode(',', env('ADMIN_EMAILS'));
        \Mail::to($to)->send(new ToAdminNewPost($slug, $post->id));

        session()->flash('success', 'Стаття успішно збережена. Після перевірки вона з\'явиться на сайті. Дякуємо');
        return redirect()->back();

    }
    
}
