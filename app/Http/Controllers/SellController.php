<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;
use App\Http\Requests;
use App\MyHelpers\Helper;

class SellController extends Controller
{

    public function post_sell_create() {
    	$user = Auth::user();
    	$city = DB::table('cities')->find($user->city_id);
    	$cats  = DB::table('categories')->get();
    	$city = DB::table('cities')->find($user->city_id);
    	$cats = Helper::cats_tree($cats);
    	return view('front/post_sell_create', ['user'=>$user, 'city'=>$city, 'cats'=>$cats]);
    }

    public function post_study_save(Request $query) {
        //dd($query);
        $user = Auth::user();
        if ($query->teach_text && !is_null($query->teach_tags) ) {

            $teach = new Teach;
            $teach->user_id = $user->id;
            $teach->text = $query->teach_text;
            $teach->text = $query->teach_text;
            $teach->phone = $query->phone;
            $teach->city_id = $query->city_id;
            if ($query->teach_cost_pay && $query->teach_cost_free) {
                $teach->for_pay = 2;
            }elseif($query->teach_cost_pay){
                $teach->for_pay = 1;
            }
            $teach->save();

            //работаем с тегами
            $tags = $query->teach_tags;
            foreach($tags as $tag){
                if (is_numeric($tag)) {

                    $teach_tags = new TeachTags;
                    $teach_tags->tags_id = $tag;
                    $teach_tags->teach_id = $teach->id;
                    $teach_tags->save();
                    
                }else{
                    $save_tags = new Tags;
                    $save_tags->name = $tag;
                    $save_tags->type = 'teach';
                    $save_tags->save();

                    $teach_tags = new TeachTags;
                    $teach_tags->tags_id = $save_tags->id;
                    $teach_tags->teach_id = $teach->id;
                    $teach_tags->save();
                }
            }

           //dd($learn_post);

        }
        if ($query->learn_text && !is_null($query->learn_tags)) {

            $learn = new Learn;
            $learn->user_id = $user->id;
            $learn->text = $query->learn_text;
            $learn->text = $query->learn_text;
            $learn->phone = $query->phone;
            $learn->city_id = $query->city_id;
            if ($query->learn_cost_pay && $query->learn_cost_free) {
                $learn->for_pay = 2;
            }elseif($query->learn_cost_pay){
                $learn->for_pay = 1;
            }
            $learn->save();

            //работаем с тегами
            $tags = $query->learn_tags;
            foreach($tags as $tag){
                if (is_numeric($tag)) {

                    $learn_tags = new LearnTags;
                    $learn_tags->tags_id = $tag;
                    $learn_tags->learn_id = $learn->id;
                    $learn_tags->save();
                    
                }else{
                    $save_tags = new Tags;
                    $save_tags->name = $tag;
                    $save_tags->type = 'learn';
                    $save_tags->save();

                    $learn_tags = new LearnTags;
                    $learn_tags->tags_id = $save_tags->id;
                    $learn_tags->learn_id = $learn->id;
                    $learn_tags->save();
                }
            }
             dd($learn);
        }
        dd('hello');
    	

    	

    	return view('front/post_study_create', ['user'=>$user, 'city'=>$city]);
    }
}
