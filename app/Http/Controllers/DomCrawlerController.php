<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Modules\DomCrawler;
use App\University;
use App\UniversitySubject;
use App\UnivSubject;
use App\CityPl;
use App\Post;
use App\Tags;
use App\PostTags;
use App\UniversitiesLangs;
use App\MyHelpers\Helper;
use Image;
use DB;

class DomCrawlerController extends Controller
{

    public function domain_price($link_def = 'https://freehost.com.ua/reseller/designer/'){

    	$parserClass = new DomCrawler();
		$parserLinks = $parserClass->domain_price($link_def);

		//DB::insert('insert into settings (name, value, type) values (?, ?, ?)', ['qqqq', null, 'domain']);

		//dump($parserLinks);

		foreach ($parserLinks as $key => $value) {
			//dump($value);
			DB::insert('insert into settings (name, value, type) values (?, ?, ?)', [$value['domain'], $value['price'], 'domain']);
		}

    }


    public function education_ua($link_def = 'https://www.education.ua/ua/articles/?page=6'){

		$parserClass = new DomCrawler();

		$parserLinks = $parserClass->education_ua($link_def);

		//dd($parserLinks);

		foreach ($parserLinks as $key => $post_link) {

			//dd($post_link);
			
			$post = $parserClass->education_ua_post('https://www.education.ua'.$post_link);

			// if($key == 0){
			// 	continue;
			// }else{
			// 	dd($post);
			// }
			//dd($post);
			
			$slug = Helper::ru2lat($post['title']);
			$slug = str_replace('---', '-', $slug);
			$slug = str_replace('-', '-', $slug);
			$check_exist_slug = Post::where( 'slug', '=', $slug )->first();
			if($check_exist_slug){
				$slug = $slug . '-' . mt_rand(0,100);
			}

			if($post['image']){

				$filename = null;
				//$post['image'] = strstr($post['image'] , '?', true);
				//dd($post['image']);
				$ext = strrchr($post['image'], '.');
	            $filename = 'post_'.time(). '_'. mt_rand(0,100). $ext;
	            $location = public_path('storage/posts/shares/post_main_images/'.$filename);
	            $img = Image::make('https://www.education.ua'.$post['image']); //->encode('jpg', 50);
	            if($img->width() > 350){
		           	$img->resize(350, null, function ($constraint) {
					    $constraint->aspectRatio();
					});
	            }
	            $img->save($location);   

			}else{
				$filename = null;
			}

			//dd($post['title']);

			$post_new = new Post();
			$post_new->title = $post['title'];
			$post_new->slug = $slug;
			$post_new->excerpt = mb_substr( strip_tags( $post['body'] ), 0, 200 );
			$post_new->content = $post['body'];
			$post_new->post_status = 'published';
			$post_new->type = '1';
			$post_new->sm_image = $filename;
			$post_new->lang = 'ua';
			$post_new->save();

			if($post['tag']){
				$tag = Tags::where('name_ua', '=', $post['tag'])->first();
				if(!$tag){
					$tag = '';
					$tag = new Tags();
					$tag->name_ru = $post['tag'];
					$tag->name_ua = $post['tag'];
					$tag->slug = Helper::ru2lat($post['tag']);
					$tag->save();
				}
			}

			$post_tag = new PostTags();
			$post_tag->post_id = $post_new->id;
			$post_tag->tag_id = $tag->id;
			$post_tag->save();

			//dd($post_new);
				
		}

		dd('Готово');
		
		//return $parserData;
    }

    public function eduget($link_def = 'https://www.eduget.com/uk/news/list?page=48'){

		$parserClass = new DomCrawler();

		$parserLinks = $parserClass->eduget($link_def);

		//dd($parserLinks);

		foreach ($parserLinks as $key => $post_link) {
			
			$post = $parserClass->eduget_post('https://www.eduget.com'.$post_link);
			
			$slug = Helper::ru2lat($post['title']);
			$check_exist_slug = Post::where( 'slug', '=', $slug )->first();
			if($check_exist_slug){
				$slug = $slug . '-' . mt_rand(0,100);
			}

			if($post['image']){

				$filename = null;
				$post['image'] = strstr($post['image'] , '?', true);
				//dd($post['title']);
				$ext = strrchr($post['image'], '.');
	            $filename = 'post_'.time(). '_'. mt_rand(0,100). $ext;
	            $location = public_path('storage/posts/shares/post_main_images/'.$filename);
	            $img = Image::make($post['image']); //->encode('jpg', 50);
	            if($img->width() > 350){
		           	$img->resize(350, null, function ($constraint) {
					    $constraint->aspectRatio();
					});
	            }
	            $img->save($location);   

			}else{
				$filename = null;
			}

			//dd($post['title']);

			$post_new = new Post();
			$post_new->title = $post['title'];
			$post_new->slug = $slug;
			$post_new->excerpt = mb_substr( strip_tags( $post['body'] ), 0, 180 );
			$post_new->content = $post['body'];
			$post_new->post_status = 'published';
			$post_new->type = '1';
			$post_new->sm_image = $filename;
			$post_new->lang = 'ua';
			$post_new->save();

			if($post['tag']){
				$tag = Tags::where('name_ua', '=', $post['tag'])->first();
				if(!$tag){
					$tag = '';
					$tag = new Tags();
					$tag->name_ru = $post['tag'];
					$tag->name_ua = $post['tag'];
					$tag->slug = Helper::ru2lat($post['tag']);
					$tag->save();
				}
			}

			$post_tag = new PostTags();
			$post_tag->post_id = $post_new->id;
			$post_tag->tag_id = $tag->id;
			$post_tag->save();

			//dd($post_new);
				
		}

		dd('Готово');
		
		//return $parserData;
    }

    public function cities($link_def){

		$parserClassCity = new DomCrawler();

		$parser = [];

		$parserData = $parserClassCity->getFromOnePageLinkUniver($parser, $link_def);
		return $parserData;
    }

    public function universities_in_city(){

    	//посмотреть в ручном режиме
    	// до 260 норм, 300, до конца проверить

    	$link_def = '';
    	$page_num = 0;
    	$i = 300;

    	//for ($i=20; $i < 240; $i+=20) { 

    		$link_def = 'https://mojaedukacja.com/universytety/search?per_page='.$i;
    		$cities = $this->cities($link_def);
    		//dd($cities);

    		foreach ($cities['cities'] as $value) {
    			//dd($value);
    			$cities = $this->university($value);
    		}
			//dump($cities);
    		
    	//}

    	dd('finish');

    }

    public function pages_from_city_hand(){

    }

    public function university($link = '/universytety/wnz/wielkopolska-wyzsza-szkola-spoleczno-ekonomiczna', $link_for_ua = ''){

		$link_for_ua = 'https://mojaedukacja.com'.$link;
    	$link = 'https://mojaedukacja.com/ru'.$link;

    	$check_exist_univer = University::where('parse_link', '=', $link)->first();
    	if($check_exist_univer){
    		return(0);
    	}

    	//dd($link);

		$parserClass = new DomCrawler();

		$parser = [

			'title' => 'h1',
			'image' => '.news-item .box figure.thumb img',
			'table_adress' => '.news-item .box ul',
	        'body' => 'article.news-item #info + .page-row',
	        'city' => '.news-item .box ul span[itemprop="addressLocality"]',
	       	'city_all' => '.news-item .box ul span[itemprop="address"] li:nth-child(2) em',
	        'gallery' => 'article.news-item #gallery + .page-row',

		];

		// Make parsing sites from DB. Get parsing data.
		$parserData = $parserClass->getUniversity($parser, $link);

		//dd($parserData['city_all']);

		//save foto gallery
		$univer_gallery = [];
		
		if(!empty($parserData['gallery']['href'])){
			$univer_gallery = null;

			// foreach ($parserData['gallery']['href'] as $item => $gallery_one_link) {
			// 	if($item == 8){
			// 		break;
			// 	}
			// 	$ext = strrchr($gallery_one_link, '.');
	  //           $filename = 'univ_gallery_'.time(). '_'. mt_rand(0,100). $ext;
	  //           $location = public_path('storage/universities/pl/gallery/'.$filename);
	  //           $img = Image::make("https://mojaedukacja.com".$gallery_one_link); //->encode('jpg', 50);
	  //           if($img->width() > 800){
		 //           	$img->resize(800, null, function ($constraint) {
			// 		    $constraint->aspectRatio();
			// 		});
	  //           }
	  //           $img->save($location);
	  //           $univer_gallery[] = $filename;
			// }
			// $univer_gallery = serialize($univer_gallery);
		}else{
			$univer_gallery = null;
		}

		//dd(unserialize($univer_gallery));

		if($parserData['image']){
			$filename = null;

			// $ext = strrchr($parserData['image'], '.');
   //          $filename = 'univ_logo_'.time(). '_'. mt_rand(0,100). $ext;
   //          $location = public_path('storage/universities/pl/'.$filename);
   //          $img = Image::make($parserData['image']); //->encode('jpg', 50);
   //          if($img->width() > 200){
	  //          	$img->resize(200, null, function ($constraint) {
			// 	    $constraint->aspectRatio();
			// 	});
   //          }
   //          $img->save($location);    
		}else{
			$filename = null;
		}

//сохраняем университет
		$bd_univer = new University;
		//алиас
        $alias = Helper::ru2lat($parserData['title']);
        $alias = substr($alias, 0, 55);
        $alias .= '-'.rand(1, 100);
		
		//type
		$type = '';
		if(stristr($parserData['table_adress'], 'государственный') != FALSE) {
			$type = 'state';
		}			  
		if(stristr($parserData['table_adress'], 'частный') != FALSE) {
			$type = 'private';
		}
		// другие данные
		$bd_univer->slug = $alias;
		$bd_univer->title = $parserData['title'];
		$bd_univer->logo = $filename;
		$bd_univer->adress_info = $parserData['table_adress'];
		$bd_univer->description = $parserData['bodies'];
		$bd_univer->gallery = $univer_gallery;
		$bd_univer->city = $parserData['city'];
		$bd_univer->country = 'pl';
		$bd_univer->parse_link = $link;
		$bd_univer->type = $type;
		$bd_univer->save();

		//dump($bd_univer);

		//охраняем город
		$city_exist = CityPl::where('title_pl', '=', $parserData['city'])->first();
		if(!$city_exist){
			$city_save = new CityPl;
			$city_save->title_pl = $parserData['city'];
			$city_save->title_en = $parserData['city'];
			$city_save->title_ru = $parserData['city_all'];
		}else{
			$city_save = null;
		}

//сохраняем данные на українській мові
		$parserClassUa = new DomCrawler();

		$parserUa = [

			'title' => 'h1',
			'image' => '.news-item .box figure.thumb img',
			'table_adress' => '.news-item .box ul',
	        'body' => 'article.news-item #info + .page-row',
	        'city' => '.news-item .box ul span[itemprop="addressLocality"]',
	        'city_all' => '.news-item .box ul span[itemprop="address"] li:nth-child(2) em',
	        'gallery' => 'article.news-item #gallery + .page-row',

		];

		//Make parsing sites from DB. Get parsing data.
		$parserDataUa = $parserClassUa->getUniversity($parserUa, $link_for_ua);

		//dd($link_for_ua);

		if($bd_univer->id){
			$bd_univer_ua = new UniversitiesLangs;
			$bd_univer_ua->universities_id = $bd_univer->id;
			$bd_univer_ua->lang = "ua";
			$bd_univer_ua->title = $parserDataUa['title'];
			$bd_univer_ua->adress_info = $parserDataUa['table_adress'];
			$bd_univer_ua->description = $parserDataUa['bodies'];
			$bd_univer_ua->country = "pl";
			$bd_univer_ua->save();
		}
		if($city_save){
			$city_save->title_ua = $parserDataUa['city_all'];
			$city_save->save();
		}

    }

    public function fix_univer_table(){


    	$posts = Post::get();
    	foreach ($posts as $key => $post) {
    		$post->image = str_replace('/storage/posts/shares/post_main_images/', '', $post->image);
    		$post->sm_image = str_replace('/storage/posts/shares/post_main_images/', '', $post->sm_image);
    		$post->save();
    	}
    	dd('hi');


    	/*Выбрать чтото из таблицы и вставить в оддельную ячейку*/

    	//$univers = University::get();
    	// foreach ($univers as $univer) {
    	// 	  $string = $univer->adress_info;
			  // if(stristr($string, 'государственный') != FALSE) {
			  //   $univer->type = 'state';
			  //   $univer->save();
			  // }			  
			  // if(stristr($string, 'частный') != FALSE) {
			  //   $univer->type = 'private';
			  //   $univer->save();
			  // }
    	// }

    	// $exit = '123456 erwe erwer 321 "/ua/c-ua/universities/Oleksandriya" 123 wwrwrwrwertert 123423423';
    	// $exit = preg_replace('/(\/ua\/c-ua([^"]{1,}))/', "$1"."/all", $exit);

    	// dd($exit);

    	// удалить тег
    	$univers = UniversitiesLangs::leftJoin('universities', 'universities_langs.universities_id', '=', 'universities.id')
    		->select('universities.city','universities.id', 'universities_langs.universities_id', 'universities_langs.adress_info')
    		->get();

    	
    	foreach ($univers as $univer) {
    		
    		  $string = $univer->adress_info;
    		 // $string_1 = str_replace(array('/cities/'), '/', $string);
    		  $string = preg_replace('/(\/c-ua([^"]{1,}))/', "$1"."/all", $string);
    		  //$string = $string . '/all';
    		  $univer->adress_info = $string;
			  $univer->save();   

			  //dump($univer);
			    		
    	// 	  $string = $univer->adress_info;
    	// 	  $string = str_replace(array('/ua/c-ua/universities/cities/'), '/ua/c-ua/universities/cities/'.$univer->city , $string);
    	// 	  $univer->adress_info = $string;
			  // $univer->save();  

    		// поиск параметра юрл 
			/*  $string = $univer->adress_info;
    		  $string = preg_replace('/(\?city=)[0-9]{1,100}/', '', $string);
    		  $univer->adress_info = $string;
			  $univer->save(); */

    	}

    }

}
