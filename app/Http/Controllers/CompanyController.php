<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App;
use App\User;
use Auth;
use Image;
use Illuminate\Support\Facades\Input;
use App\Courses;
use App\School;
use App\CompanyCoursGroup;
use App\CompanyCourses;
use App\CompanyCoursesLang;
use App\CompaniesLang;
use App\CompanyCoursesCatRel;
use App\MyHelpers\Helper;
use Session;

class CompanyController extends Controller
{
   //get страница регистрации конпании
    public function company_register() {

        $courses_group = CompanyCoursGroup::all();
        return view('front/company/create', ['courses_group' => $courses_group ]);

    }

    // сохранение при регистрации компании
    public function company_register_save(Request $query) {
        
        $this->validate($query, [
            // 'adress' => 'required',
            // 'phone' => 'required',
            // 'country' => 'required',
            // 'contact_name' => 'required',
        ]);
        
        	$user = Auth::user();

            if($query->company_type == 'cours'){

                $note = new Courses;
                $note->name = $query->company_name; 
                $note->user_id = $user->id; 
                $note->country = $query->company_country;
                $note->city = $query->company_city;
                $note->price = $query->price;
                if($query->price_free == 'yes'){
                    $note->price = '01';
                }
                $note->site = $query->site;
                $note->soc_page = $query->soc_page;
                $note->phone = $query->phone;
                $note->description = $query->company_desc; 
                $note->street = $query->company_street;
                //алиас
                $alias = Helper::ru2lat($query->company_name);
                $alias = substr($alias, 0, 35);
                $note->alias = $alias . '-' . mt_rand(0,500);
                $note->save();

                //сохраняем категории курсов
                foreach ($query->course_cat as $cat_id) {
                    $cours_cat = new CompanyCoursesCatRel;
                    $cours_cat->course_id = $note->id;
                    $cours_cat->cat_id = $cat_id;
                    $cours_cat->save();
                }
            }

            if($query->company_type == 'school'){

                $note = new School;
                $note->name = $query->company_name; 
                $note->user_id = $user->id; 
                $note->country = $query->company_country;
                $note->city = $query->company_city;
                $note->price = $query->price;
                if($query->price_free == 'yes'){
                    $note->price = '01';
                }
                $note->site = $query->site;
                $note->soc_page = $query->soc_page;
                $note->phone = $query->phone;
                $note->description = $query->company_desc; 
                $note->street = $query->company_street;
                if($query->school_class){
                    $note->class = implode(",", $query->school_class);
                }
                //алиас
                $alias = Helper::ru2lat($query->company_name);
                $alias = substr($alias, 0, 35);
                $note->alias = $alias . '-' . mt_rand(0,500);
                $note->save();

            }

			//Сохраняем логотип     
	        if (Input::hasFile('company_logo')) {

                $filename = $note->id .'_company_'.time(). '_'. mt_rand(0,100). '.jpg';
                $location = public_path('storage/companies/logos/'.$filename);
                $img = Image::make($_FILES['company_logo']['tmp_name']);
                if($img->width() > 180){
                    $img->resize(180, null, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                }
                $img->save($location);   
                $note->logo = $filename;
                $note->save();

	        }

            //сохраняем галерею
            if (Input::hasFile('filenames')) {

                $gallery = [];

                foreach (Input::file('filenames') as $item) {
                    $filename  = $note->id . '_company_' .time(). '_'. mt_rand(0,100) . '.' . $item->getClientOriginalExtension();
                    $location = public_path('storage/companies/gallery/'.$filename);
                    $img = Image::make($item->getRealPath());
                    if($img->width() > 1100){
                        $img->resize(1100, null, function ($constraint) {
                            $constraint->aspectRatio();
                        });
                    }
                    $img->save($location);   
                    $gallery[] = $filename;

                }

                $gallery = serialize($gallery);
                $note->gallery = $gallery;
                $note->save();

            }

            session()->flash('success', 'Компания "' . $query->company_name . '" успешно сохранена. После проверки она появится на сайте в поиске.');
        	return redirect()->route('company_all_my_notes');


    }

    public function editProfile() {

        return view('front/company/edit_profile_info');
    }

    public function updateProfile(Request $query) {
        
        //dd($query);
        $this->validate($query, [
            'name' => 'required',
            'email' => 'required'
        ]);
        
        //выбираем пользователя
        $user = Auth::user();

        //Сохраняем логотип       
        if ($query->image_crop) {

            if ($user->avatar != '') {
                $img = public_path('storage/users/'.$user->avatar);
                unlink($img);
            }
            
            $image_crop = $query->image_crop; // Your data 'data:image/png;base64,AAAFBfj42Pj4';
            $image_crop = str_replace('data:image/jpeg;base64,', '', $image_crop);
            $image_crop = str_replace(' ', '+', $image_crop);
            $image_crop_good = base64_decode($image_crop);
            $image_crop_good = $image_crop;
            $filename = 'crop_'.time(). '_'. mt_rand(0,100). '.jpg';
            $location = public_path('storage/users/'.$filename);

            $img = Image::make($image_crop_good); //->encode('jpg', 50);
            $img->save($location);  

            $user->avatar = $filename;
            $user->save();

        }
        
        //обновляем профиль учителя
        $user->name = $query->name;
        $user->email = $query->email;
        $user->save();

        Session::flash('success', 'Ваш профиль обновлен!');

        return redirect()->back();
    }

    public function companyAllMyNotes() {

        $user = Auth::user();
        $courses = Courses::where('user_id', '=', $user->id)->get();
        $courses->map(function ($cours) {
            $cours->type = 'cours';
            return $cours;
        });
        $schools = School::where('user_id', '=', $user->id)->get();
        $schools->map(function ($school) {
            $school->type = 'school';
            return $school;
        });
        $notes = $courses->merge($schools);
        $notes = $notes->sortByDesc('created_at');
        return view('front/company/notes_all', ['notes' => $notes]);
    }

    public function courseEdit($id) {

        $courses_group = CompanyCoursGroup::all();
        $note = Courses::where('id', '=', $id)->first();
        $note_cat = CompanyCoursesCatRel::where('course_id', '=', $note->id)->select('cat_id')->get();
        $cat_ids = [];
        foreach ($note_cat as $cat) {
            $cat_ids[] = $cat->cat_id;
        }
        $gallery = unserialize($note->gallery);
        return view('front/company/edit_course', ['courses_group' => $courses_group, "note" => $note, "cat_ids" => $cat_ids, "gallery" => $gallery ]);

    }

    public function courseEditSave(Request $query) {

        $this->validate($query, [
            // 'adress' => 'required',
            // 'phone' => 'required',
            // 'country' => 'required',
            // 'contact_name' => 'required',
        ]);

        $id = $query->note_id;

        $note = Courses::find($id);
        $note->name = $query->company_name; 
        $note->country = $query->company_country;
        $note->city = $query->company_city;
        $note->site = $query->site;
        $note->soc_page = $query->soc_page;
        $note->phone = $query->phone;
        $note->description = $query->company_desc; 
        $note->price = $query->price;
        if($query->price_free == 'yes'){
            $note->price = '01';
        }
        $note->street = $query->company_street;
        $note->save();

        //удаляем старые категории
        CompanyCoursesCatRel::where('course_id','=', $id)->delete();
        //сохраняем новые категории курсов
        foreach ($query->course_cat as $cat_id) {
            $cours_cat = new CompanyCoursesCatRel;
            $cours_cat->course_id = $note->id;
            $cours_cat->cat_id = $cat_id;
            $cours_cat->save();
        }

        //Сохраняем новий логотип     
        if (Input::hasFile('company_logo')) {

            //удаляем старый логотип 
            if ($note->logo != '') {
                $img_del = public_path('storage/companies/logos/'.$note->logo);
                if(file_exists($img_del)){
                    unlink($img_del);
                }
            }

            $filename = $note->id . '_company_logo_'.time(). '_'. mt_rand(0,100). '.jpg';
            $location = public_path('storage/companies/logos/'.$filename);
            $img = Image::make($_FILES['company_logo']['tmp_name']);
            if($img->width() > 180){
                $img->resize(180, null, function ($constraint) {
                    $constraint->aspectRatio();
                });
            }
            $img->save($location);   
            $note->logo = $filename;
            $note->save();

        }

        //работаем с галереей
        $img_gallery_to_delete = [];    

        if($note->gallery != null){
            $gallery = unserialize($note->gallery);
        }else{
            $gallery = null;
        }

        if (Input::hasFile('filenames')) {
            if($gallery == null){
                $gallery = [];
            }
            foreach (Input::file('filenames') as $index => $item) {
                $filename  = $note->id . '_company_' .time(). '_'. mt_rand(0,100) . '.' . $item->getClientOriginalExtension();
                $location = public_path('storage/companies/gallery/'.$filename);
                $img = Image::make($item->getRealPath());
                if($img->width() > 1100){
                    $img->resize(1100, null, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                }
                if($img->height() > 1100){
                    $img->resize(null, 1100, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                }
                $img->save($location);   
                $gallery[] = $filename;
                //удаляем если изменили фото
                $pos = strripos($index, 'company');
                if($pos === false){

                }else{
                    $img = public_path('storage/companies/gallery/'.$index);
                    if(file_exists($img)){
                        unlink($img);
                    }
                    $img_gallery_to_delete[] = $index;
                }
            }
        }

        if(count($img_gallery_to_delete) > 0){
            foreach ($img_gallery_to_delete as $delete) {
                if(($key = array_search($delete,$gallery)) !== FALSE){
                     unset($gallery[$key]);
                }
            }

        }

        //удаляем картинки галереи после нажатия хрестика
       // dd($query->delete_file);
        if (isset($query->delete_file)) {
            foreach ($query->delete_file as $delete) {
                if(($key = array_search($delete,$gallery)) !== FALSE){
                     unset($gallery[$key]);
                }
                $img = public_path('storage/companies/gallery/'.$delete);
                if(file_exists($img)){
                    unlink($img);
                }
            }
        }

        if($gallery != null){
            $gallery = serialize($gallery);
            $note->gallery = $gallery;
            $note->save();
        }
        //работа с галереей закончена


        session()->flash('success', 'Изменения сохранены');
        return redirect()->back();

    }
    
    public function scloolEdit($id) {

        $note = School::where('id', '=', $id)->first();
        $class = explode(',',$note->class);
        $gallery = unserialize($note->gallery);
        return view('front/company/edit_school', ["note" => $note, "class" => $class, "gallery" => $gallery ]);

    }

    public function schoolEditSave(Request $query) {

        $this->validate($query, [
            // 'adress' => 'required',
            // 'phone' => 'required',
            // 'country' => 'required',
            // 'contact_name' => 'required',
        ]);

        $id = $query->note_id;

        $note = School::find($id);
        $note->name = $query->company_name; 
        $note->country = $query->company_country;
        $note->city = $query->company_city;
        $note->site = $query->site;
        $note->soc_page = $query->soc_page;
        $note->phone = $query->phone;
        $note->description = $query->company_desc; 
        $note->price = $query->price;
        if($query->price_free == 'yes'){
            $note->price = '01';
        }
        $note->street = $query->company_street;
        if($query->school_class){
            $note->class = implode(",", $query->school_class);
        }
        $note->save();

        //Сохраняем новий логотип     
        if (Input::hasFile('company_logo')) {

            //удаляем старый логотип 
            if ($note->logo != '') {
                $img_del = public_path('storage/companies/logos/'.$note->logo);
                if(file_exists($img_del)){
                    unlink($img_del);
                }
            }

            $filename = $note->id . '_company_logo_'.time(). '_'. mt_rand(0,100). '.jpg';
            $location = public_path('storage/companies/logos/'.$filename);
            $img = Image::make($_FILES['company_logo']['tmp_name']);
            if($img->width() > 180){
                $img->resize(180, null, function ($constraint) {
                    $constraint->aspectRatio();
                });
            }
            $img->save($location);   
            $note->logo = $filename;
            $note->save();

        }

        //работаем с галереей
        $img_gallery_to_delete = [];    

        if($note->gallery != null){
            $gallery = unserialize($note->gallery);
        }else{
            $gallery = null;
        }

        if (Input::hasFile('filenames')) {
            if($gallery == null){
                $gallery = [];
            }
            foreach (Input::file('filenames') as $index => $item) {
                $filename  = 'company_' .time(). '_'. mt_rand(0,100) . '.' . $item->getClientOriginalExtension();
                $location = public_path('storage/companies/gallery/'.$filename);
                $img = Image::make($item->getRealPath());
                if($img->width() > 1100){
                    $img->resize(1100, null, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                }
                $img->save($location);   
                $gallery[] = $filename;
                //удаляем если изменили фото
                $pos = strripos($index, 'company');
                if($pos === false){

                }else{
                    $img = public_path('storage/companies/gallery/'.$index);
                    if(file_exists($img)){
                        unlink($img);
                    }
                    $img_gallery_to_delete[] = $index;
                }
            }
        }

        if(count($img_gallery_to_delete) > 0){
            foreach ($img_gallery_to_delete as $delete) {
                if(($key = array_search($delete,$gallery)) !== FALSE){
                     unset($gallery[$key]);
                }
            }

        }

        //удаляем картинки галереи после нажатия хрестика
        if (isset($query->delete_file)) {
            foreach ($query->delete_file as $delete) {
                if(($key = array_search($delete,$gallery)) !== FALSE){
                     unset($gallery[$key]);
                }
                $img = public_path('storage/companies/gallery/'.$delete);
                if(file_exists($img)){
                    unlink($img);
                }
            }
        }

        if($gallery != null){
            $gallery = serialize($gallery);
            $note->gallery = $gallery;
            $note->save();
        }
        //работа с галереей закончена

        session()->flash('success', 'Изменения сохранены');
        return redirect()->back();

    }
    public function one_company_vip($type, $id) {
        if($type == 'all' && $id == 'all'){
            $company_name = '';
        }else{
            if($type == 'cours'){
                $note = Courses::where('id', '=', $id)->first();
            }elseif($type == 'school'){
                $note = School::where('id', '=', $id)->first();
            }
            $company_name = $note->name;
        }

        return view('front/company/promotion_one', ['company_name' => $company_name]);

    }










    //get обновление компании
    public function company_update() {

        $contries = DB::table('countries')->orderBy('name_ru', 'asc')->get(); 
        $user = Auth::user();
        $company = Courses::where('user_id', '=', $user->id)->first();

        //находим город и пересохраняем город
        $company_city = DB::table('cities_'.$company->country )->where('title_en', '=', $company->city)->first();
        if($company_city){
            $city = $company_city;
        }else{
            $city = null;
        }

        return view('front/company/update', ['contries' => $contries, 'company' => $company, 'city' => $city]);

    }

    // сохранение измененных данных компании
    public function company_update_save(Request $query) {
        
        $this->validate($query, [
        	'email' => 'required',
            // 'adress' => 'required',
            // 'phone' => 'required',
            // 'country' => 'required',
            // 'contact_name' => 'required',
        ]);

        $user = Auth::user();

        $company = Company::where('user_id', '=', $user->id)->first();
        $company->country = $query->country;
        $company->site = $query->site;
        $company->city = $query->city;
        $company->phone = $query->phone;
        $company->name_def = $query->lang_1['name']; 
        $company->desc_def = $query->lang_1['desc'];
        $company->adress_def = $query->lang_1['adress'];
        //$company->contact_name_def = $query->lang_1['contact_name'];
        $company->save();

        //сохраняем ререводы на разных языках
        if($company->id){

            //сохраняем второй язык
            if(isset($query->lang_2) && isset($query->lan_code_2)){
                if ($query->lang_2['name'] != null && $query->lang_2['desc'] != null) {

                    $company_lang_2 = CompaniesLang::where('company_id', '=', $company->id)->where('lang', '=', $query->lan_code_2)->first();

                    if($company_lang_2){

                        $company_lang_2->lang = $query->lan_code_2;
                        $company_lang_2->company_name = $query->lang_2['name'];
                        $company_lang_2->company_desc = $query->lang_2['desc'];
                        $company_lang_2->company_adress = $query->lang_2['adress'];
                        //$company_lang_2->company_contact_name = $query->lang_2['contact_name'];
                        $company_lang_2->save();

                    }else{
                        
                        $company_lang_2 = new CompaniesLang;
                        $company_lang_2->company_id = $company->id;
                        $company_lang_2->lang = $query->lan_code_2;
                        $company_lang_2->company_name = $query->lang_2['name'];
                        $company_lang_2->company_desc = $query->lang_2['desc'];
                        $company_lang_2->company_adress = $query->lang_2['adress'];
                        //$company_lang_2->company_contact_name = $query->lang_2['contact_name'];
                        $company_lang_2->save();

                    }

                }
            }

            //сохраняем третий язык
            if(isset($query->lang_3) && isset($query->lan_code_3)){
                if ($query->lang_3['name'] != null && $query->lang_3['desc'] != null) {

                    $company_lang_3 = CompaniesLang::where('company_id', '=', $company->id)->where('lang', '=', $query->lan_code_3)->first();

                    if($company_lang_3){

                        $company_lang_3->lang = $query->lan_code_3;
                        $company_lang_3->company_name = $query->lang_3['name'];
                        $company_lang_3->company_desc = $query->lang_3['desc'];
                        $company_lang_3->company_adress = $query->lang_3['adress'];
                        //$company_lang_3->company_contact_name = $query->lang_3['contact_name'];
                        $company_lang_3->save();

                    }else{
                        
                        $company_lang_3 = new CompaniesLang;
                        $company_lang_3->company_id = $company->id;
                        $company_lang_3->lang = $query->lan_code_3;
                        $company_lang_3->company_name = $query->lang_3['name'];
                        $company_lang_3->company_desc = $query->lang_3['desc'];
                        $company_lang_3->company_adress = $query->lang_3['adress'];
                       // $company_lang_3->company_contact_name = $query->lang_3['contact_name'];
                        $company_lang_3->save();

                    }
                }
            }
        }


		//Работаем с изображение кадрированным
        if (Input::hasFile('company_logo')) {

            if ($user->avatar != '') {
                $img = public_path('storage/companies/logos/'.$user->avatar);
                if(file_exists($img)){
                    unlink($img);
                }
            }

            $filename = 'company_logo_'.time(). '_'. mt_rand(0,100). '.jpg';
            $location = public_path('storage/companies/logos/'.$filename);
            $img = Image::make($_FILES['company_logo']['tmp_name']);
            $img->resize(180, null, function ($constraint) {
                $constraint->aspectRatio();
            });
            $img->save($location);   
            $user->avatar = $filename;
            $user->save();

        }
        
        session()->flash('success', 'Информация о компании обновлена. Изменения сохранены!');
        return redirect()->back();
        
    	//return redirect()->route('tutor_account_main_info');

    }

    public function course_new() {

        $user = Auth::user();
        $lang_codes = [];
        $lang_codes = explode(",", $user->langs);
        $lang_def = $user->lang_def;
        array_unshift($lang_codes, $lang_def);
        $langs = [];
        foreach ($lang_codes as $code) {
            $langs[$code] = '1';
        }

        ///dd($langs);

        $courses_group = CompanyCoursGroup::all();
        return view('front/courses/create', ['courses_group' => $courses_group, 'langs' => $langs]);

    }
    public function course_new_save(Request $query) {

      //dump($query->lan_code_1);

      $this->validate($query, [
            //'email' => 'required|string|email|max:255|unique:users',
            //'password' => 'required',
            // 'adress' => 'required',
            // 'phone' => 'required',
            // 'country' => 'required',
            // 'contact_name' => 'required',
        ]);

        $user = Auth::user();
        //получаем данные компании
        $company = Company::where('user_id', '=', $user->id)->first();
        //сохраняем возраст учеников в таблицу
        $pupil_age = implode(',', $query->pupil_age );

        $course = new CompanyCourses;
        $course->company_id = $company->id; 
        $course->pupil_age = $pupil_age; 
        $course->price = $query->price_value;
        $course->type = $query->type;
        $course->currency = $query->price_valute;
        if($query->status == "active"){
            $course->active = '1';
        }elseif($query->status == "no_active"){
            $course->active = '0';
        }
        if($query->price_free != null){
            $course->free = '1';
            $course->price = null;
        }
        //алиас
        if($query->lang_1['name'] != null){
            $alias = Helper::ru2lat($query->lang_1['name']);
        }elseif($query->lang_2['name'] != null){
            $alias = Helper::ru2lat($query->lang_2['name']);
        }else{
            $alias = Helper::ru2lat($query->lang_2['name']);
        }
        $alias = substr($alias, 0, 35);
        $course->alias = $alias . '-' . mt_rand(0,500);
        $course->course_name = $query->lang_1['name']; 
        //$course->course_desc = preg_replace( '/style=(["\'])[^\1]*?\1/i', '', $query->lang_1['desc'], -1 );
        $course->course_desc = $query->lang_1['desc'];
        $course->save();

        //сохраняем название, описание на разных языках
        if($course->id){

            //сохраняем второй язык
            if(isset($query->lang_2) && isset($query->lan_code_2)){
                if ($query->lang_2['name'] != null && $query->lang_2['desc'] != null) {
                    $course_lang_2 = new CompanyCoursesLang;
                    $course_lang_2->course_id = $course->id;
                    $course_lang_2->lang = $query->lan_code_2;
                    $course_lang_2->course_name = $query->lang_2['name'];
                    //$course_lang_2->course_desc = preg_replace( '/style=(["\'])[^\1]*?\1/i', '', $query->lang_2['desc'], -1 );
                    $course_lang_2->course_desc = $query->lang_2['desc'];
                    $course_lang_2->save();
                }
            }

            //сохраняем третий язык
            if(isset($query->lang_3) && isset($query->lan_code_3)){
                if ($query->lang_3['name'] != null && $query->lang_3['desc'] != null) {
                    $course_lang_3 = new CompanyCoursesLang;
                    $course_lang_3->course_id = $course->id;
                    $course_lang_3->lang = $query->lan_code_3;
                    $course_lang_3->course_name = $query->lang_3['name'];
                    //$course_lang_3->course_desc = preg_replace( '/style=(["\'])[^\1]*?\1/i', '', $query->lang_3['desc'], -1 );
                    $course_lang_3->course_desc = $query->lang_3['desc'];
                    $course_lang_3->save();
                }
            }

        }

        //return redirect()->back();
        session()->flash('success', 'Курс "'.$course->course_name.'" успешно создан! ');
        return redirect()->route('courses_all');

    }

    public function course_edit_save(Request $query) {


      $this->validate($query, [
            //'email' => 'required|string|email|max:255|unique:users',
            //'password' => 'required',
            // 'adress' => 'required',
            // 'phone' => 'required',
            // 'country' => 'required',
            // 'contact_name' => 'required',
        ]);

        //strip_tags($query->lang_1['desc'], '<p><a><h2><ol><ul><li><em><strong>');

        //dd($query);

        $user = Auth::user();
        //получаем данные компании
        $company = Company::where('user_id', '=', $user->id)->first();
        //сохраняем возраст учеников в таблицу
        $pupil_age = implode(',', $query->pupil_age );

        $course = CompanyCourses::where('id', '=', $query->course)->where('company_id', '=', $company->id)->first();
        $course->pupil_age = $pupil_age; 
        $course->price = $query->price_value;
        $course->currency = $query->price_valute;
        $course->type = $query->type;
        if($query->price_free != null){
            $course->free = '1';
            $course->price = null;
        }else{
            $course->free = '0';
        }
        if($query->status == "active"){
            $course->active = '1';
        }elseif($query->status == "no_active"){
            $course->active = '0';
        }

        //сохраняем первый язык
        if ($query->lang_1['name'] != null && $query->lang_1['desc'] != null) {
            $course->course_name = $query->lang_1['name'];
           // $course->course_desc = preg_replace( '/style=(["\'])[^\1]*?\1/i', '', $query->lang_1['desc'], -1 );
            $course->course_desc = $query->lang_1['desc'];
        }else{

            if ($query->lang_2['name'] != null && $query->lang_2['desc'] != null) {
                $course->course_name = $query->lang_2['name'];
                //$course->course_desc = preg_replace( '/style=(["\'])[^\1]*?\1/i', '', $query->lang_2['desc'], -1 );
                $course->course_desc = $query->lang_2;

            }else{

                if ($query->lang_1['name'] != null && $query->lang_1['desc'] != null) {
                    $course->course_name = $query->lang_1['name'];
                    //$course->course_desc = preg_replace( '/style=(["\'])[^\1]*?\1/i', '', $query->lang_1['desc'], -1 );
                    $course->course_desc = $query->lang_1['desc'];
                }
            }
        }

        $course->save();

        //сохраняем название, описание на других языках
        if($course->id){

            //сохраняем второй язык
            if(isset($query->lang_2) && isset($query->lan_code_2)){
                if ($query->lang_2['name'] != null && $query->lang_2['desc'] != null) {

                    $course_lang_2 = CompanyCoursesLang::where('course_id', '=', $course->id)->where('lang', '=', $query->lan_code_2)->first();

                    if($course_lang_2){
                        $course_lang_2->lang = $query->lan_code_2;
                        $course_lang_2->course_name = $query->lang_2['name'];
                        //$course_lang_2->course_desc = preg_replace( '/style=(["\'])[^\1]*?\1/i', '', $query->lang_2['desc'], -1 );
                        $course_lang_2->course_desc = $query->lang_2['desc'];
                        $course_lang_2->save();
                    }else{
                        $course_lang_2 = new CompanyCoursesLang;
                        $course_lang_2->course_id = $course->id;
                        $course_lang_2->lang = $query->lan_code_2;
                        $course_lang_2->course_name = $query->lang_2['name'];
                        $course_lang_2->course_desc = $query->lang_2['desc'];
                        $course_lang_2->course_desc = $query->lang_2['desc'];
                        $course_lang_2->save();
                    }
                }
            }

            //сохраняем третий язык
            if(isset($query->lang_3) && isset($query->lan_code_3)){
                if ($query->lang_3['name'] != null && $query->lang_3['desc'] != null) {

                    $course_lang_3 = CompanyCoursesLang::where('course_id', '=', $course->id)->where('lang', '=', $query->lan_code_3)->first();

                    if($course_lang_3){
                        $course_lang_3->lang = $query->lan_code_3;
                        $course_lang_3->course_name = $query->lang_3['name'];
                        $course_lang_3->course_desc = $query->lang_3['desc'];
                        $course_lang_3->save();
                    }else{
                        $course_lang_3 = new CompanyCoursesLang;
                        $course_lang_3->course_id = $course->id;
                        $course_lang_3->lang = $query->lan_code_3;
                        $course_lang_3->course_name = $query->lang_3['name'];
                        $course_lang_3->course_desc = $query->lang_3['desc'];
                        $course_lang_3->save();
                    }

                }
            }

        }

        //если изменился базовый язык то удаляем из таблицы переодов дублирующие записи на этом языке
        $course_lang_1_duble = CompanyCoursesLang::where('course_id', '=', $course->id)->where('lang', '=', $user->lang_def)->first();
        if($course_lang_1_duble != null){
            $course_lang_1_duble->delete();
        }


        //обновляем категории курсов
        $courses = CompanyCoursesCatRel::where('course_id', '=', $course->id)->delete();
        foreach ($query->course_cat as $cat_id) {
            $courses = new CompanyCoursesCatRel;
            $courses->course_id = $course->id;
            $courses->cat_id = $cat_id;
            $courses->save();
        }




        //Сохраняем логотип     
        // if ($query->image_crop) {
            
        //     $image_crop = $query->image_crop; // Your data 'data:image/png;base64,AAAFBfj42Pj4';
        //     $image_crop = str_replace('data:image/jpeg;base64,', '', $image_crop);
        //     $image_crop = str_replace(' ', '+', $image_crop);
        //     $image_crop_good = base64_decode($image_crop);
        //     $image_crop_good = $image_crop;
        //     $filename = 'company_logo_'.time(). '_'. mt_rand(0,100). '.jpg';
        //     $location = public_path('storage/companies/logos/'.$filename);

        //     $img = Image::make($image_crop_good); //->encode('jpg', 50);
        //     $img->save($location);   
        //     $user->avatar = $filename;
        //     $user->save();

        // }

        //return redirect()->back();
        session()->flash('success', 'Курс отредактирован, изменения сохранены!');
        return redirect()->back();
        return redirect()->route('courses_all');

    }

    public function company_one_show($alias) {

        $local = App::getLocale();
        $title_loc = "title_".$local;
        $company_local = [];
        $company = Company::where('alias', '=', $alias)->first();
        $user = User::where('id', '=', $company->user_id)->first();
        $company_local['lang_def'] = $user->lang_def;
        $courses = CompanyCourses::
        where('active', '=', '1')
        ->leftJoin('companies', 'companies.id',  '=', 'company_courses.company_id')
        ->leftJoin('users', 'users.id',  '=', 'companies.user_id')
        ->where('company_id', '=', $company->id)
        ->orderBy('company_courses.updated_at', 'desc')
        ->select(
            'company_courses.id As id', 
            'course_name', 
            'pupil_age', 
            'price', 
            'currency', 
            'company_courses.alias As alias', 
            'lang_def',
            'companies.country'
        );
        $courses = $courses->get();  

        //dd($company);

        if($company->city){
            if( $company->country == 'ru' ){
                if(isset($company->city_ru->{$title_loc})){
                    $company_local['city'] = $company->city_ru->{$title_loc};
                }
            }elseif( $company->country == 'ua'){
                if(isset($company->city_ua->{$title_loc})){
                    $company_local['city'] = $company->city_ua->{$title_loc};
                }
            }elseif( $company->country == 'kz'){
                if(isset($company->city_kz->{$title_loc})){
                    $company_local['city'] = $company->city_kz->{$title_loc};
                }
            }    
        }else{
            $company_local['city'] = null;
        } 

        //название и описание компании на разных языках
        if($user->lang_def == $local){
           $company_local['name'] = $company->name_def;
           $company_local['desc'] = $company->desc_def;
           $company_local['adress'] = $company->adress_def;
          // $company_local['contact_name'] = $company->contact_name_def;
        }else{
            if($company->company_langs){
                $company_local['name'] = $company->company_langs->company_name;
                $company_local['desc'] = $company->company_langs->company_desc;
                $company_local['adress'] = $company->company_langs->company_adress;
               // $company_local['contact_name'] = $company->company_langs->company_contact_name;
            }else{
                $company_local['name'] = $company->name_def;
                $company_local['desc'] = $company->desc_def;
                $company_local['adress'] = $company->adress_def;
               // $company_local['contact_name'] = $company->contact_name_def;
            }

        }
            

        //dd($cours);

        return view('front/company/show_one', ['courses'=>$courses, 'local' => $local, 'company_local' => $company_local, 'company' => $company, 'user' => $user]);

    }
}
