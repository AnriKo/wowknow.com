<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Conversation;
use App\User;
use App\ConversationReply;
use Auth;
use DB;

class ConversationController extends Controller
{
    
        
    public function messageStore(Request $query)
    {
        $user_id = Auth::user()->id;
        $recipient_id = $query->recipient_id;
        if(!Auth::check()){
            return 'login please';
        }

        $conversation = Conversation::where('sender_id', '=', $user_id)
            ->where('recipient_id', '=', $recipient_id)
            ->orWhere(function ($query) use ( $user_id, $recipient_id ) {
                $query->where('recipient_id', '=', $user_id)
                        ->where('sender_id', '=', $recipient_id);
            })
            ->first();

        if($conversation == null ){

            $conversation = new Conversation;
            $conversation->sender_id = $user_id;
            $conversation->recipient_id = $recipient_id;
            $conversation->save();

        }

        $conversation_reply = new ConversationReply;
        $conversation_reply->reply = $query->message;
        $conversation_reply->user_id = $user_id;
        $conversation_reply->recipient_user_id = $recipient_id;
        $conversation_reply->c_id = $conversation->id;
        $conversation_reply->status = '0';
        $conversation_reply->save();
        
        return '<i class="fa fa-check-circle-o" aria-hidden="true"></i>Сообщение отправлено.';

    }        

    public function showConversation($id){

        $user_id = Auth::user()->id;
        $friend = '';

        $conversation = Conversation::find($id);
        
        if ($conversation->sender_id == $user_id) {
            $friend = $conversation->recipient_id;
        }elseif ($conversation->recipient_id == $user_id) {
            $friend = $conversation->sender_id;
        }

        $friend = User::find($friend);

        $messages = ConversationReply::where('c_id', '=', $id)
            ->get();

        $params = [
              'messages' => $messages,
              'conv_id' => $id,
              'friend_name' => $friend->name,
              'friend_avatar' => $friend->avatar,
              'friend_id' => $friend->id,
              'friend_type' => $friend->type,
              'friend' => $friend,
        ];

        return view('front/user_account/show_conversation' )->with($params);

    }

    public function messageNew(Request $query){

        $message = new ConversationReply;
        $message->reply = $query->reply;
        $message->user_id = Auth::user()->id;
        $message->c_id = $query->conv_id;
        $message->recipient_user_id = $query->recip;
        $message->save();

        return back();

    }

    public function show_all_conversations(){

        $messages = Conversation::where('sender_id', '=', Auth::user()->id)
            ->orWhere('recipient_id', '=', Auth::user()->id)
            ->get();

        $params = [
              'messages' => $messages,
        ];

        return view('front/user_account/user_all_conversations' )->with($params);
    }

    public function read_conversation(Request $query){

        $messages = ConversationReply::where('c_id', '=', $query->conv)
            ->where('user_id', '=', $query->rec_id)
            ->where('status', '=', '0')
            ->get();

        foreach ($messages as $message) {
                $message->status = '1';
                $message->save();
            }    

        return $messages;

    }
}
