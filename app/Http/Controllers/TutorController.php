<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\User;
use App\Tags;
use App\TeachTags;
use App\TeacherSubject;
use App\TeacherSubjectDirection;
use App\TeacherEducation;
use App\LearnTags;
use Auth;
use App\Learn;
use App\Teachers;
use App\TeacherPrice;
use App\Subjects;
use App\MyHelpers\Helper;
use App\MyHelpers\Helper_two;
use Image;
use App\Conversation;
use App\TeachersLangs;
use App\Mail\NewSubject;



class TutorController extends Controller
{
    public function index() {
    	return view('front/create_all_posts');
    }

    //get вывод базовой страницы при создании нового аккаунта репетитора
    public function tutor_account_basic() {

        $user = Auth::user();
        $teacher = Teachers::where( 'user_id', '=', $user->id)->first();
        $data_user = [];

        //выбираем годы для года рождения
        $data_user['years_born'] = Helper::get_years_born();
        $contries = DB::table('countries')->orderBy('name_ru', 'asc')->get();        

        if (!$teacher) {
            return view('front/tutor_account/tutor_create', ['user'=>$user, 'data_user'=>$data_user, 'contries' => $contries]);
        }

        return view('front/tutor_account/tutor_update', ['teacher'=>$teacher, 'data_user'=>$data_user, 'contries' => $contries]);
    }

    //post сохранение базовой информации при создании нового аккаунта репетитора
    public function tutor_save_basic(Request $query) {
        
        $this->validate($query, [
            'name' => 'required',
            'last_name' => 'required',
            'email' => 'required',
            'country_teach' => 'required',
            'gender' => 'required',
        ]);
        
        //выбираем пользователя
        $user = Auth::user();

        //проверяем было ли изменено имя
        if ($user->name != $query->name || $user->email != $query->email) {
            $user->name = $query->name;
            $user->email = $query->email;
            $user->save();
        }

        $teacher = new Teachers;
        $teacher->user_id = $user->id;
        $teacher->last_name = $query->last_name;
        $teacher->country_teach = $query->country_teach;
       // $teacher->teach_city_en = $query->teach_city_en;
        $teacher->gender = $query->gender;

        //Создание слага учителя из имени и фамилии
        $slug = $query->name . '-' . $query->last_name;
        $slug = Helper::ru2lat($slug);
        $check_similar_slug_in_db = Teachers::where('slug', '=', $slug)->get();
        if($check_similar_slug_in_db->count() > 0){
            $slug .= '-'.rand(1, 1000);
        }
        $teacher->slug = $slug;

        //Работаем с изображение кадрированным        
        if ($query->image_crop) {
            
            $image_crop = $query->image_crop; // Your data 'data:image/png;base64,AAAFBfj42Pj4';
            $image_crop = str_replace('data:image/jpeg;base64,', '', $image_crop);
            $image_crop = str_replace(' ', '+', $image_crop);
            $image_crop_good = base64_decode($image_crop);
            $image_crop_good = $image_crop;
            $filename = 'crop_'.time(). '_'. mt_rand(0,100). '.jpg';
            $location = public_path('storage/users/'.$filename);

            $img = Image::make($image_crop_good); //->encode('jpg', 50);
            $img->save($location);    
            $user->avatar = $filename;
            $user->save();

        }

        //complite
        $teacher->phone = $query->phone;
        $teacher->year_born = $query->year_born;
        $teacher->save();
        $user->type = 'user_teach';
        $user->save();
        session()->flash('success', 'Ваш профиль учителя создан. Заполните пожалуйста следующие разделы');
        return redirect()->route('tutor_account_main_info');

    }


//обновление базовой информации аккаунта репетитора
    public function tutor_update_basic(Request $query) {
        
        $this->validate($query, [
            'name' => 'required',
            'last_name' => 'required',
            'email' => 'required',
            'country_teach' => 'required',
            'gender' => 'required',
        ]);
        
        //выбираем пользователя
        $user = Auth::user();
        //выбираем профиль учителя
        $teacher = Teachers::where('user_id', '=', $user->id)->first();

        //проверяем было ли изменено имя
        if ($user->name != $query->name || $user->email != $query->email) {
            $user->name = $query->name;
            $user->email = $query->email;
            $user->save();
        }

        //Работаем с изображение кадрированным        
        if ($query->image_crop) {

            if ($user->avatar != '') {
                $img = public_path('storage/users/'.$user->avatar);
                if(file_exists($img)){
                    unlink($img);
                }
            }
            
            $image_crop = $query->image_crop; // Your data 'data:image/png;base64,AAAFBfj42Pj4';
            $image_crop = str_replace('data:image/jpeg;base64,', '', $image_crop);
            $image_crop = str_replace(' ', '+', $image_crop);
            $image_crop_good = base64_decode($image_crop);
            $image_crop_good = $image_crop;
            $filename = 'crop_'.time(). '_'. mt_rand(0,100). '.jpg';
            $location = public_path('storage/users/'.$filename);

            $img = Image::make($image_crop_good); //->encode('jpg', 50);
            $img->save($location);  

            $user->avatar = $filename;
            $user->save();

        }
        
        //обновляем профиль учителя
        $teacher->last_name = $query->last_name;
        $teacher->country_teach = $query->country_teach;
        //$teacher->teach_city_en = $query->teach_city_en;
        $teacher->gender = $query->gender;
        $teacher->phone = $query->phone;
        $teacher->year_born = $query->year_born;
        $teacher->save();
        session()->flash('success', 'Изменения раздела "Базовая информация" сохранены.');

        //проверяем все ли заполнено и делаем active or not
        $tutor_active = Helper_two::check_tutor_active();

        return redirect()->route('tutor_account_basic');
    }


//get вывод основной (main) страницы при создании аккаунта репетитора
    public function tutor_account_main_info() {
        $user = Auth::user();
        $teacher = Teachers::where( 'user_id', '=', $user->id)->first();
        $teacher_price = TeacherPrice::where('teach_id', '=', $teacher->id)->first();
        $video = unserialize($teacher->video_link);
        $teacher->video_link = $video;
        return view('front/tutor_account/tutor_account_main_info', ['teacher'=>$teacher, 'teacher_price'=>$teacher_price]);
    }

//post обновляем основную (main)  информацию о пользователе 
    public function tutor_account_main_info_save(Request $query) {
            $this->validate($query, [
            'profile_title' => 'required',
            'tutor_edu_text' => 'required',
            'teach_place' => 'required',
            'pupil_age' => 'required',
        ]);

        //выбираем пользователя
        $user = Auth::user();
        //выбираем профиль учителя
        $teacher = Teachers::where('user_id', '=', $user->id)->first();
        if (!$teacher) {
            dd('Начните пожалуйста заполнение профиля с первой части "Основная информация"');
           return redirect()->route('tutor_create_update');
        }

        //сохраняем возраст учеников в таблицу
        $pupil_ages = implode(',', $query->pupil_age );

        //сохраняем место проведения уроков
        if(isset($query->teach_place['online'])){
            $teacher->teach_online = '1';
        }else{
            $teacher->teach_online = '0';
        }
        if(isset($query->teach_place['local'])){
            $teacher->teach_local = '1';
        }else{
            $teacher->teach_local = '0';
        }
        if(isset($query->teach_local_pupil)){
            $teacher->teach_local_pupil = '1';
        }else{
            $teacher->teach_local_pupil = '0';
        }
        if(isset($query->teach_local_me)){
            $teacher->teach_local_me = '1';
        }else{
            $teacher->teach_local_me = '0';
        }

        //сохраняем дни преподавания
        $teach_days = '';
        if (isset($query->teach_days)) {
            $teach_days = implode(',', $query->teach_days);
        }
        
        //обновляем профиль учителя
        $teacher->profile_title = $query->profile_title;
        $teacher->tutor_edu_text = $query->tutor_edu_text;
        $teacher->skype_login = $query->skype_login;
        $teacher->teach_exp = $query->teach_exp;
        $teacher->is_group_teach = $query->is_group_teach;
        $teacher->pupil_age = $pupil_ages;
        $teacher->teach_days = $teach_days;

        $teacher->country_teach = $query->country_teach;
        $teacher->teach_city_en = $query->teach_city_en;

        //работа с видео
        if (strripos($query->video_link, 'youtube') ) {
            $video_id = Helper::getYoutubeVideoID($query->video_link);
            if ($video_id != false) {

                $video_arr_save = [];
                $video_arr_save['link'] = $query->video_link;
                $video_arr_save['video_id'] = $video_id;
                $video_arr_save['video_type'] = 'youtube';
                $video_arr_save = serialize($video_arr_save);
                                            //dd($video_arr_save);
                $teacher->video_link = $video_arr_save;

            }else{
                $teacher->video_link = '';
            }
        }elseif($query->video_link == ''){
            $teacher->video_link = '';
        }else{
            $teacher->video_link = '';
        }

        //работа с группой учеников
        if ($query->is_group_teach == 1) {
            $teacher->group_size = $query->group_size;
        }else{
            $teacher->group_size = '';
        }

        $teacher->save();

        if(isset($query->price_value) && isset($query->price_valute)){
        //сохраняем цены
            $teacher_price = TeacherPrice::where('teach_id', '=', $teacher->id)->first();

            if (!$teacher_price) {
                $teacher_price = new TeacherPrice;
                $teacher_price->teach_id = $teacher->id;
            }

            $teacher_price->price_def = $query->price_value;
            $teacher_price->cur_def = $query->price_valute;
            $teacher_price->save();

        }

        //проверяем все ли заполнено и делаем active or not
        $tutor_active = Helper_two::check_tutor_active();

        $exist_subjects = TeacherSubject::where('teach_id', '=', $teacher->id)->get();
        if($exist_subjects->count() <= 0){

            session()->flash('success', 'Раздел "Осовная информация" сохранен. Теперь выберите предметы, которые вы преподаете.');
            return redirect()->route('tutor_account_subjects_info');

        }

        session()->flash('success', 'Изменения раздела "Осовная информация" сохранены.');

        return redirect()->route('tutor_account_main_info');
    }

//get вывод страницы предметов при создании аккаунта репетитора
    public function tutor_account_subjects_info() {

        $user = Auth::user();
        $teacher = Teachers::where( 'user_id', '=', $user->id)->first();
        $data_user = [];

        //выбираем предметы
        $data_user['subject'] = TeacherSubject::where('teach_id','=', $teacher->id)
            ->join('subjects', 'teacher_subject.subject_id','=','subjects.id')
            ->leftJoin('subject_direction', 'teacher_subject.subject_id','=','subject_direction.subject_id')
            ->with('sub_dir_saved')
            ->select('teacher_subject.*', 'subjects.name_ru', 'subject_direction.id as check_sub_dir')
            ->groupBy('teacher_subject.id')
            ->get();

        return view('front/tutor_account/tutor_account_subjects_info', ['teacher'=>$teacher, 'data_user'=>$data_user]);
    }

//post обновляем страницу предметов  аккаунта репетитора 
    public function tutor_account_subjects_info_save(Request $query) {

        $subject_name = array_unique($query->subject_name);
        $teacher_subject_data = [];
        $teacher_subject_direction_data = [];
        $user_id = Auth::user()->id;
        $teacher = Teachers::where( 'user_id', '=', $user_id)->select('id')->first();
        
        TeacherSubject::where('teach_id','=', $teacher->id)->delete();
        TeacherSubjectDirection::where('teachers_id','=', $teacher->id)->delete();

        $subjects = $query->subject_direction;
        $subjects_name = $query->subject_name;

        foreach ($subjects_name as $subject_id ) {

            if(isset($subjects[$subject_id])){

                foreach ($subjects[$subject_id] as $subject_direction_id => $subject_name ) {
                    
                    $teacher_subject_direction_data[] = ['teachers_id' => $teacher->id, 'subject_id' => $subject_id, 'subject_direction_id'=> $subject_direction_id, 'subject_direction_exp'=> $query->subject_direction_exp[$subject_direction_id] ];
                }

            }

            $teacher_subject_data[] = ['teach_id' => $teacher->id, 'subject_id'=> $subject_id ];
        }

        TeacherSubject::insert($teacher_subject_data);
        TeacherSubjectDirection::insert($teacher_subject_direction_data);

        //проверяем все ли заполнено и делаем active or not
        $tutor_active = Helper_two::check_tutor_active();

        $exist_edu = TeacherEducation::where('teach_id', '=', $teacher->id)->get();
        if($exist_edu->count() <= 0){

            session()->flash('success', 'Раздел "Предметы преподавания" сохранен. Заполните последний раздел "Мое образование, интересы"');
            return redirect()->route('tutor_account_about_me_info');

        }

        session()->flash('success', 'Изменения раздела "Предметы преподавания" сохранены.');
        return redirect()->route('tutor_account_subjects_info');

    }

//get вывод обо мне страницы при создании аккаунта репетитора
    public function tutor_account_about_me_info() {

        $user = Auth::user();
        $teacher = Teachers::where( 'user_id', '=', $user->id)->first();
        $data_user = [];

        //выбираем предметы
        $data_user['education'] = TeacherEducation::where('teach_id','=', $teacher->id)
            ->get();
        //выбираем годы для года рождения
        $data_user['years_study_begin'] = Helper::get_years_study(0);
        $data_user['years_study_end'] = Helper::get_years_study(-6);

        return view('front/tutor_account/tutor_account_about_me_info', ['teacher'=>$teacher, 'data_user'=>$data_user]);
    }

//post обновляем обо мне страницу репетитора
    public function tutor_account_about_me_info_save(Request $query) {

        $user_id = Auth::user()->id;
        $teacher = Teachers::where( 'user_id', '=', $user_id)->select('id')->first();
        $data = [];

        foreach ($query->name_institut as $key => $name_institut) {

            if ($name_institut != '') {
                
                $data[$key]['teach_id'] = $teacher->id;
                $data[$key]['name_institut'] = $name_institut;
                $data[$key]['degree'] = $query->degree[$key];
                $data[$key]['year_enter_edu'] = $query->year_enter_edu[$key];
                $data[$key]['year_finish_edu'] = $query->year_finish_edu[$key];

            }

        }
        
        TeacherEducation::where('teach_id','=', $teacher->id)->delete();

        TeacherEducation::insert($data);

        $teacher->tutor_hobie_text = $query->tutor_hobie_text;
        $teacher->save();

        //проверяем все ли заполнено и делаем active or not
        $tutor_active = Helper_two::check_tutor_active();

        if($tutor_active){
            session()->flash('success', 'Поздравляем! Ваша страница репетитора создана и активирована. Ссылка на вашу страницу репетитора находиться под заголовком этой страницы');
            return redirect()->route('tutor_account_about_me_info');
        }

        session()->flash('success', 'Изменения раздела "Мое образование, интересы" сохранены.');

        return redirect()->route('tutor_account_about_me_info');
    }

    //get страница добавления переводов репетитора
    public function tutor_langs() {
        $ua = '';
        $teacher_en = '';
        $langs = [
            "ru" => '1',
            'ua' => '1',
            'en' => '1',
        ];
        $user = Auth::user();
        $teacher = Teachers::where( 'user_id', '=', $user->id)->first();
        $ua = TeachersLangs::where('teacher_id', '=', $teacher->id)->where('lang', '=', 'ua')->first();
        $en = TeachersLangs::where('teacher_id', '=', $teacher->id)->where('lang', '=', 'en')->first();

        return view('front/tutor_account/tutor_langs', ['langs'=>$langs, 'teacher' => $teacher, 'ua' => $ua, 'en' => $en ]);
    }

    //post страница добавления переводов репетитора
    public function tutor_langs_save(Request $query) {
        $langs = [
            "ru" => '1',
            'ua' => '1',
            'en' => '1',
        ];

        $user = Auth::user();
        $teacher = Teachers::where( 'user_id', '=', $user->id)->first();
        $user->name = $query->lang_1['name'];
        $user->save();

        $teacher->last_name = $query->lang_1['last_name'];
        $teacher->profile_title = $query->lang_1['profile_title'];
        $teacher->tutor_edu_text = $query->lang_1['tutor_edu_text'];
        $teacher->tutor_hobie_text = $query->lang_1['tutor_hobie_text'];
        $teacher->save();

        //сохраняем второй язык
        if(isset($query->lang_2) && isset($query->lan_code_2)){

            $teacher_lang_2 = TeachersLangs::where('teacher_id', '=', $teacher->id)->where('lang', '=', $query->lan_code_2)->first();
            if($teacher_lang_2){

                $teacher_lang_2->lang = $query->lan_code_2;
                $teacher_lang_2->name = $query->lang_2['name'];
                $teacher_lang_2->last_name = $query->lang_2['last_name'];
                $teacher_lang_2->profile_title = $query->lang_2['profile_title'];
                $teacher_lang_2->tutor_edu_text = $query->lang_2['tutor_edu_text'];
                $teacher_lang_2->tutor_hobie_text = $query->lang_2['tutor_hobie_text'];
                $teacher_lang_2->save();

            }else{
                
                $teacher_lang_2 = new TeachersLangs;
                $teacher_lang_2->teacher_id = $teacher->id;
                $teacher_lang_2->lang = $query->lan_code_2;
                $teacher_lang_2->name = $query->lang_2['name'];
                $teacher_lang_2->last_name = $query->lang_2['last_name'];
                $teacher_lang_2->profile_title = $query->lang_2['profile_title'];
                $teacher_lang_2->tutor_edu_text = $query->lang_2['tutor_edu_text'];
                $teacher_lang_2->tutor_hobie_text = $query->lang_2['tutor_hobie_text'];
                $teacher_lang_2->save();

            }

        }

        if(isset($query->lang_3) && isset($query->lan_code_3)){

                $teacher_lang_3 = TeachersLangs::where('teacher_id', '=', $teacher->id)->where('lang', '=', $query->lan_code_3)->first();

                if($teacher_lang_3){

                    $teacher_lang_3->lang = $query->lan_code_3;
                    $teacher_lang_3->name = $query->lang_3['name'];
                    $teacher_lang_3->last_name = $query->lang_3['last_name'];
                    $teacher_lang_3->profile_title = $query->lang_3['profile_title'];
                    $teacher_lang_3->tutor_edu_text = $query->lang_3['tutor_edu_text'];
                    $teacher_lang_3->tutor_hobie_text = $query->lang_3['tutor_hobie_text'];
                    $teacher_lang_3->save();

                }else{
                    
                    $teacher_lang_3 = new TeachersLangs;
                    $teacher_lang_3->teacher_id = $teacher->id;
                    $teacher_lang_3->lang = $query->lan_code_3;
                    $teacher_lang_3->name = $query->lang_3['name'];
                    $teacher_lang_3->last_name = $query->lang_3['last_name'];
                    $teacher_lang_3->profile_title = $query->lang_3['profile_title'];
                    $teacher_lang_3->tutor_edu_text = $query->lang_3['tutor_edu_text'];
                    $teacher_lang_3->tutor_hobie_text = $query->lang_3['tutor_hobie_text'];
                    $teacher_lang_3->save();

                }

        }        

        session()->flash('success', 'Изменения сохранены');

        return back();
    }

    public function create_new_subject(Request $query) {

        $new_subject = Subjects::where('name_ru', '=', $query->query('subject'))->get();
        if ($new_subject->count() > 0) {
            return 'Такой предмет "'.$query->query('subject').'" уже существует';
        }


        $new_subject = new Subjects;
        $new_subject->name_ru = $query->query('subject');
        $new_subject->status = 'check';
        $new_subject->save();

        //письмо менеджеру сайта, проверить новый предмет
        $user = Auth::user();
        $to = explode(',', env('ADMIN_EMAILS'));
        \Mail::to($to)->send(new NewSubject($user, $query->query('subject')));

        return 'Новый предмет "'.$query->query('subject').'" создан. После проверки он будет доступен для выбора на сайте' ;
    }

    public function requests_to_me(){

        $requests = Conversation::where('recipient_id', '=', Auth::user()->id)
            ->where('type', '=', 'request')
            ->get();

        $params = [
              'requests' => $requests,
        ];

        return view('front/tutor_account/tutor_account_request_to_me' )->with($params);
    }

    public function requests_accept($id){

        $request = Conversation::find($id);

        $request->status = 'accepted';

        $request->save();

        return back();
    }
    public function requests_no_accept($id){

        $request = Conversation::find($id);

        $request->status = 'no_accepted';

        $request->save();

        return back();
    }
}
