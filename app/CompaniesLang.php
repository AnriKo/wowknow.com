<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompaniesLang extends Model
{
    protected $table = 'companies_langs';
    public $timestamps = false;
}
