<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TeacherSubjectDirection extends Model
{
	protected $fillable = ['id', 'teachers_id', 'subject_id', 'subject_direction_id', 'subject_direction_exp'];

    protected $table = 'subject_direction_teachers';
    public $timestamps = false;

    public function sub_dir_name()
    {
        return $this->belongsTo('App\SubjectDirection', 'subject_id' , 'subject_id');
    }
}
