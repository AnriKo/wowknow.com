<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PupilTask extends Model
{
    protected $table = 'pupil_task';

    public function subject_name(){
        return $this->hasOne('App\Subjects', 'id', 'subject_id');
    }   
}
