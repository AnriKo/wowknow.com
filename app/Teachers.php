<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App;

class Teachers extends Model
{
    protected $table = 'teachers';

    public function subjects()
    {
        return $this->belongsToMany('App\Subjects', 'teacher_subject', 'teach_id' , 'subject_id');
    }

    public function subjects_dir()
    {
        return $this->hasMany('App\TeacherSubjectDirection', 'teachers_id' , 'id');
    }
    public function education()
    {
        return $this->hasMany('App\TeacherEducation', 'teach_id' , 'id');
    }
    public function teacher_user(){
        return $this->hasOne('App\User', 'id', 'user_id');
    } 
    public function teacher_langs(){
        $local = App::getLocale();
        return $this->hasOne('App\TeachersLangs', 'teacher_id', 'id')->where('lang', '=', $local);
    }
    public function city_ua(){
        return $this->hasOne('App\CityUa', 'title_en', 'teach_city_en');
    }
    public function city_ru(){
        return $this->hasOne('App\CityRu', 'title_en', 'teach_city_en');
    }
    public function city_kz(){
        return $this->hasOne('App\CityKz', 'title_en', 'teach_city_en');
    }
    public function city_by(){
        return $this->hasOne('App\CityBy', 'title_en', 'teach_city_en');
    }
}
