<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CityBy extends Model
{
    protected $table = 'cities_by';
    public $timestamps = false;
}