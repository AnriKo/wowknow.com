<?php

namespace App\Console\Commands;

use App\Teachers;
use App\PupilTask;
use App\TeacherSubject;
use App\Mail\NewPupilTask;
use Illuminate\Console\Command;

class MailsToTeachersWhenNewTask extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mail_to_teachers_when_new_task:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send letters to teachers when pupil create new task with subject which study teachers';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $new_pupil_tasks = PupilTask::where('letters_to_teachers', '=', '0')
            ->where('active', '=', "1")
            ->groupBy('subject_id')
            //->select('subject_id', 'letters_to_teachers', 'text', 'name', 'price_def', 'cur_def')
            ->get();   
        ///dd($new_pupil_tasks);

        if($new_pupil_tasks){

            foreach ($new_pupil_tasks as $new_task) {

                $teacher_subject = TeacherSubject::where('subject_id', '=', $new_task->subject_id)->get();

                    foreach ($teacher_subject as $one_teacher_subject) {
                        $teacher = Teachers::where('id', '=', $one_teacher_subject->teach_id)
                            ->where('subs_new_tasks', '=', '1')
                            ->first();
                           
                        if($teacher){
                            $user = $teacher->teacher_user; 
                            $recipient = $user->name;
                            $task_subject = $new_task->subject_name;
                            \Mail::to($user->email)->send(new NewPupilTask($recipient, $task_subject, $new_task) );
                        }
     
                    }

                //обозначаем что отправили письмо учителям в этих новых заданиях учеников
                $new_task_sended = PupilTask::where('letters_to_teachers', '=', '0')
                                    ->where('active', '=', '1')
                                    ->where('subject_id', '=', $new_task->subject_id)
                                    ->update(['letters_to_teachers' => '1']);

             } 
        }
    }
}
