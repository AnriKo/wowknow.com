<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\ConversationReply;
use Carbon\Carbon;
use App\Mail\YouGetNewMessage;

class SendEmailsNewLetter extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'email_new_message:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send emails when users get new letters';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $current = Carbon::now()->subHours(1);
        $unread_mes = ConversationReply::where('status', '=', '0')
            ->where('created_at', '<', $current)
            ->where('letter_to_user', '=', '0')
            ->whereNotNull('recipient_user_id')
            ->groupBy('recipient_user_id')
            ->select('recipient_user_id', 'user_id', 'letter_to_user')
            ->get(); 
        if($unread_mes->count() > 0)  {  
            foreach ($unread_mes as $user_id) {
                if($user_id->recipient_info){
                    $recipient = $user_id->recipient_info->name;
                    $sender = $user_id->sender_info->name;
                    \Mail::to($user_id->recipient_info->email)->send(new YouGetNewMessage($recipient, $sender));  
                }   
                $user_sended_letter = ConversationReply::where('status', '=', '0')
                    ->where('recipient_user_id', '=', $user_id->recipient_user_id)
                    ->update(['letter_to_user' => '1']);   
            } 
        }    
    }
}
