<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;
use App\Teachers;
use Carbon\Carbon;
use App\Mail\CompleteRegisterOneDay;

class MailsToTeacherCompleteRegisterOneDay extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'complete_register_one_day:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send letters to teacher when he do not complete register and ask him to complete it. Only one time right away after they begin register.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // $current = Carbon::now()->subHours(24);

        $unregister_teacher = User::where('users.type', '=', 'user_teach')
            ->leftJoin('teachers', 'teachers.user_id',  '=', 'users.id')
            ->where('teachers.id', '=', NULL)
            //->where('users.created_at', '>', $current)
            ->select('users.email', 'users.name')
            ->get()
            ->toArray();  

        $unactive_teacher = Teachers::where('active', '=', '0')
            ->leftJoin('users', 'users.id',  '=', 'teachers.user_id')
            //->where('users.created_at', '>', $current)
            ->select('users.email', 'users.name')
            ->get()
            ->toArray();

        //$users_send_email = $unactive_teacher->merge($unregister_teacher);
        $users_send_email = array_merge($unregister_teacher, $unactive_teacher);
        //$users_send_email = array_unique($users_send_email);

        if(count($users_send_email) > 0){

            foreach ($users_send_email as $key => $value) {

                //dump($value['email'] . ' - ' . $value['name']);

                \Mail::to($value['email'])->send(new CompleteRegisterOneDay($value['name']) );

                dump($value['email']);

            } 

            dump(count($users_send_email));
        }
    }
}
