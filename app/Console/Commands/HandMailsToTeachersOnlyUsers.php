<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;
use App\Mail\CompleteRegisterOneDay;

class HandMailsToTeachersOnlyUsers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'complete_register_teachers_only_users:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send letters to teachers which register like teacher but only user now';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $unregister_teacher = User::where('users.type', '=', 'user_teach')
            ->leftJoin('teachers', 'teachers.user_id',  '=', 'users.id')
            ->where('teachers.id', '=', NULL)
            ->select('users.email', 'users.name')
            ->pluck('users.name', 'users.email');  

        if($unregister_teacher->count() > 0){

            foreach ($unregister_teacher as $user_mail => $user_name) {

                 //echo $user_mail.' | ';

                \Mail::to($user_mail)->send(new CompleteRegisterOneDay($user_name) );

            } 
        }
    }
}
