<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\MyHelpers\Helper;
use App\TeacherPrice;

class UpdateTeacherPricesCur extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'prices:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Updates teachers prices in other currencies';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $all_teachers_prices = TeacherPrice::get();
        foreach($all_teachers_prices as $teacher_price){

        if ($teacher_price->cur_def == 'UAH') {
            $teacher_price->price_RUB = Helper::convert_cur($teacher_price->price_def, $teacher_price->cur_def, 'RUB'); 
            $teacher_price->price_USD = Helper::convert_cur($teacher_price->price_def, $teacher_price->cur_def, 'USD');
            $teacher_price->price_KZT = Helper::convert_cur($teacher_price->price_def, $teacher_price->cur_def, 'KZT');
            $teacher_price->price_BYN = Helper::convert_cur($teacher_price->price_def, $teacher_price->cur_def, 'BYN');
        }elseif ($teacher_price->cur_def == 'RUB') {
            $teacher_price->price_UAH = Helper::convert_cur($teacher_price->price_def, $teacher_price->cur_def, 'UAH'); 
            $teacher_price->price_USD = Helper::convert_cur($teacher_price->price_def, $teacher_price->cur_def, 'USD');
            $teacher_price->price_KZT = Helper::convert_cur($teacher_price->price_def, $teacher_price->cur_def, 'KZT');
            $teacher_price->price_BYN = Helper::convert_cur($teacher_price->price_def, $teacher_price->cur_def, 'BYN');
        }elseif ($teacher_price->cur_def == 'USD') {
            $teacher_price->price_UAH = Helper::convert_cur($teacher_price->price_def, $teacher_price->cur_def, 'UAH'); 
            $teacher_price->price_RUB = Helper::convert_cur($teacher_price->price_def, $teacher_price->cur_def, 'RUB');
            $teacher_price->price_KZT = Helper::convert_cur($teacher_price->price_def, $teacher_price->cur_def, 'KZT');
            $teacher_price->price_BYN = Helper::convert_cur($teacher_price->price_def, $teacher_price->cur_def, 'BYN');
        }elseif ($teacher_price->cur_def == 'KZT') {
            $teacher_price->price_UAH = Helper::convert_cur($teacher_price->price_def, $teacher_price->cur_def, 'UAH'); 
            $teacher_price->price_RUB = Helper::convert_cur($teacher_price->price_def, $teacher_price->cur_def, 'RUB');
            $teacher_price->price_USD = Helper::convert_cur($teacher_price->price_def, $teacher_price->cur_def, 'USD');
            $teacher_price->price_BYN = Helper::convert_cur($teacher_price->price_def, $teacher_price->cur_def, 'BYN');
        }elseif ($teacher_price->cur_def == 'BYN') {
            $teacher_price->price_UAH = Helper::convert_cur($teacher_price->price_def, $teacher_price->cur_def, 'UAH'); 
            $teacher_price->price_RUB = Helper::convert_cur($teacher_price->price_def, $teacher_price->cur_def, 'RUB');
            $teacher_price->price_USD = Helper::convert_cur($teacher_price->price_def, $teacher_price->cur_def, 'USD');
            $teacher_price->price_KZT = Helper::convert_cur($teacher_price->price_def, $teacher_price->cur_def, 'KZT');
        }

        $teacher_price->save();

        }
    }
}
