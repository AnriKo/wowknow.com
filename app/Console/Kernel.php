<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\SendEmailsNewLetter::class,
        Commands\UpdateTeacherPricesCur::class,
        Commands\MailsToTeachersWhenNewTask::class,
        Commands\MailsToTeacherCompleteRegisterOneDay::class,
        Commands\HandMailsToTeachersOnlyUsers::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
         // $schedule->command('mail_to_teachers_when_new_task:send')
         //          ->daily();
         // $schedule->command('email_new_message:send')
         //          ->hourly();
         // $schedule->command('prices:update')
         //          ->monthly();
         $schedule->command('complete_register_one_day:send')
                  ->dailyAt('07:00');
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
