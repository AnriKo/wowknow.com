<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ConversationReply extends Model
{
    protected $table = 'conversation_reply';

    public function recipient_info(){
        return $this->hasOne('App\User', 'id', 'recipient_user_id');
    } 
    public function sender_info(){
        return $this->hasOne('App\User', 'id', 'user_id');
    } 	
}
