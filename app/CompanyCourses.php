<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App;

class CompanyCourses extends Model
{

    protected $table = 'company_courses';

    public function cours_langs(){
        $local = App::getLocale();
        return $this->hasOne('App\CompanyCoursesLang', 'course_id', 'id')->where('lang', '=', $local);
    }

    public function company_langs(){
    	$local = App::getLocale();
        return $this->hasOne('App\CompaniesLang', 'company_id', 'company_id')->where('lang', '=', $local);
    }

    public function company(){
        return $this->belongsTo('App\Company', 'company_id', 'id');
    }
}
