<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CongratulateNewTutor extends Mailable
{
    use Queueable, SerializesModels;

    public $recipient;
    public $teacher_slug;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($recipient, $teacher_slug)
    {
        $this->recipient = $recipient;
        $this->teacher_slug = $teacher_slug;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.to_teacher.congratulate_tutor_after_finish_register')
            ->from('info@wowknow.com', 'WowKnow')
            ->subject('Вы успешно зарегистрировались на Wowknow.com' );
    }
}
