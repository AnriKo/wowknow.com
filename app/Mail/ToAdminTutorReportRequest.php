<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ToAdminTutorReportRequest extends Mailable
{
    use Queueable, SerializesModels;

    public $for_tutor_mail;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($for_tutor_mail)
    {
        $this->for_tutor_mail = $for_tutor_mail;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        //dd($this);
        return $this->view('emails.to_manager.to_admin_tutor_report_request')
            ->from('info@wowknow.com', 'WowKnow')
            ->subject($this->for_tutor_mail['status']);
    }
}
