<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CongratulateNewUser extends Mailable
{
    use Queueable, SerializesModels;

    public $recipient;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($recipient)
    {
        $this->recipient = $recipient;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.to_teacher.congratulate_after_register')
            ->from('info@wowknow.com', 'WowKnow')
            ->subject('Wowknow.com, регистрация учителя' );
    }
}
