<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NewRequest extends Mailable
{
    use Queueable, SerializesModels;

    public $user_pupil;
    public $user_teacher;
    public $sent_letter_to;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user_pupil, $user_teacher, $sent_letter_to)
    {
        $this->user_pupil = $user_pupil->name;
        $this->user_teacher = $user_teacher->name;
        $this->sent_letter_to = $sent_letter_to;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        if($this->sent_letter_to == 'teacher'){
            return $this->view('emails.to_teacher.new_request')
                ->from('info@wowknow.com', 'WowKnow')
                ->subject('Новая заявка на обучение');
        }

        return $this->view('emails.to_pupil.new_request')
            ->from('info@wowknow.com', 'WowKnow')
            ->subject('Новая заявка на обучение');

    }
}
