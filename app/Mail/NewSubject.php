<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NewSubject extends Mailable
{
    

    use Queueable, SerializesModels;

    public $user;
    public $subject_new;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user, $subject_new)
    {
        $this->user = $user;
        $this->subject_new = $subject_new;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.to_manager.new_subject')
                    ->from('info@wowknow.com', 'WowKnow')
                    ->subject('Добавлен новый предмет');
    }
}
