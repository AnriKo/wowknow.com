<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Config;

class NewPupilTask extends Mailable
{
    use Queueable, SerializesModels;

    public $recipient;
    public $task_subject;
    public $new_task;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($recipient, $task_subject, $new_task)
    {
        $this->recipient = $recipient;
        $this->task_subject = $task_subject;
        $this->new_task = $new_task;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.to_teacher.new_pupil_task')
                    ->from('info@wowknow.com', 'WowKnow')
                    ->subject('Новые заказы от учеников. ' . Config::get('app.domain') );
    }
}
