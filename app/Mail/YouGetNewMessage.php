<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class YouGetNewMessage extends Mailable
{
    use Queueable, SerializesModels;

    public $recipient;
    public $sender;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($recipient, $sender)
    {
        $this->recipient = $recipient;
        $this->sender = $sender;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.to_all_users.you_get_new_message')
                    ->from('info@wowknow.com', 'WowKnow')
                    ->subject('Новое сообщение на wowknow.com');
    }
}
