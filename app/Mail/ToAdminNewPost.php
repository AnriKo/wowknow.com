<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ToAdminNewPost extends Mailable
{
    

    use Queueable, SerializesModels;

    public $slug;
    public $id;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($slug, $id)
    {
        $this->slug = $slug;
        $this->id = $id;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.to_manager.user_add_article')
                    ->from('info@wowknow.com', 'WowKnow')
                    ->subject('Додана нова стаття, перевірте WowKnow');
    }
}
