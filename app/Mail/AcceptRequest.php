<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AcceptRequest extends Mailable
{
    use Queueable, SerializesModels;

    public $who_create_request;
    public $who_accept_request;
    public $sent_letter_to;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($who_create_request, $who_accept_request, $sent_letter_to)
    {
        $this->sent_letter_to = $sent_letter_to;
        $this->who_create_request = $who_create_request;
        $this->who_accept_request = $who_accept_request;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if($this->sent_letter_to == 'pupil'){
            return $this->view('emails.to_all_users.accept_your_request')
                        ->from('info@wowknow.com', 'WowKnow')
                        ->subject('Ваша заявка принята');
        }

        return $this->view('emails.to_teacher.accept_your_request')
            ->from('info@wowknow.com', 'WowKnow')
            ->subject('Ваша заявка принята');

    }
}
