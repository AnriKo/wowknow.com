<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ContactMessage extends Mailable
{
    

    use Queueable, SerializesModels;

    public $user_name;
    public $user_email;
    public $user_message;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user_name, $user_email, $user_message)
    {
        $this->user_name = $user_name;
        $this->user_email = $user_email;
        $this->user_message = $user_message;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.to_manager.contact_message')
                    ->from('info@wowknow.com', 'WowKnow')
                    ->subject('Лист з сайту WowKnow');
    }
}
