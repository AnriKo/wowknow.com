<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ToAdminNewRequest extends Mailable
{
    use Queueable, SerializesModels;

    public $for_tutor_mail;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($for_tutor_mail)
    {
        $this->for_tutor_mail = $for_tutor_mail;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.to_manager.to_admin_new_request')
            ->from('info@wowknow.com', 'WowKnow')
            ->subject('Новая заявка на сайте');
    }
}
